/*******************************************************************************
--------------------------------------------------------------------------------
Before Compile:
0. source /opt/intel/inteloneapi/setvars.sh 
1.Go to Common.h and set the MDP_problem_size(0..12) in #define INPUT_ID MDP_problem_size
  We used in the paper the MDP_problem_size 2,4,6,8,10
2.Go to main.cpp and comment one of the following lines. Leave uncommented the
one corresponding to the scheduler that you want to use:
  #define DYNAMIC 5
  #define ORACLE 4
3. IF you change the platform, check output file name & set the proper TEST_PLATFORM ID
--------------------------------------------------------------------------------
Compile: go to the root fo the project (where main.cpp is) and run "cmake ."
--------------------------------------------------------------------------------
Execution:
1. For 0=SEQ1D,1=OMP,2=TBB,3=G-TBB implementations. No Scheduler.
sudo ./ValueIteration ImplemId(0-3,0=SEQ1D,1=OMP,2=TBB,3=G-TBB) NCpus(0-4)
  e.g.: sudo ./ValueIteration 1 3
2. For "#define ORACLE 4" implementations. ORACLE Scheduler.
sudo ./ValueIterationOracle NCpus(0-4) NGpus(0-1) RatioGPU(0.0-1.0)
  e.g.: sudo ./ValueIteration 0 3 0.5
3. For "#define DYNAMIC 5" implementations. DYNAMIC Scheduler.
sudo ./ValueIterationDynamic  NCpus(0-4) NGpus(0-1) ChunkGPU(>1)
  e.g.: sudo ./ValueIteration 0 3 2048
--------------------------------------------------------------------------------
*/
#include <cstdlib>

#include "CL/sycl.hpp"
#include "utils/device_selector.hpp"

//#define __APPLE__
//#define LOCAL
//#define NDEBUG
//#define ENERGY_METER_ODROID
#define ENERGY_METER_INTEL
// #define HACK_TO_REMOVE_DUPLICATE_ERROR
//#define PCM_DEBUG_TOPOLOGY


//#define DEBUG
// #define DEEP_GPU_REPORT
//#define DEEP_CPU_REPORT
// #define OVERHEAD_ANALYSIS //OVERHEAD_STUDY


//#define SCHEDULER
// #define DYNAMIC 5
//#define ORACLE 4
//#define LOGFIT 6


#define FILE_INPUT //read T matrix arrays data from file or compute it (if not defined)
// #define TEST_PLATFORM 2

//#define VALIDATE_POLICY

//#include "src/RobotState.h"
//#include "src/TransitionMatrix.h"
#include "navigation_mdp/Common.h"
//#include "src/ValueIterationOMP.h"
//#include "src/ValueIteration.h"
//#include "src/ValueIterationUtils.h"
#include "src/Norm2.h"


#include <iostream>
#include <chrono>
#include <fstream>
#include <stdio.h>


#include "utils/timing.h"
#include <cstring>
#include <unistd.h>
//#include "energy-meter/energy_meter.h"
#include <unistd.h>
#include <stdlib.h>
// #include <omp.h>
#include <string>
#include <sstream>
// #include "src/ValueIterationTBB.h"
// #include "src/ValueIterationTBBClass.h"

#include <tbb/task_scheduler_init.h>
#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>

#ifdef SCHEDULER
#include "src/ValueIterationPlanifier.h"
#endif

#include "src/ValueIterationOneApi.h"

#ifdef ENERGY_METER_INTEL
#include "cpucounters.h"
#include "utils.h"
#include "tbb/tick_count.h"
#endif

using namespace tbb;
using namespace std;


// Device buffers
cl_mem d_probabilities = NULL;
cl_mem d_nextCellPos = NULL;
cl_mem d_nextStatePos = NULL;
cl_mem d_R = NULL;
cl_mem d_Q = NULL;
cl_mem d_P = NULL;
cl_mem d_V = NULL;
cl_mem d_Z = NULL;
cl_ulong probabilitySize = 0;

cl_ulong  NS, NR, NG, ND, NA, NX, NT;

cl_float*   h_probability = NULL;
cl_ulong*   h_nextCellPos = NULL;
cl_ulong*   h_nextStatePos = NULL;
cl_float*   h_R      = NULL;
cl_float*   h_Q     = NULL;
cl_float*   h_V     = NULL;
cl_int*     h_P     = NULL;
cl_int*     h_Pseq   = NULL;
cl_float*   h_Z      = NULL;

cl_uint ns, nx, na, nr, ng, nt, nd;

//!NEEDED FOR SCHEDULER.H USE
extern cl_context context;
extern cl_command_queue command_queue;
extern cl_kernel kernel;

/** Value iteration algorithm - time measurement output files */
const static string ExecTimeEnergyLogName =         "./data/EnergyTimeMeasurements20MS100x1000E.txt";
const static string IntelExecTimeEnergyLogName =    "./data/IntelEnergyTimeMeasurements100x10E_TBB.txt";

const static string TPxTimeEnergyLogName =          "./data/evaluation/TPx-TBB-TE-K1.txt";
const static string OdroidExecTimeEnergyLogName =   "./data/OdroidEnergyTimeMeasurements100E.txt";
const static string execTimeLogName =               "./data/ExecTimeTest.txt";

string inFileDim;


//enum iBroadwell{SEQ, SEQ1D, OMP4C, OCL4C, TBB4C};
string implName[5] = {"SEQ", "SEQ1D",  "OMP1D", "OMP1D_b.L", "OCL"};



/** Read discretization ., probability, nextCellPos, nextStatePos and R arrays*/
void ReadInput(int inputId);



/** Freee allocated mem*/
void releaseMemory();

int inputId;

int main(cl_int argc, char **argv) {

    float offload_ratio = 0.5;

    if (argc < 2) {
        cout << "Indicate the input id as the first argument! \n";
        return -1;
    }



    if (argc == 3) {
        inputId = atof(argv[1]);
        offload_ratio = atof(argv[2]);

    }

    inputId = atof(argv[1]);

    ReadInput(inputId);

    cout << "NS: " << NS << endl;
    cout << "NX: " << NX << endl;



    //Test OneAPI

    float runtime_k1, energy_k1;

    timestamp_type time1, time2;
    auto begin = std::chrono::high_resolution_clock::now();
    get_timestamp(&time1);
    int nrIt = ValueIterationOneApi(runtime_k1, energy_k1, offload_ratio) ;
    get_timestamp(&time2);
    double elapsed = timestamp_diff_in_seconds(time1,time2);
    
    cout << "\n\nThe OneAPI implem converged in " << nrIt << " iterations. Total ExecTime = " << elapsed << "\nPolicy values sample: ";
    cout << "\n\nExecTime K1: " << runtime_k1 << " Energy K1 " << energy_k1 << "\n ";
//    print1DimArray(h_V, NS, 20);


    char outputFileName[30];//  = "OneApiUSM.txt";

    if (((int)TEST_PLATFORM == 2))  sprintf(outputFileName, "./data/evaluationO3/Broadwell1_OneApiBUFF");
    if (((int)TEST_PLATFORM == 3)) sprintf(outputFileName, "./data/evaluationO3/Envy_OneApiBUFF");
    if (((int)TEST_PLATFORM == 4)) sprintf(outputFileName, "./data/evaluationO3/Cafe_OneApiBUFF");


    // ReadInput(inputFileName);
    FILE *f_out;

    f_out = fopen(outputFileName, "a");
    if (f_out == NULL) {
        fprintf(stderr, "file not created: %s\n", outputFileName);
        exit(-1);
    }
//    fprintf(f_out, "IN: %d \t GPU_Ratio: %f\t Het_kernel_time: %f \t Het_kernel_energy: %f \t Total_exec_time: %f \t Nr_iter: %d\n",
    fprintf(f_out, "%d\t%f\t%f\t%f\t%f\t%d\n",
            inputId,
            offload_ratio,
            runtime_k1,
            energy_k1,
            elapsed,
            nrIt
    );



    releaseMemory();
    return 0;
}

/** Freee allocated mem*/
void releaseMemory() {
    free(h_R);
    free(h_Q);
    free(h_V);
    free(h_P);
    free(h_Pseq);
    free(h_Z);

    //released in T_matrix...
    free(h_probability);
    free(h_nextCellPos);
    free(h_nextStatePos);
}


#ifdef ENERGY_METER_ODROID

#endif

//returns the percentage of similar policies P and Pxxx
float comparePolicies(const cl_int *P, const cl_int *Pxxx, bool verbose) {
    float sameP = NS;
    for (cl_int p = 0; p < NS ; ++p) {
        if(P[p] != Pxxx[p]) {
            sameP--;
            if (verbose)
                cout << "Pseq[" << p << "]=" << P[p] << "; " << "Pxxx[" << p << "]=" << Pxxx[p] << "\n";
            //break;
        }
    }
    return sameP/NS;
}

void ReadInput(int inputId) {

    uint nt = tD[inputId];    // number of theta discrete values
    uint nr = 3;    // number of rangefinders
    uint ng = gD[inputId];    // number of discrete values for a rangefinder reading
    uint nd = dD[inputId];    // number of discrete values for the distance from the robot to the target
    uint na = aD[inputId];    // number of discrete values for the angle between robot orientation and target
    uint nx = 6;

    uint ns = nt * ng * ng * ng * nd * na;


    // Using these events to time command group execution

    probabilitySize = 0;

    //setbuf(stdout, NULL); //stop buffering, for right order in printf/fprintf, etc
//int inputId[4] = {40445, 61012, 84565, 143771};


    printf("Algorithm input id : %d\n", inputId);

    probabilitySize = pNZ[inputId];

    cout << "probabilitySize: " << probabilitySize << std::endl;

    stringstream ss;
    stringstream fnss;
    ss  << "PS" << probabilitySize
        << "_" << "S" << ns
        << "_" << "T" << nt
        << "_" << "R" << nr
        << "_" << "G" << ng
        << "_" << "D" << nd
        << "_" << "A" << na;

    string inFileDim = ss.str();
    //cout << inFileDim;

    fnss << "./data/in/" << inFileDim << ".txt";
    string fns = fnss.str();
    char* inputFileName  = (char*)fns.c_str();

    // ReadInput(inputFileName);
    FILE *f;

    f = fopen(inputFileName, "r+t");
    if (f == NULL) {
        fprintf(stderr, "file not found: %s\n", inputFileName);
        exit(-1);
    }

//
//    fscanf(f, "%lu", &probabilitySize);
//    fscanf(f, "%d", &ns);
//    fscanf(f, "%d", &nx);
//    fscanf(f, "%d", &nt);
//    fscanf(f, "%d", &nr);
//    fscanf(f, "%d", &ng);
//    fscanf(f, "%d", &nd);
//    fscanf(f, "%d", &na);

    fscanf(f, "%lu", &probabilitySize);
    fscanf(f, "%d", &NS);
    fscanf(f, "%d", &NX);
    fscanf(f, "%d", &NT);
    fscanf(f, "%d", &NR);
    fscanf(f, "%d", &NG);
    fscanf(f, "%d", &ND);
    fscanf(f, "%d", &NA);


    if (h_probability == NULL) {
        h_probability = (cl_float *) malloc(sizeof(cl_float) * probabilitySize);
    }

    if (h_nextCellPos == NULL) {
        h_nextCellPos = (cl_ulong *) malloc(sizeof(cl_ulong) * (NS * NX + 1));
    }

    if (h_nextStatePos == NULL) {
        h_nextStatePos = (cl_ulong *) malloc(sizeof(cl_ulong) * probabilitySize);
    }

    if (h_R == NULL) {
        h_R = (cl_float*)calloc(NS*NX,sizeof(cl_float));
    }

    h_Q      = (cl_float*)calloc(NS*NX,sizeof(cl_float));
    h_V      = (cl_float*)calloc(NS,sizeof(cl_float));
    h_P      = (cl_int*)calloc(NS, sizeof(cl_int));
    h_Pseq      = (cl_int*)calloc(NS, sizeof(cl_int));
    h_Z      = (cl_float*)calloc(NS,sizeof(cl_float));

    unsigned int i;
    //probability
    for (i = 0; i < probabilitySize; i++) {
        fscanf(f, "%fE", &h_probability[i]);
    }
    //nextCellPos
    for (i = 0; i < NS * NX + 1; i++) {
        fscanf(f, "%lu", &h_nextCellPos[i]);
    }
    //nextStatePos
    for (i = 0; i < probabilitySize; i++) {
        fscanf(f, "%lu", &h_nextStatePos[i]);
    }
    //R
    for (i = 0; i < NS * NX; i++) {
        fscanf(f, "%f", &h_R[i]);
    }
    fclose(f);
}
