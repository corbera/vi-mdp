//============================================================================
// Name			: HDSS.h
// Author		: Antonio Vilches
// Version		: 1.0
// Date			: 06 / 01 / 2014
// Copyright	: Department. Computer's Architecture (c)
// Description	: HDSS scheduler implementation
//============================================================================

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <list>
#include "Scheduler.h"
#include "tbb/pipeline.h"
#include "tbb/tick_count.h"
#include "tbb/task.h"

#ifdef Win32
#include "PCM_Win/windriver.h"
#else
#include "cpucounters.h"
#endif

#ifdef PJTRACER
#include "pfortrace.h"
#endif

using namespace tbb;
using namespace std;

/*****************************************************************************
 * Defines
 * **************************************************************************/
#define CPU 0
#define GPU 1
#define GPU_OFF -100
#define SEP "\t"

/*****************************************************************************
 * types
 * **************************************************************************/
typedef struct{
	int numcpus;
	int numgpus;
	int gpuChunk;
	char benchName[100];
	char kernelName[100];
}Params;

typedef struct{
	float x;
	float y;
}Point;

/*****************************************************************************
 * Global variables
 * **************************************************************************/

atomic<int> gpuStatus;
int chunkGPU;
int chunkCPU;

bool BuildingModelPhase, monitoringPhase, completionPhase;
int measurements;
int numberPointsBuildingPhase;
float cpuThroughput;
float gpuThroughput;
float previousGPUThroughput;
float threshold;

float y[1024];
float x[1024];

int iterationBorder;
bool convergenceReached;

atomic<int> computedOnCPU, computedOnGPU;
atomic<int>totalIterations;

//list<Point> points;

/*****************************************************************************
 * Heterogeneous Scheduler
 * **************************************************************************/

int calculateLogarithmicModel(float *x, float *y, int numberPoints){
		/*variables*/
		int calculatedChunk;
		float numerador = 0.0, denominador = 0.0;
		float sumatorio1 = 0.0, sumatorio2 = 0.0, sumatorio3 = 0.0;
		float sumatorio4 = 0.0, acums4 = 0.0, sumatorio5 = 0.0, acums5 = 0.0;
		float a;
		/*end variables*/

		//GPU usual case
		for(int i = 0; i < numberPoints; i++){
			sumatorio1 += y[i]*logf(x[i]);
		}
		for(int i = 0; i < numberPoints; i++){
			sumatorio2 += logf(x[i]);
		}
		for(int i = 0; i < numberPoints; i++){
			sumatorio3 += y[i];
		}
		numerador = (numberPoints * sumatorio1) - (sumatorio3 * sumatorio2);
		for(int i = 0; i < numberPoints; i++){
			acums4 += powf(logf(x[i]),2);
		}
		sumatorio4 = numberPoints * (acums4);
		for(int i = 0; i < numberPoints; i++){
			acums5 += logf(x[i]);
		}
		sumatorio5 = powf(acums5 , 2);
		denominador = sumatorio4 - sumatorio5;
		a = numerador / denominador;
		calculatedChunk = ceil((a/threshold)/(float)computeUnits)*(float)computeUnits;

		return (calculatedChunk < computeUnits) ? computeUnits : calculatedChunk;
	}

/*Bundle class: This class is used to store the information that items need while walking throught pipeline's stages.*/
class Bundle {
public:
	int begin;
	int end;
	int type; //GPU = 0, CPU=1

	Bundle() {};
};

/*My serial filter class represents the partitioner of the engine. This class selects a device and a rubrange of iterations*/
class MySerialFilter: public filter {
private:
	int begin;
	int end;
	int numCPUs;
public:
	MySerialFilter(int b, int e, int nC) : 	filter(true) {
		begin = b;
		end = e;
		numCPUs = nC;
	}

	void * operator()(void *) {

		Bundle *bundle = new Bundle();
		/*If there are remaining iterations*/
		if (begin < end) {
			/*Checking if there is a GPU idle*/
			if ( --gpuStatus >= 0 ){
				//GPU WORK
				/*If we are un Completion Phase, We will take GPU chunks depending on weights*/
				if(completionPhase){
					chunkGPU = max((end-begin)*(gpuThroughput/(gpuThroughput+(cpuThroughput*numCPUs))), computeUnits);
					//cerr << "Completion Phase: chunkGPU" << chunkGPU << ",  chunkCPU: " << chunkCPU << endl;
				}
#ifndef NDEBUG
				cerr << "GPU: " << chunkGPU << ",  begin: " << begin << ", end: " << end << endl;
#endif
				/*If GPU's chunk is smaller than the number of remaining iterations, then take a chunk with chunkGPU size*/
				if((end-begin)>chunkGPU){
					bundle->begin = begin;
					bundle->end = begin + chunkGPU;
					begin = begin + chunkGPU;
					bundle->type = GPU;
					return bundle;
				}else{
					/*If GPU's chunk is bigger than the number of remaining iterations, then take all remaining iterations*/
					bundle->begin = begin;
					bundle->end = end;
					begin = end;
					bundle->type = GPU;
					return bundle;
				}
			}else{
				//CPU WORK
				gpuStatus++;
				/*If we are un Completion Phase, We will take CPU chunks depending on weights*/
				if((!completionPhase) && (gpuThroughput > 0) && (cpuThroughput > 0)){
					chunkCPU = chunkGPU/(gpuThroughput/cpuThroughput);
				}else{
					chunkCPU = max((end-begin)*(cpuThroughput/(gpuThroughput+(cpuThroughput*numCPUs))), 1);
				}
				/*If CPU's chunk is smaller than the number of remaining iterations, then take a chunk with chunkCPU size*/
				if((end-begin)>chunkCPU){
					bundle->begin = begin;
					bundle->end = begin + chunkCPU;
					begin = begin + chunkCPU;
					bundle->type = CPU;
					return bundle;
				}else{
					/*If CPU's chunk is bigger than the number of remaining iterations, then take all remaining iterations*/
					bundle->begin = begin;
					bundle->end = end;
					begin = end;
					bundle->type = CPU;
					return bundle;
				}
			}
		}
		return NULL;
	} // end operator
};

/*MyParallelFilter class is the executor component of the engine, it executes the subrange onto the device selected by SerialFilter*/
template <class B>
class MyParallelFilter: public filter {
private:
	B *body;

public:
	int computedOnCPU, computedOnGPU;
	int totalIterations;

	/*Constructor*/
	MyParallelFilter(B *b) : filter(false) {
		body = b;
		computedOnCPU = 0;
		computedOnGPU = 0;
		totalIterations = 0;
	}

	/*TBB operator*/
	void * operator()(void * item) {
		//variables
		Bundle *bundle = (Bundle*) item;
		tick_count t0, t1;
		// end variables
		/*If this is a task to be performed on GPU*/
		if(bundle->type == GPU){
			/*Compute the GPU Chunk*/
			tick_count start = tick_count::now();
			body->sendObjectToGPU(bundle->begin, bundle->end, NULL);
			body->OperatorGPU(bundle->begin, bundle->end, NULL);
			body->getBackObjectFromGPU(bundle->begin, bundle->end, NULL);
			clFinish(command_queue);
			computedOnGPU = computedOnGPU + (bundle->end- bundle->begin);
			tick_count end = tick_count::now();

			/*Make some monitoring of throughput*/
			if(measurements!=0){
				previousGPUThroughput = gpuThroughput;
			}
			/*Building phase takes a few executions (equal to numberPoints) to build the model*/
			if(BuildingModelPhase){
				x[measurements] = (bundle->end - bundle->begin);
				y[measurements] = gpuThroughput = ((x[measurements]) / ((end-start).seconds() * 1000));
				measurements++;
				chunkGPU = chunkGPU * 2;
				/*If the number of measurements is equal to number of points, then we build the model and go to monitoring phase*/
				if(measurements >= (numberPointsBuildingPhase)){
					BuildingModelPhase=false;
					monitoringPhase = true;
					int newchunkGPU = calculateLogarithmicModel(x, y, measurements);
					if(newchunkGPU > chunkGPU){
						chunkGPU = newchunkGPU;
					}
				}
			/*While we are in monitoringPhase we rebuild the model and check*/
			}else if( monitoringPhase ){
				x[measurements] = bundle->end - bundle->begin;
				y[measurements] = gpuThroughput = ((x[measurements]) / ((end-start).seconds() * 1000));
				measurements++;
				int newchunkGPU = calculateLogarithmicModel(x, y, measurements);
				if(newchunkGPU > chunkGPU){
					chunkGPU = newchunkGPU;
				}
#ifndef NDEBUG
				cerr << "Previous GPU Th: " << previousGPUThroughput << ", Current GPU Th: " << gpuThroughput << endl;
#endif

				/*if we reach the iterationBorder or gpuThroughput is more than 0.01, then we go to CompletionPhase*/
				if( (bundle->end > iterationBorder) || ((y[measurements-1] > (0.99 * y[measurements])) ) ){
					monitoringPhase = false;
					completionPhase = true;
				}
			}
			#ifdef WRITERESULT
				resultfile << "\t" << (bundle->end - bundle->begin) << "\t" << (bundle->end - bundle->begin)/((end-start).seconds() * 1000) << "\t" << chunkGPU << endl;
			#endif
			/*To release the GPU token*/
			gpuStatus++;
		}else{
			// CPU WORK
			/*Performing a CPU task*/
			tick_count start = tick_count::now();
			body->OperatorCPU(bundle->begin, bundle->end);
			tick_count end = tick_count::now();

			/*If we are in completionPhase we dont update througputs*/
			cpuThroughput = (bundle->end - bundle->begin)/((end-start).seconds()*1000);

		}
		delete bundle;
		return NULL;
	}
};

class HDSS : public Scheduler<HDSS>{
	Params *pars;
public:
	/*This constructor just call his parent's contructor*/
	HDSS(void *params) : Scheduler<HDSS>(params){
		Params * p = (Params*) params;
		pars=p;

		//Initializing library PJTRACER
		initializePJTRACER();

		numberPointsBuildingPhase = 4;
		threshold = 0.01;
		BuildingModelPhase=true;
		monitoringPhase=false;
		completionPhase = false;
		chunkGPU = /*computeUnits*/512;
		chunkCPU = 10;
		measurements = 0;
		cpuThroughput = 0;
		gpuThroughput = 0;
	}

	/*Initializes PJTRACER library*/
	void initializePJTRACER(){
#ifdef PJTRACER
		char traceFname[1024];
		sprintf(traceFname, "HDSS_C_%d_G_%d.trace", nCPUs, nGPUs);
		tracer = new PFORTRACER(traceFname);
		tracer->beginThreadTrace();
#endif
	}

	/*The main function to be implemented*/
	template<class T>
	void heterogeneous_parallel_for(int begin, int end,  T *body) {
		gpuStatus = nGPUs;
		iterationBorder = (end-begin) * 0.2;

		//cerr << "Step " << step << " Heretogeneous parallel for" << endl;
		body->firsttime = true;
		pipeline pipe;
		MySerialFilter serial_filter(begin, end, nCPUs);
		MyParallelFilter<T> parallel_filter(body);
		pipe.add_filter(serial_filter);
		pipe.add_filter(parallel_filter);

		/*Launch the pipeline*/
		pipe.run(nCPUs + nGPUs);
		pipe.clear();
	}

	/*this function print info to a Log file*/
	void saveResultsForBench(){

		char * execution_name = (char *)malloc(sizeof(char)*50);
		sprintf(execution_name, "_HDSS_%d_%d.txt", nCPUs, nGPUs);
		strcat(pars->benchName, execution_name);

		/*Checking if the file already exists*/
		bool fileExists = isFile(pars->benchName);
		ofstream file(pars->benchName, ios::out | ios::app);
		if(!fileExists){
			printHeaderToFile(file);
		}
		file << nCPUs << "\t" << nGPUs << "\t"  << runtime << "\t"
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
		file.close();
#ifndef NDEBUG
		cerr << nCPUs << "\t" << nGPUs << "\t" << runtime << "\t"
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
#endif
	}

	void printHeaderToFile(ofstream &file){
		file << "N. CPUs" << SEP << "N. GPUs" << SEP << "Time (ms)" << SEP
		<< "CPU Energy(J)" << SEP << "GPU Enegy(J)" << SEP << "Uncore Energy(J)" << SEP << "Total Energy (J)" << SEP
		<< "L2 Cache Hits" << SEP << "L2 Cache Misses" << SEP << "L2 Cache Hit Ratio" << SEP
		<< "L3 Cache Hits" << SEP << "L3 Cache Misses" << SEP << "L3 Cache Hit Ratio" << SEP << "Cycles lost Due to L3 Misses" << endl;
	}
};
