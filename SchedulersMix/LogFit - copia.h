//============================================================================
// Name			: LogFit.h
// Author		: Antonio Vilches
// Version		: 1.0
// Date			: 06 / 01 / 2014
// Copyright	: Department. Computer's Architecture (c)
// Description	: LogFit scheduler implementation
//============================================================================

#define HOST_PRIORITY
//define GPU_THRESHOLD

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <list>
#include "Scheduler.h"
#include "tbb/pipeline.h"
#include "tbb/tick_count.h"
#include "tbb/task.h"

#ifdef Win32
#include "PCM_Win/windriver.h"
#else 
#include "cpucounters.h"
#endif

#ifdef PJTRACER
#include "pfortrace.h"
#endif

using namespace tbb;
using namespace std;

/*****************************************************************************
 * Defines
 * **************************************************************************/
#define CPU 0
#define GPU 1
#define GPU_OFF -100
//#define GPU_THRESHOLD 10
#define SEP "\t"

/*****************************************************************************
 * types
 * **************************************************************************/
typedef struct{
	int numcpus;
	int numgpus;
	int gpuChunk;
	char benchName[100];
	char kernelName[50];
}Params;

/*****************************************************************************
 * Global variables
 * **************************************************************************/

atomic<int> gpuStatus;
int chunkGPU;
int chunkCPU;
int minChunkGPU;
int minChunkCPU;

bool BuildingModelPhase;
int measurements;
int numberPoints;
float cpuThroughput;
float gpuThroughput;
float maxGpuThroughput;
float alfa;
float fG;

float y[4];
float x[4];
float threshold;

atomic<int> computedOnCPU, computedOnGPU;
atomic<int>totalIterations;

#ifdef OVERHEAD_ANALYSIS
// overhead accumulators
double overhead_sp = 0.0;
double overhead_h2d = 0.0;
double overhead_kl = 0.0;
double kernel_execution = 0.0;
double overhead_d2h = 0.0;
double overhead_td = 0.0;
#endif


/*****************************************************************************
 * Heterogeneous Scheduler
 * **************************************************************************/

int calculateLogarithmicModel(float *x, float *y, int numberPoints){
		/*variables*/
		int calculatedChunk;
		float numerador, denominador;
		float sumatorio1, sumatorio2, sumatorio3;
		float sumatorio4, sumatorio5;
		float a;
		/*end variables*/

		//GPU usual case
		sumatorio1 = y[0]*logf(x[0]) + y[1]*logf(x[1]) + y[2]*logf(x[2]) + y[3]*logf(x[3]);
		sumatorio2 = logf(x[0]) + logf(x[1]) + logf(x[2]) + logf(x[3]);
		sumatorio3 = y[0]  + y[1]  + y[2]  + y[3] ;
		numerador = (numberPoints * sumatorio1) - (sumatorio3 * sumatorio2);
		sumatorio4 = numberPoints * (powf(logf(x[0]),2) + powf(logf(x[1]),2) + powf(logf(x[2]),2) + powf(logf(x[3]),2));
		sumatorio5 = powf((logf(x[0])+logf(x[1])+logf(x[2])+logf(x[3])) , 2);
		denominador = sumatorio4 - sumatorio5;
		a = numerador / denominador;
		calculatedChunk = ceil((a/threshold)/(float)computeUnits)*(float)computeUnits;

		return (calculatedChunk < computeUnits) ? computeUnits : calculatedChunk;
	}

/*Bundle class: This class is used to store the information that items need while walking throught pipeline's stages.*/
class Bundle {
public:
	int begin;
	int end;
	int type; //GPU = 0, CPU=1

	Bundle() {};
};

/*My serial filter class represents the partitioner of the engine. This class selects a device and a rubrange of iterations*/
class MySerialFilter: public filter {
private:
	int begin;
	int end;
	int nCPUs;
public:
	MySerialFilter(int b, int e, int nC) :
			filter(true) {
		begin = b;
		end = e;
		nCPUs = nC;
	}

	void * operator()(void *) {
		Bundle *bundle = new Bundle();
		if (begin < end) { //If there are remaining iterations
			//Checking which resources are available
			if ( --gpuStatus >= 0 ){
				//GPU WORK
				//cerr << "chunkGPU " << chunkGPU << " begin " << begin << " end " << end << " fG " << fG << endl;
#ifndef GPU_THRESHOLD
				if ((nCPUs < 1) || (chunkGPU/fG) < ((end-begin)-chunkGPU)/(nCPUs) ){ // checking if there is enough iterations for GPU			
#else
				if ((nCPUs < 1) || (chunkGPU/fG) < ((end-begin)-chunkGPU)/(nCPUs) || (fG/(float)(nCPUs+1) > 1.0)){ // checking if there is enough iterations for GPU			
#endif
					if((end-begin)>chunkGPU){
						bundle->begin = begin;
						bundle->end = begin + chunkGPU;
						begin = begin + chunkGPU;
						bundle->type = GPU;
						return bundle;
					}else{
						bundle->begin = begin;
						bundle->end = end;
						begin = end;
						bundle->type = GPU;
						return bundle;
					}
				}else{
					//we have to turn off the GPU, this device has to be off in order to leave iterations for CPU threads.
					gpuStatus = GPU_OFF; //turning off GPU =>
					fG = 1; //Now the GPU-thread works as a CPU

					chunkCPU = min( chunkGPU/fG, max((end-begin)/( fG + nCPUs), minChunkCPU));
					if((end-begin)>chunkCPU){
						bundle->begin = begin;
						bundle->end = begin + chunkCPU;
						begin = begin + chunkCPU;
						bundle->type = CPU;
						return bundle;
					}else{
						bundle->begin = begin;
						bundle->end = end;
						begin = end;
						bundle->type = CPU;
						return bundle;
					}
				}
			}else{
				//CPU WORK
				gpuStatus++;
				//cerr << "chunkCPU " << chunkCPU << " begin " << begin << " end " << end << " fG " << fG << " numCPUs " << numCPUs << endl;
				chunkCPU = min(chunkGPU/fG, max((end-begin)/( fG + nCPUs), minChunkCPU));
				if((end-begin)>chunkCPU){
					bundle->begin = begin;
					bundle->end = begin + chunkCPU;
					begin = begin + chunkCPU;
					bundle->type = CPU;
					return bundle;
				}else{
					bundle->begin = begin;
					bundle->end = end;
					begin = end;
					bundle->type = CPU;
					return bundle;
				}
			}
		}
		return NULL;
	} // end operator
};

/*MyParallelFilter class is the executor component of the engine, it executes the subrange onto the device selected by SerialFilter*/
template <class B>
class MyParallelFilter: public filter {
private:
	B *body;
	
public:
	int computedOnCPU, computedOnGPU;
	int totalIterations;

	MyParallelFilter(B *b) :
			filter(false) {
		body = b;
		computedOnCPU = 0;
		computedOnGPU = 0;
		totalIterations = 0;
	}
	void * operator()(void * item) {                                                                                                                                                                            

		//variables
		Bundle *bundle = (Bundle*) item;
				
		if(bundle->type == GPU){
			// GPU WORK
				#ifdef DEBUG
			cerr << "launchGPU(): begin: " << bundle->begin << " end: " << bundle->end << endl; 
#endif
			tick_count start_tc = tick_count::now();
#ifdef OVERHEAD_ANALYSIS
			//Calculating partition scheduling overhead
			overhead_sp += (start_tc - end_tc).seconds()*1000;

			//Adding a marker in the command queue
			cl_event event_before_h2d;
			int error = clEnqueueMarkerWithWaitList(command_queue,	0, NULL, &event_before_h2d);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl; 
				exit(0);
			}
#endif
			body->sendObjectToGPU(bundle->begin, bundle->end, NULL);

#ifdef OVERHEAD_ANALYSIS
			//Adding a marker in the command queue
			cl_event event_before_kernel;
			error = clEnqueueMarkerWithWaitList(command_queue,	0, NULL, &event_before_kernel);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl; 
				exit(0);
			}
#endif
			cl_event event_kernel;
			body->OperatorGPU(bundle->begin, bundle->end, &event_kernel);

#ifdef OVERHEAD_ANALYSIS
			//Adding a marker in the command queue
			cl_event event_after_kernel;
			error = clEnqueueMarkerWithWaitList(command_queue,	0, NULL, &event_after_kernel);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl; 
				exit(0);
			}
#endif
			body->getBackObjectFromGPU(bundle->begin, bundle->end, NULL);

#ifdef OVERHEAD_ANALYSIS
			//Adding a marker in the command queue
			cl_event event_after_d2h;
			error = clEnqueueMarkerWithWaitList(command_queue,	0, NULL, &event_after_d2h);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl; 
				exit(0);
			}
#endif
			clFinish(command_queue);

#ifdef OVERHEAD_ANALYSIS
			//Adding a marker in the command queue to computer thread dispatch
			cl_event event_after_finish;
			error = clEnqueueMarkerWithWaitList(command_queue,	0, NULL, &event_after_finish);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl; 
				exit(0);
			}
#endif
			tick_count end_tc = tick_count::now();

#ifdef OVERHEAD_ANALYSIS
			
			//Calculating host to device transfer overhead
			cl_ulong tg1, tg2, tg3, tg4, tg5, tc3;
			clGetEventProfilingInfo(event_before_h2d, CL_PROFILING_COMMAND_COMPLETE, sizeof(cl_ulong), &tg1, NULL);
			clGetEventProfilingInfo(event_before_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg2, NULL);
			overhead_d2h += (tg2-tg1)/1000000.0; // ms
			
			//Calculating kernel launch overheads
			clGetEventProfilingInfo(event_before_kernel, CL_PROFILING_COMMAND_COMPLETE, sizeof(cl_ulong), &tg2, NULL);
			clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg3, NULL);
			overhead_kl += (tg3-tg2)/1000000.0; // ms

			//Calculating kernel execution
			clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg3, NULL);
			clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_COMPLETE, sizeof(cl_ulong), &tg4, NULL);
			kernel_execution += (tg4-tg3)/1000000.0; // ms

			//Calculating device to host transfer overhead
			clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_COMPLETE, sizeof(cl_ulong), &tg4, NULL);
			clGetEventProfilingInfo(event_after_d2h, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg5, NULL);
			overhead_d2h += (tg5-tg4)/1000000.0; // ms

			//Calculating device overhead thread dispatch
			clGetEventProfilingInfo(event_after_d2h, CL_PROFILING_COMMAND_COMPLETE, sizeof(cl_ulong), &tg5, NULL);
			clGetEventProfilingInfo(event_after_finish, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tc3, NULL);
			overhead_td += (tc3-tg5)/1000000.0; // ms

			//Releasing event objects
			clReleaseEvent(event_before_h2d);
			clReleaseEvent(event_before_kernel);
			clReleaseEvent(event_kernel);
			clReleaseEvent(event_after_kernel);
			clReleaseEvent(event_after_d2h);
			clReleaseEvent(event_after_finish);
#endif
			float time =(end_tc - start_tc).seconds()*1000;
			gpuThroughput = (bundle->end - bundle->begin) / time;

				computedOnGPU = computedOnGPU + (bundle->end-bundle->begin);

#ifdef OUTPUT_FREQ_HISTOGRAM
				float th = (bundle->end - bundle->begin)/((end-start).seconds() * 1000);
				for (int i = bundle->begin; i < bundle->end;i++){
					histogram << bundle->end - bundle->begin << "\t" << th << endl;
				}
#endif			
			if(BuildingModelPhase){
				x[measurements] = bundle->end - bundle->begin;
				y[measurements] = gpuThroughput;
				measurements++;
				chunkGPU = chunkGPU * 2;
				if(measurements >= (numberPoints)){
					BuildingModelPhase=false;
					chunkGPU = calculateLogarithmicModel(x, y, numberPoints);
				}
			}else{
				x[3] = bundle->end - bundle->begin;
				y[3] = gpuThroughput;
				chunkGPU = calculateLogarithmicModel(x, y, numberPoints);
			}
			
			if (cpuThroughput > 0){
				fG = gpuThroughput/cpuThroughput;
			}
		#ifdef DEBUG
				cerr << "\t" << bundle->end - bundle->begin << "\t" << (bundle->end - bundle->begin)/ ((end_tc-start_tc).seconds() * 1000) << "\t" << chunkGPU << endl;
		#endif

			computedOnGPU = computedOnGPU + (bundle->end-bundle->begin);
			/*To release the GPU token*/
			gpuStatus++;
		}else{
			// CPU WORK
			tick_count start = tick_count::now();
			body->OperatorCPU(bundle->begin, bundle->end);
			tick_count end = tick_count::now();
			cpuThroughput = (bundle->end - bundle->begin)/((end-start).seconds()*1000);
			if (gpuThroughput > 0){
				fG = gpuThroughput/cpuThroughput;
			}
			#ifdef WRITERESULT
				//resultfile << "cpu\t" << (bundle->end - bundle->begin) << "\t" << cpuThroughput << endl;
			#endif
		}
		delete bundle;
		return NULL;
	}
};
//end class

class LogFit : public Scheduler<LogFit>{
	Params *pars;
public:
	/*This constructor just call his parent's contructor*/
	LogFit(void *params) : Scheduler(params){
		Params * p = (Params*) params;
		pars=p;

		BuildingModelPhase = true;
		chunkGPU = computeUnits;
		chunkCPU = 200;
		minChunkCPU = 100;
		minChunkGPU = 256;
		measurements = 0;
		maxGpuThroughput = 0;
		alfa = 0.5;
		fG = 0.0;
		gpuThroughput=0.0;
		cpuThroughput=0.0;
		computedOnCPU=0;
		computedOnGPU=0;
		totalIterations=0;

		//Initializing library PJTRACER
		initializePJTRACER();

#ifdef HOST_PRIORITY
		sprintf(p->benchName, "%s_PRIO", p->benchName);
#endif
	}
	
	/*Initializes PJTRACER library*/
	void initializePJTRACER(){
#ifdef PJTRACER
		char traceFname[1024];
		sprintf(traceFname, "LOGFIT_C_%d_G_%d.trace", nCPUs, nGPUs);
		tracer = new PFORTRACER(traceFname);
		tracer->beginThreadTrace();
#endif
	}

public:
	/*The main function to be implemented*/
	template<class T> 
	void heterogeneous_parallel_for(int begin, int end,  T *body) {
		gpuStatus = nGPUs;
		numberPoints = 4;
		threshold = 0.01;
		
		//cerr << "Step " << step << " Heretogeneous parallel for" << endl;
		body->firsttime = true;
		pipeline pipe;
		MySerialFilter serial_filter(begin, end, nCPUs);
		MyParallelFilter<T> parallel_filter(body);
		pipe.add_filter(serial_filter);
		pipe.add_filter(parallel_filter);
		
#ifdef OVERHEAD_ANALYSIS
		end_tc = tick_count::now();
#endif
		/*Launch the pipeline*/
		pipe.run(nCPUs + nGPUs);
		pipe.clear();
	}

	/*this function print info to a Log file*/
	void saveResultsForBench(){

		char * execution_name = (char *)malloc(sizeof(char)*50);
		sprintf(execution_name, "_LOGFIT_%d_%d.txt", nCPUs, nGPUs);
		strcat(pars->benchName, execution_name);

		/*Checking if the file already exists*/
		bool fileExists = isFile(pars->benchName);
		ofstream file(pars->benchName, ios::out | ios::app);
		if(!fileExists){
			printHeaderToFile(file);
		}
		file << nCPUs << "\t" << nGPUs << "\t"  << runtime << "\t"
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"  
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t" 
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t" 
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t" 
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
		file.close();
#ifdef DEBUG
		cerr << nCPUs << "\t" << nGPUs << "\t" << runtime << "\t"
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"  
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t" 
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t" 
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t" 
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
#endif
	}

	void printHeaderToFile(ofstream &file){
		file << "N. CPUs" << SEP << "N. GPUs" << SEP << "Time (ms)" << SEP 
		<< "CPU Energy(J)" << SEP << "GPU Enegy(J)" << SEP << "Uncore Energy(J)" << SEP << "Total Energy (J)" << SEP
		<< "L2 Cache Hits" << SEP << "L2 Cache Misses" << SEP << "L2 Cache Hit Ratio" << SEP 
		<< "L3 Cache Hits" << SEP << "L3 Cache Misses" << SEP << "L3 Cache Hit Ratio" << SEP << "Cycles lost Due to L3 Misses" << endl;
	}
};