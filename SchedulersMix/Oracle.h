//============================================================================
// Name			: HSOracle.h
// Author		: Antonio Vilches
// Version		: 1.0
// Date			: 29 / 12 / 2014
// Copyright	: Department. Computer's Architecture (c)
// Description	: Oracle scheduler implementation
//============================================================================

//#define USEBARRIER

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>
#include "Scheduler.h"
#include "tbb/pipeline.h"
#include "tbb/tick_count.h"
#include <atomic>

#ifdef Win32
#include "PCM_Win/windriver.h"
//#else
//#include "cpucounters.h"
#endif

#ifdef PJTRACER
#include "pfortrace.h"
#endif

using namespace std;
using namespace tbb;

/*****************************************************************************
 * Defines
 * **************************************************************************/
#define CPU 0
#define isGPU 1
#define GPU_OFF -100 //Arbitrary value
#define SEP "\t"

/*****************************************************************************
 * types
 * **************************************************************************/
typedef struct{
	int numcpus;
	int numgpus;
	float ratioG;
	char benchName[100];
	char kernelName[100];
}Params;

/*****************************************************************************
 * Global variables
 * **************************************************************************/
std::atomic<int> gpuStatus;
int chunkCPU;
int chunkGPU;
float gpuThroughput, cpuThroughput;


/*****************************************************************************
 * Heterogeneous Scheduler
 * **************************************************************************/
/*Bundle class: This class is used to store the information that items need while walking throught pipeline's stages.*/
class Bundle {
public:
	int begin;
	int end;
	int type; //GPU = 0, CPU=1

	Bundle() {};
};

/*My serial filter class represents the partitioner of the engine. This class selects a device and a rubrange of iterations*/
class MySerialFilter: public filter {
private:
	int begin;
	int end;
public:
	/*Class constructor, it only needs the first and last iteration indexes.*/
	MySerialFilter(int b, int e) : filter(true) {
		begin = b;
		end = e;
	}

	/*Mandatory operator method, TBB rules*/
	void * operator()(void *) {
		Bundle *bundle = new Bundle();
		/*If there are remaining iterations*/
		if (begin < end) {
			//Checking which resources are available
			if ( --gpuStatus >= 0 ){
				//GPU WORK
				int auxEnd = begin + chunkGPU;
				auxEnd = (auxEnd > end) ? end : auxEnd;
				bundle->begin = begin;
				bundle->end = auxEnd;
				begin = auxEnd;
				bundle->type = isGPU;
				return bundle;
			}else{
				//CPU WORK
				gpuStatus++;
				/*Taking a iteration chunk for CPU*/
				int auxEnd = begin + chunkCPU;
				auxEnd = (auxEnd > end) ? end : auxEnd;
				bundle->begin = begin;
				bundle->end = auxEnd;
				begin = auxEnd;
				bundle->type = CPU;
				return bundle;
			}
		}
		return NULL;
	} // end operator
};

/*MyParallelFilter class is the executor component of the engine, it executes the subrange onto the device selected by SerialFilter*/
template <class B>
class MyParallelFilter: public filter {
private:
	B *body;

public:
	/*Class' constructor*/
//	template <class B>
	MyParallelFilter(B *b) : filter(false) {
		body = b;
	}

	/*Operator function*/
	void * operator()(void * item) {
		//variables
		Bundle *bundle = (Bundle*) item;
		if(bundle->type == isGPU){
			// GPU WORK
#ifdef PJTRACER
			tracer->gpuStart();
#endif
			body->sendObjectToGPU(bundle->begin, bundle->end, NULL);
			body->OperatorGPU(bundle->begin, bundle->end, NULL);
			body->getBackObjectFromGPU(bundle->begin, bundle->end, NULL);
			clFinish(command_queue);
#ifdef PJTRACER
			tracer->gpuStop();
#endif
			/*To release GPU token*/
			gpuStatus++;
		}else{
			// CPU WORK
#ifndef NDEBUG
			cerr << "launchCPU(): begin: " << bundle->begin << " end: " << bundle->end << endl;
#endif
#ifdef PJTRACER
			tracer->cpuStart();
#endif
			body->OperatorCPU(bundle->begin, bundle->end);

#ifdef PJTRACER
			tracer->cpuStop();
#endif
		}
		/*Deleting bundle to avoid memory leaking*/
		delete bundle;
		return NULL;
	}
};
//end class

/*Oracle Class: This scheduler version let us to split the workload in two subranges, one for GPU and one for CPUs*/
class Oracle : public Scheduler<Oracle, Params> {
	float ratioGPU;
	Params *pars;
public:
	/*This constructor just call his parent's contructor*/
 Oracle(Params *params) : Scheduler<Oracle, Params>(params){
   		Params * p = (Params *) params;
		chunkCPU = 0;
		chunkGPU = 0;
		gpuStatus = p->numgpus;
		ratioGPU = p->ratioG;
		pars = p;

		//Initializing library PJTRACER
		initializePJTRACER(p->benchName);
	}

	/*Initializes PJTRACER library*/
	void initializePJTRACER(char* bench){
#ifdef PJTRACER
		char traceFname[50];
		sprintf(traceFname, "%s_ORACLE_C_%d_G_%d.trace", bench, nCPUs, nGPUs);
		tracer = new PFORTRACER(traceFname);
		tracer->beginThreadTrace();
#endif
	}

	/*The main function to be implemented*/
	template<class T>
	void heterogeneous_parallel_for(int begin, int end, T* body){
#ifndef NDEBUG
		cerr << "Heterogeneous Parallel For Oracle step " << step << ": " << nCPUs << " , " << nGPUs << endl;
#endif
		/*Preparing pipeline*/
		pipeline pipe;
		MySerialFilter serial_filter(begin, end);
		MyParallelFilter<T> parallel_filter(body);
		pipe.add_filter(serial_filter);
		pipe.add_filter(parallel_filter);
		body->firsttime = true;

		/*Seeting a mark to recognize a timestep*/
#ifdef PJTRACER
		tracer->newEvent();
#endif
		/*Parameter checking*/
		if((nCPUs > 0) && (nGPUs > 0) && (ratioGPU != 0.0) && (ratioGPU != 1.0)){
			//Pipeline case
			chunkGPU = (end-begin)*ratioGPU;
			chunkCPU = (end - begin) * (1 - ratioGPU);
			chunkCPU = floor((chunkCPU/nCPUs)+1);
			gpuStatus = nGPUs;

			pipe.run(nCPUs + nGPUs);
			pipe.clear();

		}else if((nCPUs > 0) && (nGPUs > 0) && (ratioGPU == 0)){
			//Only CPUs working
			chunkCPU = ceil((end - begin)/nCPUs)+1;
			gpuStatus = 0;

			pipe.run(nCPUs);
			pipe.clear();

		}else if((nCPUs > 0) && (nGPUs > 0) && (ratioGPU == 1)){
			/*Only GPU work*/
			body->sendObjectToGPU(begin, end, NULL);
			body->OperatorGPU(begin, end, NULL);
			body->getBackObjectFromGPU(begin, end, NULL);
			clFinish(command_queue);

		}else if(nGPUs < 1){
			/*Only CPUs working*/
			/*In case of decimal division, we round to the upper int*/
			chunkCPU = ceil((end - begin)/nCPUs);
			gpuStatus = 0;

			pipe.run(nCPUs);
			pipe.clear();

		}else if(nCPUs < 1){
			/*Only GPU working*/
			body->sendObjectToGPU(begin, end, NULL);
			body->OperatorGPU(begin, end, NULL);
			body->getBackObjectFromGPU(begin, end, NULL);
			clFinish(command_queue);

		}else{
			cerr << "This case is not supported: numCPUS " << nCPUs << " numGPUs " << nGPUs << " ratioGPU " << ratioGPU << endl;
			exit(-1);
		}
	}

	/*this function print info to a Log file*/
	void saveResultsForBench(){

		char * execution_name = (char *)malloc(sizeof(char)*100);
		sprintf(execution_name, "%s_Oracle_%d_%d.txt", pars->benchName, nCPUs, nGPUs, ratioGPU);

		/*Checking if the file already exists*/
		bool fileExists = isFile(execution_name);
		ofstream file(execution_name, ios::out | ios::app);
		if(!fileExists){
			printHeaderToFile(file);
		}
		file << nCPUs << "\t" << nGPUs << "\t"  << ratioGPU << "\t" << runtime << "\t"
#ifdef PMLIB
		<< energiaCPU << SEP  << energiaFPGA <<  SEP << energiaCPU+energiaFPGA <<  SEP 
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"

		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) 
#endif
		<< endl;
		file.close();
#ifndef NDEBUG
		cerr << nCPUs << "\t" << nGPUs << "\t"  << ratioGPU << "\t" << runtime << "\t"
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
#endif
	}

	void printHeaderToFile(ofstream &file){
		file << "N. CPUs" << SEP << "N. GPUs" << SEP << "GPU Ratio" << SEP << "Time (ms)" << SEP
#ifdef PMLIB
		<< "CPU Energy (BBB)" << SEP << "FPGA Energy (BBB) " << SEP<< "TOTAL Energy (BBB) " << SEP

		<< "CPU Energy(J)" << SEP << "GPU Enegy(J)" << SEP << "Uncore Energy(J)" << SEP << "Total Energy (J)" << SEP
	
		<< "L2 Cache Hits" << SEP << "L2 Cache Misses" << SEP << "L2 Cache Hit Ratio" << SEP
		<< "L3 Cache Hits" << SEP << "L3 Cache Misses" << SEP << "L3 Cache Hit Ratio" << SEP << "Cycles lost Due to L3 Misses" 
#endif
		<< endl;
	}
	
	//Den---------------------------
	
	void saveResultsForBenchDen(int inputId, int nrIter){

		char * execution_name = (char *)malloc(sizeof(char)*100);
		sprintf(execution_name, "%s_Oracle.txt", pars->benchName);

		/*Checking if the file already exists*/
		bool fileExists = isFile(execution_name);
		ofstream file(execution_name, ios::out | ios::app);
		if(!fileExists){
			printHeaderToFileDen(file);
		}
		file 
        << inputId << SEP 
        << nCPUs << SEP 
        << nGPUs << SEP 
        << ratioGPU << SEP
        << runtime << SEP 
#ifdef ODROID
		<< total_em((*sample1)) << SEP
        << sample1->A15 << SEP
        << sample1->A7 << SEP
        << sample1->GPU << SEP
        << sample1->MEM << SEP
#endif        
#ifdef PMLIB
		<< energiaCPU << SEP  << energiaFPGA <<  SEP << energiaCPU+energiaFPGA <<  SEP 
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"

		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) 
#endif
		<< nrIter << endl;
		file.close();
#ifndef NDEBUG
		cerr << nCPUs << "\t" << nGPUs << "\t"  << ratioGPU << "\t" << runtime << "\t"
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
#endif
	}

	void printHeaderToFileDen(ofstream &file){
		file << "InputId" << SEP << "N. CPUs" << SEP << "N. GPUs" << SEP << "GPU Ratio" << SEP << "Time (ms)" << SEP
#ifdef ODROID
        << "T.E" << SEP << "A15" << SEP << "A7" << SEP << "GPU" << SEP << "MEM" << SEP 
#endif
#ifdef PMLIB
		<< "CPU Energy (BBB)" << SEP << "FPGA Energy (BBB) " << SEP<< "TOTAL Energy (BBB) " << SEP

		<< "CPU Energy(J)" << SEP << "GPU Enegy(J)" << SEP << "Uncore Energy(J)" << SEP << "Total Energy (J)" << SEP
	
		<< "L2 Cache Hits" << SEP << "L2 Cache Misses" << SEP << "L2 Cache Hit Ratio" << SEP
		<< "L3 Cache Hits" << SEP << "L3 Cache Misses" << SEP << "L3 Cache Hit Ratio" << SEP << "Cycles lost Due to L3 Misses" 
#endif
		<< endl;
	}
};
