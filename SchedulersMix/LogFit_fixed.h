//============================================================================
// Name			: LogFit.h
// Author		: Antonio Vilches
// Version		: 1.0
// Date			: 06 / 01 / 2014
// Copyright	: Department. Computer's Architecture (c)
// Description	: LogFit scheduler implementation
//============================================================================

#define HOST_PRIORITY

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <list>
#include "Scheduler.h"
#include "tbb/pipeline.h"
#include "tbb/tick_count.h"
#include "tbb/task.h"
#include "tbb/atomic.h"

#ifdef Win32
#include "PCM_Win/windriver.h"
#else
#include "cpucounters.h"
#endif

#ifdef PJTRACER
#include "pfortrace.h"
#endif

#define FIXED_CHUNK 1024

//#define DEBUG 

using namespace tbb;
// using namespace std;

/*****************************************************************************
 * Defines
 * **************************************************************************/
#define CPU 0
#define GPU 1
#define GPU_OFF -100
#define SEP "\t"
#define MAXPOINTS 1024

/*****************************************************************************
 * types
 * **************************************************************************/
typedef struct{
	int numcpus;
	int numgpus;
	int gpuChunk;
	char benchName[100];
	char kernelName[50];
}Params;

/*****************************************************************************
 * Global variables
 * **************************************************************************/

atomic<int> gpuStatus;
int chunkGPU;
int chunkCPU;
int minChunkGPU;
int minChunkCPU;
bool stopConditionModeOn;
bool concordCase;

bool explorationPhase;
bool explotationPhase;
int measurements;
int numberPoints;
float cpuThroughput;
float gpuThroughput;
float fG;

// Model variables
float y[MAXPOINTS];
float x[MAXPOINTS];
float threshold;

//Summarize GPU statistics
int itemsOnGPU = 0;
int totalIterationsGPU = 0;
tick_count end_tc;

#ifdef OVERHEAD_STUDY
// overhead accumulators
float overhead_sp = 0.0;
float overhead_h2d = 0.0;
float overhead_kl = 0.0;
float kernel_execution = 0.0;
float overhead_d2h = 0.0;
float overhead_td = 0.0;
cl_ulong tg1, tg2, tg3, tg4, tg5;
#endif


/*****************************************************************************
 * Heterogeneous Scheduler
 * **************************************************************************/

int calculateLogarithmicModel(float *x, float *y, int numberPoints, int last_point){
		//variables
		int calculatedChunk;
		float numerador, denominador;
		float sumatorio1, sumatorio2, sumatorio3;
		float sumatorio4, sumatorio5;
		float a;
		//points
		int cero, one, two, three;
		//end variables

		cero = 0;
		one = ceil(last_point * 0.33);
		two = ceil(last_point * 0.66);
		three = last_point;

		//GPU usual case
		sumatorio1 = y[cero]*logf(x[cero]) + y[one]*logf(x[one]) + y[two]*logf(x[two]) + y[three]*logf(x[three]);
		sumatorio2 = logf(x[cero]) + logf(x[one]) + logf(x[two]) + logf(x[three]);
		sumatorio3 = y[cero]  + y[one]  + y[two]  + y[three] ;
		numerador = (numberPoints * sumatorio1) - (sumatorio3 * sumatorio2);
		sumatorio4 = numberPoints * (powf(logf(x[cero]),2) + powf(logf(x[one]),2) + powf(logf(x[two]),2) + powf(logf(x[three]),2));
		sumatorio5 = powf((logf(x[cero])+logf(x[one])+logf(x[two])+logf(x[three])) , 2);
		denominador = sumatorio4 - sumatorio5;
		//Getting the value for a
		a = numerador / denominador;
		//The threshold is set during first call
		if(threshold==0.0){
			threshold = a / x[last_point];
		}
		//Just to get a multiple of computeUnit
		calculatedChunk = ceil((a/threshold)/(float)computeUnits)*(float)computeUnits;

//		return (calculatedChunk < computeUnits) ? computeUnits : calculatedChunk;
                return FIXED_CHUNK;
	}

/*Bundle class: This class is used to store the information that items need while walking throught pipeline's stages.*/
class Bundle {
public:
	int begin;
	int end;
	int type; //GPU = 0, CPU=1

	Bundle() {};
};

/*My serial filter class represents the partitioner of the engine. This class selects a device and a rubrange of iterations*/
class MySerialFilter: public filter {
private:
	int begin;
	int end;
	int nCPUs;
public:
	MySerialFilter(int b, int e, int nC) :
			filter(true) {
		begin = b;
		end = e;
		nCPUs = nC;
		stopConditionModeOn = false;
	}

	void * operator()(void *) {
		Bundle *bundle = new Bundle();
		//If there are remaining iterations
		if (begin < end){
			//Checking whether the GPU is idle or not.
			if ( --gpuStatus >= 0)  {
				//Checking stop condition
				if(explorationPhase){
					int auxEnd = begin + chunkGPU;
					auxEnd = (auxEnd > end) ? end : auxEnd;
#ifdef DEBUG
					cerr << "Serial Filter GPU: " << chunkGPU << " Begin = " << begin << ", End = " << auxEnd << std::endl;
#endif
					bundle->begin = begin;
					bundle->end = auxEnd;
					begin = auxEnd;
					bundle->type = GPU;
					return bundle;
				}else if(explotationPhase){
					stopConditionModeOn = GPU_Stop_Condition();
					if (!stopConditionModeOn){
						int auxEnd = begin + chunkGPU;
						auxEnd = (auxEnd > end) ? end : auxEnd;
#ifdef DEBUG
					cerr << "Serial Filter GPU: " << chunkGPU << " Begin = " << begin << ", End = " << auxEnd << std::endl;
#endif
						bundle->begin = begin;
						bundle->end = auxEnd;
						begin = auxEnd;
						bundle->type = GPU;
						return bundle;
					}else{ //stop Condition is true
						//cerr << "StopCondition" << std::endl;
						if(begin == 0){
							//concord case
							concordCase = true;
							cpuThroughput = cpuThroughput*nCPUs;
							chunkGPU = std::max((end-begin)*gpuThroughput/(gpuThroughput+cpuThroughput),
                                                            static_cast<float>(computeUnits));
							chunkCPU = ceil( ((end-begin)-chunkGPU)/nCPUs);

							//cerr << "Concord Case: " << chunkGPU << ", " << chunkCPU << std::endl;
							int auxEnd = begin + chunkGPU;
							auxEnd = (auxEnd > end) ? end : auxEnd;
#ifdef DEBUG
					cerr << "Serial Filter GPU: " << chunkGPU << " Begin = " << begin << ", End = " << auxEnd << std::endl;
#endif
							bundle->begin = begin;
							bundle->end = auxEnd;
							begin = auxEnd;
							bundle->type = GPU;
							return bundle;
						}else{
							//Journal SuperComputing Phase
							chunkCPU = std::max(ceil((end-begin)/nCPUs), 1.0);
							int auxEnd = begin + chunkCPU;
							auxEnd = (auxEnd > end) ? end : auxEnd;
#ifdef DEBUG
					cerr << "Serial Filter GPU: " << chunkGPU << " Begin = " << begin << ", End = " << auxEnd << std::endl;
#endif
							bundle->begin = begin;
							bundle->end = auxEnd;
							begin = auxEnd;
							bundle->type = CPU;
							return bundle;
						}
					}
				}

			}else{
				//CPU WORK
				gpuStatus++;
				if(explotationPhase){
					if(!stopConditionModeOn){
						chunkCPU = std::min(chunkGPU/fG, std::max((end-begin)/( fG + nCPUs), 10.0f)); // min 10 Andres
					}else if(!concordCase){
						chunkCPU = std::max(ceil((end-begin)/nCPUs), 1.0);
					}
				}
				int auxEnd = begin + chunkCPU;
				auxEnd = (auxEnd > end) ? end : auxEnd;
#ifdef DEBUG
					cerr << "Serial Filter CPU: " << chunkCPU << " Begin = " << begin << ", End = " << auxEnd << std::endl;
#endif
				bundle->begin = begin;
				bundle->end = auxEnd;
				begin = auxEnd;
				bundle->type = CPU;
				return bundle;
			}
		}
		return NULL;
	} // end operator

	bool GPU_Stop_Condition(){
		bool stopGPU=false;

		//Checking whether there are enough remaining iterations
		if( (nCPUs < 1) || ( (chunkGPU/fG) < ((end-begin)-chunkGPU)/(nCPUs) ) ){
			stopGPU = false; //Andres
		}
		return stopGPU;
	}

	/*bool calculateGPUThroughputBySingleSlope(int &res){
		bool workForGPU;
		int remaining = (end-begin);
		int chunkgpu1;
		int chunkgpu2;

		if(remaining > x[measurements-1]){
			// We should extrapolate
			float slope = (y[measurements-1] - y[measurements-2])/(x[measurements-1]-x[measurements-2]);
			chunkgpu1 = ((y[measurements-1]+cpuThroughput - remaining*slope - x[measurements-1]*slope) + sqrt(pow(remaining*slope+x[measurements-1]*slope-y[measurements-1]-cpuThroughput,2) + 4*slope*(-remaining*slope*x[measurements-1]+remaining*y[measurements-1]))) / -2*slope;
			chunkgpu2 = ((y[measurements-1]+cpuThroughput - remaining*slope - x[measurements-1]*slope) - sqrt(pow(remaining*slope+x[measurements-1]*slope-y[measurements-1]-cpuThroughput,2) + 4*slope*(-remaining*slope*x[measurements-1]+remaining*y[measurements-1]))) / -2*slope;
			//cerr << "Extrapolate: " << x[measurements-1] << std::endl;
			//Taking the best option
			if(((chunkgpu1 <= 0) && (chunkgpu2 <= 0))||((chunkgpu1 > remaining)&&(chunkgpu2 > remaining))){//Both points are under cero
				if(nCPUs*cpuThroughput > (slope*remaining+y[measurements-2])){//CPU's throughput is bigger than GPU's throughput
					workForGPU = false;
					chunkCPU = ceil((remaining - res)/(nCPUs+1));
				}else{
					res = remaining;
					workForGPU = true;
				}
			}else{ //There is at least one point in the region of interest
				res = std::max(chunkgpu1, chunkgpu2);
				workForGPU = true;
				chunkCPU = ceil((remaining - res)/nCPUs);
			}
		}else{
			//We should interpolate
			int i=0;
			while((i < measurements) && (x[i] < remaining)){
				i++;
			}

			//Calculating slope
			float slope = (y[i-1] - y[i-2])/(x[i-1]-x[i-2]);
			chunkgpu1 = ((y[i-2]+cpuThroughput - remaining*slope - x[i-2]*slope) + sqrt(pow(remaining*slope+x[i-2]*slope-y[i-2]-cpuThroughput,2) + 4*slope*(-remaining*slope*x[i-2]+remaining*y[i-2]))) / -2*slope;
			chunkgpu2 = ((y[i-2]+cpuThroughput - remaining*slope - x[i-2]*slope) - sqrt(pow(remaining*slope+x[i-2]*slope-y[i-2]-cpuThroughput,2) + 4*slope*(-remaining*slope*x[i-2]+remaining*y[i-2]))) / -2*slope;
			//cerr << "Interpolate: " << x[i-2] << std::endl;
			//Taking the best option
			if(((chunkgpu1 <= 0) && (chunkgpu2 <= 0))||((chunkgpu1 > remaining)&&(chunkgpu2 > remaining))){//Both points are under cero
				if(nCPUs*cpuThroughput > (slope*remaining+y[i-2])){//CPU's throughput is bigger than GPU's throughput
					workForGPU = false;
					chunkCPU = ceil((remaining - res)/(nCPUs+1));
				}else{
					res = remaining;
					workForGPU = true;
				}
			}else{ //There is at least one point in the region of interest
				res = std::max(chunkgpu1, chunkgpu2);
				workForGPU = true;
				chunkCPU = ceil((remaining - res)/nCPUs);
			}
		}
		//Finding out the best tram
		cerr << "Vector Y: [";
		for(int i = 0; i < measurements; i++){
			cerr << y[i] << ", ";
		}
		cerr << "]"<<endl;
		cerr << "Vector X: [";
		for(int i = 0; i < measurements; i++){
			cerr << x[i] << ", ";
		}
		cerr << "]"<<endl;

		int i=0;
		while((i < measurements) && (x[i] < remaining)){
			i++;
		}
		cerr << "Border point: " << i << " - " << x[i] << " - " << x[measurements-1] << std::endl;
		cerr << "Begin: " << begin << " End: " << end << std::endl;
		cerr << "chunkgpu1: " << chunkgpu1 << std::endl;
		cerr << "chunkgpu2: " << chunkgpu2 << std::endl;
		cerr << "Remaining: " << remaining << std::endl;

		return workForGPU;
	}*/

};

/*MyParallelFilter class is the executor component of the engine, it executes the subrange onto the device selected by SerialFilter*/
template <class B>
class MyParallelFilter: public filter {
private:
	B *body;
	int iterationSpace;

public:
	MyParallelFilter(B *b, int i) :
			filter(false) {
		body = b;
		iterationSpace=i;
	}
	void * operator()(void * item) {

		//variables
		Bundle *bundle = (Bundle*) item;

		if(bundle->type == GPU){
			// GPU WORK
			executeOnGPU(bundle);
			//To release the GPU token
			gpuStatus++;
		}else{
			// CPU WORK
			executeOnCPU(bundle);
		}

		delete bundle;
		return NULL;
	}

	void executeOnGPU(Bundle *bundle){
		tick_count start_tc = tick_count::now();
#ifdef OVERHEAD_STUDY
			//Calculating partition scheduling overhead
			overhead_sp = overhead_sp + ((start_tc - end_tc).seconds()*1000);

			//Adding a marker in the command queue
			cl_event event_before_h2d;
			int error = clEnqueueMarker(command_queue, &event_before_h2d);
			if (error != CL_SUCCESS) {
        std::cerr <<  "Failed equeuing start event" << std::endl;
				exit(0);
			}
#endif
			body->sendObjectToGPU(bundle->begin, bundle->end, NULL);

#ifdef OVERHEAD_STUDY
			//Adding a marker in the command queue
			cl_event event_before_kernel;
			error = clEnqueueMarker(command_queue, &event_before_kernel);
			if (error != CL_SUCCESS) {
        std::cerr <<  "Failed equeuing start event" << std::endl;
				exit(0);
			}
#endif
			cl_event event_kernel;
			body->OperatorGPU(bundle->begin, bundle->end, &event_kernel);

#ifdef OVERHEAD_STUDY
			//Adding a marker in the command queue
			cl_event event_after_kernel;
			error = clEnqueueMarker(command_queue,	 &event_after_kernel);
			if (error != CL_SUCCESS) {
        std::cerr <<  "Failed equeuing start event" << std::endl;
				exit(0);
			}
#endif
			body->getBackObjectFromGPU(bundle->begin, bundle->end, NULL);

#ifdef OVERHEAD_STUDY
			//Adding a marker in the command queue
			cl_event event_after_d2h;
			error = clEnqueueMarker(command_queue,	&event_after_d2h);
			if (error != CL_SUCCESS) {
        std::cerr <<  "Failed equeuing start event" << std::endl;
				exit(0);
			}
#endif
			clFinish(command_queue);

#ifdef OVERHEAD_STUDY
			//Adding a marker in the command queue to computer thread dispatch
			cl_event event_after_finish;
			error = clEnqueueMarker(command_queue,	&event_after_finish);
			if (error != CL_SUCCESS) {
        std::cerr <<  "Failed equeuing start event" << std::endl;
				exit(0);
			}
#endif
			end_tc = tick_count::now();

#ifdef OVERHEAD_STUDY

			//Calculating host to device transfer overhead
			clGetEventProfilingInfo(event_before_h2d, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &tg1, NULL);
			clGetEventProfilingInfo(event_before_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg2, NULL);
			if(tg2 > tg1){
				overhead_h2d = overhead_h2d + (tg2-tg1)/1000000.0; // ms
			}
			//cerr << "Overhead h2d: " << overhead_h2d << ": " << tg1 << ", " << tg2 << " = " << (tg2-tg1) <<endl;

			//Calculating kernel launch overheads
			clGetEventProfilingInfo(event_before_kernel, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &tg2, NULL);
			clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg3, NULL);
			if(tg3 > tg2){
				overhead_kl = overhead_kl + (tg3-tg2)/1000000.0; // ms
			}
			//Calculating kernel execution
			//clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg3, NULL);
			clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &tg4, NULL);
			if(tg4 > tg3){
				kernel_execution = kernel_execution + (tg4-tg3)/1000000.0; // ms
			}
			//Calculating device to host transfer overhead
			//clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_COMPLETE, sizeof(cl_ulong), &tg4, NULL);
			clGetEventProfilingInfo(event_after_d2h, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg5, NULL);
			if(tg5 > tg4){
				overhead_d2h = overhead_d2h + (tg5-tg4)/1000000.0; // ms
			}
			//Calculating device overhead thread dispatch
			if(tg5 > tg1){
				overhead_td = overhead_td +((end_tc - start_tc).seconds()*1000) - ((tg5-tg1)/1000000.0); // ms
			}

			//Releasing event objects
			clReleaseEvent(event_before_h2d);
			clReleaseEvent(event_before_kernel);
			clReleaseEvent(event_kernel);
			clReleaseEvent(event_after_kernel);
			clReleaseEvent(event_after_d2h);
			clReleaseEvent(event_after_finish);
#endif
			float time = (end_tc - start_tc).seconds()*1000;
			if(!concordCase){
				gpuThroughput = (bundle->end - bundle->begin) / time;
			}

#ifdef OUTPUT_FREQ_HISTOGRAM
				float th = (bundle->end - bundle->begin)/((end-start).seconds() * 1000);
				for (int i = bundle->begin; i < bundle->end;i++){
					histogram << bundle->end - bundle->begin << "\t" << th << std::endl;
				}
#endif
			if ((cpuThroughput > 0) && (!stopConditionModeOn)){
				fG = gpuThroughput/cpuThroughput;
			}
#ifdef DEBUG
			cerr << "Chunk GPU: " << bundle->end - bundle->begin << " TH: " << gpuThroughput /*((tg5-tg1)/1000000.0) */ << std::endl;
#endif
			//Summarizing GPU Statistics
			totalIterationsGPU = totalIterationsGPU + (bundle->end - bundle->begin);
			itemsOnGPU = itemsOnGPU + 1;

			if(explorationPhase){
				if(x[measurements-1] < (bundle->end - bundle->begin)){
					//Update the number of points
					y[measurements] = gpuThroughput;
					x[measurements] = (bundle->end - bundle->begin);

					//Checking for Stable condition
					if( (measurements > (numberPoints)) && (y[measurements]*0.95 < y[measurements-1]) && (y[measurements]*0.95 < y[measurements-2])){
						chunkGPU = calculateLogarithmicModel(x, y, numberPoints, measurements-1);
						explorationPhase = false;
						explotationPhase = true;
	#ifdef DEBUG
						cerr << "Mea: " << measurements << " x[mea-1]=" << x[measurements-2] << " chunkGPU: " << chunkGPU << std::endl;
						//exit(0);
	#endif
					}else{
						chunkGPU = chunkGPU * 2;
						if(chunkGPU >= iterationSpace){
							chunkGPU = iterationSpace;
							explorationPhase = false;
							explotationPhase = true;
							concordCase=true;
						}
					}
					measurements++;
				}
			}else if(explotationPhase && !stopConditionModeOn){
				//explotationPhase
				y[measurements] = gpuThroughput;
				x[measurements] = (bundle->end - bundle->begin);
				chunkGPU = calculateLogarithmicModel(x, y, numberPoints, measurements);  //Andres


			}
	}

	void executeOnCPU(Bundle *bundle){
		tick_count start = tick_count::now();
		body->OperatorCPU(bundle->begin, bundle->end);
		tick_count end = tick_count::now();
		if(!concordCase){
			cpuThroughput = (bundle->end - bundle->begin)/((end-start).seconds()*1000);
		}

#ifdef DEBUG_ALL
			cerr << "Chunk CPU: " << bundle->end - bundle->begin << " TH: " << cpuThroughput /*((tg5-tg1)/1000000.0) */ << std::endl;
#endif

		if (gpuThroughput > 0){
			fG = gpuThroughput/cpuThroughput;
		}
	}
};
//end class

template<typename T> class ROOTTask;

class LogFit : public Scheduler<LogFit, Params>{
	Params *pars;
public:
	/*This constructor just call his parent's contructor*/
	LogFit(Params *params) : Scheduler<LogFit, Params>(params){
		pars=params;

		//explorationPhase = true;
		//explotationPhase = false;
		
		explorationPhase = false;
		explotationPhase = true;
		concordCase = false;
		//minChunkCPU = 100;
		//minChunkGPU = computeUnits;
//		chunkCPU = 100;
//		chunkGPU = computeUnits;
		chunkGPU = FIXED_CHUNK;
		measurements = 0;
		fG = 1.0;
		gpuThroughput=0.0;
		cpuThroughput=0.0;
		numberPoints = 4;
		threshold = 0.0;

		//Initializing library PJTRACER
		initializePJTRACER();

#ifdef HOST_PRIORITY
		sprintf(pars->benchName, "%s_PRIO", pars->benchName);
#endif

#ifdef OVERHEAD_STUDY
		sprintf(pars->benchName, "%s_OVERHEAD", pars->benchName);
#endif
	}

	/*Initializes PJTRACER library*/
	void initializePJTRACER(){
#ifdef PJTRACER
		char traceFname[1024];
		sprintf(traceFname, "LOGFIT_C_%d_G_%d.trace", nCPUs, nGPUs);
		tracer = new PFORTRACER(traceFname);
		tracer->beginThreadTrace();
#endif
	}

public:
	/*The main function to be implemented*/
	template<class T>
	void heterogeneous_parallel_for(int begin, int end,  T *body) {
		gpuStatus = nGPUs;
		body->firsttime = true;
		if(!concordCase){
			pipeline pipe;
			MySerialFilter serial_filter(begin, end, nCPUs);
			MyParallelFilter<T> parallel_filter(body, end-begin);
			pipe.add_filter(serial_filter);
			pipe.add_filter(parallel_filter);

#ifdef OVERHEAD_STUDY
			end_tc = tick_count::now();
#endif
		/*Launch the pipeline*/
#ifdef DEBUG
			cerr << "Running Pipeline: " << nCPUs << " " << nGPUs << " " << gpuStatus << std::endl;
#endif
			pipe.run(nCPUs + nGPUs);
			pipe.clear();
		}else{
			int newbegin = begin;
			int newend=end;

#ifdef DEBUG
			cerr << "Execution phase::> begin: " << newbegin << " newend: " << newend << std::endl;
#endif
			tick_count start = tick_count::now();
			/*After the first pipeline finishes, The new ratios are computed*/
			if (nCPUs < 1) {
				//cerr << "Execution Phase: cpus " << nCPUs << " gpus active " <<endl;
				chunkGPU = newend - newbegin;
				chunkCPU = 0;

				body->sendObjectToGPU(newbegin, newend, NULL);
				body->OperatorGPU(newbegin, newend, NULL);
				body->getBackObjectFromGPU(newbegin, newend, NULL);
				clFinish(command_queue);

			} else if (nGPUs < 1) {
				chunkGPU = newend - newbegin;
				chunkCPU = 0;
				ParallelFORCPUs(newbegin, newend, body);

			} else {
				float gpuweight = (float)(newend - newbegin) * gpuThroughput/(gpuThroughput + cpuThroughput);
				chunkGPU = std::max((int)((gpuweight/(float)computeUnits))*computeUnits, computeUnits);
				chunkCPU = (newend - newbegin) - chunkGPU;
				//cerr << "ChunkGPU: " << chunkGPU << " chunkCPU: " << chunkCPU << std::endl;
				/*Creates a task that will create one task for GPU and one for CPU*/
				ROOTTask<T>& a = *new(task::allocate_root()) ROOTTask<T>(body, newbegin, newend);
				task::spawn_root_and_wait(a);
				//computedOnCPU = computedOnCPU + chunkCPU;
				//computedOnGPU = computedOnGPU + chunkGPU;
			}
			tick_count finish = tick_count::now();
			//gpuThroughput = chunkGPU/((finish-start).seconds()*1000);

#ifdef DEBUG
		cerr << "chunkGPU " << chunkGPU << " chunkCPU " << chunkCPU << std::endl;
		cerr << "GPUTH: " << gpuThroughput << std::endl;
#endif
		}

	}

	/*this function print info to a Log file*/
	void saveResultsForBench(){

		char * execution_name = (char *)malloc(sizeof(char)*50);
		sprintf(execution_name, "_LOGFIT_%d_%d.txt", nCPUs, nGPUs);
		strcat(pars->benchName, execution_name);

		/*Checking if the file already exists*/
		bool fileExists = isFile(pars->benchName);
    std::ofstream file(pars->benchName, std::ios::out | std::ios::app);
		if(!fileExists){
			printHeaderToFile(file);
		}


		//Save information to file
		file << nCPUs << SEP << nGPUs << SEP  << runtime << SEP
#ifdef OVERHEAD_STUDY
			<< kernel_execution << SEP << overhead_td << SEP << overhead_sp << SEP << overhead_h2d << SEP << overhead_kl << SEP << overhead_d2h << SEP
#endif
		<< getPP0ConsumedJoules(sstate1, sstate2) << SEP << getPP1ConsumedJoules(sstate1, sstate2) << SEP
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << SEP <<  getConsumedJoules(sstate1, sstate2) << SEP
#ifdef PMLIB
		<< energiaCPU << SEP << energiaFPGA << SEP
#endif
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << SEP << getL2CacheMisses(sktstate1[0], sktstate2[0]) << SEP << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) << SEP
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << SEP << getL3CacheMisses(sktstate1[0], sktstate2[0]) << SEP << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<SEP
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << SEP << totalIterationsGPU /* / itemsOnGPUi */ << SEP << itemsOnGPU << SEP
		<< std::endl;
		file.close();

		//Print information in console
#ifdef DEBUG
		cerr << nCPUs << SEP << nGPUs << SEP << runtime << SEP
#ifdef OVERHEAD_STUDY
		<< overhead_td << SEP << overhead_sp << SEP << overhead_h2d / 1000000.0 << SEP << overhead_kl << SEP << overhead_d2h << SEP
#endif
		<< getPP0ConsumedJoules(sstate1, sstate2) << SEP << getPP1ConsumedJoules(sstate1, sstate2) << SEP
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << SEP <<  getConsumedJoules(sstate1, sstate2) << SEP
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << SEP << getL2CacheMisses(sktstate1[0], sktstate2[0]) << SEP << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) << SEP
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << SEP << getL3CacheMisses(sktstate1[0], sktstate2[0]) << SEP << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) << SEP
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << SEP << totalIterationsGPU / itemsOnGPU << SEP << itemsOnGPU << SEP
		<< std::endl;
#endif
	}

	void printHeaderToFile(std::ofstream &file){
		file << "N. CPUs" << SEP << "N. GPUs" << SEP << "Time (ms)" << SEP

#ifdef OVERHEAD_STUDY
		 << "Kernel Execution" << SEP << "O. Th. Dispatch" << SEP << "O. sched. Partitioning" << SEP << "O. Host2Device" << SEP << "O. Kernel Launch" << SEP << "O. Device2Host" << SEP
#endif
		<< "CPU Energy(J)" << SEP << "GPU Enegy(J)" << SEP << "Uncore Energy(J)" << SEP << "Total Energy (J)" << SEP
#ifdef PMLIB
		<< "CPU Energy (BBB)" << SEP << "FPGA Energy (BBB) " << SEP
#endif
		<< "L2 Cache Hits" << SEP << "L2 Cache Misses" << SEP << "L2 Cache Hit Ratio" << SEP
		<< "L3 Cache Hits" << SEP << "L3 Cache Misses" << SEP << "L3 Cache Hit Ratio" << SEP << "Cycles lost Due to L3 Misses" << SEP << "Total GPU chunkSize" << SEP << "items GPU" << SEP
		<< std::endl;
	}
};
