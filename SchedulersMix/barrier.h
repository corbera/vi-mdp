//============================================================================
// Name			: barrier.h
// Author		: Francisco Corbera
// Version		: 1.0
// Date			: 30 / 12 / 2014
// Copyright	: Department. Computer's Architecture (c)
// Description	: Implementation of barriers
//============================================================================
#ifndef Win32
	#include <mutex>

	// Locks for the barrier
	std::mutex arrive, depart;
	int barrier_count;

	void barrier_init()
	{
		barrier_count=0;
		//cerr << "antes de lock" << endl;
		depart.lock();
		//cerr << "despues de lock" << endl;
	}

	void barrier(int n)
	{
		arrive.lock();
		barrier_count++;
		if(barrier_count<n) arrive.unlock();
		else depart.unlock();

		depart.lock();
		barrier_count--;
		if(barrier_count>0) depart.unlock();
		else arrive.unlock();
	}
#else
#include <windows.h>

	CRITICAL_SECTION gpulock, cpulock;

	void barrier_init() {
		InitializeCriticalSection( &gpulock );
		InitializeCriticalSection( &cpulock );
	}

	void getLockCPU() {
		EnterCriticalSection( &cpulock );
	}

	void getLockGPU() {
		EnterCriticalSection( &gpulock );
	}

	void freeLockCPU() {
		LeaveCriticalSection( &cpulock );
	}

	void freeLockGPU() {
		LeaveCriticalSection( &gpulock );
	}
#endif