//============================================================================
// Name			: Concord.h
// Author		: Antonio Vilches
// Version		: 1.0
// Date			: 05 / 01 / 2014
// Copyright	: Department. Computer's Architecture (c)
// Description	: Concord scheduler implementation
//============================================================================

#define USEBARRIER

#include <cstdlib>
#include <iostream>
#include <fstream>
#include "Scheduler.h"
#include "tbb/pipeline.h"
#include "tbb/tick_count.h"
#include "tbb/task.h"

#ifdef Win32
#include "PCM_Win/windriver.h"
#else
#include "cpucounters.h"
#endif

#ifdef PJTRACER
#include "pfortrace.h"
#endif

using namespace tbb;
using namespace std;

/*****************************************************************************
 * Defines
 * **************************************************************************/
#define CPU 0
#define GPU 1
#define SEP "\t"

/*****************************************************************************
 * types
 * **************************************************************************/
typedef struct{
	int numcpus;
	int numgpus;
	int gpuChunk;
	char benchName[100];
	char kernelName[100];
}Params;

/*****************************************************************************
 * Global variables
 * **************************************************************************/

atomic<int> gpuStatus;
int chunkGPU;
int chunkCPU;
bool profilingPhase;
bool gpuCompleted;

float cpuThroughput;
float gpuThroughput;
float fG;

atomic<int> computedOnCPU, computedOnGPU;
atomic<int>totalIterations;

/*****************************************************************************
 * Heterogeneous Scheduler
 * **************************************************************************/
/*Bundle class: This class is used to store the information that items need while walking throught pipeline's stages.*/
class Bundle {
public:
	int begin;
	int end;
	int type; //GPU = 0, CPU=1

	Bundle() {};
};

/*My serial filter class represents the partitioner of the engine. This class selects a device and a rubrange of iterations*/
class MySerialProfilingFilter: public filter {
public:
	int begin;
	int end;
	MySerialProfilingFilter(int b, int e) : filter(true), begin(b), end(e) {	}

	void * operator()(void *) {

		Bundle *bundle = new Bundle();
		if(!gpuCompleted){
			if (begin < end) { //If there are remaining iterations
				if ( --gpuStatus >= 0 ){//This is a task for GPU
					if((end-begin)>chunkGPU){
						bundle->begin = begin;
						bundle->end = begin + chunkGPU;
						begin = begin + chunkGPU;
						bundle->type = GPU;
						return bundle;
					}else{
						bundle->begin = begin;
						bundle->end = end;
						begin = end;
						bundle->type = GPU;
						return bundle;
					}
				}else{//This is a task for CPU
					gpuStatus++;
					if((end-begin)>chunkCPU){
						bundle->begin = begin;
						bundle->end = begin + chunkCPU;
						begin = begin + chunkCPU;
						bundle->type = CPU;
						return bundle;
					}else{
						bundle->begin = begin;
						bundle->end = end;
						begin = end;
						bundle->type = CPU;
						return bundle;
					}
				}//end cpu work
			}//end iteration space
		}//end GPU profiling => clean the pipeline
		return NULL;
	} // end operator
};

/*MyParallelFilter class is the executor component of the engine, it executes the subrange onto the device selected by SerialFilter*/
template <class B>
class MyParallelProfilingFilter: public filter {
private:
	B *body;

public:
	MyParallelProfilingFilter(B *b) : filter(false) {
		body = b;
	}
	void * operator()(void * item) {

		//variables
		Bundle *bundle = (Bundle*) item;
		tick_count t0, t1;

		if(bundle->type == GPU){
			// GPU WORK

#ifdef PJTRACER
			tracer->gpuStart();
#endif
			tick_count start = tick_count::now();
			body->sendObjectToGPU(bundle->begin, bundle->end, NULL);
			body->OperatorGPU(bundle->begin, bundle->end, NULL);
			body->getBackObjectFromGPU(bundle->begin, bundle->end, NULL);
			clFinish(command_queue);
			tick_count end = tick_count::now();
#ifdef PJTRACER
			tracer->gpuStop();
#endif
			gpuThroughput = ((bundle->end-bundle->begin) / ((end-start).seconds() * 1000));
			//Activating this flag, just to ramp down the pipeline
			gpuCompleted = true;

			computedOnGPU = computedOnGPU + (bundle->end-bundle->begin);
			gpuStatus++;
		}else{
			// CPU WORK
#ifdef PJTRACER
			tracer->cpuStart();
#endif
			tick_count start = tick_count::now();
			body->OperatorCPU(bundle->begin, bundle->end);
			tick_count end = tick_count::now();
#ifdef PJTRACER
			tracer->cpuStop();
#endif
			cpuThroughput = (bundle->end - bundle->begin)/((end-start).seconds()*1000);

			computedOnCPU = computedOnCPU + (bundle->end-bundle->begin);
		}
  		delete bundle;
		return NULL;
	}
};

class Concord : public Scheduler<Concord>{
	Params *pars;
public:
	/*This constructor just call his parent's contructor*/
	Concord(void *params) : Scheduler(params){
		Params * p = (Params*) params;

		pars=p;
		profilingPhase = true;
		gpuCompleted = false;
		chunkCPU = 100;
		chunkGPU = computeUnits * 7 * 16;
		fG = 0.0;
		gpuThroughput=0.0;
		cpuThroughput=0.0;
		computedOnCPU=0;
		computedOnGPU=0;
		totalIterations=0;

		//Initializing library PJTRACER
		initializePJTRACER();
	}

	/*Initializes PJTRACER library*/
	void initializePJTRACER(){
#ifdef PJTRACER
		char traceFname[1024];
		sprintf(traceFname, "CONCORD_C_%d_G_%d.trace", nCPUs, nGPUs);
		tracer = new PFORTRACER(traceFname);
		tracer->beginThreadTrace();
#endif
	}

	/*The main function to be implemented*/
	template<class T>
	void heterogeneous_parallel_for(int begin, int end, T* body){
#ifndef NDEBUG
		cerr << "Heterogeneous Parallel For Concord " << nCPUs << " , " << nGPUs << ", " << chunkGPU << endl;
		cerr << "Begin: " << begin << " end: " << end << endl;
#endif
		gpuStatus = nGPUs;
		int newbegin = begin;
		int newend = end;
		body->firsttime = true;

		/*Seeting a mark to recognize a timestep*/
#ifdef PJTRACER
		tracer->newEvent();
#endif

		/*In this scheduler we have implemented two pipelines, the former (just the one below) performs the profiling phase. This phase should assing only one block to the GPU and several blocks to the CPU threads,
		this pipeline is active while the GPU is computing the first block, once the GPU finish it, the flag profilingPhase is set to false. Then no new tokens start computation, so the pipeline is cleaned up.
		*/
		if(profilingPhase && (nGPUs > 0) && (nCPUs>0)){
#ifndef NDEBUG
		cerr << "Profiling phase " << endl;
#endif
			pipeline pipe;
			MySerialProfilingFilter filterSerialProfiling(begin, end);
			MyParallelProfilingFilter<T> filterParallelProfiling(body);
			pipe.add_filter(filterSerialProfiling);
			pipe.add_filter(filterParallelProfiling);

			/*Launching the pipeline*/
			pipe.run(nCPUs + nGPUs);
			pipe.clear();

			/*Updating the begin iteration*/
			profilingPhase = false;
			newbegin = filterSerialProfiling.begin;
			cpuThroughput= cpuThroughput * nCPUs;
		}

		if (newend != newbegin) {
#ifndef NDEBUG
		cerr << "Execution phase::> begin: " << newbegin << " newend: " << newend << endl;
#endif
		tick_count start = tick_count::now();
			/*After the first pipeline finishes, The new ratios are computed*/
			if (nCPUs < 1) {
				//cerr << "Execution Phase: cpus " << nCPUs << " gpus active " <<endl;
				chunkGPU = newend - newbegin;
				chunkCPU = 0;

				body->sendObjectToGPU(newbegin, newend, NULL);
				body->OperatorGPU(newbegin, newend, NULL);
				body->getBackObjectFromGPU(newbegin, newend, NULL);
				clFinish(command_queue);

			} else if (nGPUs < 1) {
				chunkGPU = newend - newbegin;
				chunkCPU = 0;
				ParallelFORCPUs(newbegin, newend, body);

			} else {
				float gpuweight = (float)(newend - newbegin) * gpuThroughput/(gpuThroughput + cpuThroughput);
				chunkGPU = max((int)((gpuweight/(float)computeUnits))*computeUnits, computeUnits);
				chunkCPU = (newend - newbegin) - chunkGPU;
				//cerr << "ChunkGPU: " << chunkGPU << " chunkCPU: " << chunkCPU << endl;
				/*Creates a task that will create one task for GPU and one for CPU*/
				ROOTTask<T>& a = *new(task::allocate_root()) ROOTTask<T>(body, newbegin, newend);
				task::spawn_root_and_wait(a);
				computedOnCPU = computedOnCPU + chunkCPU;
				computedOnGPU = computedOnGPU + chunkGPU;
			}
			tick_count finish = tick_count::now();
			//gpuThroughput = chunkGPU/((finish-start).seconds()*1000);

#ifndef NDEBUG
		cerr << "chunkGPU " << chunkGPU << " chunkCPU " << chunkCPU << endl;
		cerr << "GPUTH: " << gpuThroughput << endl;
#endif


		}
	}

	/*this function print info to a Log file*/
	void saveResultsForBench(){

		char * execution_name = (char *)malloc(sizeof(char)*50);
		sprintf(execution_name, "_Concord_%d_%d.txt", nCPUs, nGPUs);
		strcat(pars->benchName, execution_name);

		/*Checking if the file already exists*/
		bool fileExists = isFile(pars->benchName);
		ofstream file(pars->benchName, ios::out | ios::app);
		if(!fileExists){
			printHeaderToFile(file);
		}
		file << nCPUs << "\t" << nGPUs << "\t"  << runtime << "\t"
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
		file.close();
#ifndef NDEBUG
		cerr << nCPUs << "\t" << nGPUs << "\t" << runtime << "\t"
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
#endif
	}

	void printHeaderToFile(ofstream &file){
		file << "N. CPUs" << SEP << "N. GPUs" << SEP << "Time (ms)" << SEP
		<< "CPU Energy(J)" << SEP << "GPU Enegy(J)" << SEP << "Uncore Energy(J)" << SEP << "Total Energy (J)" << SEP
		<< "L2 Cache Hits" << SEP << "L2 Cache Misses" << SEP << "L2 Cache Hit Ratio" << SEP
		<< "L3 Cache Hits" << SEP << "L3 Cache Misses" << SEP << "L3 Cache Hit Ratio" << SEP << "Cycles lost Due to L3 Misses" << endl;
	}
};
