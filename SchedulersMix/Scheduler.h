//============================================================================
// Name			: Scheduler.h
// Author		: Antonio Vilches
// Version		: 1.0
// Date			: 26 / 12 / 2014
// Copyright	: Department. Computer's Architecture (c)
// Description	: Main scheduler interface class
//============================================================================
#define useGPU
//#define FPGA  //(altera)
//#define PMLIB
//#define ODROID // for using energy-meter


#define ENERGYCOUNTERS

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

#include <unistd.h>	

#if defined(__APPLE__)
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#ifdef Win32
#include "PCM_Win/windriver.h"
//#else
//#include "cpucounters.h"
#endif

#include "tbb/task_scheduler_init.h"
#include "tbb/tick_count.h"

#ifdef PJTRACER
#include "pfortrace.h"
#endif

#include "AOCLUtils/opencl.h"



#ifdef ODROID
/*****************************************************************************
 * Energy for Odroid
 * **************************************************************************/
#include "../energy-meter/energy_meter.h"
#include "thread_funcs.cpp"
#endif 

#ifdef PMLIB
// Adding energy using BBB 8/2/2016

#include "pmlib.h" /* PMLIB headers */



// function to read energy from pmlib output file

int integra_potencia(char *filename, int debug, float *ECPU, float *EFPGA) {
  char cadena[1025];
  float potencia[8];
  float energiaCPU, energiaFPGA;
  float tiempo1, tiempo2;
  FILE *f = fopen(filename,"r");
  int i=0, res;

  if(!f) return -1;

  fgets(cadena, 1024, f);

  tiempo1=0.0f;
  energiaCPU=0.0f;
  energiaFPGA=0.0f;

  while(!feof(f))
  {
   res= fscanf(f," %*d;%f;%f;%f;%f;%f;%f;%f;%f;%f;%*f ", &tiempo2, potencia, potencia+1, potencia+2, potencia+3, potencia+4, potencia+5, potencia+6, potencia+7);
   if(debug)   fprintf(stderr, "#%05d Tiempo: %f, elapsed: %f, energía CPU: %f, energía FPGA: %f\n",i,tiempo2,tiempo2-tiempo1,energiaCPU, energiaFPGA);
   if (res!=9) return -1;
   energiaCPU+=(tiempo2-tiempo1)*(potencia[0]+potencia[1]+potencia[2]+potencia[3]);
   energiaFPGA+=(tiempo2-tiempo1)*(potencia[4]+potencia[5]+potencia[6]+potencia[7]);

   tiempo1=tiempo2;
   i++;
  }
  if(debug)
	{
	  fprintf(stderr,"\nTOTAL CPU: %f J\n",energiaCPU/1000.0f);
	  fprintf(stderr,"\nTOTAL FPGA: %f J\n\n",energiaFPGA/1000.0f);
	}
  *ECPU  = energiaCPU/1000.0f;
  *EFPGA = energiaFPGA/1000.0f;
  return  0;
}

#endif

// using namespace std;
using namespace tbb;

/*****************************************************************************
 * Global Variables For OpenCL
 * **************************************************************************/

cl_int error;
cl_uint num_max_platforms;
cl_uint num_max_devices;
cl_uint num_platforms;
cl_uint num_devices;
cl_platform_id platforms_id;
cl_device_id device_id;
cl_context context;
cl_command_queue command_queue;

cl_program program;
cl_kernel kernel;
#ifdef CHANNELS
cl_command_queue command_queue2;
cl_kernel kernel_control;
#endif
int computeUnits;

int main_thread=0; // andres

//profiler
#ifdef PJTRACER
PFORTRACER * tracer;
#endif

/*****************************************************************************
 * OpenCL fucntions
 * **************************************************************************/

char *ReadSources(const char *fileName){
// #ifdef __linux__
//     std::string tmp = wstringToString(fileName);
//    FILE *file = fopen(tmp.c_str(), "rb");
//    if (!file)
//    {
//        printf("ERROR: Failed to open file '%s'\n", tmp.c_str());
//        return NULL;
//    }
// #else
    FILE *file = fopen(fileName, "rb");
    if (!file)
    {
        printf("ERROR: Failed to open file '%ls'\n", fileName);
        return NULL;
    }
// #endif

    if (fseek(file, 0, SEEK_END))
    {
        printf("ERROR: Failed to seek file '%ls'\n", fileName);
        fclose(file);
        return NULL;
    }

    long size = ftell(file);
    if (size == 0)
    {
        printf("ERROR: Failed to check position on file '%ls'\n", fileName);
        fclose(file);
        return NULL;
    }

    rewind(file);

    char *src = (char *)malloc(sizeof(char) * size + 1);
    if (!src)
    {
        printf("ERROR: Failed to allocate memory for file '%ls'\n", fileName);
        fclose(file);
        return NULL;
    }

    printf("Reading file: %s (size %d bytes)\n", fileName, size);
    size_t res = fread(src, 1, sizeof(char) * size, file);
    if (res != sizeof(char) * size)
    {
        printf("ERROR: Failed to read file '%ls'\n", fileName);
        fclose(file);
        free(src);
        return NULL;
    }
	/* NULL terminated */
    src[size] = '\0';
    fclose(file);

    return src;
}

void createCommandQueue() {
	num_max_platforms = 1;
	error = clGetPlatformIDs(num_max_platforms, &platforms_id, &num_platforms);
	if (error != CL_SUCCESS) {
		fprintf(stderr, "No platforms were found.\n");
		exit(0);
	}
	num_max_devices = 1;
#ifdef FPGA
	error = clGetDeviceIDs(platforms_id, CL_DEVICE_TYPE_ACCELERATOR, num_max_devices, &device_id, &num_devices);
#endif

#ifdef useGPU
	error = clGetDeviceIDs(platforms_id, CL_DEVICE_TYPE_GPU, num_max_devices, &device_id, &num_devices);
#endif

  if (error != CL_SUCCESS) {
		fprintf(stderr, "No accelerator devices were found\n");
		exit(0);
	}
	error = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &computeUnits, NULL);
	if (error != CL_SUCCESS) {
		fprintf(stderr, "No max compute units  were found");
		exit(0);
	}

	char device_name[50];
	error = clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof(char)*50, &device_name, NULL);
	if (error != CL_SUCCESS) {
		fprintf(stderr, "No device name were found");
		exit(0);
	}

  std::cerr << "Accelerator's name: " << device_name << " with "<< computeUnits << " computes Units" << std::endl;
	num_devices=1;
	context = clCreateContext(NULL, num_devices, &device_id, NULL, NULL,
			&error);
	if (error != CL_SUCCESS) {
		fprintf(stderr, "Context couldn't be created");
		exit(0);
	}

#ifdef OVERHEAD_STUDY
//cl_queue_properties properties[] = {};
	command_queue = clCreateCommandQueue/*WithProperties*/(context, device_id, CL_QUEUE_PROFILING_ENABLE, &error);
#else
	//cl_queue_properties properties[] = {};
	command_queue = clCreateCommandQueue/*WithProperties*/(context, device_id, NULL, &error);
#ifdef CHANNELS        
	command_queue2 = clCreateCommandQueue/*WithProperties*/(context, device_id, NULL, &error);
#endif
#endif
	if (error != CL_SUCCESS) {
		fprintf(stderr, "Command Queue couldn't be created");
		exit(0);
	}
}

void CreateAndCompileProgram(char * kernelName) {
#ifndef NDEBUG
  std::cerr << "Reading FILE" << std::endl;
#endif
	char * programSource = ReadSources("kernel.cl");
#ifndef NDEBUG
  std::cerr << "End Reading FILE" << std::endl;
#endif
//	char *textPolicyEvaluationKernel = read_file("kernel.cl"/*"./src/valueIterationKernel.cl"*/);
//	kernel = kernel_from_string(context, textPolicyEvaluationKernel, kname, NULL);
//	free(textPolicyEvaluationKernel);

	// Create a program using clCreateProgramWithSource()
	program = clCreateProgramWithSource(context, 1,
			(const char**) &programSource, NULL, &error);
	if (error != CL_SUCCESS) {
		fprintf(stderr, "Failed creating programm with source!\n");
		exit(0);
	}
	// Build (compile) the program for the devices with // clBuildProgram()
	error = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
	if (error != CL_SUCCESS) {
		fprintf(stderr, "Failed Building Program!\n");
		char message[16384];
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG,
				sizeof(message), message, NULL);
		fprintf(stderr,"Message Error:\n %s\n",message);
		exit(0);
	}
	// Use clCreateKernel() to create a kernel from the
	// vector addition function (named "vecadd")
	kernel = clCreateKernel(program, kernelName, &error);
	if (error != CL_SUCCESS) {
		fprintf(stderr, "Failed Creating kernel!\n");
		exit(0);
	}

}

// Read an already compiled kernel
// Used with Altera FPGA platform
#ifdef FPGA
void
CreateAndLoadBinaryProgram(char * kernelName) {

#ifndef NDEBUG
  std::cerr << "Reading BINARY FILE" << std::endl;
#endif

  auto binary_file = aocl_utils::getBoardBinaryFile("kernel", device_id);
  std::cerr << "Using AOCX: " << binary_file << std::endl;

#ifndef NDEBUG
  std::cerr << "End Reading BINARY FILE" << std::endl;
#endif

  auto program = aocl_utils::createProgramFromBinary(context, binary_file.c_str(), &device_id, 1);

	// Build (compile) the program for the devices with // clBuildProgram()
	error = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	if (error != CL_SUCCESS) {
		fprintf(stderr, "Failed Building Program!\n");
		char message[16384];
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG,
				sizeof(message), message, NULL);
		fprintf(stderr,"Message Error:\n %s\n",message);
		exit(0);
	}
	// Use clCreateKernel() to create a kernel from the
	// vector addition function (named "vecadd")
	kernel = clCreateKernel(program, kernelName, &error);
	if (error != CL_SUCCESS) {
		fprintf(stderr, "Failed Creating kernel!\n");
		exit(0);
	}


#ifdef CHANNELS
	kernel_control = clCreateKernel(program, "launcher", &error);
	if (error != CL_SUCCESS) {
		fprintf(stderr, "Failed Creating kernel!\n");
		exit(0);
	}

#endif
}

#endif

/*****************************************************************************
 * Base Scheduler class
 * **************************************************************************/
/*This Scheduler Base class implementation follows a Singleton pattern*/
template <typename T, typename PARAMS >
class Scheduler{
protected:
// Class members
	//Scheduler Itself
	static T *instance;
	task_scheduler_init *init;
	int nCPUs;
	int nGPUs;
	
#ifdef ODROID
struct energy_sample *sample1;
struct em_t final_em; // to get final energies
#endif

#ifdef PMLIB  
	//Energy Counters
	float energiaCPU, energiaFPGA;   // BBB varaibles to print results to output file (will be used in the scheduler header file)
	PCM * pcm;
  std::vector<CoreCounterState> cstates1, cstates2;
  std::vector<SocketCounterState> sktstate1, sktstate2;
  SystemCounterState sstate1, sstate2;
#endif  
	//timing
	tick_count start, end;
	float runtime;

#ifdef PMLIB
  // Some variables to use pmlib.
  server_t servidor;
  counter_t contador;
  line_t lineas;

  int frequency;
  int aggregate;
#endif
//End class members

	/*Scheduler Constructor, forbidden access to this constructor from outside*/
	Scheduler(PARAMS * params) {
#ifdef PMLIB    
    frequency=500;
		aggregate=1;
#endif    
		nCPUs = params->numcpus;
		nGPUs = params->numgpus;
#ifndef NDEBUG
    std::cerr << "TBB scheduler is active " << "(" << nCPUs << ", " << nGPUs << ")" << std::endl;
#endif
		init = new task_scheduler_init(nCPUs + nGPUs);
#ifndef NDEBUG
   std::cerr << "INITIALIZING OPENCL" << std::endl;
#endif
		initializeOPENCL(params->kernelName);
#ifndef NDEBUG
    std::cerr << "INITIALIZING pcm" << std::endl;
#endif
#ifdef ENERGYCOUNTERS
		initializePCM();
#endif
#ifndef NDEBUG
    std::cerr << "INITIALIZING HOSTPRIORITY" << std::endl;
#endif
		initializeHOSTPRI();
		runtime = 0.0;
	}

	/*Initialize OpenCL environment*/
	void initializeOPENCL(char * kernelName){
		createCommandQueue();
    // Used with GPU
    #ifdef useGPU
		 CreateAndCompileProgram(kernelName);
#endif
#ifdef FPGA
    // Used with FPGA
		CreateAndLoadBinaryProgram(kernelName);
		#endif

#ifndef NDEBUG
    const size_t STRING_BUFFER_LEN = 1024;
    char char_buffer[STRING_BUFFER_LEN];
    std::cerr << "Querying platform for info:" << std::endl << "==========================" << std::endl;
    clGetPlatformInfo(platforms_id, CL_PLATFORM_NAME, STRING_BUFFER_LEN, char_buffer, NULL);
    std::cerr << "CL_PLATFORM_NAME = " << char_buffer << std::endl;
    clGetPlatformInfo(platforms_id, CL_PLATFORM_VENDOR, STRING_BUFFER_LEN, char_buffer, NULL);
    std::cerr << "CL_PLATFORM_VENDOR = " << char_buffer << std::endl;
    clGetPlatformInfo(platforms_id, CL_PLATFORM_VERSION, STRING_BUFFER_LEN, char_buffer, NULL);
    std::cerr << "CL_PLATFORM_VERSION = " << char_buffer << std::endl;
#endif
	}

	void initializeHOSTPRI(){
// FIXME: Check if setting the priority of the thread also helps in linux
#if defined(__WIN32)
#ifdef HOST_PRIORITY  // rise GPU host-thread priority
		unsigned long dwError, dwThreadPri;
		//dwThreadPri = GetThreadPriority(GetCurrentThread());
		//printf("Current thread priority is 0x%x\n", dwThreadPri);
		if(!SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL)){
			dwError = GetLastError();
			if(dwError){
				cerr << "Failed to set hight priority to host thread (" << dwError << ")" << endl;
			}
		}
	//dwThreadPri = GetThreadPriority(GetCurrentThread());
	//printf("Current thread priority is 0x%x\n", dwThreadPri);
#endif
#endif // defined(__WIN32)
	}

public:
	/*Class destructor*/
	~Scheduler(){
		init->~task_scheduler_init();
		delete instance;
		instance = NULL;
	}

	/*This function creates only one instance per process, if you want a thread safe behavior protect the if clausule with a Lock*/
	static T * getInstance(PARAMS * params){
		if(! instance){
			instance = new T(params);
		}
		return instance;
	}

	/*Initializing PCM library*/
	void initializePCM(){
		/*This function prints lot of information*/
		
/************************************
 * ENERGY ODROID
 * ***********************/
 
std::cout << "Main thread id: " << (main_thread=syscall(__NR_gettid)) << std::endl;
	std::cout << "          core: " << sched_getcpu() << std::endl;

#ifdef ODROID
    sample1=energy_meter_init(100, 0  /*0=no debug*/);  // sample period 100 miliseconds
    std::cout << "Set energy sampler thread affinity to A7s... return= " << set_thread_affinity_A7(sample1->th_meter) << std::endl; // processor 0123 = A7  4567=A15
   // later on... energy_meter_start(sample1);  // starts sampling thread
#endif
#ifdef PMLIB    
		pcm = PCM::getInstance();
		pcm->resetPMU();
		if (pcm->program() != PCM::Success){
      std::cerr << "Error in PCM library initialization" << std::endl;
			exit(-1);
		}

// Adding energy using BBB 8/2/2016

  device_t disp;

  // Support variables.
  char **lista;
  int num_devices;
  char name[20];

  int i = 0;

  // Clear all lines.
  LINE_CLR_ALL(&lineas);

  // Set lines of interest (8 INA219).
  LINE_SET( 0, &lineas );
  LINE_SET( 1, &lineas );
  LINE_SET( 2, &lineas );
  LINE_SET( 3, &lineas );
  LINE_SET( 4, &lineas );
  LINE_SET( 5, &lineas );
  LINE_SET( 6, &lineas );
  LINE_SET( 7, &lineas );

  printf("\n");
  printf("[PMLIB] =========================\n");

  // Connect to server (BeagleBone Black).
  printf("[PMLIB] pm_set_server 192.168.7.2\n");
  pm_set_server((char*)"192.168.7.2", 6526, &servidor);

  // Get some information about available devices.
  printf("[PMLIB] pm_get_devices...\n");
  pm_get_devices(servidor, &lista, &num_devices);

  printf("[PMLIB] Number of devices detected: %d\n", num_devices);
  for(i=0; i<num_devices; i++) {
    printf("[PMLIB] pm_get_device_info...\n");
    pm_get_device_info(servidor, lista[i], &disp);
    printf("[PMLIB]  Name: %s\n", disp.name);
    printf("[PMLIB]  Max. frequency: %d\n", disp.max_frecuency);
    printf("[PMLIB]  Available lines: %d\n", disp.n_lines);
  }
  printf("[PMLIB] =========================\n");
  printf("\n");
#endif


	}

	/*Sets the start mark of energy and time*/
	void startTimeAndEnergy(){
#ifdef ODROID
     energy_meter_start(sample1);  // starts sampling thread
#endif
#ifdef PMLIB
		pcm->getAllCounterStates(sstate1, sktstate1, cstates1);

  // Create a counter.

  printf("[PMLIB] pm_create_counter...\n");
  pm_create_counter((char*)"INA219Device", lineas, !aggregate, frequency, servidor, &contador);

  // Start counter.
  printf("[PMLIB] pm_start_counter...\n");
  pm_start_counter(&contador);
#endif
		start = tick_count::now();
	}

	/*Sets the end mark of energy and time*/
	void endTimeAndEnergy(){
		end = tick_count::now();
		
   /******
   * Energy stop
   * ****/
#ifdef ODROID
    energy_meter_read(sample1,&final_em);  // final & total
    energy_meter_stop(sample1);  	// stops sampling
    energy_meter_printf(sample1, stdout);  // print total results
        // cout<< "Total energy: "<< sample1->A15 + sample1->A7 + sample1->GPU + sample1->MEM <<endl;
    //energy_meter_destroy(sample1);     // clean up everything
#endif
#ifdef PMLIB
		pcm->getAllCounterStates(sstate2, sktstate2, cstates2);

  // Stop counter.
  printf("[PMLIB] pm_stop_counter...\n");
  pm_stop_counter(&contador);

  // Get data.
  char profile_filename[256];
  pm_get_counter_data(&contador);
  sprintf( profile_filename, "profile.out" );
  printf("[PMLIB] Saving power data to %s...\n", profile_filename);
  // Print data as CSV.
  pm_get_counter_data(&contador);
  pm_print_data_csv( profile_filename, contador, lineas, -1);

  // Finalize counter.
  printf("[PMLIB] pm_finalize_counter...\n");
  pm_finalize_counter(&contador);

  integra_potencia((char*)"profile.out", 0, &energiaCPU, &energiaFPGA);
  printf("[PMLIB] CPU: %f J, FPGA: %f J\n", energiaCPU, energiaFPGA);

#endif
		runtime = (end-start).seconds()*1000;
	}

	/*Checks if a File already exists*/
	bool isFile(char *filename){
		//open file
 FILE *file = fopen(filename, "rb");
    if (!file)
    {
        printf("ERROR: Failed to open file '%ls'\n", filename);
        return false;
    }
   fclose(file);
return true;
	}
};

void cleanup() 
{
        if (kernel) {
                clReleaseKernel(kernel);
        }
        if (command_queue) {
                clReleaseCommandQueue(command_queue);
        }
        
        if (program) {
                clReleaseProgram(program);
        }
        if (context) {
                clReleaseContext(context);
        }
}

template <typename T, typename PARAMS>
T* Scheduler<T, PARAMS>::instance = NULL;

