//============================================================================
//============================================================================
// Name			: Dynamic.h
// Author		: Antonio Vilches
// Version		: 1.0
// Date			: 02 / 01 / 2014
// Copyright	: Department. Computer's Architecture (c)
// Description	: Dynamic scheduler implementation
//============================================================================

#include <cstdlib>
#include <atomic>
#include <iostream>
#include <fstream>
#include "Scheduler.h"
#include "tbb/pipeline.h"
#include "tbb/tick_count.h"

#ifdef Win32
#include "PCM_Win/windriver.h"
//#else
//#include "cpucounters.h"
#endif

#ifdef PJTRACER
#include "pfortrace.h"
#endif

#define  OVERHEAD_ANALYSIS
#define HOSTA7

using namespace std;
using namespace tbb;

/*****************************************************************************
 * Defines
 * **************************************************************************/
#define CPU 0
#define isGPU 1
#define GPU_OFF -100 //Arbitrary value
#define SEP "\t"

/*****************************************************************************
 * types
 * **************************************************************************/
typedef struct{
	int numcpus;
	int numgpus;
	int gpuChunk;
	char benchName[100];
	char kernelName[100];
}Params;

/*****************************************************************************
 * Global variables
 * **************************************************************************/

//atomic<int> firsttime_A15; //andres
std::atomic<int> gpuStatus;
std::atomic<int> chunkGPU;
std::atomic<float> gpuThroughput;
__thread int chunkCPU;
__thread int mythid=0;
__thread int myclass; // 0 = GPU; 1=CPU
__thread float cpuThroughput=0.0f;
__thread float fG;
int hpfor_main_thread=0;

//int cores[8]={4,5,6,7,0,1,2,3};
//Thread_pinning pinner (8, cores);  // For auto pinning: 8 cores. A15s go first, then A7s

int order[8]={0,0,0,0,1,1,1,1};
int cluster0[4]={4,5,6,7};
int cluster1[4]={0,1,2,3};

Thread_pinning pinner (8, order, cluster0, cluster1);  // For auto pinning: 8 cores. A15s go first, then A7s


//float fG;
//int numCPUs;



/*****************************************************************************
 * Global variables
 * **************************************************************************/

//int chunkCPU;
//int chunkGPU;

//float gpuThroughput, cpuThroughput;
// To calculate scheduling partition overhead
tick_count end_tc;

#ifdef DEEP_CPU_REPORT
ofstream deep_cpu_report;
#endif

#ifdef DEEP_GPU_REPORT
ofstream deep_gpu_report;
#endif

#ifdef OVERHEAD_ANALYSIS
// overhead accumulators
double overhead_sp = 0.0;
double overhead_h2d = 0.0;
double overhead_kl = 0.0;
double kernel_execution = 0.0;
double overhead_d2h = 0.0;
double overhead_td = 0.0;
#endif


/*****************************************************************************
 * Heterogeneous Scheduler
 * **************************************************************************/
/*Bundle class: This class is used to store the information that items need while walking throught pipeline's stages.*/
class Bundle {
public:
	int begin;
	int end;
	int type; //GPU = 0, CPU=1

	Bundle() {};
};

/*My serial filter class represents the partitioner of the engine. This class selects a device and a rubrange of iterations*/
class MySerialFilter: public filter {
private:
	int begin;
	int end;
	int nCPUs;
	int nGPUs;
public:
	/*Class constructor, it only needs the first and last iteration indexes.*/
	MySerialFilter(int b, int e, int ncpus, int ngpus) : filter(true) {
		begin = b;
		end = e;
		nCPUs = ncpus;
		nGPUs = ngpus;
	}

	/*Mandatory operator method, TBB rules*/
	void * operator()(void *) {
		Bundle *bundle = new Bundle();

		/*If there are remaining iterations*/
		//~ printf("My class: %d, remaining: %d, %d\n", myclass, begin, end);
		if (begin < end) {
			 if(mythid==0)
            {
                mythid = syscall(__NR_gettid);

/*
                display_thread_prio("INITIAL THREAD SETTINGS.....");
                std::cout << "  id: " << mythid << " (main: "<< main_thread <<")" << std::endl;
                std::cout << "core: " << sched_getcpu() << std::endl;
                std::cout << "gpuStatus: " <<  gpuStatus << std::endl;
  */
				printf("gpuStatus= %d\n", (int)gpuStatus);
				printf("nCPUs=%d\n", nCPUs);
				printf("equal?= %d\n", mythid!= hpfor_main_thread);
                if ((mythid!= hpfor_main_thread || nCPUs==0) &&  --gpuStatus >= 0   )  // I'm in the GPU I'm not the main thread
                {
                    myclass=	0; //GPU
#ifdef HOSTA7
                    std::cout << "Set host thread affinity to A7\n";
                 //   set_thread_affinity_CORE(pthread_self(),3);
                 pinner.group(4,cluster1);
                 // pinner.core(3);
#else
                    std::cout << "Set host thread affinity to A15\n";
                 //   set_thread_affinity_CORE(pthread_self(),7);
                 // pinner.core(7);
                  pinner.group(4,cluster0);

#endif
#ifdef HOSTPRI_LINUX
                    std::cout << "Set GPU prio to SCHED_RR 2 (RT)\n";
                    set_thread_prio_RT(pthread_self(),2);
#endif

                } else
                {
					myclass= 1; // CPU
					pinner.next(); // get the next free core
#ifdef HOSTPRI_LINUX
                        std::cout << "Set CPU prio to SCHED_OTHER 0\n";
                        set_thread_prio(pthread_self(),0);
#endif
                }
                std::cout << " thid: " << mythid << " core: " << sched_getcpu() << " THREAD CLASS: " << myclass << "  -- 0=GPU; 1=CPU" << " (main: "<< hpfor_main_thread <<")" << std::endl;

            /*
                display_thread_prio("FINAL THREAD SETTINGS.....");
                std::cout << "  id: " << mythid << " (main: "<< main_thread <<")" << std::endl;
               std::cout << "core: " << sched_getcpu() << std::endl;
                std::cout << "THREAD CLASS: " << myclass << "  -- 0=GPU; 1=CPU" << std::endl << std::endl << std::endl;
              */
            }

			 //   std::cout << " thid: " << mythid << " core: " << sched_getcpu() << " THREAD CLASS: " << myclass << "  -- 0=GPU; 1=CPU" << " (main: "<< main_thread <<")" << std::endl;

			bundle->begin = begin;

			//Checking which resources are available
			if ( myclass==0 ){
				//GPU WORK
 				bundle->end = std::min(begin + chunkGPU, end);

//~ printf("GPU: %d, (%d,%d) %d\n", (int) chunkGPU, bundle->begin, bundle->end, bundle->end-bundle->begin+1);
//~ printf("thr   --> FPGA %f CPU %f  fG %f\n",(float)gpuThroughput,(float)cpuThroughput, (float) fG);
					//GPU CHUNK
					begin = bundle->end;
					bundle->type = isGPU;
				  return bundle;
			}else{
				//CPU WORK
				gpuStatus++; //Denisa - uncommented
				/*Calculating next chunkCPU*/
                                if(gpuThroughput > 0 && cpuThroughput > 0){
                                        fG = gpuThroughput/cpuThroughput;
                                        //printf("\nDEBUNG: fG = %f\n", fG);
                                        chunkCPU = ((end-begin < (int)chunkGPU)? end-begin : (int) chunkGPU) / fG ;
                                        chunkCPU = std::min((float)chunkCPU, (float)(end-begin)/((float)nCPUs + fG));
                                }
                                if(chunkCPU==0) chunkCPU=1;  // aqui si se establece minimo para CPU
                                if(nGPUs==0) //implement guided scheduling between the cores
                                {
                                        chunkCPU=max((end-begin)/(nCPUs), 1);
                                }

                                //~ printf("CPU: %d / %d\n",chunkCPU, end-begin);
                                //~ printf("thr   --> FPGA %f CPU %f  fG %f\n",(float)gpuThroughput,(float)cpuThroughput, (float)fG);

                                /*Taking a iteration chunk for CPU*/
                                bundle->end = std::min(begin + chunkCPU, end);
                        begin = bundle->end;
                                bundle->type = CPU;
				return bundle;
			}
		}
		//~ printf("return null\n");

		return NULL;
	} // end operator
};

/*MyParallelFilter class is the executor component of the engine, it executes the subrange onto the device selected by SerialFilter*/
template <class B>
class MyParallelFilter: public filter {
private:
	B *body;

public:
	/*Class' constructor*/
	//template <class B>
	MyParallelFilter(B *b) : filter(false) {
		body = b;
	}

	/*Operator function*/
	void * operator()(void * item) {
		//variables
		Bundle *bundle = (Bundle*) item;

		if(bundle->type == isGPU){
			// GPU WORK
#ifndef NDEBUG
			cerr << "launchGPU(): begin: " << bundle->begin << " end: " << bundle->end << endl;
#endif
			tick_count start_tc = tick_count::now();
#ifdef OVERHEAD_ANALYSIS
			//Calculating partition scheduling overhead
			overhead_sp += (start_tc - end_tc).seconds()*1000;

			//Adding a marker in the command queue
			cl_event event_before_h2d;
			int error = clEnqueueMarker(command_queue, &event_before_h2d);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl;
				exit(0);
			}
#endif
			body->sendObjectToGPU(bundle->begin, bundle->end, NULL);

#ifdef OVERHEAD_ANALYSIS
			//Adding a marker in the command queue
			cl_event event_before_kernel;
			error = clEnqueueMarker(command_queue,	&event_before_kernel);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl;
				exit(0);
			}
#endif
			cl_event event_kernel;
			body->OperatorGPU(bundle->begin, bundle->end, &event_kernel);

#ifdef OVERHEAD_ANALYSIS
			//Adding a marker in the command queue
			cl_event event_after_kernel;
			error = clEnqueueMarker(command_queue,	 &event_after_kernel);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl;
				exit(0);
			}
#endif
			body->getBackObjectFromGPU(bundle->begin, bundle->end, NULL);

#ifdef OVERHEAD_ANALYSIS
			//Adding a marker in the command queue
			cl_event event_after_d2h;
			error = clEnqueueMarker(command_queue,	 &event_after_d2h);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl;
				exit(0);
			}
#endif
			clFinish(command_queue);

#ifdef OVERHEAD_ANALYSIS
			//Adding a marker in the command queue to computer thread dispatch
			cl_event event_after_finish;
			error = clEnqueueMarker(command_queue,	 &event_after_finish);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl;
				exit(0);
			}
#endif
			end_tc = tick_count::now();

#ifdef OVERHEAD_ANALYSIS

			//Calculating host to device transfer overhead
			cl_ulong tg1, tg2, tg3, tg4, tg5, tc3;
			clGetEventProfilingInfo(event_before_h2d, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &tg1, NULL);
			clGetEventProfilingInfo(event_before_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg2, NULL);
			overhead_h2d += (tg2-tg1)/1000000.0; // ms

			//Calculating kernel launch overheads
			clGetEventProfilingInfo(event_before_kernel, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &tg2, NULL);
			clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg3, NULL);
			overhead_kl += (tg3-tg2)/1000000.0; // ms

			//Calculating kernel execution
			clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg3, NULL);
			clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &tg4, NULL);
			kernel_execution += (tg4-tg3)/1000000.0; // ms

			//Calculating device to host transfer overhead
			clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &tg4, NULL);
			clGetEventProfilingInfo(event_after_d2h, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg5, NULL);
			overhead_d2h += (tg5-tg4)/1000000.0; // ms

			//Calculating device overhead thread dispatch
//			clGetEventProfilingInfo(event_after_d2h, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &tg5, NULL);
//			clGetEventProfilingInfo(event_after_finish, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tc3, NULL);
//			overhead_td += (tc3-tg5)/1000000.0; // ms
    //Calculating device overhead thread dispatch
      if(tg5 > tg1){
        overhead_td = overhead_td +((end_tc - start_tc).seconds()*1000) - ((tg5-tg1)/1000000.0); // ms
      }

			//Releasing event objects
			clReleaseEvent(event_before_h2d);
			clReleaseEvent(event_before_kernel);
			clReleaseEvent(event_after_kernel);
			clReleaseEvent(event_after_d2h);
			clReleaseEvent(event_after_finish);
#endif
clReleaseEvent(event_kernel);
			float time =(end_tc - start_tc).seconds()*1000;
		//	gpuThroughput = max((bundle->end - bundle->begin) / time , gpuThroughput) ;
			gpuThroughput = (bundle->end - bundle->begin) / time ;
		//cerr << "Chunk GPU: " << bundle->end - bundle->begin << " TH: " << gpuThroughput /*((tg5-tg1)/1000000.0) */ << endl;

			if(gpuThroughput >0 && cpuThroughput>0)	fG = gpuThroughput/cpuThroughput;
//printf("FPGA %d->%d:%d %f %f\n", bundle->begin, bundle->end, bundle->end - bundle->begin, time, (bundle->end - bundle->begin) / time);

#ifdef DEEP_GPU_REPORT

deep_gpu_report << " Step: " << step << " chunkCPU: " << chunkCPU << " chunkGPU: " << chunkGPU << " cpuThroughput: " << cpuThroughput << " gpuThroughput: " << gpuThroughput << " fG: " << fG << " begin: "<< bundle->begin <<endl;
#endif

			/*If CPU has already computed some chunk, then we update fG (factor GPU)
			if (cpuThroughput > 0){
				fG = gpuThroughput/cpuThroughput;
			}*/

			/*To release GPU token*/
			gpuStatus++; // Denisa - uncommented
		}else{
			// CPU WORK
#ifndef NDEBUG
			cerr << "launchCPU(): begin: " << bundle->begin << " end: " << bundle->end << endl;
#endif
			tick_count start = tick_count::now();
			body->OperatorCPU(bundle->begin, bundle->end);
			tick_count end = tick_count::now();
			float time =(end-start).seconds()*1000;
			cpuThroughput = (bundle->end - bundle->begin) / time;
			if(gpuThroughput >0 && cpuThroughput>0)	fG = gpuThroughput/cpuThroughput;
#ifdef DEEP_CPU_REPORT
			//deep_cpu_report << bundle->end-bundle->begin << "\t" << cpuThroughput << endl;

deep_cpu_report << " Step: " << step << " chunkCPU: " << chunkCPU << " chunkGPU: " << chunkGPU << " cpuThroughput: " << cpuThroughput << " gpuThroughput: " << gpuThroughput << " fG: " << fG << " begin: "<< bundle->begin << endl;
#endif
//printf("CPU %d->%d:%d %f %f\n", bundle->begin, bundle->end, bundle->end - bundle->begin, time, cpuThroughput);
			/*If GPU has already computed some chunk, then we update fG (factor GPU)
			if (gpuThroughput > 0){
				fG = gpuThroughput/cpuThroughput;
			}*/
		}
		/*Deleting bundle to avoid memory leaking*/
	//	printf("END: %d,%d\n", bundle->begin, bundle->end);
		delete bundle;
		return NULL;
	}
};
//end class

/*Oracle Class: This scheduler version let us to split the workload in two subranges, one for GPU and one for CPUs*/
class Dynamic : public Scheduler<Dynamic, Params> {
	Params * pars;
public:
	/*This constructor just call his parent's contructor*/
	Dynamic(Params *params) : Scheduler<Dynamic, Params>(params){
		Params * p = (Params*) params;
		pars = p;
       // numCPUs=p->numcpus;
		chunkCPU = 0;
		chunkGPU = 0;
		fG = 0.0;
		chunkGPU = p->gpuChunk;
		gpuThroughput=0.0;
		cpuThroughput=0.0;

		//Initializing library PJTRACER
		initializePJTRACER();
	}

	/*Initializes PJTRACER library*/
	void initializePJTRACER(){
#ifdef PJTRACER
		char traceFname[1024];
		sprintf(traceFname, "DYNAMIC_C_%d_G_%d.trace", nCPUs, nGPUs);
		tracer = new PFORTRACER(traceFname);
		tracer->beginThreadTrace();
#endif
	}

	/*The main function to be implemented*/
	template<class T>
	void heterogeneous_parallel_for(int begin, int end, T* body){
#ifndef NDEBUG
		cerr << "Heterogeneous Parallel For Dynamic " << nCPUs << " , " << nGPUs << ", " << chunkGPU << endl;
#endif
        hpfor_main_thread=syscall(__NR_gettid);
		/*Preparing pipeline*/
		pipeline pipe;
		MySerialFilter serial_filter(begin, end, nCPUs,nGPUs);
		MyParallelFilter<T> parallel_filter(body);
		pipe.add_filter(serial_filter);
		pipe.add_filter(parallel_filter);
		//chunkCPU = chunkGPU * 0.2;
		if(fG==0.0) fG = 8; // 25 para Nbody?
	    chunkCPU = chunkGPU / fG;
		if(!chunkCPU) chunkCPU=1;
		gpuStatus = nGPUs;
		/*
		if(nCPUs>4)
        {
            firsttime_A15=4; //andres

        }
        else
        {
            firsttime_A15=nCPUs; //andres

        }
        * */

		body->firsttime = true;

#ifdef DEEP_CPU_REPORT
		char nombre[1024];
		sprintf(nombre, "%s_Dynamic_deep_CPU_report_step_%d_GPU_%d.txt", pars->benchName, step, chunkGPU);


		deep_cpu_report.open(nombre, ios::out | ios::app);
#endif
#ifdef DEEP_GPU_REPORT
{
		char nombre[1024];
		sprintf(nombre, "%s_Dynamic_deep_GPU_report_step_%d_GPU_%d.txt", pars->benchName, step, chunkGPU);
	deep_gpu_report.open(nombre, ios::out | ios::app);
}
#endif

		/*Seeting a mark to recognize a timestep*/
#ifdef PJTRACER
		tracer->newEvent();
#endif

#ifdef OVERHEAD_ANALYSIS
		end_tc = tick_count::now();
#endif
		/*Run the pipeline*/
		pipe.run(nCPUs + nGPUs);
		pipe.clear();
#ifdef DEEP_CPU_REPORT
		deep_cpu_report.close();
#endif
#ifdef DEEP_GPU_REPORT
		deep_gpu_report.close();
#endif
	}

	/*this function print info to a Log file*/
	void saveResultsForBenchDen(int inputId, int nrIter){

		char * execution_name = (char *)malloc(sizeof(char)*100);
		sprintf(execution_name, "_Dynamic_Param.txt");
		strcat(pars->benchName, execution_name);

		/*Checking if the file already exists*/
		bool fileExists = isFile(pars->benchName);
		ofstream file(pars->benchName, ios::out | ios::app);
		if(!fileExists){
			printHeaderToFileDen(file);
		}

		cout << "TIEMPO TOTAL: " << runtime << endl;
#ifdef OVERHEAD_STUDY
cout
<< "Kernel Execution: " << kernel_execution << endl
<< "O. Kernel Launch: " << overhead_kl << endl
<< "O. Th. Dispatch:  " << overhead_td << endl
<< "O. sched. Part:   " << overhead_sp << endl
<< "O. Host2Device:   " << overhead_h2d << endl
<< "O. Device2Host:   " << overhead_d2h << endl;
#endif

		file
        << inputId << SEP
        << nCPUs << SEP
        << nGPUs << SEP
        << chunkGPU << SEP
        << runtime << SEP

#ifdef ODROID
		<< total_em((*sample1)) << SEP
        << sample1->A15 << SEP
        << sample1->A7 << SEP
        << sample1->GPU << SEP
        << sample1->MEM << SEP
#endif
#ifdef PMLIB
    << energiaCPU << SEP  << energiaFPGA << SEP<< energiaFPGA +energiaCPU<< SEP

		<< getPP0ConsumedJoules(sstate1, sstate2) << SEP << getPP1ConsumedJoules(sstate1, sstate2) << SEP
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << SEP <<  getConsumedJoules(sstate1, sstate2) << SEP
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << SEP << getL2CacheMisses(sktstate1[0], sktstate2[0]) << SEP << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<SEP
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << SEP << getL3CacheMisses(sktstate1[0], sktstate2[0]) << SEP << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<SEP
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2)
#endif
		<< nrIter << endl;
		file.close();
#ifndef NDEBUG
		cerr << nCPUs << SEP << nGPUs << SEP  << runtime << SEP
		<< getPP0ConsumedJoules(sstate1, sstate2) << SEP << getPP1ConsumedJoules(sstate1, sstate2) << SEP
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << SEP <<  getConsumedJoules(sstate1, sstate2) << SEP
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << SEP << getL2CacheMisses(sktstate1[0], sktstate2[0]) << SEP << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<SEP
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << SEP << getL3CacheMisses(sktstate1[0], sktstate2[0]) << SEP << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<SEP
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
#endif
	}

	void printHeaderToFileDen(ofstream &file){
		file << "InputId" << SEP << "N. CPUs" << SEP << "N. GPUs" << SEP << "Chunk" << SEP << "Time (ms)" << SEP
#ifdef ODROID
        << "T.E" << SEP << "A15" << SEP << "A7" << SEP << "GPU" << SEP << "MEM" << SEP
#endif
#ifdef PMLIB
    << "CPU Energy (BBB)" << SEP << "FPGA Energy (BBB) " << SEP

		<< "CPU Energy(J)" << SEP << "GPU Enegy(J)" << SEP << "Uncore Energy(J)" << SEP << "Total Energy (J)" << SEP
		<< "L2 Cache Hits" << SEP << "L2 Cache Misses" << SEP << "L2 Cache Hit Ratio" << SEP
		<< "L3 Cache Hits" << SEP << "L3 Cache Misses" << SEP << "L3 Cache Hit Ratio" << SEP << "Cycles lost Due to L3 Misses"
#endif
		<< "NrIter" << endl;
	}
	/*this function print info to a Log file*/
	void saveResultsForBench(){

		char * execution_name = (char *)malloc(sizeof(char)*100);
		sprintf(execution_name, "_Dynamic_%d_%d.txt", nCPUs, nGPUs);
		strcat(pars->benchName, execution_name);

		/*Checking if the file already exists*/
		bool fileExists = isFile(pars->benchName);
		ofstream file(pars->benchName, ios::out | ios::app);
		if(!fileExists){
			printHeaderToFile(file);
		}

		cout << "TIEMPO TOTAL: " << runtime << endl;
#ifdef OVERHEAD_STUDY
cout
<< "Kernel Execution: " << kernel_execution << endl
<< "O. Kernel Launch: " << overhead_kl << endl
<< "O. Th. Dispatch:  " << overhead_td << endl
<< "O. sched. Part:   " << overhead_sp << endl
<< "O. Host2Device:   " << overhead_h2d << endl
<< "O. Device2Host:   " << overhead_d2h << endl;
#endif

		file << nCPUs << SEP << nGPUs << SEP  << chunkGPU << SEP <<  runtime << SEP
#ifdef ODROID
		  << total_em((*sample1)) << SEP<< sample1->A15 << SEP<< sample1->A7 << SEP<< sample1->GPU << SEP<< sample1->MEM
#endif
#ifdef PMLIB
    << energiaCPU << SEP  << energiaFPGA << SEP<< energiaFPGA +energiaCPU<< SEP

		<< getPP0ConsumedJoules(sstate1, sstate2) << SEP << getPP1ConsumedJoules(sstate1, sstate2) << SEP
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << SEP <<  getConsumedJoules(sstate1, sstate2) << SEP
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << SEP << getL2CacheMisses(sktstate1[0], sktstate2[0]) << SEP << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<SEP
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << SEP << getL3CacheMisses(sktstate1[0], sktstate2[0]) << SEP << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<SEP
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2)
#endif
		<< endl;
		file.close();
#ifndef NDEBUG
		cerr << nCPUs << SEP << nGPUs << SEP  << runtime << SEP
		<< getPP0ConsumedJoules(sstate1, sstate2) << SEP << getPP1ConsumedJoules(sstate1, sstate2) << SEP
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << SEP <<  getConsumedJoules(sstate1, sstate2) << SEP
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << SEP << getL2CacheMisses(sktstate1[0], sktstate2[0]) << SEP << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<SEP
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << SEP << getL3CacheMisses(sktstate1[0], sktstate2[0]) << SEP << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<SEP
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
#endif
	}

	void printHeaderToFile(ofstream &file){
		file << "N. CPUs" << SEP << "N. GPUs" << SEP << "Chunk" << SEP << "Time (ms)" << SEP
		#ifdef ODROID
		<< "T.E" << SEP << "A15" << SEP << "A7" << SEP << "GPU" << SEP << "MEM" << SEP
		#endif
#ifdef PMLIB
    << "CPU Energy (BBB)" << SEP << "FPGA Energy (BBB) " << SEP

		<< "CPU Energy(J)" << SEP << "GPU Enegy(J)" << SEP << "Uncore Energy(J)" << SEP << "Total Energy (J)" << SEP
		<< "L2 Cache Hits" << SEP << "L2 Cache Misses" << SEP << "L2 Cache Hit Ratio" << SEP
		<< "L3 Cache Hits" << SEP << "L3 Cache Misses" << SEP << "L3 Cache Hit Ratio" << SEP << "Cycles lost Due to L3 Misses"
#endif
		<< endl;
	}


};
