//============================================================================
// Name     : FixedChunkSize.h
// Author   : Antonio Vilches
// Version    : 1.0
// Date     : 02 / 01 / 2014
// Copyright  : Department. Computer's Architecture (c)
// Description  : FixedChunkSize scheduler implementation
//============================================================================

#include <cassert>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include "Scheduler.h"
#include "tbb/pipeline.h"
#include "tbb/tick_count.h"

#ifdef Win32
#include "PCM_Win/windriver.h"
#else
#include "cpucounters.h"
#endif

#ifdef PJTRACER
#include "pfortrace.h"
#endif

//#define DEBUG

//#undef NDEBUG

using namespace std;
using namespace tbb;

/*****************************************************************************
 * Defines
 * **************************************************************************/
#define CPU 0
#define GPU 1
#define GPU_OFF -100 //Arbitrary value
#define SEP "\t"

/*****************************************************************************
 * types
 * **************************************************************************/
typedef struct{
  int numcpus;
  int numgpus;
  int gpuChunk;
  int cpuChunk;
  char benchName[100];
  char kernelName[100];
}Params;

/*****************************************************************************
 * Global variables
 * **************************************************************************/
atomic<int> gpuStatus;
int chunkCPU;
int chunkGPU;

float gpuThroughput, cpuThroughput;

#ifdef DEEP_CPU_REPORT
ofstream deep_cpu_report;
#endif

#ifdef DEEP_GPU_REPORT
ofstream deep_gpu_report;
#endif

tick_count end_tc;
#ifdef OVERHEAD_STUDY
// overhead accumulators
float overhead_sp = 0.0;
float overhead_h2d = 0.0;
float overhead_kl = 0.0;
float kernel_execution = 0.0;
float overhead_d2h = 0.0;
float overhead_td = 0.0;
cl_ulong tg1, tg2, tg3, tg4, tg5;
#endif


/*****************************************************************************
 * Heterogeneous Scheduler
 * **************************************************************************/
/*Bundle class: This class is used to store the information that items need while walking throught pipeline's stages.*/
class Bundle {
public:
  int begin;
  int end;
  int type; //GPU = 0, CPU=1

  Bundle() {};
};

/*My serial filter class represents the partitioner of the engine. This class selects a device and a rubrange of iterations*/
class MySerialFilter: public filter {
private:
  int begin;
  int end;
  int nCPUs;
public:
  /*Class constructor, it only needs the first and last iteration indexes.*/
  MySerialFilter(int b, int e, int ncpus) : filter(true) {
    begin = b;
    end = e;
    nCPUs = ncpus;
  }

  /*Mandatory operator method, TBB rules*/
  void * operator()(void *) {
    Bundle *bundle = new Bundle();
    /*If there are remaining iterations*/
    if (begin < end) {
      // always we start the chunk at begin
      bundle->begin = begin;
      // Only send to GPU exact number of work items
      // the kernel uses reqd_work_group_size
      if((end - begin) < chunkGPU) {
        bundle->end = std::min(begin + chunkCPU, end);
        begin = bundle->end;
        bundle->type = CPU;
        return bundle;
      }

      //Checking which resources are available
      if ( --gpuStatus >= 0 ){
        //GPU WORK
        assert((begin + chunkGPU) <= end);
        bundle->end = begin + chunkGPU;
        begin = bundle->end;
        bundle->type = GPU;
      }else{
        //CPU WORK
        gpuStatus++;
        /*Taking a iteration chunk for CPU*/
        bundle->end = std::min(begin + chunkCPU, end);
        begin = bundle->end;
        bundle->type = CPU;
      }
      return bundle;
    }
    return NULL;
  } // end operator
};

/*MyParallelFilter class is the executor component of the engine, it executes the subrange onto the device selected by SerialFilter*/
template <class B>
class MyParallelFilter: public filter {
private:
  B *body;

public:
  /*Class' constructor*/
  //template <class B>
  MyParallelFilter(B *b) : filter(false) {
    body = b;
  }

  /*Operator function*/
  void * operator()(void * item) {
    //variables
    Bundle *bundle = (Bundle*) item;

    if(bundle->type == GPU){
      // GPU WORK
#ifdef PJTRACER
      tracer->gpuStart();
#endif
#ifndef NDEBUG
      cerr << "launchGPU(): begin: " << bundle->begin << " end: " << bundle->end << endl;
#endif

      tick_count start = tick_count::now();
      tick_count start_tc = tick_count::now();
#ifdef OVERHEAD_STUDY
      //Calculating partition scheduling overhead
      overhead_sp = overhead_sp + ((start_tc - end_tc).seconds()*1000);

      //Adding a marker in the command queue
      cl_event event_before_h2d;
      int error = clEnqueueMarker(command_queue, &event_before_h2d);
      if (error != CL_SUCCESS) {
        std::cerr <<  "Failed equeuing start event" << std::endl;
        exit(0);
      }
#endif
      body->sendObjectToGPU(bundle->begin, bundle->end, NULL);
#ifdef OVERHEAD_STUDY
      //Adding a marker in the command queue
      cl_event event_before_kernel;
      error = clEnqueueMarker(command_queue, &event_before_kernel);
      if (error != CL_SUCCESS) {
        std::cerr <<  "Failed equeuing start event" << std::endl;
        exit(0);
      }
#endif

cl_event event_kernel;
      body->OperatorGPU(bundle->begin, bundle->end, &event_kernel);

#ifdef OVERHEAD_STUDY
      //Adding a marker in the command queue
      cl_event event_after_kernel;
      error = clEnqueueMarker(command_queue,   &event_after_kernel);
      if (error != CL_SUCCESS) {
        std::cerr <<  "Failed equeuing start event" << std::endl;
        exit(0);
      }
#endif

      body->getBackObjectFromGPU(bundle->begin, bundle->end, NULL);


#ifdef OVERHEAD_STUDY
      //Adding a marker in the command queue
      cl_event event_after_d2h;
      error = clEnqueueMarker(command_queue,  &event_after_d2h);
      if (error != CL_SUCCESS) {
        std::cerr <<  "Failed equeuing start event" << std::endl;
        exit(0);
      }
#endif
      clFinish(command_queue);

#ifdef OVERHEAD_STUDY
      //Adding a marker in the command queue to computer thread dispatch
      cl_event event_after_finish;
      error = clEnqueueMarker(command_queue,  &event_after_finish);
      if (error != CL_SUCCESS) {
        std::cerr <<  "Failed equeuing start event" << std::endl;
        exit(0);
      }
#endif

      tick_count end = tick_count::now();


            end_tc = tick_count::now();

#ifdef OVERHEAD_STUDY

      //Calculating host to device transfer overhead
      clGetEventProfilingInfo(event_before_h2d, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &tg1, NULL);
      clGetEventProfilingInfo(event_before_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg2, NULL);
      if(tg2 > tg1){
        overhead_h2d = overhead_h2d + (tg2-tg1)/1000000.0; // ms
      }
      //cerr << "Overhead h2d: " << overhead_h2d << ": " << tg1 << ", " << tg2 << " = " << (tg2-tg1) <<endl;

      //Calculating kernel launch overheads
      clGetEventProfilingInfo(event_before_kernel, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &tg2, NULL);
      clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg3, NULL);
      if(tg3 > tg2){
        overhead_kl = overhead_kl + (tg3-tg2)/1000000.0; // ms
      }
      //Calculating kernel execution
      //clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg3, NULL);
      clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &tg4, NULL);
      if(tg4 > tg3){
        kernel_execution = kernel_execution + (tg4-tg3)/1000000.0; // ms
      }
      //Calculating device to host transfer overhead
      //clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_COMPLETE, sizeof(cl_ulong), &tg4, NULL);
      clGetEventProfilingInfo(event_after_d2h, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg5, NULL);
      if(tg5 > tg4){
        overhead_d2h = overhead_d2h + (tg5-tg4)/1000000.0; // ms
      }
      //Calculating device overhead thread dispatch
      if(tg5 > tg1){
        overhead_td = overhead_td +((end_tc - start_tc).seconds()*1000) - ((tg5-tg1)/1000000.0); // ms
      }

      //Releasing event objects
      clReleaseEvent(event_before_h2d);
      clReleaseEvent(event_before_kernel);
      clReleaseEvent(event_after_kernel);
      clReleaseEvent(event_after_d2h);
      clReleaseEvent(event_after_finish);
#endif
      clReleaseEvent(event_kernel);
#ifdef PJTRACER
      tracer->gpuStop();
#endif
      float time =(end-start).seconds()/1000;
      gpuThroughput = (bundle->end - bundle->begin) / time;

      /*To release GPU token*/
      gpuStatus++;
    }else{
      // CPU WORK
#ifndef NDEBUG
      cerr << "launchCPU(): begin: " << bundle->begin << " end: " << bundle->end << endl;
#endif
#ifdef PJTRACER
      tracer->cpuStart();
#endif
      tick_count start = tick_count::now();
      body->OperatorCPU(bundle->begin, bundle->end);
      tick_count end = tick_count::now();
      cpuThroughput = (bundle->end-bundle->begin)/((end-start).seconds()*1000);
#ifdef PJTRACER
      tracer->cpuStop();
#endif
#ifdef DEEP_CPU_REPORT
      deep_cpu_report << bundle->end-bundle->begin << "\t" << cpuThroughput << endl;
#endif
    }
    /*Deleting bundle to avoid memory leaking*/
    delete bundle;
    return NULL;
  }
};
//end class

/*Oracle Class: This scheduler version let us to split the workload in two subranges, one for GPU and one for CPUs*/
class FixedChunkSize : public Scheduler<FixedChunkSize, Params> {
  Params *pars;
public:
  /*This constructor just call his parent's contructor*/
  FixedChunkSize(Params *params) : Scheduler<FixedChunkSize, Params>(params){
    Params * p = (Params*) params;
    pars = p;

    chunkCPU = p->cpuChunk;
    chunkGPU = p->gpuChunk;
    gpuThroughput=0.0;
    cpuThroughput=0.0;

    //Initializing library PJTRACER
    initializePJTRACER();
  }

  /*Initializes PJTRACER library*/
  void initializePJTRACER(){
#ifdef PJTRACER
    char traceFname[1024];
    sprintf(traceFname, "FIXEDCHUNK_C_%d_G_%d.trace", nCPUs, nGPUs);
    tracer = new PFORTRACER(traceFname);
    tracer->beginThreadTrace();
#endif
  }

  /*The main function to be implemented*/
  template<class T>
  void heterogeneous_parallel_for(int begin, int end, T* body){
#ifndef NDEBUG
    cerr << "Heterogeneous Parallel For Dynamic " << nCPUs << " , " << nGPUs << ", " << chunkGPU << endl;
#endif
    /*Preparing pipeline*/
    pipeline pipe;
    MySerialFilter serial_filter(begin, end, nCPUs);
    MyParallelFilter<T> parallel_filter(body);
    pipe.add_filter(serial_filter);
    pipe.add_filter(parallel_filter);

#ifdef OVERHEAD_STUDY
      end_tc = tick_count::now();
#endif
    gpuStatus = nGPUs;
    body->firsttime = true;

#ifdef DEEP_CPU_REPORT
    char version[100];
    char version2[100];
    sprintf(version, "FixedChunkSize_deep_CPU_report_grain_%d.txt", chunkCPU);
    //strcat(pars->benchName, version);
    sprintf(version2,"%s_%s", pars->benchName, version);
    deep_cpu_report.open(version2, ios::out | ios::app);
#endif

    /*Seeting a mark to recognize a timestep*/
#ifdef PJTRACER
    tracer->newEvent();
#endif
    /*Run the pipeline*/
    pipe.run(nCPUs + nGPUs);
    pipe.clear();

#ifdef DEEP_CPU_REPORT
    deep_cpu_report.close();
#endif
  }

  /*this function print info to a Log file*/
  void saveResultsForBench(){

    char execution_name[50];
//    sprintf(execution_name, "_FixedChunkSize_%d_%d_%d_%d.txt", nCPUs, nGPUs, chunkCPU, chunkGPU);
    sprintf(execution_name, "_FixedChunkSize_%d_%d.txt", nCPUs, nGPUs);  // Andres all results together

    strcat(pars->benchName, execution_name);

    /*Checking if the file already exists*/
    bool fileExists = isFile(pars->benchName);
    ofstream file(pars->benchName, ios::out | ios::app);
    if(!fileExists){
      printHeaderToFile(file);
    }

    file << nCPUs << "\t" << nGPUs << "\t"  << chunkCPU << "\t" << chunkGPU << "\t" << runtime << "\t"
#ifdef OVERHEAD_STUDY
      << kernel_execution << SEP << overhead_td << SEP << overhead_sp << SEP << overhead_h2d << SEP << overhead_kl << SEP << overhead_d2h << SEP
#endif
    << getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"


<<  getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t"
<<  getConsumedJoules(sstate1, sstate2) << "\t"
#ifdef PMLIB
    << energiaCPU << "\t"  << energiaFPGA << "\t"
#endif

    << getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
    << getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
    << getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
    file.close();

#ifndef NDEBUG
    cerr << nCPUs << "\t" << nGPUs << "\t"  << chunkCPU << "\t"  << chunkGPU << "\t" << runtime << "\t"
    << getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"
    << getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
    << getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
    << getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
#endif
  }

  /*This function prints the header of a CSV file*/
  void printHeaderToFile(ofstream &file){
    file << "N. CPUs" << SEP << "N. GPUs" << SEP << "CPU Chunk" << SEP << "GPU Chunk" << SEP << "Time (ms)" << SEP
#ifdef OVERHEAD_STUDY
     << "Kernel Execution" << SEP << "O. Th. Dispatch" << SEP << "O. sched. Partitioning" << SEP << "O. Host2Device" << SEP << "O. Kernel Launch" << SEP << "O. Device2Host" << SEP
#endif
    << "CPU Energy(J)" << SEP << "GPU Enegy(J)" << SEP << "Uncore Energy(J)" << SEP << "Total Energy (J)" << SEP
#ifdef PMLIB
    << "CPU Energy (BBB)" << SEP << "FPGA Energy (BBB) " << SEP
#endif
    << "L2 Cache Hits" << SEP << "L2 Cache Misses" << SEP << "L2 Cache Hit Ratio" << SEP
    << "L3 Cache Hits" << SEP << "L3 Cache Misses" << SEP << "L3 Cache Hit Ratio" << SEP << "Cycles lost Due to L3 Misses" << endl;
  }
};
