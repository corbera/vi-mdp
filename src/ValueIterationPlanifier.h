//============================================================================
// Name			: ValueIterationPlanifier.h
// Author		: Denisa Constantinescu
// Version		: 1.0
// Date			: 19 / 06 / 2017
// Copyright	: Department. Computer's Architecture (c)
// Description	: ValueIterationPlanifier.h implementation for ValueIteration project
//                The workload will be distributed row-wise, not by T matrix cell,
//                ({begin, end} in 0..NS)
//============================================================================

#ifndef VALUEITERATION_VALUEITERATIONPLANIFIER_H
#define VALUEITERATION_VALUEITERATIONPLANIFIER_H

//#define POLICY_IMPROVMENT
//#define DYNAMIC
//#define ORACLE

#if defined(__APPLE__)
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#include <cstdlib>
#include <iostream>

#include <tbb/tick_count.h>
#include <tbb/blocked_range.h>
#include "tbb/parallel_for.h"
#include "tbb/task.h"

#include "../navigation_mdp/Common.h"

#ifdef ORACLE
#include <Oracle.h>
#endif
#ifdef DYNAMIC
#include <Dynamic.h>
#endif
#ifdef LOGFIT
#include <LogFit.h>
#endif

using namespace std;
using namespace tbb;

/*****************************************************************************
 * Global variables
 * **************************************************************************/

// Device buffers
extern cl_mem d_probabilities;
extern cl_mem d_nextCellPos;
extern cl_mem d_nextStatePos;
extern cl_mem d_R;
extern cl_mem d_Q;
extern cl_mem d_P;
extern cl_mem d_V;
extern cl_mem d_Z;
extern cl_ulong probabilitySize;

// Host buffers
extern cl_float*    h_probability;
extern cl_ulong*    h_nextCellPos;
extern cl_ulong*    h_nextStatePos;
extern cl_float*    h_R;
extern cl_float*    h_Q;
extern cl_int*      h_P;
extern cl_float*    h_V;
extern cl_float*    h_Z;

static  size_t WORK_GROUP_SIZE = 256;

class ValueIterationPlanifier {
public:
    bool firsttime = true;
public:
    /*Allocate memory objects on GPU memory address space*/
    void AllocateMemoryObjects() {
        cl_int err = 0;
        cl_int size;

        /** Allocate device memory */
        d_probabilities = clCreateBuffer(context,  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, probabilitySize * sizeof(cl_float), h_probability, &err );
        //CHECK_CL_ERROR(err, "clCreateBuffer");
        if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_probabilities (size:%d) => %d\n", probabilitySize, err); exit(0);}

        //nextCellPos
        size = NX * NS + 1;
        //d_nextCellPos = clCreateBuffer( context,  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, size * sizeof(cl_ulong), h_nextCellPos, &err );
        d_nextCellPos = clCreateBuffer(context,  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, size * sizeof(cl_ulong), h_nextCellPos, &err );
        if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_nextCellPos (size:%d) => %d\n", NX * NS + 1, err); exit(0);}

        //V
        //d_V = clCreateBuffer( context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, NS * sizeof(cl_float), NULL, &err );
        d_V = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR/*CL_MEM_ALLOC_HOST_PTR*/, NS * sizeof(cl_float), h_V, &err );
        if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_V (size:%d) => %d\n", NS, err);exit(0); }

        //Q
        //d_Q = clCreateBuffer( context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, NX * NS  * sizeof(cl_float), NULL, &err );
        d_Q = clCreateBuffer(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR /*CL_MEM_ALLOC_HOST_PTR*/, NX * NS  * sizeof(cl_float), h_Q, &err );
        if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_Q (size:%d) => %d\n", NX * NS, err); exit(0);}

        //nextStatePos
        //d_nextStatePos = clCreateBuffer( context,  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, probabilitySize * sizeof(cl_ulong), h_nextStatePos, &err );
        d_nextStatePos = clCreateBuffer(context,  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, probabilitySize * sizeof(cl_ulong), h_nextStatePos, &err );
        if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_nextStatePos (size:%d) => %d\n", probabilitySize, err); exit(0);}

        //R
        //d_R = clCreateBuffer( context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, NX * NS  * sizeof(cl_float), h_R, &err );
        d_R = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, NX * NS  * sizeof(cl_float), h_R, &err );
        if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_R (size:%d) => %d\n", NX * NS, err); exit(0);}

    }

    /*This function send the data to be computed in the GPU*/
    void sendObjectToGPU( cl_ulong begin, cl_ulong end, cl_event * event)  {
        cl_int err;
        //V        
        // h_V = (cl_float*)clEnqueueMapBuffer( command_queue,   // Corresponding command queue
        //                                      d_V,     // Buffer to be mapped
        //                                      CL_FALSE,         // block_map, CL_TRUE: can't be unmapped before at least 1 read
        //                                      CL_MAP_WRITE,    // Host to device. mapped for reading or writing?
        //                                      0,               // offset
        //                                      NS *sizeof(cl_float),         // number of bytes mapped
        //                                      0,               // number of events in the wait list
        //                                      NULL,            // event wait list
        //                                      event,            // event
        //                                      &err );
        // if(err != CL_SUCCESS) { printf("ERROR: clEnqueueMapBuffer d_V  => %d\n", err);  exit(0);}

        //V
        err = clEnqueueWriteBuffer( command_queue, d_V, /*non blocking*/ CL_FALSE, /*offset*/ 0, NS * sizeof(cl_float), h_V, 0, NULL, event );
#ifdef DEBUG
        printf("\nsendObjectToGPU\n->begin: %d, end: %d \n", begin, end);
#endif
        if(err != CL_SUCCESS) { printf("ERROR: clEnqueueWriteBuffer d_V (size:%d) => %d\n", NS, err); exit(0); }

    }

    /*This function launches the kernel*/
    void OperatorGPU(cl_ulong begin, cl_ulong end, cl_event * event) {
        const cl_uint d_NS = NS;
        const cl_uint d_NX = NX;
        cl_int err;
        err  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_probabilities);
        err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_V);
        err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_Q);
        err |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &d_nextCellPos);
        err |= clSetKernelArg(kernel, 4, sizeof(cl_mem), &d_nextStatePos);
        err |= clSetKernelArg(kernel, 5, sizeof(cl_mem), &d_R);
        err |= clSetKernelArg(kernel, 6, sizeof(cl_ulong), &d_NS);
        err |= clSetKernelArg(kernel, 7, sizeof(cl_uint), &d_NX);
        err |= clSetKernelArg(kernel, 8, sizeof(cl_ulong), &begin);
        err |= clSetKernelArg(kernel, 9, sizeof(cl_ulong), &end);

        //printf("\nclSetKernelArg err:%d",err);
        if (err != CL_SUCCESS) {
            printf("Failed setting kernel Args!\n");
            exit(0);
        }

        // Define an index space (global work size) of work
        // items for execution. A workgroup size (local work size) is not required,
        // but can be used.
        size_t globalWorkSize[3] = { end-begin, 1, 1 };
        size_t localWorkSize[] = {WORK_GROUP_SIZE};

        if(globalWorkSize[0]%localWorkSize[0] !=0)
            globalWorkSize[0]=(globalWorkSize[0]/localWorkSize[0]+1)*localWorkSize[0];


        err = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, event);
        if (err != CL_SUCCESS) {
            printf("Failed launching kernel!\n");
            exit(0);
        }

    }

    /*Function to receive the data from GPU memory*/
    void getBackObjectFromGPU(cl_ulong begin, cl_ulong end, cl_event *event) {
        cl_int err = 0;
		err = clEnqueueReadBuffer(
           command_queue,
           d_Q,
           CL_FALSE,
           begin * sizeof(cl_float) /*offset*/,
           (end - begin) * sizeof(cl_float) /*size of bytes being read*/ ,
           &(h_Q[begin]),//h_Q,
           0, NULL, event);
        // h_Q = (cl_float*)clEnqueueMapBuffer( command_queue,   // Corresponding command queue
        //                                      d_Q,     // Buffer to be mapped
        //                                      CL_FALSE,         // block_map, CL_TRUE: can't be unmapped before at least 1 read
        //                                      CL_MAP_READ,    // mapped for reading or writing?
        //                                      begin * sizeof(cl_float),               // offset
        //                                      (end - begin) * sizeof(cl_float) ,         // number of bytes mapped
        //                                      0,               // number of events in the wait list
        //                                      NULL,            // event wait list
        //                                      event,            // event
        //                                      &err );
#ifdef DEBUG
        cout << "getBackObjectFromGPU" << endl;
        printf("->begin: %d, end: %d \n", begin, end);
        printf("->offset: %d, elements: %d \n", begin , (end - begin));
#endif
        if (err != CL_SUCCESS) {
            //CL_INVALID_VALUE if the region being read specified by (offset, cb) is out of bounds or if ptr is a NULL value.
            printf("\nclEnqueueReadBuffer d_Q err:%d ",err);
            printf(" Failed copying  d_Q to host!\n");
            printf(" begin: %d, end: %d \n", begin, end);
            exit(0);
        }
        //clFinish(command_queue); corbera
    }

    /*Serial version of the code */
    void OperatorCPU(cl_ulong begin, cl_ulong end) { // 0,NS*NX
        cl_float w;//, maxQs;
        for (cl_ulong idxCell = begin; idxCell < end; idxCell++) { // divide the outer loop, by state-action cell
            w = 0;
            /* for all non zero probability nextState for <s,a> tuple*/
            for (cl_ulong s2 = h_nextCellPos[idxCell]; s2 < h_nextCellPos[idxCell + 1]; s2++) {
                w += h_probability[s2] * h_V[h_nextStatePos[s2]];
            }
            h_Q[idxCell] = h_R[idxCell] + GAMMA * w;
        }
    }
};

/*****************************************************************************
 * Parallel For (TBB only CPUs)
 * **************************************************************************/
class MYParallelFor {
    ValueIterationPlanifier *vip;
public:
    /*Class Constructor*/
    MYParallelFor(ValueIterationPlanifier *vip_) {
        vip = vip_;
    }

    /*Operator function*/
    void operator()( const blocked_range<cl_ulong>& range ) const {
        vip->OperatorCPU(range.begin(), range.end());
    }
};

/*This Function launches the parallel for pattern only on CPU cores, based on TBB template*/
void ParallelFORCPUs(size_t start, size_t end, ValueIterationPlanifier *vip ) {
    MYParallelFor pf(vip);
    parallel_for( blocked_range<cl_ulong>( start, end ), pf);
}

/***************************************************************************** 
 * Raw Tasks
 * ************************************************************************** //corbera 
template <class V>
class GPUTask: public task {
public:
    V *vi;
    cl_ulong begin;
    cl_ulong end;

    GPUTask<V>(V *vi, cl_ulong begin, cl_ulong end) : vi(vi), begin(begin), end(end) {}

    //Override virtual function task::execute
    task* execute() {
        #ifdef USEBARRIER
        //cerr << "GPU before barrier1" << endl;
        getLockGPU();
        //cerr << "GPU after barrier1" << endl;
        #endif
        #ifdef PJTRACER
        tracer->gpuStart();
        #endif

        tick_count start = tick_count::now();
        vi->sendObjectToGPU(begin, end, NULL);
        vi->OperatorGPU(begin, end, NULL);
        vi->getBackObjectFromGPU(begin, end, NULL);
        clFinish(command_queue);
        tick_count finish = tick_count::now();
        gpuThroughput = (end -  begin) / ((finish-start).seconds()*1000);

        #ifdef PJTRACER
        tracer->gpuStop();
        #endif
        #ifdef USEBARRIER
        //cerr << "GPU antes de barrier2" << endl;
        freeLockGPU();
        //cerr << "GPU despues de barrier2" << endl;
        getLockCPU();
        freeLockCPU();
        #endif
        return NULL;
    }
};

template <class V>
class CPUTask: public task {
public:
    V *vi;
    cl_ulong begin;
    cl_ulong end;

    CPUTask<V>(V *vi, cl_ulong begin, cl_ulong end) : vi(vi), begin(begin), end(end) {}

    // Overrides virtual function task::execute
    task* execute() {
#ifdef USEBARRIER
        //cerr << "CPU antes de barrier1" << endl;
        getLockCPU();
        //cerr << "CPU despues de barrier1" << endl;
#endif
        tick_count start = tick_count::now();
        ParallelFORCPUs(begin, end, vi);
        tick_count finish = tick_count::now();
        cpuThroughput = (end -  begin) / ((finish-start).seconds()*1000);

#ifdef USEBARRIER
        //cerr << "CPU antes de barrier2" << endl;
        freeLockCPU();
        //cerr << "CPU despues de barrier2" << endl;
        getLockGPU();
        freeLockGPU();
#endif
        return NULL;
    }
};

template <class V>
class ROOTTask : public task {
public:
    V *vi;
    cl_ulong begin;
    cl_ulong end;

    ROOTTask<V>(V *vi, cl_ulong begin, cl_ulong end) : vi(vi), begin(begin), end(end) {}

    task *execute() {
        #ifdef USEBARRIER
        //cerr << "before barrier_init" << endl;
        barrier_init();
        //cerr << "after barrier_init" << endl;
        #endif
        GPUTask<V>& a = *new( allocate_child() ) GPUTask<V>(vi, begin, begin + chunkGPU);
        CPUTask<V>& b = *new( allocate_child() ) CPUTask<V>(vi, begin + chunkCPU, end);

        // Set ref_count to "2 children + 1 for teh wait"
        set_ref_count(3);

        //Start b
        spawn(b);
        // Start a running and wait for all children (a and b).
        spawn_and_wait_for_all(a);
        return NULL;
    }
};
 * */ //corbera

#endif //VALUEITERATION_VALUEITERATIONPLANIFIER_H
