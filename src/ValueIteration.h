//
// Created by pingu on 26/03/17.
//

#ifndef VALUEITERATION_VALUEITEARTION_H
#define VALUEITERATION_VALUEITEARTION_H
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif
/** Value iteration algorithm */
cl_int valueIteration(TransitionMatrix &T, cl_float* R, cl_float* Q, cl_float* V,cl_float* Z, cl_int* P, cl_float gamma, cl_float d);
#endif //VALUEITERATION_VALUEITEARTION_H
