//
// Created by root on 4/1/20.
//

#ifndef VALUEITERATION_VIFUNCTORS_H
#define VALUEITERATION_VIFUNCTORS_H

#include "CL/sycl.hpp"

//using namespace cl::sycl;

// Few useful acronyms; 'using namespace cl::sycl;' also helps.
constexpr auto sycl_read = cl::sycl::access::mode::read;
constexpr auto sycl_write = cl::sycl::access::mode::write;
constexpr auto sycl_global_buffer = cl::sycl::access::target::global_buffer;


static void report_time(const std::string &msg, cl::sycl::event e) {
    cl::sycl::cl_ulong time_start = e.get_profiling_info<cl::sycl::info::event_profiling::command_start>();
    cl::sycl::cl_ulong time_end = e.get_profiling_info<cl::sycl::info::event_profiling::command_end>();
    double elapsed = (time_end - time_start) / 1e6;
    std::cout << msg << elapsed << " milliseconds" << std::endl;
}

class PolicyEvaluationFunctorSYCL {
private:
    unsigned long offset;
    cl::sycl::accessor<cl_ulong, 1, sycl_read, sycl_global_buffer> b_nextCellPos_acc;
    cl::sycl::accessor<cl_float, 1, sycl_read, sycl_global_buffer> b_probabilities_acc;
    cl::sycl::accessor<cl_float, 1, sycl_read, sycl_global_buffer> b_V_acc;
    cl::sycl::accessor<cl_ulong, 1, sycl_read, sycl_global_buffer> b_nextStatePos_acc;
    cl::sycl::accessor<cl_float, 1, sycl_read, sycl_global_buffer> b_R_acc;
    cl::sycl::accessor<cl_float, 1, sycl_write, sycl_global_buffer> b_Q_acc;

public:
    PolicyEvaluationFunctorSYCL(
            unsigned long offset_,
            cl::sycl::accessor<cl_ulong, 1, sycl_read, sycl_global_buffer> &b_nextCellPos_acc_,
            cl::sycl::accessor<cl_float, 1, sycl_read, sycl_global_buffer> &b_probabilities_acc_,
            cl::sycl::accessor<cl_float, 1, sycl_read, sycl_global_buffer> &b_V_acc_,
            cl::sycl::accessor<cl_ulong, 1, sycl_read, sycl_global_buffer> &b_nextStatePos_acc_,
            cl::sycl::accessor<cl_float, 1, sycl_read, sycl_global_buffer> &b_R_acc_,
            cl::sycl::accessor<cl_float, 1, sycl_write, sycl_global_buffer> &b_Q_acc_
    ):  offset(offset_), b_nextCellPos_acc(b_nextCellPos_acc_),
        b_probabilities_acc(b_probabilities_acc_), b_V_acc(b_V_acc_),
        b_nextStatePos_acc(b_nextStatePos_acc_), b_R_acc(b_R_acc_), b_Q_acc(b_Q_acc_) {}

    void operator()(cl::sycl::id<1> index) {

        float w = 0.0;
        float gamma = 0.1;
        unsigned long /*s,*/ s2, myCellPos;

        myCellPos = index.get(0) + offset;
        // evaluate the actions for the current state
        // for all non zero probability nextState for <s,a> tuple (i.e. all elements from one cell of T)
        for (s2 = b_nextCellPos_acc[myCellPos]; s2 < b_nextCellPos_acc[myCellPos + 1]; s2++) {
            w += b_probabilities_acc[s2] * b_V_acc[b_nextStatePos_acc[s2]];
        }
        b_Q_acc[myCellPos] =  b_R_acc[myCellPos] + gamma * w ; //w * gamma + R[myCellPos];

    }

};

class PolicyEvaluationFunctorUSM {
private:
    cl_ulong offset;
    cl_ulong* nextCellPos;
    cl_float* probability;
    cl_float* V;
    cl_ulong* nextStatePos;
    cl_float* R;
    cl_float* Q;

public:
    PolicyEvaluationFunctorUSM(cl_ulong offset_,
                            cl_ulong* nextCellPos_,
                            cl_float* probability_,
                            cl_float* V_,
                            cl_ulong* nextStatePos_,
                            cl_float* R_,
                            cl_float* Q_
    ):  offset(offset_), nextCellPos(nextCellPos_),
        probability(probability_), V(V_),
        nextStatePos(nextStatePos_), R(R_), Q(Q_) {}

    void operator()(cl::sycl::id<1> index) {
        float w = 0.0;
        float gamma = 0.1;
        cl_ulong /*s,*/ s2, myCellPos;

        myCellPos = index.get(0) + offset; // myCellPos < end ??
        // evaluate the value of the actions for the current state
        // for all non zero probability nextState for <s,a> tuple (i.e. all elements from one cell of T)
        for (s2 = nextCellPos[myCellPos]; s2 < nextCellPos[myCellPos + 1]; s2++) {
            w += probability[s2] * V[nextStatePos[s2]];
        }
        Q[myCellPos] =  R[myCellPos] + gamma * w ; //w * gamma + R[myCellPos];
    }
};


#endif //VALUEITERATION_VIFUNCTORS_H
