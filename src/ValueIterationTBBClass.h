//
// Created by dconstantinescu on 6/06/17.
//

#ifndef VALUEITERATION_VALUEITERATIONTBB_H
#define VALUEITERATION_VALUEITERATIONTBB_H


#include <CL/cl.h>
#include "../navigation_mdp/TransitionMatrix.h"
#include "../navigation_mdp/Common.h"
#include <tbb/task_scheduler_init.h>
#include <tbb/blocked_range.h>
#include <tbb/parallel_for.h>
using namespace tbb;
class ValueIterationTBB {
    cl_float const *probability;
    cl_ulong const *nextCellPos;
    cl_ulong const *nextStateId;
    cl_float const * R;
    cl_float * Q;
    cl_float* V;
    cl_float* Z;
    cl_int* P;
    cl_float gamma;
public:
    ValueIterationTBBClass(cl_float const *p_probability,
                      cl_ulong const *p_nextCellPos,
                      cl_ulong const *p_nextStateId,
                      cl_float const * p_R,
                      cl_float* p_Q,
                      cl_float* p_V,
                      cl_float* p_Z,
                      cl_int* p_P,
                      cl_float p_gamma
                      )                      
                    : probability(p_probability),
                      nextCellPos(p_nextCellPos),
                      nextStateId(p_nextStateId),
                      R(p_R),
                      Q(p_Q),
                      V(p_V),
                      Z(p_Z),
                      P(p_P),
                      gamma(p_gamma){}

    void operator() (const blocked_range<cl_ulong> &r)  const {
        cl_ulong end = r.end();
        for (cl_ulong s = r.begin(); s != end; s++) {
            for (int a = 0; a < NX; a++) {
                cl_float w = 0;
                cl_ulong idx = s * NX + a; // go to the next cell of the T/Q/R matrix
                // pol eval
                for (cl_ulong s2 = nextCellPos[idx]; s2 < nextCellPos[idx + 1]; s2++) {
                    w += probability[s2] * V[nextStateId[s2]];
                }
                Q[idx] = R[idx] + gamma * w;
                // pol impr
                if (Q[idx] - Z[s] > EPSILON) {
                    Z[s] = Q[idx];
                    P[s] = a;
                }
            }
        }
    }
};

#endif //VALUEITERATION_VALUEITERATIONTBB_H
