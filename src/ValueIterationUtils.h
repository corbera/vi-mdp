//
// Created by denisa on 26/03/17.
//

#ifndef VALUEITERATION_VALUEITERATIONUTILS_H
#define VALUEITERATION_VALUEITERATIONUTILS_H
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
// #include <CL/cl2.hpp>
#endif
#include <iostream>
using namespace std;

/** Return norm2 of two vectors of size n*/
float norm2VectorDifference(float Z[], float* , cl_int n);

/** Display the values of a 1D array separated by a tab */
//! OBS: template functions must be defined in the .h file, not .cpp!!!
template<typename Type> void print1DimArray(Type *array, cl_int size) {
    cout << std::endl;
    for (cl_int i = 0; i < size ; ++i) {
        cout << array[i] << "\t";
    }
    cout << std::endl;
}

/** Display the first num values not null of a 1D array of size "size" separated by a tab */
//! OBS: template functions must be defined in the .h file, not .cpp!!!
template<typename Type> void print1DimArray(Type *array, cl_int size, cl_int num) {
    cout << "\nprinting: " << std::endl;
    cl_int count = 0;
    for (cl_int i = 0; i < size && count < num; ++i) {
        if (array[i]) {
            cout << i << ": " << array[i] << "\t";
            count++;
        }
    }
    cout << std::endl;
}

/** Compute the rewards matrix*/
void setRewardMatrix(TransitionMatrix &T, float* R, float hitObstacleR, float reachTargetR, float regularStateR, bool verbose);

/** Return the value and position of best action for each state */
void bestPolicyValue(float **Q, float *Z, cl_int *P, cl_int s);

// int find_devices() ;



#endif //VALUEITERATION_VALUEITERATIONUTILS_H
