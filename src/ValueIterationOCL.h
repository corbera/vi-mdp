//
// Created by dconstantinescu on 17/04/17.
//
#ifndef VALUEITERATION_VALUEITERATIONOCL_H
#define VALUEITERATION_VALUEITERATIONOCL_H
#include "../navigation_mdp/TransitionMatrix.h"
#include "../navigation_mdp/Common.h"
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif
/**
 * Initialize and launch ocl kernel(s)
 * @param transitionMatrix
 * @param h_R
 * @param h_Q
 * @param h_P
 * @param h_V
 * @return
 */
int ValueIterationOCL(float &runtime_k1, float &energy_k1);
/** Initialize gpu device*/
void prepareOclEnvironment();
/** Allocate OpenCL memory  and write the kernel input buffers */
cl_int allocate();
/** Deallocate OpenCL memory*/
void deallocateMemory();

#endif //VALUEITERATION_VALUEITERATIONOCL_H
