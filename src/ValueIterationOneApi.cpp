//
// Created by Denisa on 17/04/17.
//


#include "ValueIterationOneApi.h"

#include <iostream>
#include <cstring>
#include <limits>
#include <assert.h>
#include "../utils/timing.h"
// #include "ValueIterationUtils.h"
#include "../navigation_mdp/Common.h"
#include <math.h>
#include <string>

#include "CL/sycl.hpp"
#include "../utils/device_selector.hpp"

// Few useful acronyms; 'using namespace cl::sycl;' also helps.
constexpr auto sycl_read = cl::sycl::access::mode::read;
constexpr auto sycl_write = cl::sycl::access::mode::write;
constexpr auto sycl_global_buffer = cl::sycl::access::target::global_buffer;

using namespace cl::sycl;
using namespace std;


#include "Norm2.h"
#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>


#define MEASURE_K1

#ifdef MEASURE_K1
#include "cpucounters.h"
#include "utils.h"
#include "tbb/tick_count.h"
#endif



// Host buffers
extern cl_float*    h_probability;
extern cl_ulong*    h_nextCellPos;
extern cl_ulong*    h_nextStatePos;
extern cl_float*    h_R;
extern cl_float*    h_Q; // = (cl_float*)calloc(NS*NX,sizeof(cl_float));
extern cl_int*      h_P;
extern cl_float*    h_V;//= (cl_float*)calloc(NS,sizeof(cl_float));
extern cl_float*    h_Z;
extern cl_ulong probabilitySize;

static  size_t WORK_GROUP_SIZE = 256;

extern cl_ulong  NS, NR, NG, ND, NA, NX, NT;

//#define PCM_BROADWELL

#ifdef PCM_BROADWELL
#include "cpucounters.h"
#include <tbb/tick_count.h>
using namespace tbb;
#endif


static void report_time(const std::string &msg, cl::sycl::event e) {
  cl::sycl::cl_ulong time_start = e.get_profiling_info<cl::sycl::info::event_profiling::command_start>();
  cl::sycl::cl_ulong time_end = e.get_profiling_info<cl::sycl::info::event_profiling::command_end>();
  double elapsed = (time_end - time_start) / 1e6;
  std::cout << msg << elapsed << " milliseconds" << std::endl;
}

class PolicyEvaluationFunctor {
    private:
        unsigned long offset; 
        cl::sycl::accessor<cl_ulong, 1, sycl_read, sycl_global_buffer> b_nextCellPos_acc;
        cl::sycl::accessor<cl_float, 1, sycl_read, sycl_global_buffer> b_probabilities_acc;
        cl::sycl::accessor<cl_float, 1, sycl_read, sycl_global_buffer> b_V_acc;
        cl::sycl::accessor<cl_ulong, 1, sycl_read, sycl_global_buffer> b_nextStatePos_acc;
        cl::sycl::accessor<cl_float, 1, sycl_read, sycl_global_buffer> b_R_acc;
        cl::sycl::accessor<cl_float, 1, sycl_write, sycl_global_buffer> b_Q_acc;
    
    public:
    PolicyEvaluationFunctor(
        unsigned long offset_, 
        cl::sycl::accessor<cl_ulong, 1, sycl_read, sycl_global_buffer> &b_nextCellPos_acc_,
        cl::sycl::accessor<cl_float, 1, sycl_read, sycl_global_buffer> &b_probabilities_acc_,
        cl::sycl::accessor<cl_float, 1, sycl_read, sycl_global_buffer> &b_V_acc_,
        cl::sycl::accessor<cl_ulong, 1, sycl_read, sycl_global_buffer> &b_nextStatePos_acc_,
        cl::sycl::accessor<cl_float, 1, sycl_read, sycl_global_buffer> &b_R_acc_,
        cl::sycl::accessor<cl_float, 1, sycl_write, sycl_global_buffer> &b_Q_acc_
    ):  offset(offset_), b_nextCellPos_acc(b_nextCellPos_acc_), 
        b_probabilities_acc(b_probabilities_acc_), b_V_acc(b_V_acc_), 
        b_nextStatePos_acc(b_nextStatePos_acc_), b_R_acc(b_R_acc_), b_Q_acc(b_Q_acc_) {}

    void operator()(cl::sycl::id<1> index) {
        
        float w = 0.0;
        float gamma = 0.1;
        unsigned long /*s,*/ s2, myCellPos;

        myCellPos = index.get(0) + offset;
        // evaluate the actions for the current state
        // for all non zero probability nextState for <s,a> tuple (i.e. all elements from one cell of T) 
        for (s2 = b_nextCellPos_acc[myCellPos]; s2 < b_nextCellPos_acc[myCellPos + 1]; s2++) {
            w += b_probabilities_acc[s2] * b_V_acc[b_nextStatePos_acc[s2]];
        }
        b_Q_acc[myCellPos] =  b_R_acc[myCellPos] + gamma * w ; //w * gamma + R[myCellPos];
    
    }

};

int ValueIterationOneApi(  float& runtime_k1, float& energy_k1, float offload_ratio) {
    bool notReady = true;
    cl_float norm = 0;
    ulong nrIter = 0;
    ulong idx,s;
    cl_int a;

    float gpu_ratio;

    // Create a device selector which rates available devices in the preferred order
    // for the runtime to select the highest rated device
    // default_selector device_selector; 
    queue q_cpu(cpu_selector{});
    queue q_gpu(gpu_selector{});
   // MyDeviceSelector sel; 
       
    // Using these events to time command group execution
    cl::sycl::event e1, e2, e3;

    try {
        // Create a command queue using the device selector above, and request profiling
        //auto propList = cl::sycl::property_list{ cl::sycl::property::queue::enable_profiling() };
        //cl::sycl::queue q(sel, propList);

        // See what device was actually selected for this queue.
        std::cout << "Running on " << q_gpu.get_device().get_info<cl::sycl::info::device::name>() << "\n";


        // Create SYCL buffer representing source data .
        //
        // By default, this buffers will be created with global_buffer access
        // target, which means the buffer "projection" to the device (actual
        // device memory chunk allocated or mapped on the device to reflect
        // buffer's data) will belong to the SYCL global address space - this
        // is what host data usually maps to. Other address spaces are:
        // private, local and constant.
        // Notes:
        // - access type (read/write) is not specified when creating a buffer -
        //   this is done when actuall accessor is created
        // - there can be multiple accessors to the same buffer in multuple command
        //   groups
        // - 'h_x' pointer was passed to the constructor, so this host memory will
        //   be used for "host projection", no allocation will happen on host
        cl::sycl::buffer<cl_ulong, 1> b_nextCellPos(h_nextCellPos, cl::sycl::range<1>(NX * NS + 1)); //, range<1>(NX * NS + 1), {cl::sycl::property::buffer::use_host_ptr()});
        cl::sycl::buffer<cl_float, 1> b_probabilities(h_probability, cl::sycl::range<1>(probabilitySize));
        cl::sycl::buffer<cl_float, 1> b_V(h_V, cl::sycl::range<1>(NS));
        cl::sycl::buffer<cl_ulong, 1> b_nextStatePos(h_nextStatePos, cl::sycl::range<1>(probabilitySize));
        cl::sycl::buffer<cl_float, 1> b_R(h_R, cl::sycl::range<1>(NS * NX));
        cl::sycl::buffer<cl_int, 1> b_P(h_P, cl::sycl::range<1>(NS));
        cl::sycl::buffer<cl_float, 1> b_Z(h_Z, cl::sycl::range<1>(NS));

        // This is the output buffer device writes to
        cl::sycl::buffer<cl_float, 1> b_Q(h_Q, cl::sycl::range<1>(NS * NX));

        //std::cout << "Created buffers" << "\n";

        // // policy improvement functor
        // auto f = [&](const blocked_range<size_t>& r)
        // {  
        //     for (cl_ulong s = r.begin(); s != r.end(); ++s) {
        //         for (int a = 0; a < NX; a++) {
        //             size_t idx = s * NX + a;
        //             if (h_Q[idx] - h_Z[s] > EPSILON) {
        //                 h_Z[s] = h_Q[idx];
        //                 h_P[s] = a;
        //                 //printf("h_Z[%d] = %f \n", s, h_Z[s]);
                        
        //             }
        //         }
        //     }
        // };


        #ifdef MEASURE_K1
        float rt = 0.0;
        //Energy Counters
        PCM * pcm;
        SystemCounterState sstate1, sstate2;
        //timing
        tick_count start, end;
        //Initializing PCM library
        pcm = PCM::getInstance();
        pcm->resetPMU();
        if (pcm->program() != PCM::Success){
            std:cout << "Error in PCM library initialization" << std::endl;
            exit(-1);
        }
        #endif //MEASURE_K1

        while(notReady) {

            #ifdef MEASURE_K1
                tick_count bt, et;
                bt = tick_count::now();
                sstate1 = pcm->getSystemCounterState();
            #endif MEASURE_K1
            //code to be measured - K1
            
            /** Policy evaluation on device*/
            // Submit  command group to gpu device. The kernel is represented as a functor
    {
        if (offload_ratio > 0) {
            e2 = q_gpu.submit([&](cl::sycl::handler& cgh) {
                // create accessors to read  the buffers on the device
                auto b_nextCellPos_acc = b_nextCellPos.get_access<sycl_read>(cgh);
                auto b_probabilities_acc = b_probabilities.get_access<sycl_read>(cgh);
                auto b_V_acc = b_V.get_access<sycl_read>(cgh);
                auto b_nextStatePos_acc = b_nextStatePos.get_access<sycl_read>(cgh);
                auto b_R_acc = b_R.get_access<sycl_read>(cgh);

//                accessor<cl_ulong, 1, access::mode::read, access::target::constant_buffer> b_nextCellPos_acc(b_nextCellPos, cgh);

                // create accessors to update  the buffers on the device
                auto b_Q_acc = b_Q.get_access<sycl_write>(cgh);

                //std::cout << " accessors created" << "\n";

                // execute the kernel
                unsigned long offset = 0;
                unsigned long elements = ceil(NS*NX * offload_ratio);

                PolicyEvaluationFunctor kernel( offset,
                                                b_nextCellPos_acc,
                                                b_probabilities_acc,
                                                b_V_acc,
                                                b_nextStatePos_acc,
                                                b_R_acc,
                                                b_Q_acc );
                //** functor kernel */
                cgh.parallel_for(cl::sycl::range<1>(elements), kernel);
            });


        }

        if (offload_ratio < 1) {


        e1 = q_cpu.submit([&](cl::sycl::handler& cgh) {
                // create accessors to read  the buffers on the device
                auto b_nextCellPos_acc = b_nextCellPos.get_access<sycl_read>(cgh);
                auto b_probabilities_acc = b_probabilities.get_access<sycl_read>(cgh);
                auto b_V_acc = b_V.get_access<sycl_read>(cgh);
                auto b_nextStatePos_acc = b_nextStatePos.get_access<sycl_read>(cgh);
                auto b_R_acc = b_R.get_access<sycl_read>(cgh);
                // create accessors to update  the buffers on the device
                auto b_Q_acc = b_Q.get_access<sycl_write>(cgh);

                //std::cout << " accessors created" << "\n";

                // execute the kernel
                unsigned long offset = ceil(NS*NX * offload_ratio);
                unsigned long elements = NS*NX - offset;

                PolicyEvaluationFunctor kernel( offset,
                                                b_nextCellPos_acc,
                                                b_probabilities_acc,
                                                b_V_acc,
                                                b_nextStatePos_acc,
                                                b_R_acc,
                                                b_Q_acc );
                //** functor kernel */
                cgh.parallel_for(cl::sycl::range<1>(elements), kernel);

                //** lambda kernel */
                // cgh.parallel_for<class kernel>(cl::sycl::range<1>(elements), [&](id<1> index) {
                    
                //     float w = 0.0;
                //     float gamma = 0.1;
                //     unsigned long /*s,*/ s2, myCellPos;

                //     myCellPos = index.get(0); //+offset;
                //     // evaluate the actions for the current state
                //     // for all non zero probability nextState for <s,a> tuple (i.e. all elements from one cell of T) 
                //     for (s2 = b_nextCellPos_acc[myCellPos]; s2 < b_nextCellPos_acc[myCellPos + 1]; s2++) {
                //         w += b_probabilities_acc[s2] * b_V_acc[b_nextStatePos_acc[s2]];
                //     }
                //     b_Q_acc[myCellPos] =  b_R_acc[myCellPos] + gamma * w ; //w * gamma + R[myCellPos];
                    
                // });
            });  
        }
            //std::cout << " waiting for kernerl" << "\n";
        }
             q_gpu.wait(); 
             q_cpu.wait();
            //q.wait_and_throw();

            #ifdef MEASURE_K1
            et = tick_count::now();
            sstate2 = pcm->getSystemCounterState();
            runtime_k1 += (et-bt).seconds();
            energy_k1 += getConsumedJoules(sstate1, sstate2);
            //cout << "runtime & total_energy K1:" << runtime_k1 << "\t" << energy_k1 << endl;
            #endif MEASURE_K1

            //std::cout << " done waiting for kernerl" << "\n";
            
            /** Policy improvement */
            //select the policy with the best value (max_a Q(s,a)) for every state

            e3 = q_cpu.submit([&](cl::sycl::handler& cgh) {
                // create accessors to read  the buffers on the host
                auto h_Q_acc = b_Q.get_access<sycl_read>(cgh); 
                auto b_Z_acc = b_Z.get_access<sycl_write>(cgh);
                auto b_P_acc = b_P.get_access<sycl_write>(cgh);

                cl::sycl::cl_ulong ns(NS), nx(NX);

                // execute the kernel
                cgh.parallel_for<class kernel2>(range<1>(ns), [=](id<1> index) {
                    cl_ulong  s = index.get(0);
                    for (int a = 0; a < nx; a++) {
                        size_t idx = s * nx + a;
                        if (h_Q_acc[idx] - b_Z_acc[s] > EPSILON) {
                            b_Z_acc[s] = h_Q_acc[idx];
                            b_P_acc[s] = a;
                            //printf("h_Z[%d] = %f \n", s, h_Z[s]);
                            
                        }
                    }

                });
            }); 

            q_cpu.wait();



            auto b_Z_acc = b_Z.get_access<sycl_read>();
            auto b_V_acc = b_V.get_access<sycl_write>();

 

            //print1DimArray(b_P_acc, NS, 20);

            //std::cout << " done with policy improvement" << "\n";

            //Norm2
            //parallel_reduce( blocked_range<size_t>(0,NS,1), n2sum);
            norm = parallel_reduce(blocked_range<size_t>(0,NS), 0.0f,
                                [&](blocked_range<size_t> r, double total_norm)
                                {
                                    for (size_t i=r.begin(); i<r.end(); ++i)
                                    {
                                        total_norm += pow((b_Z_acc[i]-b_V_acc[i]),2);
                                    }

                                    return total_norm;
                                }, std::plus<double>() );

            norm = sqrt(norm);
            //std::cout <<"\nnorm: " << norm << std::endl;
            //norm = sqrt(n2sum.my_sum);



            notReady = (norm > g) && nrIter < MAX_ITER;
            norm = 0;
            //swap old V with new V (Z)

            /*for (int i=0; i<NS; i++)
                b_V_acc[i] = b_Z_acc[i];
                */
            memcpy(h_V, h_Z, NS*sizeof(cl_float));
    
            nrIter++; // #iters to convergence

            //std::cout << "\nValtItOCL #iter: " << nrIter << "\n";
            


        } // while
    }
    catch (cl::sycl::exception e) {
        std::cout << "SYCL exception caught: " << e.what() << std::endl;
        return 1;
    }

    // report execution times:
    // report_time(" kerne1 time: ", e1);
    // report_time(" kerne2 time: ", e2);

    return nrIter;

}
