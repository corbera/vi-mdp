//
// Created by dconstantinescu on 5/05/17.
//

#ifndef VALUEITERATION_VALUEITERATIONOMP1D_H
#define VALUEITERATION_VALUEITERATIONOMP1D_H

#include "../navigation_mdp/TransitionMatrix.h"
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif
cl_int valueIterationOMP1D(cl_float* probability, cl_ulong* nextStateId, cl_ulong* nextCellPos, cl_float* R, cl_float* Q, cl_float* V/*, cl_float* Z*/, cl_int* P, cl_float gamma, cl_float d);

#endif //VALUEITERATION_VALUEITERATIONOMP1D_H
