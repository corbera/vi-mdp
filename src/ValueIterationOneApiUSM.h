#ifndef VALUEITERATION_VI_ONEAPI_USM_H
#define VALUEITERATION_VI_ONEAPI_USM_H

#include "../navigation_mdp/TransitionMatrix.h"
#include "../navigation_mdp/Common.h"
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif
int ValueIterationOneApiUSM(  float& runtime_k1, float& energy_k1) ;

#endif