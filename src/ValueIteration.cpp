//
// Created by pingu on 26/03/17.
//
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif

#include "../navigation_mdp/TransitionMatrix.h"
#include "ValueIterationUtils.h"
#include "ValueIteration.h"
#include <iostream>
#include <chrono>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>

using namespace std;

extern cl_ulong  NS, NR, NG, ND, NA, NX, NT;

float norm2VectorDifference(float Z[], float* V, cl_int n) {
    float norm = 0;
    for (cl_int i = 0; i < n; i++) {
        norm += pow((Z[i]-V[i]),2);
    }
    return sqrt(norm);
    //return norm;
}

cl_int valueIteration(TransitionMatrix &T, cl_float* R, cl_float* Q, cl_float* V, cl_float* Z, cl_int* P, cl_float gamma, cl_float d) {

    //cl_float g = 0.0001; // d * (1 - gamma_)/(2 * gamma_);
    cl_float w;
    bool notReady = true;
    cl_float normVZ;

    //ofstream seqVFile("../data/PolicyValuesDiffPerIteration.txt");
    //seqVFile.clear();

    cl_ulong nrIter = 0;
    cl_ulong idx;
    while(notReady) {
        nrIter ++;
        /** Policy evaluation */
        for (cl_ulong s = 0; s < NS; s++) {
            for (cl_ulong a = 0; a < NX; a++) {
                w = 0;
                idx = s * NX + a;
                //for (cl_ulong s2 = 0; s2 < NS; s2++) {
                for(auto pair : T.transProbMap[s][a]) {
                    //w += T.getProbabilitySAS(s,a,s2) * V[s2];
                    w += pair.second * V[pair.first];
                }
                Q[idx] = R[idx] + gamma * w;
            }
        }
        /** Policy improvement */
        for (cl_int s = 0; s < NS; s++) { // states
            //bestPolicyValue(Q, Z, P, s);
            //select the policy with the best value
            for (cl_int a = 0; a < NX; a++) { // actions
                idx = s * NX + a;
                //if (Q[s][a] > Z[s]) {
                if (Q[idx] - Z[s] > EPSILON)  {
                    Z[s] = Q[idx];
                    P[s] = a;
                }
            }
        }
        normVZ = norm2VectorDifference(Z, V, NS);
        //seqVFile << normVZ << "\t";
        notReady = (normVZ >= g) && nrIter <= MAX_ITER;
        for (cl_int s = 0; s < NS; ++s) { // states
            V[s] = Z[s];
        }
    }
    //cout << "\nThe seq algorithm converged in " << nrIter << " iterations.\n";
    //seqVFile.flush();
    //seqVFile.close();
    return nrIter;
}
