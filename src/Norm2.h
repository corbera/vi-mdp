#ifndef NORM2_H
#define NORM2_H

#include <math.h>
#include <tbb/parallel_reduce.h>
#include <tbb/blocked_range.h>

using namespace tbb;

/* Class for norm2 TBB parallel_reduce and copy values from b vector to a vector
Use:
        Norm2 n2sum(a,b);
        parallel_reduce( blocked_range<size_t>(0,N), n2sum);  
        norm = sqrt(n2sum.my_sum);
*/
class Norm2 {
    float* my_a;
    float* my_b;
public:
    float my_sum;
    void operator()( const blocked_range<size_t>& r ) {
        float *a = my_a;
        float *b = my_b;
        //float sum = 0;//my_sum; corbera //denisa: this computes the norm correctly
        float sum = my_sum; // corbera // denisa: like this, VI does not converge
        size_t end = r.end();
        for( size_t i=r.begin(); i!=end; ++i )  {
            sum += pow((a[i]-b[i]),2); 
        }
        my_sum = sum;    
    }
 
    Norm2( Norm2& x, split ) : my_a(x.my_a), my_b(x.my_b), my_sum(0) {}
 
    void join( const Norm2& y ) {my_sum+=y.my_sum;}
             
    Norm2(float *a, float *b) :
        my_a(a), my_b(b), my_sum(0)
    {}
};

#endif // NORM2_H
