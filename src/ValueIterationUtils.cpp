//
// Created by denisa on 26/03/17.
//
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif

#include <iostream>
#include "../navigation_mdp/TransitionMatrix.h"
#include "../navigation_mdp/RobotState.h"
#include "ValueIterationUtils.h"



float norm2VectorDifference(float Z[], float* V, cl_int n) {
    float norm = 0;
    for (cl_int i = 0; i < n; i++) {
        norm += pow((Z[i]-V[i]),2);
    }
    return sqrt(norm);
    //return norm;
}

void setRewardMatrix(TransitionMatrix &T, float* R, float hitObstacleR, float reachTargetR, float regularStateR,
                     bool verbose) {
    cl_int stateIndexesTuple[STATE_DIM]; // tt=0,rr=1,gg=2,dd=3,aa=5 // tt=0,gg1=1,gg2=2, gg3=3,dd=4,aa=5
    float* Rs = (float*)calloc(NS,sizeof(float));

    // compute R(s)
    for (cl_ulong s = 0; s < NS; ++s) {
        RobotState::getRobotStateTupleFromId(s, stateIndexesTuple);
        if(stateIndexesTuple[4] == 0) // target
            Rs[s] = reachTargetR;
        else if (stateIndexesTuple[2] == 0 || stateIndexesTuple[3] == 0 || stateIndexesTuple[4] == 0) // obstacle
            Rs[s] = hitObstacleR;
        else
            Rs[s] = regularStateR;
    }

    if (verbose) {
        cout << "Computing rewards matrix R\n-----------------------\n";
        cout << "\nRs: ";
        print1DimArray(Rs, NS);
        cout << "R: ";
    }

    cl_int ind;
    cl_ulong s;
    // compute R(s,a)
    cout << "Computing R(S,A)" << "\n";
    for (s = 0; s < NS; ++s) {
        for (cl_int a = 0; a < NX; ++a) {
            ind = s * NX + a;
            R[ind] = 0;
/*          for (cl_int s2 = 0; s2 < NS ; ++s2) {
                R[s][a] += Rs[s2]*T.getProbabilitySAS(s,a,s2);
            }
*/
            for(auto pair : T.transProbMap[s][a]) {
                R[ind] = R[ind]  +  pair.second * Rs[pair.first];
            }
            if (verbose)
                cout << R[ind] << "\t";
        }
        if (verbose)
            cout << "\n";
    }
    free(Rs);
}

void bestPolicyValue(float **Q, float *Z, cl_int *P, cl_int s) {
    for (cl_int a = 0; a < NX; a++) {
        if (Q[s][a] > Z[s]) {
            Z[s] = Q[s][a];
            P[s] = a;
        }
    }
}

// int find_devices() {
//     vector<cl::Device> devices;
//     std::vector<cl::Platform> platforms; // get all platforms
//     std::vector<cl::Device> devices_available;
//     int n = 0; // number of available devices
//     cl::Platform::get(&platforms);
//     for(int i=0; i<(int)platforms.size(); i++) {
//         devices_available.clear();
//         platforms[i].getDevices(CL_DEVICE_TYPE_ALL, &devices_available);
//         if(devices_available.size()==0) continue; // no device found in plattform i
//         for(int j=0; j<(int)devices_available.size(); j++) {
//             n++;
//             devices.push_back(devices_available[j]);
//         }
//     }
//     if(platforms.size()==0||devices.size()==0) {
//         std::cout << "Error: There are no OpenCL devices available!" << std::endl;
//         return -1;
//     }
//     for(int i=0; i<n; i++) std::cout << "ID: " << i << ", Device: " << devices[i].getInfo<CL_DEVICE_NAME>() << std::endl;
//     return n; // return number of available devices
// }