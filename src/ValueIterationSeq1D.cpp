//
// Created by dconstantinescu on 4/04/17.
//
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif
#include <limits>
#include "ValueIterationSeq1D.h"
//#include "ValueIterationUtils.h"
#include <math.h>
#include "../utils/timing.h"
#include "../navigation_mdp/Common.h"

#define MEASURE_K1

#ifdef MEASURE_K1
#include "cpucounters.h"
#include "utils.h"
#include "tbb/tick_count.h"
#endif

using namespace tbb;

using namespace std;

extern cl_ulong  NS, NR, NG, ND, NA, NX, NT;

cl_int valueIterationSeq1D(cl_float* h_probability, cl_ulong* h_nextStatePos, cl_ulong* h_nextCellPos, cl_float* h_R,
                           cl_float* h_Q, cl_float* h_V, cl_int* h_P, cl_float gamma_, cl_float d, float& runtime_k1,
                           float& energy_k1) {

    cl_float *h_Z = (cl_float*)calloc(NS,sizeof(cl_float));
    //cl_float g = 0.0001; // d * (1 - gamma_)/(2 * gamma_);
    double w;
    bool notReady = true;


    cl_ulong nrIter = 0;
    cl_ulong cellPos = 0;
    cl_ulong  idx;
    cl_float norm = 0;

  //  timestamp_type time1, time2;
  //  get_timestamp(&time1);
#ifdef MEASURE_K1
    float rt = 0.0;
        //Energy Counters
        PCM * pcm;
        SystemCounterState sstate1, sstate2;
        //timing
        tick_count start, end;
        //Initializing PCM library
        pcm = PCM::getInstance();
        pcm->resetPMU();
        if (pcm->program() != PCM::Success){
            cerr << "Error in PCM library initialization" << std::endl;
            exit(-1);
        }
#endif //MEASURE_K1

    while(notReady) {
        nrIter ++;
        cellPos = 0; // reset to the first cell of the T matrix
#ifdef MEASURE_K1
        tick_count bt, et;
            bt = tick_count::now();
            sstate1 = pcm->getSystemCounterState();
#endif
        /** Policy evaluation */
        for (cl_ulong s = 0; s < NS; s++) {
            for (cl_ulong a = 0; a < NX; a++) {
                w = 0;
                idx = s * NX + a;
                /* for all non zero h_probability nextState for <s,a> tuple (i.e. all elements from one cell of T) */
                for (cl_ulong s2 = h_nextCellPos[cellPos]; s2 < h_nextCellPos[cellPos+1]; s2++) {
                    w += h_probability[s2] * h_V[h_nextStatePos[s2]];
                }
                cellPos = cellPos + 1; // go to the next cell of the T matrix
                h_Q[idx] = h_R[idx] + gamma_ * w;
            }
        }
#ifdef MEASURE_K1
        et = tick_count::now();
            sstate2 = pcm->getSystemCounterState();
            runtime_k1 += (et-bt).seconds();
            energy_k1 += getConsumedJoules(sstate1, sstate2);
            //cout << "runtime & total_energy K1:" << runtime_k1 << "\t" << energy_k1 << endl;
#endif
        /** Policy improvement */
        for (cl_ulong s = 0; s < NS; s++) { // states
            //select the policy with the best value
            for (cl_int a = 0; a < NX; a++) { // actions
                idx = s * NX + a;
                //if (h_Q[s][a] > h_Z[s]) {
                if (h_Q[idx] - h_Z[s] > EPSILON) {
                    h_Z[s] = h_Q[idx];
                    h_P[s] = a;
                }
            }
        }

        /*Norm2*/
        norm = 0;
        for (ulong s = 0; s < NS; s++) {
	        norm += pow((h_Z[s]-h_V[s]),2);
        }
        norm = sqrt(norm);
        //cout << "Seq iter: " << nrIter << ". Norm: " << norm << endl ;
        notReady = (norm >= g) && nrIter != MAX_ITER;
        for (cl_int s = 0; s < NS; ++s) { // states
            h_V[s] = h_Z[s];
        }
    }
/*    get_timestamp(&time2);
    double elapsed = timestamp_diff_in_seconds(time1,time2);
    printf("%f s elapsed in seq1d execution\n", elapsed);
    cout << "\nThe seq1D algorithm converged in " << nrIter << " iterations.\n";*/
    return nrIter;
}
