//
// Created by dconstantinescu on 17/04/17.
//
#include "ValueIterationOCL.h"
#include "../utils/cl_helper.h"
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif
#include <cstring>
#include <limits>
#include <assert.h>
#include "../utils/timing.h"
#include "ValueIterationUtils.h"
#include "../navigation_mdp/Common.h"
#include <omp.h>
#include <math.h>
#include <string>

#define ENERGY_METER_INTEL
//#define OMP

#ifndef OMP
#include "Norm2.h"
#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>
#endif

#include <iostream>
#include <chrono>


using namespace std;

#ifdef MEASURE_K1
#include "cpucounters.h"
#include "utils.h"
#include "tbb/tick_count.h"
#endif

using namespace tbb;


//#define ENERGY_METER_ODROID

extern cl_ulong  NS, NR, NG, ND, NA, NX, NT;

//! DEVICE
// global variables
static cl_context	    context;
static cl_command_queue cmd_queue;
static cl_device_type   device_type;
static cl_device_id   * device_list;
static cl_int           num_devices;

cl_kernel kernelPolicyEvaluation;
cl_kernel kernelPolicyImprovement;



// Device buffers
extern cl_mem d_probabilities;
extern cl_mem d_nextCellPos;
extern cl_mem d_nextStatePos;
extern cl_mem d_R;
extern cl_mem d_Q;
extern cl_mem d_P;
extern cl_mem d_V;
const cl_ulong d_NS = NS;
const cl_ulong d_NX = NX;
extern cl_ulong probabilitySize;

// Host buffers
extern cl_float*    h_probability;
extern cl_ulong*    h_nextCellPos;
extern cl_ulong*    h_nextStatePos;
extern cl_float*    h_R;
extern cl_float*    h_Q; // = (cl_float*)calloc(NS*NX,sizeof(cl_float));
extern cl_int*      h_P;
extern cl_float*    h_V;//= (cl_float*)calloc(NS,sizeof(cl_float));
extern cl_float*    h_Z;

static  size_t WORK_GROUP_SIZE = 256;

//#define PCM_BROADWELL

#ifdef PCM_BROADWELL
#include "cpucounters.h"
#include <tbb/tick_count.h>
using namespace tbb;
#endif

#define MEASURE_K1

#ifdef MEASURE_K1
#include "cpucounters.h"
#include "utils.h"
#include "tbb/tick_count.h"
#endif

/** Initialize gpu device*/
static cl_int initialize(cl_int use_gpu);

/** Release resources from the gpu device*/
static cl_int shutdown();

/** Allocate OpenCL memory  and write the kernel input buffers */
cl_int allocate();

/** Transfer IO data from host buffers to device buffers*/


/** Transfer IO data from host buffers to device buffers,
 * run kernel code on device and transfer the result to CPU */
cl_int runPolicyEvaluationKernelOnDevice();

/** Transfer IO data from host buffers to device buffers,
 * run kernel code on device and transfer the result to CPU */
cl_int runPolicyImprovementKernelOnDevice();

/** Deallocate OpenCL memory*/
void deallocateMemory();

//cl_ulong* nextCellPos;
void prepareOclEnvironment();



int ValueIterationOCL(float &runtime_k1, float &energy_k1) {
    //The next line can be done outside. in thjis case it must be commented!!!
    //prepareOclEnvironment();
    /** Allocate device memory*/
    //allocate();

    //cl_float g = 0.001; // d * (1 - gamma)/(2 * gamma);
    bool notReady = true;
    cl_float norm = 0;
    ulong nrIter = 0;
    ulong idx,s;
    cl_int a;

#ifndef OMP
    //Norm2 n2sum(h_Z,h_V);
    // pol improvement
    auto f = [&](const blocked_range<size_t>& r)
    {
        for (cl_ulong s = r.begin(); s != r.end(); ++s) {
            for (int a = 0; a < NX; a++) {
                size_t idx = s * NX + a;
                if (h_Q[idx] - h_Z[s] > EPSILON) {
                    h_Z[s] = h_Q[idx];
                    h_P[s] = a;
                    //printf("h_Z[%d] = %f \n", s, h_Z[s]);
                }
            }
        }
    };
#endif


    #ifdef MEASURE_K1
//    double runtime_k1(0), energy_k1(0);
    timestamp_type time1, time2;
    double elapsed(0);
    float rt = 0.0;
    tick_count bt, et;
    //Energy Counters
    PCM * pcm;
    SystemCounterState sstate1, sstate2;
    //timing
    tick_count start, end;
    //Initializing PCM library
    pcm = PCM::getInstance();
    pcm->resetPMU();
    if (pcm->program() != PCM::Success){
        std:cout << "\nError in PCM library initialization" << std::endl;
        exit(-1);
    }
    #endif

    while(notReady) {

        #ifdef MEASURE_K1
        tick_count bt, et;
        bt = tick_count::now();
        sstate1 = pcm->getSystemCounterState();
        #endif MEASURE_K1
        /** Policy evaluation */
        runPolicyEvaluationKernelOnDevice();

        #ifdef MEASURE_K1
        et = tick_count::now();
        sstate2 = pcm->getSystemCounterState();
        runtime_k1 += (et-bt).seconds();
        energy_k1 += getConsumedJoules(sstate1, sstate2);
        //cout << "runtime & total_energy K1:" << runtime_k1 << "\t" << energy_k1 << endl;
        #endif MEASURE_K1

        /** Policy improvement */
#ifdef OMP
        #pragma omp parallel
		    {
		    #pragma omp  for schedule(guided) private(s,a,idx)
            for (s = 0; s < NS; s++) { // states
                //select the policy with the best value
                for (a = 0; a < NX; a++) { // actions
                    idx = s * NX + a;
                    if (h_Q[idx] > h_Z[s]) {
                        h_Z[s] = h_Q[idx];
                        h_P[s] = a;
                    }
                }
            }

            /*Check on algorithm convergence*/
	        // compute norm2 of vector difference -- omp
		    #pragma omp  for schedule(guided) reduction(+:norm)
            for (s = 0; s < NS; s++) {
	            norm += pow((h_Z[s]-h_V[s]),2);
            }
            norm = sqrt(norm);
#else //tbb
        //select the policy with the best value (max_a Q(s,a)) for every state
        parallel_for( blocked_range<size_t>(0,NS,1), f);
        //Norm2
        //parallel_reduce( blocked_range<size_t>(0,NS,1), n2sum);
        norm = parallel_reduce(blocked_range<size_t>(0,NS), 0.0f,
                               [&](blocked_range<size_t> r, double total_norm)
                               {
                                   for (size_t i=r.begin(); i<r.end(); ++i)
                                   {
                                       total_norm += pow((h_Z[i]-h_V[i]),2);
                                   }

                                   return total_norm;
                               }, std::plus<double>() );

        //cout << "Norm: " << norm << endl;
        norm = sqrt(norm);
        //norm = sqrt(n2sum.my_sum);
#endif

#ifdef OMP
        }
#endif
        notReady = (norm > g) && nrIter < MAX_ITER;
        norm = 0;
        //swap old V with new V (Z)
        memcpy(h_V, h_Z, NS*sizeof(cl_float));
#ifdef DEBUG
        cout <<"\nnorm: " << norm << endl;
#endif
        nrIter++; // #iters to convergence


    } // while

    cout << "\n\nThe OpenCL implem converged in " << nrIter << " iterations. Total ExecTime = " << elapsed << "\nPolicy values sample: ";
    cout << "\n\nExecTime K1: " << runtime_k1 << " Energy K1 " << energy_k1 << "\n ";

#ifdef DEBUG
    cout << "\n ValtItOCL #iter: " << nrIter << "\n";
#endif
    // // Release ocl buffers
    // deallocateMemory();
    // Release device resources
    shutdown();

    return nrIter;
}

cl_int allocate() {
    cl_int err = 0;

    unsigned int size;
    /** Allocate device memory */

    //nextCellPos
    size = NX * NS + 1;
    d_nextCellPos = clCreateBuffer(context,  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, size * sizeof(cl_ulong), h_nextCellPos, &err );
    if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_nextCellPos (size:%d) => %d\n", NX * NS + 1, err); return -1;}

    //probabilities
    d_probabilities = clCreateBuffer(context,  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, probabilitySize * sizeof(cl_float), h_probability, &err );
    //CHECK_CL_ERROR(err, "clCreateBuffer");
    if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_probabilities (size:%d) => %d\n", probabilitySize, err); return -1;}

    //V
    d_V = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR/*CL_MEM_ALLOC_HOST_PTR*/, NS * sizeof(cl_float), h_V, &err );
    if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_V (size:%d) => %d\n", NS, err); return -1;}

    //Q
    d_Q = clCreateBuffer(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR /*CL_MEM_ALLOC_HOST_PTR*/, NX * NS  * sizeof(cl_float), h_Q, &err );
    if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_Q (size:%d) => %d\n", NX * NS, err); return -1;}


    //nextStatePos
    size = NS;

    d_nextStatePos = clCreateBuffer(context,  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, probabilitySize * sizeof(cl_ulong), h_nextStatePos, &err );
    if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_nextStatePos (size:%d) => %d\n", NS, err); return -1;}

    //R
    d_R = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, NX * NS  * sizeof(cl_float), h_R, &err );
    if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_R (size:%d) => %d\n", NX * NS, err); return -1;}


    h_Q = (cl_float*)clEnqueueMapBuffer( cmd_queue,   // Corresponding command queue
                                         d_Q,     // Buffer to be mapped
                                         CL_TRUE,         // block_map, CL_TRUE: can't be unmapped before at least 1 read
                                         CL_MAP_READ,    // mapped for reading or writing?
                                         0,               // offset
                                         NS*NX*sizeof(cl_float) ,         // number of bytes mapped
                                         0,               // number of events in the wait list
                                         NULL,            // event wait list
                                         NULL,            // event
                                         &err );
    if(err != CL_SUCCESS) { printf("ERROR: clEnqueueMapBuffer d_Q  => %d\n", err); return -1;}


    h_V = (cl_float*)clEnqueueMapBuffer( cmd_queue,   // Corresponding command queue
                                         d_V,     // Buffer to be mapped
                                         CL_TRUE,         // block_map, CL_TRUE: can't be unmapped before at least 1 read
                                         CL_MAP_WRITE,    // Host to device. mapped for reading or writing?
                                         0,               // offset
                                         NS *sizeof(cl_float),         // number of bytes mapped
                                         0,               // number of events in the wait list
                                         NULL,            // event wait list
                                         NULL,            // event
                                         &err );
    if(err != CL_SUCCESS) { printf("ERROR: clEnqueueMapBuffer d_V  => %d\n", err); return -1;}

    return 0;
}

/** Run kernel code on device */
cl_int runPolicyEvaluationKernelOnDevice()
{
    cl_int err;

    cl_event event;
    cl_int error;
    cl_int status;

    /** Transfer  IO buffers to (read-write) device */
    //V
    err = clEnqueueWriteBuffer(cmd_queue, d_V, /*blocking*/ CL_TRUE, /*offset*/ 0, NS * sizeof(cl_float), h_V, 0, 0, 0);
    if(err != CL_SUCCESS) { printf("ERROR: clEnqueueWriteBuffer d_V (size:%d) => %d\n", NS, err); return -1; }


    SET_8_KERNEL_ARGS(kernelPolicyEvaluation,
                      d_probabilities,
                      d_V,
                      d_Q,
                      d_nextCellPos,
                      d_nextStatePos,
                      d_R,
                      NS,
                      NX
    );


    size_t global_work_size[3] = { NS * NX, 1, 1 };
    size_t local_work_size[] = {WORK_GROUP_SIZE}; // work group size
    if(global_work_size[0]%local_work_size[0] !=0)
        global_work_size[0]=(global_work_size[0]/local_work_size[0]+1)*local_work_size[0];


    err = clEnqueueNDRangeKernel(cmd_queue, kernelPolicyEvaluation, 1, NULL,  global_work_size, local_work_size, 0, NULL, 0/* &event*/);


    h_Q = (cl_float*)clEnqueueMapBuffer( cmd_queue,   // Corresponding command queue
                                         d_Q,     // Buffer to be mapped
                                         CL_TRUE,         // block_map, CL_TRUE: can't be unmapped before at least 1 read
                                         CL_MAP_READ,    // mapped for reading or writing?
                                         0,               // offset
                                         NS*NX*sizeof(cl_float) ,         // number of bytes mapped
                                         0,               // number of events in the wait list
                                         NULL,            // event wait list
                                         NULL,            // event
                                         &err );
    if(err != CL_SUCCESS) { printf("ERROR: clEnqueueMapBuffer d_Q  => %d\n", err); return -1;}

    return 0;
}

void prepareOclEnvironment() {

    cl_int error;
    int num_max_devices = 1;
    cl_uint num_platforms;
    cl_uint num_devices = 0;
    cl_device_id device_id;
    cl_program program;
    int computeUnits;
    size_t vectorization;

    /** Init ocl environment */

    // Get the number of platforms
    cl_platform_id* platform_ids = NULL;

    error = clGetPlatformIDs(0, NULL, &num_platforms);

    if (error==CL_SUCCESS && num_platforms>0){
        platform_ids = new cl_platform_id[num_platforms];
    }
    else {
        fprintf(stderr, "No platforms were found.\n");
        exit(1);
    }
    error = clGetPlatformIDs(num_platforms, platform_ids, NULL);
    if (error != CL_SUCCESS){
        fprintf(stderr, "No platforms were found.\n");
        delete[] platform_ids;
        exit(1);
    }
    // Find a GPU device in one of the platforms

    //cl_device_id device_id;
    bool found = false;
    int cont_found=0;
    for(cl_uint i = 0; i < num_platforms; i++){
        error = clGetDeviceIDs(platform_ids[i], CL_DEVICE_TYPE_GPU, num_max_devices, &device_id, &num_devices);
        if (error == CL_SUCCESS) {
            cont_found++;
            if(cont_found>=1){
                found = true;
                break;
            }
        }
    }
    delete[] platform_ids;
    if(!found){
        fprintf(stderr, "No GPU device found\n");
        exit(1);
    }


    error = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &computeUnits, NULL);
    if (error != CL_SUCCESS) {
        fprintf(stderr, "No compute units found\n");
        exit(0);
    }

    char device_name[50];
    error = clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof(char)*50, &device_name, NULL);
    if (error != CL_SUCCESS) {
        fprintf(stderr, "No device name found\n");
        exit(0);
    }

    cerr << "GPU's name: " << device_name << " with "<< computeUnits << " compute Units" << endl;
    num_devices=1;
    context = clCreateContext(NULL, num_devices, &device_id, NULL, NULL,
                              &error);
    if (error != CL_SUCCESS) {
        fprintf(stderr, "Context couldn't be created\n");
        exit(0);
    }

    cmd_queue = clCreateCommandQueue/*WithProperties*/(context, device_id, CL_QUEUE_PROFILING_ENABLE, &error);
    if (error != CL_SUCCESS) {
        fprintf(stderr, "Command Queue couldn't be created\n");
        exit(0);
    }

    char kernelname[] = "./src/policyEvaluation.cl";
    char * programSource =  read_file(kernelname);

    // Create a program using clCreateProgramWithSource()
    program = clCreateProgramWithSource(context, 1,
                                        (const char**) &programSource, NULL, &error);
    if (error != CL_SUCCESS) {
        fprintf(stderr, "Failed creating programm with source!\n");
        exit(0);
    }
    // Build (compile) the program for the devices with // clBuildProgram()
    error = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
    if (error != CL_SUCCESS) {
        fprintf(stderr, "Failed Building Program!\n");
        char message[16384];
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG,
                              sizeof(message), message, NULL);
        fprintf(stderr,"Message Error:\n %s\n",message);
        exit(0);
    }
    // Use clCreateKernel() to create a kernel from the
    // vector addition function (named "vecadd")
    kernelPolicyEvaluation = clCreateKernel(program, "policyEvaluation", &error);
    //printf("\nErr clCreateKernel: %d\n", error);
    if (error != CL_SUCCESS) {
        printf("\nErr clCreateKernel: %d\n", error);
        fprintf(stderr, "Failed Creating programm!\n");
        exit(0);
    }

    clGetKernelWorkGroupInfo(kernelPolicyEvaluation, device_id, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(size_t), &vectorization, NULL);
#ifdef DEBUG
    cerr << "Preferred vectorization: " << vectorization << endl;
#endif
    free(programSource);
}


void deallocateMemory()
{
    CALL_CL_GUARDED( clReleaseMemObject,(d_probabilities) );
    CALL_CL_GUARDED( clReleaseMemObject,(d_nextCellPos) );
    CALL_CL_GUARDED( clReleaseMemObject,(d_nextStatePos) );
    CALL_CL_GUARDED( clReleaseMemObject,(d_Q) );
    CALL_CL_GUARDED( clReleaseMemObject,(d_R) );
    CALL_CL_GUARDED( clReleaseMemObject,(d_V) );
}

static cl_int shutdown()
{
    // release resources
    if( cmd_queue ) clReleaseCommandQueue( cmd_queue );
    if( context ) clReleaseContext( context );
    if( device_list ) delete device_list;

    // reset all variables
    cmd_queue   = 0;
    context     = 0;
    device_list = 0;
    num_devices = 0;
    device_type = 0;

    return 0;
}
