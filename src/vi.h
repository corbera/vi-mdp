//
// Created by root on 26/12/19.
//

#ifndef VALUEITERATION_VI_H
#define VALUEITERATION_VI_H


#include <sstream>
#include "CL/sycl.hpp"
#include "../navigation_mdp/Common.h"

using namespace cl::sycl;
using namespace std;

extern cl::sycl::queue gpu_queue, cpu_queue;
extern cl::sycl::context ctx;

class vi {
public:
    ulong ns;
    int nx;
    cl_ulong    probabilitySize;
    cl_float*   probability = NULL;
    cl_ulong*   nextCellPos = NULL;
    cl_ulong*   nextStatePos = NULL;
    cl_float*   R      = NULL;
    cl_float*   Q      = NULL;
    cl_float*   V      = NULL;
    cl_int*     P      = NULL;
    cl_float*   Z      = NULL;


    vi(){}

    vi& operator=(const vi &v){
        ns = v.ns;
        nx = v.nx;
        probabilitySize = v.probabilitySize;
        probability = v.probability;
        nextCellPos = v.nextCellPos;
        nextStatePos = v.nextStatePos;
        R = v.R;
        Q = v.Q;
        V = v.V;
        P = v.P;
        Z = v.Z;
        return *this;
    }


    vi(int inputId) {
        uint nt = tD[inputId];    // number of theta discrete values
        uint nr = 3;    // number of rangefinders
        uint ng = gD[inputId];    // number of discrete values for a rangefinder reading
        uint nd = dD[inputId];    // number of discrete values for the distance from the robot to the target
        uint na = aD[inputId];    // number of discrete values for the angle between robot orientation and target
        nx = 6;

        ns = nt * ng * ng * ng * nd * na;


        // Using these events to time command group execution

        probabilitySize = 0;

        //setbuf(stdout, NULL); //stop buffering, for right order in printf/fprintf, etc
//int inputId[4] = {40445, 61012, 84565, 143771};


        printf("Algorithm input id : %d\n", inputId);

        probabilitySize = pNZ[inputId];

        cout << "probabilitySize: " << probabilitySize << std::endl;

        std::stringstream ss;
        std::stringstream fnss;
        ss  << "PS" << probabilitySize
            << "_" << "S" << ns
            << "_" << "T" << nt
            << "_" << "R" << nr
            << "_" << "G" << ng
            << "_" << "D" << nd
            << "_" << "A" << na;

        string inFileDim = ss.str();
        //cout << inFileDim;

        fnss << "./data/in/" << inFileDim << ".txt";
        string fns = fnss.str();
        char* inputFileName  = (char*)fns.c_str();

        // ReadInput(inputFileName);
        FILE *f;

        f = fopen(inputFileName, "r+t");
        if (f == NULL) {
            fprintf(stderr, "file not found: %s\n", inputFileName);
            exit(-1);
        }

        fscanf(f, "%lu", &probabilitySize);
        fscanf(f, "%ld", &ns);
        fscanf(f, "%d", &nx);
        fscanf(f, "%d", &nt);
        fscanf(f, "%d", &nr);
        fscanf(f, "%d", &ng);
        fscanf(f, "%d", &nd);
        fscanf(f, "%d", &na);

         Q  = (cl_float*)malloc_shared(ns*nx * sizeof(cl_float), gpu_queue.get_device(), ctx);
         V  = (cl_float*)malloc_shared(ns * sizeof(cl_float), gpu_queue.get_device(), ctx);


//        V = (float *) malloc(sizeof(float) * ns);
        Z = (cl_float *) malloc(sizeof(cl_float) * ns);
        P = (cl_int *) malloc(sizeof(cl_int) * ns);


        probability = (cl_float *) malloc(sizeof(cl_float) * probabilitySize);
        nextCellPos = (cl_ulong *) malloc(sizeof(cl_ulong) * (ns * nx + 1));
        nextStatePos = (cl_ulong *) malloc(sizeof(cl_ulong) * probabilitySize);
        R = (cl_float *) malloc(sizeof(cl_float) * ns * nx);
//        Q = (float *) malloc(sizeof(float) * ns * nx);

        for (cl_ulong i = 0; i < ns; i++) {
            V[i] = 0;
            P[i] = 0;
            Z[i] = 0;
        }

        unsigned int i;
        //probability
        for (i = 0; i < probabilitySize; i++) {
            fscanf(f, "%fE", &probability[i]);
        }

        //nextCellPos
        for (i = 0; i < ns * nx + 1; i++) {
            fscanf(f, "%lu", &nextCellPos[i]);
        }
        //nextStatePos
        for (i = 0; i < probabilitySize; i++) {
            fscanf(f, "%lu", &nextStatePos[i]);
        }
        //R
        for (i = 0; i < ns * nx; i++) {
            fscanf(f, "%f", &R[i]);
        }
        fclose(f);


    }

    void initialize(int inputId) {
        uint nt = tD[inputId];    // number of theta discrete values
        uint nr = 3;    // number of rangefinders
        uint ng = gD[inputId];    // number of discrete values for a rangefinder reading
        uint nd = dD[inputId];    // number of discrete values for the distance from the robot to the target
        uint na = aD[inputId];    // number of discrete values for the angle between robot orientation and target
        nx = 6;

        ns = nt * ng * ng * ng * nd * na;


        // Using these events to time command group execution

        probabilitySize = 0;

        //setbuf(stdout, NULL); //stop buffering, for right order in printf/fprintf, etc
//int inputId[4] = {40445, 61012, 84565, 143771};


        printf("Algorithm input id : %d\n", inputId);

        probabilitySize = pNZ[inputId];

        cout << "probabilitySize: " << probabilitySize << std::endl;

        stringstream ss;
        stringstream fnss;
        ss  << "PS" << probabilitySize
            << "_" << "S" << ns
            << "_" << "T" << nt
            << "_" << "R" << nr
            << "_" << "G" << ng
            << "_" << "D" << nd
            << "_" << "A" << na;

        string inFileDim = ss.str();
        //cout << inFileDim;

        fnss << "./data/in/" << inFileDim << ".txt";
        string fns = fnss.str();
        char* inputFileName  = (char*)fns.c_str();

        // ReadInput(inputFileName);
        FILE *f;

        f = fopen(inputFileName, "r+t");
        if (f == NULL) {
            fprintf(stderr, "file not found: %s\n", inputFileName);
            exit(-1);
        }

        fscanf(f, "%lu", &probabilitySize);
        fscanf(f, "%ld", &ns);
        fscanf(f, "%d", &nx);
        fscanf(f, "%d", &nt);
        fscanf(f, "%d", &nr);
        fscanf(f, "%d", &ng);
        fscanf(f, "%d", &nd);
        fscanf(f, "%d", &na);

        Q  = (cl_float*)malloc_shared(ns*nx * sizeof(cl_float), gpu_queue.get_device(), ctx);
        V  = (cl_float*)malloc_shared(ns * sizeof(cl_float), gpu_queue.get_device(), ctx);


        probability  = (cl_float *) malloc_host(sizeof(cl_float) * probabilitySize, ctx);
        nextCellPos  = (cl_ulong *) malloc_host(sizeof(cl_ulong) * (ns * nx +  1), ctx);
        nextStatePos = (cl_ulong *) malloc_host(sizeof(cl_ulong) * probabilitySize, ctx);
        R = (cl_float *) malloc_host(sizeof(cl_float) * ns * nx,  ctx);


//        V = (float *) malloc(sizeof(float) * ns);
        Z = (cl_float *) malloc_host(sizeof(cl_float) * ns, ctx);
        P = (cl_int *) malloc_host(sizeof(cl_int) * ns, ctx);


//        probability = (cl_float *) malloc(sizeof(cl_float) * probabilitySize);
//        nextCellPos = (cl_ulong *) malloc(sizeof(cl_ulong) * (ns * nx + 1));
//        nextStatePos = (cl_ulong *) malloc(sizeof(cl_ulong) * probabilitySize);
//        R = (cl_float *) malloc(sizeof(cl_float) * ns * nx);
//        Q = (float *) malloc(sizeof(float) * ns * nx);

        for (ulong i = 0; i < ns; i++) {
            V[i] = 0;
            P[i] = 0;
            Z[i] = 0;
        }

        unsigned int i;
        //probability
        for (i = 0; i < probabilitySize; i++) {
            fscanf(f, "%fE", &probability[i]);
        }

        //nextCellPos
        for (i = 0; i < ns * nx + 1; i++) {
            fscanf(f, "%lu", &nextCellPos[i]);
        }
        //nextStatePos
        for (i = 0; i < probabilitySize; i++) {
            fscanf(f, "%lu", &nextStatePos[i]);
        }
        //R
        for (i = 0; i < ns * nx; i++) {
            fscanf(f, "%f", &R[i]);
        }
        fclose(f);


    }

    ~vi() {
        if (R != NULL){ free(R, ctx); R = NULL; }
        if (Q != NULL){ free(Q, ctx); Q = NULL; }
        if (V != NULL){ free(V, ctx); R = NULL; }
        if (Z != NULL){ free(Z, ctx); Z = NULL; }
        if (probability != NULL){ free(probability, ctx); probability = NULL; }
        if (nextCellPos != NULL){ free(nextCellPos, ctx); nextCellPos = NULL; }
        if (nextStatePos != NULL){ free(nextStatePos, ctx); nextStatePos = NULL; }
    }
};


#endif //VALUEITERATION_VI_H
