//
// Created by dconstantinescu on 5/05/17.
//
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif
#include "valueIterationOMP1D.h"

#include <limits>
#include "ValueIterationUtils.h"
#include <math.h>
#include <omp.h>
#include "../utils/timing.h"

extern cl_ulong  NS, NR, NG, ND, NA, NX, NT;

cl_int valueIterationOMP1D(cl_float* h_probability, cl_ulong* h_nextStatePos, cl_ulong* h_nextCellPos, cl_float* h_R, cl_float* h_Q, cl_float* h_V/*, cl_float* h_Z*/, cl_int* h_P, cl_float gamma_, cl_float d) {

    cl_float *h_Z = (cl_float*)calloc(NS,sizeof(cl_float));
    //cl_float g = 0.0001; // d * (1 - gamma__)/(2 * gamma__);
    double w;
    bool notReady = true; // shared stopping condition
    bool tReady = false;
    cl_float normVZ;
    cl_int nrIter = 0; // shared loop counter

    cl_ulong cellPos = 0;
    cl_ulong idx;
    cl_ulong s, s2;
    cl_int a;
    cl_int numThreads = 4;
    //omp_set_num_threads(numThreads);
    omp_set_nested(1);

    cl_float norm = 0;
#ifdef DEBUG
    cout << "OMP: nr threads = " << omp_get_num_threads() << endl;
    timestamp_type time1, time2;
    get_timestamp(&time1);
#endif
//export OMP_NUM_THREADS=4
#pragma omp parallel  private(tReady)
{
#ifdef DEBUG
   // cout << "OMP: nr threads = " << omp_get_num_threads() << endl;
#endif
    nrIter=0;
        while (notReady) {

            /** Policy evaluation */
            #pragma omp for schedule(guided) firstprivate(gamma_) private(s,a,s2,idx,cellPos) reduction (+:w)
            for (s = 0; s < NS; s++) {
                for (a = 0; a < NX; a++) {
                    w = 0;
                    idx = s * NX + a; // go to the next cell of the T/h_Q/h_R matrix
                    /* for all non zero h_probability nextState for <s,a> tuple */
                    for (s2 = h_nextCellPos[idx]; s2 < h_nextCellPos[idx + 1]; s2++) {
                        w += h_probability[s2] * h_V[h_nextStatePos[s2]];
                    }
                    h_Q[idx] = h_R[idx] + gamma_ * w;
                }
            }
            /** Policy improvement */
            #pragma omp for schedule(guided) private(s,a,idx)
            for (s = 0; s < NS; s++) { // states
                //select the policy with the best value
                for (a = 0; a < NX; a++) { // actions
                    idx = s * NX + a;
                    //if (h_Q[s][a] > h_Z[s]) {
                    if (h_Q[idx] - h_Z[s] > EPSILON) {
                        h_Z[s] = h_Q[idx];
                        h_P[s] = a;
                    }
                }
            }

	        // compute norm2 of vector difference
          #pragma omp for schedule(guided) reduction(+:norm)
	        for (s = 0; s < NS; s++) {
		        norm += pow((h_Z[s]-h_V[s]),2);
	        }
	        tReady = sqrt(norm) <= g || nrIter >= MAX_ITER;
            #pragma omp for schedule(guided) /*s is private by default*/
            for (cl_int s = 0; s < NS; ++s) { // states
                h_V[s] = h_Z[s];
            }
            #pragma omp single      /* only one thread executes this section, whichever thread */
            {
                nrIter++;
                cellPos = 0;
                norm = 0;
            }
            if(tReady) {
	            notReady = false;
                #pragma omp flush(notReady)
            }
        }//end while
} // end pragma
#ifdef DEBUG
    get_timestamp(&time2);
    double elapsed = timestamp_diff_in_seconds(time1,time2);
    printf("%f s elapsed in omp1d execution\n", elapsed);

    cout << "\nThe OMP1D algorithm converged in " << nrIter << " iterations.\n";
#endif
    free(h_Z);
    return nrIter;
}
