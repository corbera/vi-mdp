#ifndef VALUEITERATION_VI_ONEAPI_H
#define VALUEITERATION_VI_ONEAPI_H

// #include "TransitionMatrix.h"
#include "../navigation_mdp/Common.h"
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif
int ValueIterationOneApi(  float& runtime_k1, float& energy_k1, float offload_ratio) ;

#endif