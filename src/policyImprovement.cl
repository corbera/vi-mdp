__kernel void policyImprovement(   global float* Q,
                                   global float* V,
                                   global float* P,
                                   unsigned long const NS,
                                   unsigned long const NX )
{
    unsigned long myState = get_global_id(0); // the beginning of a row
    unsigned long qIndex;
    /** Policy improvement */
    if (myState < NS) { // evaluate the actions for the current state
        //select the policy with the best value
        for (int a = 0; a < NX; a++) { // actions
            qIndex = myState * NS + a;
            if (Q[qIndex] > V[myState]) {
                V[myState] = Q[qIndex];
                P[myState] = a;
            }
        }
    }
    return;
}
