//
// Created by dconstantinescu on 13/06/17.
//

kernel void print(global char *str) {
    printf("OpenCL says '");
    for (; *str; ++str) printf("%c", *str);
    printf("'\n");
}