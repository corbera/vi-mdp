//
// Created by dconstantinescu on 6/06/17.
//

#ifndef VALUEITERATION_VALUEITERATIONTBB_H
#define VALUEITERATION_VALUEITERATIONTBB_H


#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif
#include "../navigation_mdp/TransitionMatrix.h"
#include "../navigation_mdp/Common.h"
#include "Norm2.h"
#include <tbb/task_scheduler_init.h>
#include <tbb/blocked_range.h>
#include <tbb/parallel_for.h>
#include <math.h>
#include <limits>


#define MEASURE_K1

#ifdef MEASURE_K1
#include "cpucounters.h"
#include "utils.h"
#include "tbb/tick_count.h"
#endif
//#include "timing.h"

extern cl_ulong  NS, NR, NG, ND, NA, NX, NT;

using namespace tbb;

using namespace std;

int ValueIterationTBB1D(cl_float* h_probability,
                        cl_ulong* h_nextStatePos,
                        cl_ulong* h_nextCellPos,
                        cl_float* h_R,
                        cl_float* h_Q,
                        cl_float* h_V,
                        cl_int* h_P,
                        cl_float gamma_,
                        float& runtime_k1,
                        float& energy_k1) {
    cl_float *h_Z = (cl_float*)calloc(NS,sizeof(cl_float));
    bool notReady = true;
    cl_float norm = 0.0;
    int nrIter = 0;
    //cl_float g = 1e-10;
    //functor
    auto f = [&](const blocked_range<size_t>& r)
    {
        for (size_t s = r.begin(); s != r.end(); ++s) {
            for (int a = 0; a < NX; a++) {
                cl_float w = 0;
                size_t idx = s * NX + a; // go to the next cell, T/h_Q/h_R matrix
                // pol eval
                for (size_t s2 = h_nextCellPos[idx]; s2 < h_nextCellPos[idx + 1]; s2++) {
                    w += h_probability[s2] * h_V[h_nextStatePos[s2]];
                }
                h_Q[idx] = h_R[idx] + gamma_ * w;

                // pol impr
                if (h_Q[idx] - h_Z[s] > EPSILON) {
                    h_Z[s] = h_Q[idx];
                    h_P[s] = a;
                }
            }
        }
        // pol impr
        // for (cl_ulong s = r.begin(); s != r.end(); ++s) {
        //     for (int a = 0; a < NX; a++) {
        //         size_t idx = s * NX + a;
        //         if (h_Q[idx] - h_Z[s] > EPSILON) {
        //             h_Z[s] = h_Q[idx];
        //             h_P[s] = a;
        //         }
        //     }
        // }
    };


    //Norm2 n2sum(h_Z,h_V); // using class definition
    #ifdef MEASURE_K1
        float rt = 0.0;
        //Energy Counters
        PCM * pcm;
        SystemCounterState sstate1, sstate2;
        //timing
        tick_count start, end;
        //Initializing PCM library
        pcm = PCM::getInstance();
        pcm->resetPMU();
        if (pcm->program() != PCM::Success){
            cerr << "Error in PCM library initialization" << std::endl;
            exit(-1);
        }
    #endif //MEASURE_K1
    while(notReady) {

        // lambda version
        #ifdef MEASURE_K1
            tick_count bt, et;
            bt = tick_count::now();
            sstate1 = pcm->getSystemCounterState();
        #endif
        //code to be measured - K1
        parallel_for( blocked_range<size_t>(0,NS /*, grainSize*/), f);
        #ifdef MEASURE_K1
            et = tick_count::now();
            sstate2 = pcm->getSystemCounterState();
            runtime_k1 += (et-bt).seconds();
            energy_k1 += getConsumedJoules(sstate1, sstate2);
            //cout << "runtime & total_energy K1:" << runtime_k1 << "\t" << energy_k1 << endl;
        #endif
        /*Norm2*/
        // using class definition
        // parallel_reduce( blocked_range<size_t>(0,NS/*, grainSize*/), n2sum);
        // norm = sqrt(n2sum.my_sum);
        // using lambda function
        norm = parallel_reduce(blocked_range<size_t>(0,NS), 0.0f,
                               [&](blocked_range<size_t> r, double total_norm)
                               {
                                   for (size_t i=r.begin(); i<r.end(); ++i)
                                   {
                                       total_norm += pow((h_Z[i]-h_V[i]),2);
                                   }

                                   return total_norm;
                               }, std::plus<double>() );

        //cout << "Norm: " << norm << endl;
        norm = sqrt(norm);
        notReady = (norm >= g) && nrIter != MAX_ITER;

        //swap old h_V with new h_V (h_Z)
        memcpy(h_V,h_Z,NS*sizeof(cl_float));
//        for (cl_int s = 0; s < NS; ++s) { // states
//            h_V[s] = h_Z[s];
//        }
        nrIter++;
    }
    free(h_Z);
    return nrIter;
}
#endif //VALUEITERATION_VALUEITERATIONTBB_H
