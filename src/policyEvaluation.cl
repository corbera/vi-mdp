__kernel void policyEvaluation( __global const    float* probability,
                  __global float*   V,
                  __global float*   Q,
                  __global const    unsigned long*  nextCellPos,
                  __global const    unsigned long*  nextStatePos,
                  __global const    float* R,
                  unsigned long     NS,
                  unsigned long     NX
) {
    float w = 0.0;
    float gamma = 0.1;
    ulong s2;
    unsigned int myCellPos = get_global_id(0); //  get_local_size(0)*get_group_id(0) + get_local_id(0); the beginning of a row
    // Policy evaluation
    if ( myCellPos < NX * NS) { // evaluate the actions for the current state
        /* for all non zero probability nextState for <s,a> tuple (i.e. all elements from one cell of T) */
        for (s2 = nextCellPos[myCellPos]; s2 < nextCellPos[myCellPos + 1]; s2++) {
            w += probability[s2] * V[nextStatePos[s2]];
        }
        Q[myCellPos] = R[myCellPos] + gamma * w ;
    }
    return ;
}
