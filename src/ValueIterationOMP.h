//
// Created by pingu on 26/03/17.
//

#ifndef VALUEITERATION_VALUEITERATIONOMP_H
#define VALUEITERATION_VALUEITERATIONOMP_H
/** Value iteration algorithm using OpenMP parallelism
 * returns the number of iterations to convergence*/
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif
cl_int valueIterationOMP(TransitionMatrix &T, cl_float* R, cl_float* Q, cl_float* V,  cl_float* Z, cl_int* P, cl_float gamma, cl_float d);
#endif //VALUEITERATION_VALUEITERATIONOMP_H
