//============================================================================
// Name			: ValueIterationPlanifier.h
// Author		: Denisa Constantinescu
// Version		: 1.0
// Date			: 19 / 12 / 2019
// Copyright	: Department. Computer's Architecture (c)
// Description	: ValueIterationPlanifier.h implementation for ValueIteration project
//                The workload will be distributed row-wise, not by T matrix cell,
//                ({begin, end} in 0..NS)
//============================================================================

#ifndef VALUEITERATION_VALUEITERATIONPLANIFIER_H
#define VALUEITERATION_VALUEITERATIONPLANIFIER_H

//#define POLICY_IMPROVMENT
//#define DYNAMIC
//#define ORACLE

#include "vi.h"


#include <cstdlib>
#include <iostream>

#include <tbb/tick_count.h>
#include <tbb/blocked_range.h>
#include "tbb/parallel_for.h"
#include "tbb/task.h"

#include "../navigation_mdp/Common.h"


#ifdef DYNAMIC_ONE_API
#include <DynamicOneApi.h>
#endif
#ifdef LOGFIT_ONE_API
#include <LogFitOneApi.h>
#endif

#include "CL/sycl.hpp"
#include "ViFunctors.h"

using namespace cl::sycl;

using namespace std;
using namespace tbb;

/*****************************************************************************
 * Global variables
 * **************************************************************************/

extern cl::sycl::queue gpu_queue;
extern cl::sycl::context ctx;
extern cl::sycl::event e1, e2, e3, e4, e5;

extern class vi vi;
extern cl_float*   d_probability;
extern cl_ulong*   d_nextCellPos;
extern cl_ulong*   d_nextStatePos;
extern cl_float*   d_R ;
extern cl_float*   s_Q;
extern cl_float*   s_V;

static  size_t WORK_GROUP_SIZE = 256;

class ValueIterationPlanifierOneApi {

public:
    bool firsttime;

public:

    ValueIterationPlanifierOneApi(/*class vi* h_v*/) {
        firsttime = true;
//        vi = *h_v;
    }

    /*Allocate memory objects on GPU memory address space*/
    void AllocateMemoryObjects() {
        //device
        //todo: making a copy of read only buffers just for gpu may improve performance
        cl::sycl::buffer<cl_ulong, 1> b_nextCellPos(vi.nextCellPos, cl::sycl::range<1>( vi.nx * vi.ns + 1)); //, range<1>(NX * NS + 1), {cl::sycl::property::buffer::use_host_ptr()});
        cl::sycl::buffer<cl_float, 1> b_probabilities(vi.probability, cl::sycl::range<1>(vi.probabilitySize));
        cl::sycl::buffer<cl_float, 1> b_V(vi.V, cl::sycl::range<1>(vi.ns));
        cl::sycl::buffer<cl_ulong, 1> b_nextStatePos(vi.nextStatePos, cl::sycl::range<1>(vi.probabilitySize));
        cl::sycl::buffer<cl_float, 1> b_R(vi.R, cl::sycl::range<1>(vi.nx * vi.ns));
        cl::sycl::buffer<cl_float, 1> b_Z(vi.Z, cl::sycl::range<1>(vi.ns));
        // This is the output buffer device writes to
        cl::sycl::buffer<cl_float, 1> b_Q(vi.Q, cl::sycl::range<1>(vi.nx * vi.ns));

    }

    /*This function send the data to be computed in the GPU*/
    void sendObjectToGPU( cl_ulong begin, cl_ulong end )  {
        if (firsttime) {
            firsttime = false;
        } else {
        //copy buffers that change
        e5 = gpu_queue.memcpy(vi.V, vi.Z,sizeof(cl_float)*vi.ns);
        }

    }

    /*This function launches the kernel*/
    void OperatorGPU(cl_ulong begin, cl_ulong end, cl::sycl::event* event) {



        *event = gpu_queue.submit([&](cl::sycl::handler& cgh) {
            // create accessors to read  the buffers on the device
            auto b_nextCellPos_acc = b_nextCellPos.get_access<sycl_read>(cgh);
            auto b_probabilities_acc = b_probabilities.get_access<sycl_read>(cgh);
            auto b_V_acc = b_V.get_access<sycl_read>(cgh);
            auto b_nextStatePos_acc = b_nextStatePos.get_access<sycl_read>(cgh);
            auto b_R_acc = b_R.get_access<sycl_read>(cgh);
            // create accessors to update  the buffers on the device
            auto b_Q_acc = b_Q.get_access<sycl_write>(cgh);


            PolicyEvaluationFunctorSYCL kernel( begin,
                                            b_nextCellPos_acc,
                                            b_probabilities_acc,
                                            b_V_acc,
                                            b_nextStatePos_acc,
                                            b_R_acc,
                                            b_Q_acc );
            //** functor kernel */
            cgh.parallel_for(cl::sycl::range<1>(end-begin), kernel);

        });
    }

    /*Function to receive the data from GPU memory*/
    void getBackObjectFromGPU(cl_ulong begin, cl_ulong end) {
        //d_Q
//        event = gpu_queue.memcpy(s_Q, vi.Q, sizeof(float)*vi.ns);
//        cout << "get back from GPU \n";
    }

    /*Serial version of the code */
    void OperatorCPU(int begin, int end) { // 0,NS*NX
        double w;//, maxQs;8
        for (int idxCell = begin; idxCell < end; idxCell++) { // divide the outer loop, by state-action cell
            w = 0;
            /* for all non zero probability nextState for <s,a> tuple*/
            for (ulong s2 = vi.nextCellPos[idxCell]; s2 < vi.nextCellPos[idxCell + 1]; s2++) {
//                cout << vi.nextCellPos[idxCell] << "\n";
//                cout << vi.nextStatePos[s2] << "\n";
                if (s2 > vi.ns-1 || s2 < 0 ) {
                    cout << "s2 = vi.nextCellPos is out of range: " << s2 << "\n";
                    cout << "nextStatePos: ";
                    print1DimArray(vi.nextCellPos, 10, 10);
                }
                w += vi.probability[s2] * vi.V[vi.nextStatePos[s2]];
            }
            vi.Q[idxCell] = vi.R[idxCell] + GAMMA * w;
        }
    }
};

/*****************************************************************************
 * Parallel For (TBB only CPUs)
 * **************************************************************************/
class MYParallelFor {
    ValueIterationPlanifierOneApi *vip;
public:
    /*Class Constructor*/
    MYParallelFor(ValueIterationPlanifierOneApi *vip_) {
        vip = vip_;
    }

    /*Operator function*/
    void operator()( const blocked_range<cl_ulong>& range ) const {
        vip->OperatorCPU(range.begin(), range.end());
    }
};

/*This Function launches the parallel for pattern only on CPU cores, based on TBB template*/
void ParallelFORCPUs(size_t start, size_t end, ValueIterationPlanifierOneApi *vip ) {
    MYParallelFor pf(vip);
    parallel_for( blocked_range<cl_ulong>( start, end ), pf);
}

/***************************************************************************** 
 * Raw Tasks
 * ************************************************************************** //corbera
template <class V>
class GPUTask: public task {
public:
    V *vi;
    cl_ulong begin;
    cl_ulong end;

    GPUTask<V>(V *vi, cl_ulong begin, cl_ulong end) : vi(vi), begin(begin), end(end) {}

    //Override virtual function task::execute
    task* execute() {
        #ifdef USEBARRIER
        //cerr << "GPU before barrier1" << endl;
        getLockGPU();
        //cerr << "GPU after barrier1" << endl;
        #endif
        #ifdef PJTRACER
        tracer->gpuStart();
        #endif

        tick_count start = tick_count::now();
        vi->sendObjectToGPU(begin, end, NULL);
        vi->OperatorGPU(begin, end, NULL);
        vi->getBackObjectFromGPU(begin, end, NULL);
        clFinish(command_queue);
        tick_count finish = tick_count::now();
        gpuThroughput = (end -  begin) / ((finish-start).seconds()*1000);

        #ifdef PJTRACER
        tracer->gpuStop();
        #endif
        #ifdef USEBARRIER
        //cerr << "GPU antes de barrier2" << endl;
        freeLockGPU();
        //cerr << "GPU despues de barrier2" << endl;
        getLockCPU();
        freeLockCPU();
        #endif
        return NULL;
    }
};

template <class V>
class CPUTask: public task {
public:
    V *vi;
    cl_ulong begin;
    cl_ulong end;

    CPUTask<V>(V *vi, cl_ulong begin, cl_ulong end) : vi(vi), begin(begin), end(end) {}

    // Overrides virtual function task::execute
    task* execute() {
#ifdef USEBARRIER
        //cerr << "CPU antes de barrier1" << endl;
        getLockCPU();
        //cerr << "CPU despues de barrier1" << endl;
#endif
        tick_count start = tick_count::now();
        ParallelFORCPUs(begin, end, vi);
        tick_count finish = tick_count::now();
        cpuThroughput = (end -  begin) / ((finish-start).seconds()*1000);

#ifdef USEBARRIER
        //cerr << "CPU antes de barrier2" << endl;
        freeLockCPU();
        //cerr << "CPU despues de barrier2" << endl;
        getLockGPU();
        freeLockGPU();
#endif
        return NULL;
    }
};

template <class V>
class ROOTTask : public task {
public:
    V *vi;
    cl_ulong begin;
    cl_ulong end;

    ROOTTask<V>(V *vi, cl_ulong begin, cl_ulong end) : vi(vi), begin(begin), end(end) {}

    task *execute() {
        #ifdef USEBARRIER
        //cerr << "before barrier_init" << endl;
        barrier_init();
        //cerr << "after barrier_init" << endl;
        #endif
        GPUTask<V>& a = *new( allocate_child() ) GPUTask<V>(vi, begin, begin + chunkGPU);
        CPUTask<V>& b = *new( allocate_child() ) CPUTask<V>(vi, begin + chunkCPU, end);

        // Set ref_count to "2 children + 1 for teh wait"
        set_ref_count(3);

        //Start b
        spawn(b);
        // Start a running and wait for all children (a and b).
        spawn_and_wait_for_all(a);
        return NULL;
    }
};
 * */ //corbera

#endif //VALUEITERATION_VALUEITERATIONPLANIFIER_H
