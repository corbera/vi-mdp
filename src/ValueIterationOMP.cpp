//
// Created by pingu on 26/03/17.
//
#include "../navigation_mdp/Common.h"
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif
#include "../navigation_mdp/TransitionMatrix.h"
#include "../navigation_mdp/RobotState.h"
#include "ValueIterationUtils.h"
#include "ValueIterationOMP.h"
#include <iostream>
#include <chrono>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

using namespace std;

cl_int valueIterationOMP(TransitionMatrix &T, cl_float* R, cl_float* Q, cl_float* V,  cl_float* Z, cl_int* P, cl_float gamma, cl_float d) {

    //cl_float Z[NS] = {numeric_limits<cl_float>::min()}; //it stores the maximum encountered policy value for the current iteration
    cl_float g = 0.001; // d * (1 - gamma_)/(2 * gamma_);
    cl_float w;
    bool notReady = true;
    cl_int nrIter = 0;

    cl_ulong s, s2;
    cl_int a;
    cl_int numThreads = 4;
    cl_ulong idx;

    //omp_set_num_threads(numThreads);
    omp_set_nested(1);
    #pragma omp parallel  num_threads(numThreads)
    {
        //cout << "NUM_THREADS: " << omp_get_num_threads() << "\n";
        //cout << "I am thread: " << omp_get_thread_num() << "\n";
        while(notReady) {
            //todo: set NR_THREADS=4  to use only the BIG processors, not little or use small chunks of work :: OK
            //hcl_int - top - to see how many threads are active
            /** Policy evaluation -- using reduction
             * !! cl_float addition is not commutative, the result will differ slightly from the sequential version*/
            #pragma omp for schedule(guided) collapse(2) private(s,a,s2,idx) reduction (+:w) /* s,a - private by default */
            for (s = 0; s < NS; s++) { // for all states
                for (a = 0; a < NX; a++) { // for all actions
                    w = 0;
                    idx = s * NX + a;
                    //#pragma omp parallel for schedule(guided) reduction (+:w)
                    for (s2 = 0; s2 < NS; s2++) { // for all states
                        w += T.getProbabilitySAS(s, a, s2) * V[s2];
                    }
                    Q[idx] = R[idx] + gamma * w;
                }
            }

            /** Policy improvement V2 -- no function call*/
            #pragma omp for schedule(guided) private(s,a,idx)
            for (s = 0; s < NS; s++) { // states
                Z[s] = numeric_limits<cl_float>::min();
                for (a = 0; a < NX; a++) { // actions
                    idx = s * NX + a;
                    if (Q[idx] > Z[s]) {
                        Z[s] = Q[idx];
                        P[s] = a;
                    }
                }
            }

            #pragma omp single      /* only one thread executes this section, whichever thread */
            {

                notReady = /*norm2VectorDifference(Z, V, NS) > g || */nrIter < MAX_ITER;
                nrIter++;
            }
            //#pragma omp flush(notReady)
            #pragma omp for schedule(guided) /*s is private by default*/
            for (s = 0; s < NS; ++s) { // copy to V the best policy values encountered in this iteration
                V[s] = Z[s];
            }

        }
    }
    return nrIter;
}
