//
// Created by dconstantinescu on 4/04/17.
//

#ifndef VALUEITERATION_VALUEITERATIONSEQ1D_H
#define VALUEITERATION_VALUEITERATIONSEQ1D_H
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif
//#include "TransitionMatrix.h"
#include "../navigation_mdp/Common.h"

cl_int valueIterationSeq1D(cl_float* h_probability, cl_ulong* h_nextStatePos, cl_ulong* h_nextCellPos, cl_float* h_R,
                           cl_float* h_Q, cl_float* h_V, cl_int* h_P, cl_float gamma_, cl_float d, float& runtime_k1,
                           float& energy_k1) ;

#endif //VALUEITERATION_VALUEITERATIONSEQ1D_H
