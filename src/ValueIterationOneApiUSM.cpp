//
// Created by Denisa on 17/04/17.
//


#include "ValueIterationOneApiUSM.h"

#include <iostream>
#include <cstring>
#include <limits>
#include <assert.h>
#include "../utils/timing.h"
// #include "ValueIterationUtils.h"
#include "../navigation_mdp/Common.h"
// #include <omp.h>
#include <math.h>
#include <string>

#include "CL/sycl.hpp"
#include "../utils/device_selector.hpp"

// Few useful acronyms; 'using namespace cl::sycl;' also helps.
constexpr auto sycl_read = cl::sycl::access::mode::read;
constexpr auto sycl_write = cl::sycl::access::mode::write;
constexpr auto sycl_global_buffer = cl::sycl::access::target::global_buffer;

using namespace cl::sycl;
using namespace std;


#include "Norm2.h"
#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>


#define MEASURE_K1

#ifdef MEASURE_K1
#include "cpucounters.h"
#include "utils.h"
#include "tbb/tick_count.h"
#endif



// Host buffers
extern cl_float*    h_probability;
extern cl_ulong*    h_nextCellPos;
extern cl_ulong*    h_nextStatePos;
extern cl_float*    h_R;
extern cl_float*    h_Q; // = (cl_float*)calloc(NS*NX,sizeof(cl_float));
extern cl_int*      h_P;
extern cl_float*    h_V;//= (cl_float*)calloc(NS,sizeof(cl_float));
extern cl_float*    h_Z;
extern cl_ulong probabilitySize;
extern queue q;

static  size_t WORK_GROUP_SIZE = 256;

//#define PCM_BROADWELL

#ifdef PCM_BROADWELL
#include "cpucounters.h"
#include <tbb/tick_count.h>
using namespace tbb;
#endif


static void report_time(const std::string &msg, cl::sycl::event e) {
  cl::sycl::cl_ulong time_start = e.get_profiling_info<cl::sycl::info::event_profiling::command_start>();
  cl::sycl::cl_ulong time_end = e.get_profiling_info<cl::sycl::info::event_profiling::command_end>();
  double elapsed = (time_end - time_start) / 1e6;
  std::cout << msg << elapsed << " milliseconds" << std::endl;
}

class PolicyEvaluationFunctor {
    private:
        unsigned long offset; 
        cl_float* nextCellPos;
        cl_float* probabilities;
        cl_float* V;
        cl_ulong* nextStatePos;
        cl_float* R;
        cl_float* Q;
    
    
    public:
    PolicyEvaluationFunctor(unsigned long offset_,
                            cl_float* nextCellPos_,
                            cl_float* probabilities_,
                            cl_float* V_,
                            cl_ulong* nextStatePos_,
                            cl_float* R_,
                            cl_float* Q_
    ):  offset(offset_), nextCellPos(nextCellPos_), 
        probabilities(probabilities_), V(V_),
        nextStatePos(nextStatePos_), R(R_), Q(Q_) {}

    void operator()(cl::sycl::id<1> index) {
        
        float w = 0.0;
        float gamma = 0.1;
        unsigned long /*s,*/ s2, myCellPos;

        myCellPos = index.get(0) + offset;
        // evaluate the actions for the current state
        // for all non zero probability nextState for <s,a> tuple (i.e. all elements from one cell of T) 
        for (s2 = nextCellPos[myCellPos]; s2 < nextCellPos[myCellPos + 1]; s2++) {
            w += probability[s2] * V[h_nextStatePos[s2]];
        }
        Q[myCellPos] =  R[myCellPos] + gamma * w ; //w * gamma + R[myCellPos];
    
    }

};

int ValueIterationOneApiUSM(  float& runtime_k1, float& energy_k1) {
    bool notReady = true;
    cl_float norm = 0;
    ulong nrIter = 0;
    ulong idx,s;
    cl_int a;


    queue q_cpu(cpu_selector{});
       
    // Using these events to time command group execution
    cl::sycl::event e1, e2, e3;

    try {
        // Create a command queue using the device selector above, and request profiling

        // See what device was actually selected for this queue.
        std::cout << "Running on " << q.get_device().get_info<cl::sycl::info::device::name>() << "\n";

        #ifdef MEASURE_K1
        float rt = 0.0;
        //Energy Counters
        PCM * pcm;
        SystemCounterState sstate1, sstate2;
        //timing
        tick_count start, end;
        //Initializing PCM library
        pcm = PCM::getInstance();
        pcm->resetPMU();
        if (pcm->program() != PCM::Success){
            std:cout << "Error in PCM library initialization" << std::endl;
            exit(-1);
        }
        #endif //MEASURE_K1

        while(notReady) {

            #ifdef MEASURE_K1
                tick_count bt, et;
                bt = tick_count::now();
                sstate1 = pcm->getSystemCounterState();
            #endif MEASURE_K1
            //code to be measured - K1
            
            /** Policy evaluation on device*/
            // Submit  command group to gpu device. The kernel is represented as a functor
    
            e1 = q.submit([&](cl::sycl::handler& cgh) {
     

                //std::cout << " accessors created" << "\n";

                // execute the kernel
                unsigned long offset = 0;
                unsigned long elements = NS*NX/2;

                PolicyEvaluationFunctor kernel(offset);
                //** functor kernel */
                cgh.parallel_for(cl::sycl::range<1>(elements), kernel);

            });  
            //std::cout << " waiting for kernerl" << "\n";

            e2 = q_cpu.submit([&](cl::sycl::handler& cgh) {
                // execute the kernel
                unsigned long offset = NS*NX/2;
                unsigned long elements = NS*NX/2;

                PolicyEvaluationFunctor kernel(offset);
                //** functor kernel */
                cgh.parallel_for(cl::sycl::range<1>(elements), kernel);
            });

            q.wait(); 
            q_cpu.wait();
            //q.wait_and_throw();

            #ifdef MEASURE_K1
            et = tick_count::now();
            sstate2 = pcm->getSystemCounterState();
            runtime_k1 += (et-bt).seconds();
            energy_k1 += getConsumedJoules(sstate1, sstate2);
            //cout << "runtime & total_energy K1:" << runtime_k1 << "\t" << energy_k1 << endl;
            #endif MEASURE_K1

            //std::cout << " done waiting for kernerl" << "\n";
            
            /** Policy improvement */
            //select the policy with the best value (max_a Q(s,a)) for every state

            e3 = q_cpu.submit([&](cl::sycl::handler& cgh) {

                // execute the kernel
                cgh.parallel_for<class kernel2>(range<1>(NS), [=](id<1> index) {
                    cl_ulong  s = index.get(0);
                    for (int a = 0; a < NX; a++) {
                        size_t idx = s * NX + a;
                        if (h_Q[idx] - h_Z[s] > EPSILON) {
                            h_Z[s] = h_Q[idx];
                            h_P[s] = a;
                            //printf("h_Z[%d] = %f \n", s, h_Z[s]);
                            
                        }
                    }

                });
            }); 

            q_cpu.wait();


            //print1DimArray(b_P_acc, NS, 20);

            //std::cout << " done with policy improvement" << "\n";

            //Norm2
            //parallel_reduce( blocked_range<size_t>(0,NS,1), n2sum);
            norm = parallel_reduce(blocked_range<size_t>(0,NS), 0.0f,
                                [&](blocked_range<size_t> r, double total_norm)
                                {
                                    for (size_t i=r.begin(); i<r.end(); ++i)
                                    {
                                        total_norm += pow((h_Z[i]-h_V[i]),2);
                                    }

                                    return total_norm;
                                }, std::plus<double>() );

            norm = sqrt(norm);
            //std::cout <<"\nnorm: " << norm << std::endl;
            //norm = sqrt(n2sum.my_sum);



            notReady = (norm > g) && nrIter < MAX_ITER;
            norm = 0;
            //swap old V with new V (Z)

            /*for (int i=0; i<NS; i++)
                b_V_acc[i] = b_Z_acc[i];
                */
            memcpy(h_V, h_Z, NS*sizeof(cl_float));
    
            nrIter++; // #iters to convergence

            //std::cout << "\nValtItOCL #iter: " << nrIter << "\n";
            


        } // while
    }
    catch (cl::sycl::exception e) {
        std::cout << "SYCL exception caught: " << e.what() << std::endl;
        return 1;
    }

    // report execution times:
    // report_time(" kerne1 time: ", e1);
    // report_time(" kerne2 time: ", e2);

    return nrIter;

}
