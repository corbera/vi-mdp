//============================================================================
// Name			: ValueIterationPlanifier.h
// Author		: Denisa Constantinescu
// Version		: 1.0
// Date			: 19 / 06 / 2017
// Copyright	: Department. Computer's Architecture (c)
// Description	: ValueIterationPlanifier.h implementation for ValueIteration project
//                The workload will be distributed row-wise, not by T matrix cell,
//                ({begin, end} in 0..NS)
//============================================================================

#ifndef VALUEITERATION_VALUEITERATIONPLANIFIER_H
#define VALUEITERATION_VALUEITERATIONPLANIFIER_H

//#define POLICY_IMPROVMENT
//#define DYNAMIC
//#define ORACLE

#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif

#include <cstdlib>
#include <iostream>

#include <tbb/tick_count.h>
#include <tbb/blocked_range.h>
#include "tbb/parallel_for.h"
#include "tbb/task.h"

#include "../navigation_mdp/Common.h"

#ifdef ORACLE
#include <Oracle.h>
#endif
#ifdef DYNAMIC
#include <Dynamic.h>
#endif

using namespace std;
using namespace tbb;

/*****************************************************************************
 * Global variables
 * **************************************************************************/

// Device buffers
extern cl_mem d_probabilities;
extern cl_mem d_nextCellPos;
extern cl_mem d_nextStatePos;
extern cl_mem d_R;
extern cl_mem d_Q;
extern cl_mem d_P;
extern cl_mem d_V;
extern cl_mem d_Z;
extern cl_ulong probabilitySize;

// Host buffers
extern cl_float*    h_probability;
extern cl_ulong*    h_nextCellPos;
extern cl_ulong*    h_nextStatePos;
extern cl_float*    h_R;
extern cl_float*    h_Q;
extern cl_int*      h_P;
extern cl_float*    h_V;
extern cl_float*    h_Z;


class ValueIterationPlanifier {
public:
    bool firsttime = true;
public:
    /*Allocate memory objects on GPU memory address space*/
    void AllocateMemoryObjects() {
        cl_int err = 0;
        cl_int size;

        /** Allocate device memory */
        //probabilities
        d_probabilities = clCreateBuffer(context,  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, probabilitySize * sizeof(cl_float), h_probability, &err );
        //CHECK_CL_ERROR(err, "clCreateBuffer");
        if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_probabilities (size:%d) => %d\n", probabilitySize, err); exit(0);}


        //nextCellPos
        size = NX * NS + 1;
        d_nextCellPos = clCreateBuffer( context,  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, size * sizeof(cl_ulong), h_nextCellPos, &err );
        if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_nextCellPos (size:%d) => %d\n", NX * NS + 1, err); exit(0);}

        //V
        d_V = clCreateBuffer( context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, NS * sizeof(cl_float), NULL, &err );
        if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_V (size:%d) => %d\n", NS, err);exit(0); }
#ifdef POLICY_IMPROVMENT
        //V
        d_Z = clCreateBuffer( context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR/*CL_MEM_ALLOC_HOST_PTR*/, NS * sizeof(cl_float), h_Z, &err );
        if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_Z (size:%d) => %d\n", NS, err);exit(0); }
#endif
        //Q
        d_Q = clCreateBuffer( context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, NX * NS  * sizeof(cl_float), NULL, &err );
        if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_Q (size:%d) => %d\n", NX * NS, err); exit(0);}

        //nextStatePos
        d_nextStatePos = clCreateBuffer( context,  CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, probabilitySize * sizeof(cl_ulong), h_nextStatePos, &err );
        if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_nextStatePos (size:%d) => %d\n", probabilitySize, err); exit(0);}

        //R
        d_R = clCreateBuffer( context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, NX * NS  * sizeof(cl_float), h_R, &err );
        if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_R (size:%d) => %d\n", NX * NS, err); exit(0);}
#ifdef POLICY_IMPROVMENT
        //P
        d_P = clCreateBuffer( context, CL_MEM_READ_WRITE, NS * sizeof(cl_int), NULL, &err );
        if(err != CL_SUCCESS) { printf("ERROR: clCreateBuffer d_P (size:%d) => %d\n", NS, err); exit(0);}

        h_Z = (cl_float  *)clEnqueueMapBuffer( command_queue,   // Corresponding command queue
                                               d_Z,     // Buffer to be mapped
                                               CL_TRUE,         // block_map, CL_TRUE: can't be unmapped before at least 1 read
                                               CL_MAP_READ,    // mapped for reading or writing?
                                               0,               // offset
                                               NS,         // number of bytes mapped
                                               0,               // number of events in the wait list
                                               NULL,            // event wait list
                                               NULL,            // event
                                               NULL );


        h_P = (cl_int  *)clEnqueueMapBuffer( command_queue,   // Corresponding command queue
                                             d_P,     // Buffer to be mapped
                                             CL_TRUE,         // block_map, CL_TRUE: can't be unmapped before at least 1 read
                                             CL_MAP_READ,    // mapped for reading or writing?
                                             0,               // offset
                                             NS,         // number of bytes mapped
                                             0,               // number of events in the wait list
                                             NULL,            // event wait list
                                             NULL,            // event
                                             NULL );
#endif
    }

    /*This function send the data to be computed in the GPU*/
    void sendObjectToGPU( cl_uint begin, cl_uint end, cl_event * event)  {
        //if(firsttime) {
          //  firsttime = false;
            cl_int err;
            //V
            err = clEnqueueWriteBuffer( command_queue, d_V, /*non blocking*/ CL_FALSE, /*offset*/ 0, NS * sizeof(cl_float), h_V, 0, NULL, event );
#ifdef DEBUG
            printf("\nsendObjectToGPU\n->begin: %d, end: %d \n", begin, end);
#endif
            if(err != CL_SUCCESS) { printf("ERROR: clEnqueueWriteBuffer d_V (size:%d) => %d\n", NS, err); exit(0); }

        //}
    }

    /*This function launches the kernel*/
    void OperatorGPU(cl_uint begin, cl_uint end, cl_event * event) {
        const cl_uint d_NS = NS;
        const cl_uint d_NX = NX;
        cl_int err;
        err  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_probabilities);
        err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_V);
        err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_Q);
        err |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &d_nextCellPos);
        err |= clSetKernelArg(kernel, 4, sizeof(cl_mem), &d_nextStatePos);
        err |= clSetKernelArg(kernel, 5, sizeof(cl_mem), &d_R);
        err |= clSetKernelArg(kernel, 6, sizeof(cl_ulong), &d_NS);
        err |= clSetKernelArg(kernel, 7, sizeof(cl_uint), &d_NX);
        err |= clSetKernelArg(kernel, 8, sizeof(cl_uint), &begin);
        err |= clSetKernelArg(kernel, 9, sizeof(cl_uint), &end);

        //printf("\nclSetKernelArg err:%d",err);
        if (err != CL_SUCCESS) {
            printf("Failed setting kernel Args!\n");
            exit(0);
        }

        // Define an index space (global work size) of work
        // items for execution. A workgroup size (local work size) is not required,
        // but can be used.
        size_t globalWorkSize[1];
        globalWorkSize[0] = (end-begin);

        err = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, globalWorkSize, NULL, 0, NULL, event);
        if (err != CL_SUCCESS) {
            printf("Failed launching kernel!\n");
            exit(0);
        }

    }

    /*Function to receive the data from GPU memory*/
    void getBackObjectFromGPU(cl_uint begin, cl_uint end, cl_event *event) {
        cl_int err = 0;
		err = clEnqueueReadBuffer(
            command_queue,
            d_Q,
            CL_FALSE,
            begin * NX * sizeof(cl_float) /*offset*/,
            (end - begin) * NX * sizeof(cl_float) /*size of bytes being read*/ ,
            &(h_Q[begin * NX]),
            0, NULL, event);
#ifdef DEBUG
        cout << "getBackObjectFromGPU" << endl;
        printf("->begin: %d, end: %d \n", begin, end);
        printf("->offset: %d, elements: %d \n", begin * NX , (end - begin) * NX);
#endif
        if (err != CL_SUCCESS) {
            //CL_INVALID_VALUE if the region being read specified by (offset, cb) is out of bounds or if ptr is a NULL value.
            printf("\nclEnqueueReadBuffer d_Q err:%d ",err);
            printf(" Failed copying  d_Q to host!\n");
            printf(" begin: %d, end: %d \n", begin, end);
            exit(0);
        }
    }

    /*Serial version of the code */
    void OperatorCPU(cl_uint begin, cl_uint end) { // 0,NS*NX
        int  maxIdx;
        cl_float w, maxQs;
        for (int s = begin; s < end; s++) { // divide the outer loop, by state id
            int idx = s * NX;
            for (int a = 0; a < NX; a++) {
                w = 0;
                /* for all non zero probability nextState for <s,a> tuple*/
                for (cl_ulong s2 = h_nextCellPos[idx]; s2 < h_nextCellPos[idx + 1]; s2++) {
                    w += h_probability[s2] * h_V[h_nextStatePos[s2]];
                }
                h_Q[idx] = h_R[idx] + GAMMA * w;
                idx++;
            }
        }
    }

};

/*****************************************************************************
 * Parallel For (TBB only CPUs)
 * **************************************************************************/
class MYParallelFor {
    ValueIterationPlanifier *vip;
public:
    /*Class Constructor*/
    MYParallelFor(ValueIterationPlanifier *vip_) {
        vip = vip_;
    }

    /*Operator function*/
    void operator()( const blocked_range<cl_ulong>& range ) const {
        vip->OperatorCPU(range.begin(), range.end());
    }
};

/*This Function launches the parallel for pattern only on CPU cores, based on TBB template*/
void ParallelFORCPUs(size_t start, size_t end, ValueIterationPlanifier *vip ) {
    MYParallelFor pf(vip);
    parallel_for( blocked_range<cl_ulong>( start, end ), pf);
}

/*****************************************************************************
 * Raw Tasks
 * **************************************************************************/
template <class V>
class GPUTask: public task {
public:
    V *vi;
    cl_uint begin;
    cl_uint end;

    GPUTask<V>(V *vi, cl_uint begin, cl_uint end) : vi(vi), begin(begin), end(end) {}

    //Override virtual function task::execute
    task* execute() {
        #ifdef USEBARRIER
        //cerr << "GPU before barrier1" << endl;
        getLockGPU();
        //cerr << "GPU after barrier1" << endl;
        #endif
        #ifdef PJTRACER
        tracer->gpuStart();
        #endif

        tick_count start = tick_count::now();
        vi->sendObjectToGPU(begin, end, NULL);
        vi->OperatorGPU(begin, end, NULL);
        vi->getBackObjectFromGPU(begin, end, NULL);
        clFinish(command_queue);
        tick_count finish = tick_count::now();
        gpuThroughput = (end -  begin) / ((finish-start).seconds()*1000);

        #ifdef PJTRACER
        tracer->gpuStop();
        #endif
        #ifdef USEBARRIER
        //cerr << "GPU antes de barrier2" << endl;
        freeLockGPU();
        //cerr << "GPU despues de barrier2" << endl;
        getLockCPU();
        freeLockCPU();
        #endif
        return NULL;
    }
};

template <class V>
class CPUTask: public task {
public:
    V *vi;
    cl_uint begin;
    cl_uint end;

    CPUTask<V>(V *vi, cl_uint begin, cl_uint end) : vi(vi), begin(begin), end(end) {}

    // Overrides virtual function task::execute
    task* execute() {
#ifdef USEBARRIER
        //cerr << "CPU antes de barrier1" << endl;
        getLockCPU();
        //cerr << "CPU despues de barrier1" << endl;
#endif
        tick_count start = tick_count::now();
        ParallelFORCPUs(begin, end, vi);
        tick_count finish = tick_count::now();
        cpuThroughput = (end -  begin) / ((finish-start).seconds()*1000);

#ifdef USEBARRIER
        //cerr << "CPU antes de barrier2" << endl;
        freeLockCPU();
        //cerr << "CPU despues de barrier2" << endl;
        getLockGPU();
        freeLockGPU();
#endif
        return NULL;
    }
};

template <class V>
class ROOTTask : public task {
public:
    V *vi;
    cl_uint begin;
    cl_uint end;

    ROOTTask<V>(V *vi, cl_uint begin, cl_uint end) : vi(vi), begin(begin), end(end) {}

    task *execute() {
        #ifdef USEBARRIER
        //cerr << "before barrier_init" << endl;
        barrier_init();
        //cerr << "after barrier_init" << endl;
        #endif
        GPUTask<V>& a = *new( allocate_child() ) GPUTask<V>(vi, begin, begin + chunkGPU);
        CPUTask<V>& b = *new( allocate_child() ) CPUTask<V>(vi, begin + chunkCPU, end);

        // Set ref_count to "2 children + 1 for teh wait"
        set_ref_count(3);

        //Start b
        spawn(b);
        // Start a running and wait for all children (a and b).
        spawn_and_wait_for_all(a);
        return NULL;
    }

};
#endif //VALUEITERATION_VALUEITERATIONPLANIFIER_H
