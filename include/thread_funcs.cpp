#include <pthread.h>
#include <sched.h>

#ifdef __APPLE__
#include <unistd.h>
#include <stdint.h>
#define SYSCTL_CORE_COUNT   "machdep.cpu.core_count"

typedef struct cpu_set {
  uint32_t    count;
} cpu_set_t;

static inline void
CPU_ZERO(cpu_set_t *cs) { cs->count = 0; }
static inline void
__CPU_ZERO(cpu_set_t *cs) { cs->count = 0; }

static inline void
CPU_SET(int num, cpu_set_t *cs) { cs->count |= (1 << num); }
static inline void
__CPU_SET(int num, cpu_set_t *cs) { cs->count |= (1 << num); }

static inline int
CPU_ISSET(int num, cpu_set_t *cs) { return (cs->count & (1 << num)); }

int sched_getaffinity(pid_t pid, size_t cpu_size, cpu_set_t *cpu_set)
{
  int32_t core_count = 0;
  size_t  len = sizeof(core_count);
  int ret = sysctlbyname(SYSCTL_CORE_COUNT, &core_count, &len, 0, 0);
  if (ret) {
    printf("error while get core count %d\n", ret);
    return -1;
  }
  cpu_set->count = 0;
  for (int i = 0; i < core_count; i++) {
    cpu_set->count |= (1 << i);
  }

  return 0;
}
int pthread_setaffinity_np(pthread_t thread, size_t cpu_size,
                           cpu_set_t *cpu_set)
{
  thread_port_t mach_thread;
  int core = 0;

  for (core = 0; core < 8 * cpu_size; core++) {
    if (CPU_ISSET(core, cpu_set)) break;
  }
  printf("binding to core %d\n", core);
  thread_affinity_policy_data_t policy = { core };
  mach_thread = pthread_mach_thread_np(thread);
  thread_policy_set(mach_thread, THREAD_AFFINITY_POLICY,
                    (thread_policy_t)&policy, 1);
  return 0;
}
#else
#include <asm/unistd.h>
#endif
#include <string.h>
#include <errno.h>


#define gettid()  syscall(__NR_gettid)

///   sched_getcpu()     to get core ID

#define handle_error_en(en, msg) \
               do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

int set_thread_affinity_CORE(pthread_t th, int cpu)
{
	cpu_set_t mask;
	__CPU_ZERO_S(sizeof(cpu_set_t),&mask);
	__CPU_SET_S(cpu,sizeof(cpu_set_t), &mask);
	
	return pthread_setaffinity_np(th, sizeof(cpu_set_t), &mask);
}

int set_thread_affinity_A7(pthread_t th)
{
	cpu_set_t mask;
	__CPU_ZERO_S(sizeof(cpu_set_t),&mask);
	for(int i=0; i<4; i++) __CPU_SET_S(i,sizeof(cpu_set_t), &mask);
	
	return pthread_setaffinity_np(th, sizeof(cpu_set_t), &mask);
}

int set_thread_affinity_A15(pthread_t th)
{
	cpu_set_t mask;
	__CPU_ZERO_S(sizeof(cpu_set_t),&mask);
	for(int i=4; i<8; i++) __CPU_SET_S(i,sizeof(cpu_set_t), &mask);
	
	return pthread_setaffinity_np(th, sizeof(cpu_set_t), &mask);
}

int set_thread_prio(pthread_t th, int n)
{
  struct sched_param sp;
  sp.sched_priority=n;
  pthread_setschedparam(pthread_self(), SCHED_OTHER, &sp);  // only n=0 allowed
}


int set_thread_prio_RT(pthread_t th, int n)
{
  struct sched_param sp;
  sp.sched_priority=n;
  pthread_setschedparam(pthread_self(), SCHED_RR, &sp);  // 
}

void display_sched_attr(int policy, struct sched_param *param)
       {
           printf("    policy=%s, priority=%d\n",
                   (policy == SCHED_FIFO)  ? "SCHED_FIFO" :
                   (policy == SCHED_RR)    ? "SCHED_RR" :
                   (policy == SCHED_OTHER) ? "SCHED_OTHER" :
                   "???",
                   param->sched_priority);
       }

void display_thread_prio(char *msg)
       {
           int policy, s;
           struct sched_param param;

           s = pthread_getschedparam(pthread_self(), &policy, &param);
           if (s != 0)
               handle_error_en(s, "pthread_getschedparam");

           printf("%s\n", msg);
           display_sched_attr(policy, &param);
       }
       
class Thread_pinning {
private:  
  pthread_mutex_t mux;   // for auto pinning
  int ncores;  // size of pool of cores for auto pinnig
  int * cores; // list of cores for auto pinnig
  int count;   // index of next core to assing in auto
  int nclusters; // number of clusters
  int ** clusters; //set of clusters
  int * clussize; // size of each cluster
  int type; // 0= cores, 1 = clusters
  
public:

  Thread_pinning(int n, int* c)
  {
	  ncores=n;
	  cores=(int *) malloc(n*sizeof(int));
	  memcpy(cores,c,n*sizeof(int));
	  pthread_mutex_init(&mux, NULL);
	  count=0;
	  type=0;
  }
  
  
  Thread_pinning(int n, int * c, int *cluster0, int *cluster1)
  {
	  int i, clus=2;
	  ncores=n;
	  nclusters=clus;
	  clussize=(int *) malloc(clus*sizeof(int));
	  clusters= (int **) malloc(clus*sizeof(int *));
	  cores=(int *) malloc(n*sizeof(int));
	  memcpy(cores,c,n*sizeof(int));
	  for (i=0; i<clus; i++)
	  { 
		  clussize[i]=n/clus;
		  clusters[i]=(int *) malloc(n/clus*sizeof(int));
		 
	  }
	  memcpy(clusters[0],cluster0,n/clus*sizeof(int));
	  memcpy(clusters[1],cluster1,n/clus*sizeof(int));
	  pthread_mutex_init(&mux, NULL);
	  count=0;
	  type=1;
  }
  
  Thread_pinning(int n, int clus, int * c)
  {
	  int i;
	  ncores=n;
	  nclusters=clus;
	  clussize=(int *) malloc(clus*sizeof(int));
	  clusters= (int **) malloc(clus*sizeof(int *));
	  cores=(int *) malloc(n*sizeof(int));
	  memcpy(cores,c,n*sizeof(int));
	  pthread_mutex_init(&mux, NULL);
	  count=0;
	  type=1;
  }
  
  void init_cluster(int i, int n, int *c)
  {
	   clusters[i]=(int *) malloc(n*sizeof(int));
	   memcpy(clusters[i],c,n*sizeof(int));
	   clussize[i]=n;
  }
  
  ~Thread_pinning()
  {
	  int i;
	  
	  if(type==0) free(cores);
	  if(type==1) 
	  {
		  free(clussize);
		  for(i=0; i<nclusters; i++)  free (clusters[i]);
		  free(clusters);
	  }
	  pthread_mutex_destroy(&mux);
  }
  
  int next()
  {
	  int mine=-1;
	  pthread_mutex_lock (&mux);  // protect count
	  if(count>=ncores) count=0;
	  mine=count;
	  count++;
	  pthread_mutex_unlock (&mux);
	  if(mine==-1) return mine;  // no more cores availables
	  if (type==0) return core(cores[mine]);
	  if (type==1) return group(clussize[cores[mine]],clusters[cores[mine]]);
	  
	 
  }
  int last()
  {
	  cpu_set_t mask;
	  __CPU_ZERO_S(sizeof(cpu_set_t),&mask);
	  __CPU_SET_S(cores[ncores-1] ,sizeof(cpu_set_t), &mask);
	  return pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &mask);
  }
  int core(int c)
  {
	  cpu_set_t mask;
	  __CPU_ZERO_S(sizeof(cpu_set_t),&mask);
	  __CPU_SET_S(c ,sizeof(cpu_set_t), &mask);
	  return pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &mask);
  }
  int group(int n, int *c)
  {
	  
	  cpu_set_t mask;
	  __CPU_ZERO_S(sizeof(cpu_set_t),&mask);
	  for(int i=0; i<n; i++) { printf("%d = %d ",i,c[i]); __CPU_SET_S(c[i],sizeof(cpu_set_t), &mask); }
	  return pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &mask);
  }
};

