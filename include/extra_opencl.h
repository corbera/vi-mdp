#pragma once

// copied from cl.h 2.0
#ifndef CL_BLOCKING
#define CL_BLOCKING CL_TRUE
#endif

#ifndef CL_NON_BLOCKING
#define CL_NON_BLOCKING CL_FALSE
#endif
