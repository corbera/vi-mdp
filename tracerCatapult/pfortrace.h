#ifndef PFORTRACE_H
#define PFORTRACE_H

#include <stdio.h>

#define USETBB

#include <time.h>
#ifdef USETBB
#include "tbb/task_scheduler_init.h"

	#include "tbb/tick_count.h"
#endif

/*****************************************************************************
 * MUTEX & TIMER
 * **************************************************************************/
#ifdef WIN32
	#include <Windows.h>
        typedef CRITICAL_SECTION    ThreadMutex;
	#define INITMUTEX(MUTEX)  InitializeCriticalSection(MUTEX)
	#define ENTERMUTEX(MUTEX) EnterCriticalSection(MUTEX)
	#define LEAVEMUTEX(MUTEX)  LeaveCriticalSection(MUTEX)	

	#ifdef USETBB
		using namespace tbb;
		typedef tick_count TIMESTRUCT;
	#else
		#define _DOVA_UNIX_SECONDS 11644473600
		#define CLOCK_REALTIME 0
		#define CLOCK_MONOTONIC 0
		struct timespec {
			time_t tv_sec;
			long tv_nsec;
		};
		typedef struct timespec TIMESTRUCT;
	#endif
#else
	#include <pthread.h>
        typedef pthread_mutex_t  ThreadMutex;
	#define INITMUTEX(MUTEX) pthread_mutex_init(MUTEX , NULL)
	#define ENTERMUTEX(MUTEX) pthread_mutex_lock(MUTEX)
	#define LEAVEMUTEX(MUTEX) pthread_mutex_unlock(MUTEX)
#ifdef USETBB
	using namespace tbb;
	typedef tick_count TIMESTRUCT;
#endif
#endif

/* Colores 
    thread_state_uninterruptible: new tr.b.Color(182, 125, 143),
    thread_state_iowait: new tr.b.Color(255, 140, 0),
    thread_state_running: new tr.b.Color(126, 200, 148),
    thread_state_runnable: new tr.b.Color(133, 160, 210),
    thread_state_sleeping: new tr.b.Color(240, 240, 240),
    thread_state_unknown: new tr.b.Color(199, 155, 125),
    light_memory_dump: new tr.b.Color(0, 0, 180),
    detailed_memory_dump: new tr.b.Color(180, 0, 180),
    generic_work: new tr.b.Color(125, 125, 125),
    good: new tr.b.Color(0, 125, 0),
    bad: new tr.b.Color(180, 125, 0),
    terrible: new tr.b.Color(180, 0, 0),
    black: new tr.b.Color(0, 0, 0),
    rail_response: new tr.b.Color(67, 135, 253),
    rail_animation: new tr.b.Color(244, 74, 63),
    rail_idle: new tr.b.Color(238, 142, 0),
    rail_load: new tr.b.Color(13, 168, 97),
    used_memory_column: new tr.b.Color(0, 0, 255),
    older_used_memory_column: new tr.b.Color(153, 204, 255),
    tracing_memory_column: new tr.b.Color(153, 153, 153),
    heap_dump_stack_frame: new tr.b.Color(128, 128, 128),
    heap_dump_object_type: new tr.b.Color(0, 0, 255),
    cq_build_running: new tr.b.Color(255, 255, 119),
    cq_build_passed: new tr.b.Color(153, 238, 102),
    cq_build_failed: new tr.b.Color(238, 136, 136),
    cq_build_abandoned: new tr.b.Color(187, 187, 187),
    cq_build_attempt_runnig: new tr.b.Color(222, 222, 75),
    cq_build_attempt_passed: new tr.b.Color(103, 218, 35),
    cq_build_attempt_failed: new tr.b.Color(197, 81, 81)
  */
        
#define COLOR_RED   "terrible"
#define COLOR_BLUE  "light_memory_dump"
#define COLOR_GREEN "good"

#ifdef WIN32
#include "windows.h"
// Supports functions
const VOID ErrorExit (LPSTR lpszMessage) 
{ 
   fprintf(stderr, "%s\n", lpszMessage); 
   ExitProcess(0); 
}
#else
__thread pthread_t dwTlsIndex = 0; // Thread index
#endif

class PFORTRACER {

private:
        FILE* trace_file;   // file to write in
        ThreadMutex mutex;
        TIMESTRUCT start_time;	// clock_gettime(CLOCK_REALTIME, ...) at starting point
        bool firstEvent;
        
#ifdef WIN32
	DWORD dwTlsIndex; //Thread local storage
#else
	//__thread pthread_t dwTlsIndex; // Thread index
#endif
        
#ifdef USETBB
    double diff(TIMESTRUCT start, TIMESTRUCT end)
    {
        return (end-start).seconds() * 1000 * 1000; // return us
    }
#else
    double diff(TIMESTRUCT start, TIMESTRUCT end)
    {
        struct timespec temp;
        if ((end.tv_nsec-start.tv_nsec)<0) {
            temp.tv_sec = end.tv_sec-start.tv_sec-1;
            temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
        } else {
            temp.tv_sec = end.tv_sec-start.tv_sec;
            temp.tv_nsec = end.tv_nsec-start.tv_nsec;
        }
        return (double)temp.tv_sec+ (double)temp.tv_nsec/1000000000.0;;
    }
#endif

    // print an event to the trace file: i -> instant event, B -> begin event, E -> end event
    void pushEvent(const char event, const char * color, const char * name) {
#ifdef WIN32
        LPVOID lpvData; 
        // get local id
        lpvData = TlsGetValue(dwTlsIndex);
        if ((lpvData == 0) || (GetLastError() != ERROR_SUCCESS))
                        ErrorExit("TlsGetValue error");
        char *thid = ((char*)lpvData);
#else
        char thid[256];
        sprintf(thid, "%lu", dwTlsIndex);
#endif
        TIMESTRUCT stop_time; 	// clock_gettime(CLOCK_REALTIME, ...) at finish
#ifdef USETBB
        stop_time = tick_count::now();     
#else
        clock_gettime(CLOCK_REALTIME, &stop_time); 
#endif 
	ENTERMUTEX(&mutex);
        if (firstEvent)
            firstEvent = false;
        else
            fprintf(trace_file, ",\n");
        if (name)
            fprintf(trace_file,
                    "  {\"name\": \"%s\", \"args\": {},\"pid\": 1, \"ts\": %f, \"cat\": \"trace\", \"tid\": %s, \"ph\": \"%c\", \"cname\": \"%s\"}",
                    name, diff(start_time, stop_time), thid, event, color);
        else 
            fprintf(trace_file,
                    "  {\"args\": {},\"pid\": 1, \"ts\": %f, \"cat\": \"trace\", \"tid\": %s, \"ph\": \"%c\", \"cname\": \"%s\"}",
                    diff(start_time, stop_time), thid, event, color);
	LEAVEMUTEX(&mutex);
    }
        
public:

// Trace functions
    PFORTRACER(const char *filename) {
        firstEvent = true;
        trace_file = fopen(filename,"w");
        if (trace_file == 0) return;
        // Init file
        fprintf(trace_file, "[\n");

    #ifdef WIN32
        // Init thread local storage
        if ((dwTlsIndex = TlsAlloc()) == TLS_OUT_OF_INDEXES)
            ErrorExit("TlsAlloc failed"); 
    #else
        dwTlsIndex = 0;
    #endif
#ifdef USETBB
        start_time = tick_count::now();     
#else
        clock_gettime(CLOCK_REALTIME, &start_time);
#endif
        /* Inicializa un semaforo para la exclusioni mutua */
        //pthread_mutex_init(&mutex, NULL);
        INITMUTEX(&mutex);
        pushEvent('i', COLOR_RED, "Begin Trace");

    }

    ~PFORTRACER() {
    #ifdef WIN32
        TlsFree(dwTlsIndex);
    #endif
        if (trace_file == 0) return;
        fprintf(trace_file, "\n]\n");
        fclose(trace_file);
    }

    void beginThreadTrace(void) {
    #ifdef WIN32
            LPVOID lpvData; 
            // get local id
            lpvData = TlsGetValue(dwTlsIndex);
            if (lpvData == 0) {
                    if (GetLastError() != ERROR_SUCCESS)
                            ErrorExit("TlsGetValue error");

                    // create local id if doesn't exist
                    lpvData = (LPVOID) LocalAlloc(LPTR, 256); 
                    sprintf((char*)lpvData, "%d", GetCurrentThreadId());
                    if (! TlsSetValue(dwTlsIndex, lpvData))
                            ErrorExit("TlsSetValue error");
                    // create container for this thread
                    char *thid = ((char*)lpvData);
            }
    #else
            if (!dwTlsIndex) {
                    dwTlsIndex = pthread_self();
                    char thid[256];
                    sprintf(thid, "%lu", dwTlsIndex);
            }
    #endif
            if (trace_file == 0) return;
            pushEvent('i', COLOR_RED, "Begin Thread");
    }

    void gpuStart(void) {
    #ifdef WIN32
            LPVOID lpvData; 
            // get local id
            lpvData = TlsGetValue(dwTlsIndex);
            if (lpvData == 0) {
                    if (GetLastError() != ERROR_SUCCESS)
                            ErrorExit("TlsGetValue error");

                    // create local id if doesn't exist
                    lpvData = (LPVOID) LocalAlloc(LPTR, 256); 
                    sprintf((char*)lpvData, "%d", GetCurrentThreadId());
                    if (! TlsSetValue(dwTlsIndex, lpvData))
                            ErrorExit("TlsSetValue error");

                    // create container for this thread
                    char *thid = ((char*)lpvData);
            }
    #else
            if (!dwTlsIndex) {
                    dwTlsIndex = pthread_self();
                    char thid[256];
                    sprintf(thid, "%lu", dwTlsIndex);
            }
    #endif

            if (trace_file == 0) return;
            pushEvent('B', COLOR_GREEN, "GPU");
    }

    void gpuStop(void) {
            if (trace_file == 0) return;
            pushEvent('E', COLOR_GREEN, "GPU");
    }

    void cpuStop(void) {
            if (trace_file == 0) return;
            pushEvent('E', COLOR_BLUE, "CPU");
    }

    void cpuStart(void) {
    #ifdef WIN32
            LPVOID lpvData; 
            // get local id
            lpvData = TlsGetValue(dwTlsIndex);
            if (lpvData == 0) {
                    if (GetLastError() != ERROR_SUCCESS)
                            ErrorExit("TlsGetValue error");

                    // create local id if doesn't exist
                    lpvData = (LPVOID) LocalAlloc(LPTR, 256); 
                    sprintf((char*)lpvData, "%d", GetCurrentThreadId());
                    if (! TlsSetValue(dwTlsIndex, lpvData))
                            ErrorExit("TlsSetValue error");

                    // create container for this thread
                    char *thid = ((char*)lpvData);
            }
    #else
            if (!dwTlsIndex) {
                    dwTlsIndex = pthread_self();
                    char thid[256];
                    sprintf(thid, "%lu", dwTlsIndex);
            }
    #endif
            if (trace_file == 0) return;
            pushEvent('B', COLOR_BLUE, "CPU");
    }

    void gpuStart(const char *comment) {
    #ifdef WIN32
            LPVOID lpvData; 
            // get local id
            lpvData = TlsGetValue(dwTlsIndex);
            if (lpvData == 0) {
                    if (GetLastError() != ERROR_SUCCESS)
                            ErrorExit("TlsGetValue error");

                    // create local id if doesn't exist
                    lpvData = (LPVOID) LocalAlloc(LPTR, 256); 
                    sprintf((char*)lpvData, "%d", GetCurrentThreadId());
                    if (! TlsSetValue(dwTlsIndex, lpvData))
                            ErrorExit("TlsSetValue error");

                    // create container for this thread
                    char *thid = ((char*)lpvData);
            }
    #else
            if (!dwTlsIndex) {
                    dwTlsIndex = pthread_self();
                    char thid[256];
                    sprintf(thid, "%lu", dwTlsIndex);
            }
    #endif
            if (trace_file == 0) return;
            pushEvent('B', COLOR_GREEN, comment);
    }

    void gpuStop(const char *comment) {
            if (trace_file == 0) return;
            pushEvent('E', COLOR_GREEN, comment);
    }

    void cpuStop(const char *comment) {
            if (trace_file == 0) return;
            pushEvent('E', COLOR_BLUE, comment);
    }

    void cpuStart(const char *comment) {
    #ifdef WIN32
            LPVOID lpvData; 
            // get local id
            lpvData = TlsGetValue(dwTlsIndex);
            if (lpvData == 0) {
                    if (GetLastError() != ERROR_SUCCESS)
                            ErrorExit("TlsGetValue error");

                    // create local id if doesn't exist
                    lpvData = (LPVOID) LocalAlloc(LPTR, 256); 
                    sprintf((char*)lpvData, "%d", GetCurrentThreadId());
                    if (! TlsSetValue(dwTlsIndex, lpvData))
                            ErrorExit("TlsSetValue error");

                    // create container for this thread
                    char *thid = ((char*)lpvData);
            }
    #else
            if (!dwTlsIndex) {
                    dwTlsIndex = pthread_self();
                    char thid[256];
                    sprintf(thid, "%lu", dwTlsIndex);
            }
    #endif
            if (trace_file == 0) return;
            pushEvent('B', COLOR_BLUE, comment);
    }

    void newEvent(const char *comment) {
            if (trace_file == 0) return;
            pushEvent('i', COLOR_RED, comment);
    }
    
    void newEvent() {
            if (trace_file == 0) return;
            pushEvent('i', COLOR_RED, "ev");
    }
};

#endif //PFORTRACE_H