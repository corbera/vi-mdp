#include "pfortrace.h"
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <stdlib.h>

#define NTH 5
PFORTRACER tr("example.json");
typedef struct {int iter; bool gpu;} info_t; 
void * doWork(void *info) {
    srand (time(NULL));
    int iter = ((info_t *)info)->iter;
    bool cpu = !(((info_t *)info)->gpu);
    int i;
    for (i = 0; i < iter; i++) {
        usleep(rand() % 10);
        if (cpu)
            tr.cpuStart();
        else
            tr.gpuStart();
        usleep(rand() % 90 + 10);
        if (cpu)
            tr.cpuStop();
        else
            tr.gpuStop();
    }
}

int main() {
    int i;

    pthread_t workers[NTH];
    info_t info[NTH];
    for (i = 0; i < NTH; i++) {
        info[i].iter = rand() % 5 + 1;
        info[i].gpu = (i == 0);
        pthread_create(&workers[i], NULL, doWork, &info[i]);
    }
    for (i = 0; i < NTH; i++) {
        pthread_join(workers[i], NULL);
    }
}