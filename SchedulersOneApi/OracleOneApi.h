//============================================================================
// Name			: HSOracle.h
// Author		: Antonio Vilches
// Version		: 1.0
// Date			: 29 / 12 / 2014
// Copyright	: Department. Computer's Architecture (c)
// Description	: Oracle scheduler implementation
//============================================================================

#include <atomic>
#define USEBARRIER

#ifdef USEBARRIER
#include "barrier.h"
#endif

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>
#include "SchedulerOneApi.h"
#include "tbb/pipeline.h"
#include "tbb/tick_count.h"

#ifdef Win32
#include "PCM_Win/windriver.h"
#else
#include "cpucounters.h"
#endif

#ifdef PJTRACER
#include "pfortrace.h"
#endif

using namespace std;
using namespace tbb;

#ifdef DEBUG
extern int step;
#endif

/*****************************************************************************
 * Defines
 * **************************************************************************/
#define CPU 0
#define GPU 1
#define GPU_OFF -100 //Arbitrary value
#define SEP "\t"

/*****************************************************************************
 * Global variables
 * **************************************************************************/
std::atomic<int> gpuStatus;
int chunkCPU;
int chunkGPU;
#ifdef USEBARRIER
	std::atomic<int> getCPUbarrier;
#endif
int nCPUs_g;
int nGPUs_g;
float cpuThroughput;
float gpuThroughput;

cl::sycl::event event_kernel, event_send, event_receive; // unable to create

/*****************************************************************************
 * Heterogeneous Scheduler
 * **************************************************************************/
/*Bundle class: This class is used to store the information that items need while walking throught pipeline's stages.*/
class Bundle {
public:
	int begin;
	int end;
	int type; //GPU = 0, CPU=1

	Bundle() {};
};

/*My serial filter class represents the partitioner of the engine. This class selects a device and a rubrange of iterations*/
class MySerialFilter: public filter {
private:
	int begin;
	int end;
public:
	/*Class constructor, it only needs the first and last iteration indexes.*/
	MySerialFilter(int b, int e) : filter(true) {
		begin = b;
		end = e;
	}

	/*Mandatory operator method, TBB rules*/
	void * operator()(void *) {
		Bundle *bundle = new Bundle();
		/*If there are remaining iterations*/
		if (begin < end) {
			//Checking which resources are available
			if ( --gpuStatus >= 0 ) {
				//GPU WORK
				int auxEnd = begin + chunkGPU;
				auxEnd = (auxEnd > end) ? end : auxEnd;
				bundle->begin = begin;
				bundle->end = auxEnd;
				begin = auxEnd;
				bundle->type = GPU;
				return bundle;
			}else{
				//CPU WORK
				gpuStatus++;
				/*Taking a iteration chunk for CPU*/
				int auxEnd = begin + chunkCPU;
				auxEnd = (auxEnd > end) ? end : auxEnd;
				bundle->begin = begin;
				bundle->end = auxEnd;
				begin = auxEnd;
				bundle->type = CPU;
				return bundle;
			}
		}
		return NULL;
	} // end operator
};

/*MyParallelFilter class is the executor component of the engine, it executes the subrange onto the device selected by SerialFilter*/
template <class B>
class MyParallelFilter: public filter {
private:
	B *body;

public:
	/*Class' constructor*/
	//template <class B>
	MyParallelFilter(B *b) : filter(false) {
		body = b;
	}

	/*Operator function*/
	void * operator()(void * item) {
		//variables
		Bundle *bundle = (Bundle*) item;
		if(bundle->type == GPU){
			// GPU WORK
#ifdef DEBUG
			cerr << "launchGPU(): begin: " << bundle->begin << " end: " << bundle->end << endl;
#endif
#ifdef PJTRACER
			tracer->gpuStart();
#endif
			body->sendObjectToGPU(bundle->begin, bundle->end);
			body->OperatorGPU(bundle->begin, bundle->end, &event_kernel);
			body->getBackObjectFromGPU(bundle->begin, bundle->end);
			event_kernel.wait();

#ifdef PJTRACER
			tracer->gpuStop();
#endif
#ifdef USEBARRIER
		getLockCPU();
		freeLockCPU();
#endif
			/*To release GPU token*/
			gpuStatus++;
		}else{
			// CPU WORK
#ifdef DEBUG
			cerr << "launchCPU(): begin: " << bundle->begin << " end: " << bundle->end << endl;
#endif
#ifdef PJTRACER
			tracer->cpuStart();
#endif
#ifdef USEBARRIER
		if ( --getCPUbarrier >= 0 ) {
			getLockCPU();
			body->OperatorCPU(bundle->begin, bundle->end);
			freeLockCPU();
		} else {
			body->OperatorCPU(bundle->begin, bundle->end);
		}
		getCPUbarrier++;
#else
			body->OperatorCPU(bundle->begin, bundle->end);
#endif
#ifdef PJTRACER
			tracer->cpuStop();
#endif
		}
		/*Deleting bundle to avoid memory leaking*/
		delete bundle;
		return NULL;
	}
};
//end class

/*Oracle Class: This scheduler version let us to split the workload in two subranges, one for GPU and one for CPUs*/
class Oracle : public SchedulerOneApi<Oracle> {
	float ratioGPU;
	Params *pars;
public:
	/*This constructor just call his parent's contructor*/
	Oracle(void * params) : SchedulerOneApi<Oracle>(params){
		Params * p = (Params *) params;
		chunkCPU = 0;
		chunkGPU = 0;
		gpuStatus = p->numgpus;
		ratioGPU = p->ratioG;
		pars = p;
		nCPUs_g = nCPUs;
		nGPUs_g = nGPUs;
		//Initializing library PJTRACER
		initializePJTRACER(p->benchName);
	}

	/*Initializes PJTRACER library*/
	void initializePJTRACER(char* bench){
#ifdef PJTRACER
		char traceFname[50];
		sprintf(traceFname, "%s_ORACLE_C_%d_G_%d.trace", bench, nCPUs, nGPUs);
		tracer = new PFORTRACER(traceFname);
		tracer->beginThreadTrace();
#endif
	}

	/*The main function to be implemented*/
	template<class T>
	void heterogeneous_parallel_for(int begin, int end, T* body){
#ifdef DEBUG
		cerr << "Heterogeneous Parallel For Oracle step " << step << ": " << nCPUs << " , " << nGPUs << endl;
#endif
		/*Preparing pipeline*/
		pipeline pipe;
		MySerialFilter serial_filter(begin, end);
		MyParallelFilter<T> parallel_filter(body);
		pipe.add_filter(serial_filter);
		pipe.add_filter(parallel_filter);
		body->firsttime = true;

#ifdef USEBARRIER
			barrier_init();
			getCPUbarrier = 1;
#endif

		/*Seeting a mark to recognize a timestep*/
#ifdef PJTRACER
		tracer->newEvent();
#endif
		/*Parameter checking*/
		if((nCPUs > 0) && (nGPUs > 0) && (ratioGPU != 0.0) && (ratioGPU != 1.0)){
			//Pipeline case
			chunkGPU = (end-begin)*ratioGPU;
			chunkCPU = (end - begin) * (1 - ratioGPU);
			chunkCPU = floor((chunkCPU/nCPUs)+1);
			gpuStatus = nGPUs;

			pipe.run(nCPUs + nGPUs);
			pipe.clear();

		}else if((nCPUs > 0) && (nGPUs > 0) && (ratioGPU == 0)){
			//Only CPUs working
			chunkCPU = ceil((end - begin)/nCPUs);
			gpuStatus = 0;

			pipe.run(nCPUs);
			pipe.clear();

		}else if((nCPUs > 0) && (nGPUs > 0) && (ratioGPU == 1)){
			/*Only GPU work*/
			body->sendObjectToGPU(begin, end);
			body->OperatorGPU(begin, end, &event_kernel);
			body->getBackObjectFromGPU(begin, end);
			event_kernel.wait();

		}else if(nGPUs < 1){
			/*Only CPUs working*/
			/*In case of decimal division, we round to the upper int*/
			chunkCPU = ceil((end - begin)/nCPUs);
			gpuStatus = 0;

			pipe.run(nCPUs);
			pipe.clear();

		}else if(nCPUs < 1){
			/*Only GPU working*/
			body->sendObjectToGPU(begin, end);
			body->OperatorGPU(begin, end, &event_kernel);
			body->getBackObjectFromGPU(begin, end);
			event_kernel.wait();

		}else{
			cerr << "This case is not supported: numCPUS " << nCPUs << " numGPUs " << nGPUs << " ratioGPU " << ratioGPU << std::endl;
			exit(-1);
		}
	}

	/*this function print info to a Log file*/
	void saveResultsForBench(){

		char * execution_name = (char *)malloc(sizeof(char)*100);
		sprintf(execution_name, "%s_Oracle_%d_%d_%d.txt", pars->benchName, nCPUs, nGPUs, (int)(ratioGPU*100));

		/*Checking if the file already exists*/
		bool fileExists = isFile(execution_name);
		ofstream file(execution_name, ios::out | ios::app);
		if(!fileExists){
			printHeaderToFile(file);
		}
		file << nCPUs << "\t" << nGPUs << "\t"  << ratioGPU << "\t" << runtime << "\t"
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << std::endl;
		file.close();
#ifdef DEBUG
		cerr << nCPUs << "\t" << nGPUs << "\t"  << ratioGPU << "\t" << runtime << "\t"
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
#endif
	}

	/*this function print info to a Log file*/
	void saveResultsForBenchDen(int inputId, int nrIter, int platformId, float rtHetFor, float energyHetFor){
		char * execution_name = (char *)malloc(sizeof(char)*100);
		sprintf(execution_name, "_GO-OneApi.txt");
		strcat(pars->benchName, execution_name);

		/*Checking if the file already exists*/
		bool fileExists = isFile(pars->benchName);
		ofstream file(pars->benchName, ios::out | ios::app);
		if(!fileExists){
			printHeaderToFileDen(file);
		}
		double eG, eC, eM, eT;
        eG = getPP1ConsumedJoules(sstate1, sstate2);
        eC = getPP0ConsumedJoules(sstate1, sstate2);
        eT = getConsumedJoules(sstate1, sstate2);
        eM = eT - eG - eC;
        //print implementation id and size

//		fprintf(energyMeterFile,"%d\t%d\t%d\t", (int)ORACLE, INPUT_ID, platformId);
//		//print energy and time
//		fprintf(energyMeterFile,"%f\t%f\t%f\t%f\t%f\t"
//				,eG
//				,eC
//				,eM
//				,eT
//				,runtime);

		file << (int)ORACLE_ONE_API << "\t" << inputId << "\t" << platformId << "\t"
		//file << (int)ORACLE << "\t" << inputId << "\t" << platformId << "\t"
        << ratioGPU << "\t"
        << eG << "\t"
        << eC << "\t"
        << eM << "\t"
        << eT << "\t"
        << runtime << "\t"
		<< nrIter << "\t"
		<< rtHetFor << "\t"
		<< energyHetFor << "\t" 				
		<< std::endl;
		file.close();

		//cout << "\nORACLE" << "\t" << inputId << "\t" << platformId << "\t"
		cout << "\nORACLE" << "\t IN_SIZE: " << inputId << "\t PLATFORM_ID: " << platformId << "\t ratioGPU: "
			 << ratioGPU << "\t eT(Joule): "
	 		 << eT << "\t runtime(s): "
			 << runtime << "\tnrIter: "
			 << nrIter << "\t"
			 << rtHetFor  << std::endl;
#ifdef DEBUG
		cerr << nCPUs << "\t" << nGPUs << "\t"  << ratioGPU << "\t" << runtime << "\t"
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
#endif
	}

	void printHeaderToFile(ofstream &file){
		file << "N. CPUs" << SEP << "N. GPUs" << SEP << "GPU Ratio" << SEP << "Time (ms)" << SEP
		<< "CPU Energy(J)" << SEP << "GPU Enegy(J)" << SEP << "Uncore Energy(J)" << SEP << "Total Energy (J)" << SEP
		<< "L2 Cache Hits" << SEP << "L2 Cache Misses" << SEP << "L2 Cache Hit Ratio" << SEP
		<< "L3 Cache Hits" << SEP << "L3 Cache Misses" << SEP << "L3 Cache Hit Ratio" << SEP << "Cycles lost Due to L3 Misses" << std::endl;
	}

	void printHeaderToFileDen(ofstream &file){
		file << "ALG_ID" << SEP << "INPUT_ID" << SEP << "PLATFORM_ID" << SEP
        << "GPU_RATIO" << SEP << "E_GPU(J)" << SEP << "E_CPU(J)" << SEP << "E_MEM(J)" << SEP << "E_TOT(J)" << SEP
		<< "TIME(s)" << SEP
        << "NR_ITER" << SEP
		<< "HET_FOR_TIME(s)" << SEP
		<< "HET_FOR_ENERGY(Joule)"
		<< std::endl;
	}
};
