/*******************************************************************************
--------------------------------------------------------------------------------
Before Compile:
1.Go to Common.h and set the MDP_problem_size(0..12) in #define INPUT_ID MDP_problem_size
  We used in the paper the MDP_problem_size 2,4,6,8,10
2.Go to main.cpp and comment one of the following lines. Leave uncommented the
one corresponding to the scheduler that you want to use:
  #define DYNAMIC 5
  #define ORACLE 4
3. IF you change the platform, check output file name & set the proper TEST_PLATFORM ID
--------------------------------------------------------------------------------
Compile: go to the root fo the project (where main.cpp is) and run "cmake ."
--------------------------------------------------------------------------------
Execution:
1. For 0=SEQ1D,1=OMP,2=TBB,3=G-TBB implementations. No Scheduler.
sudo ./ValueIteration ImplemId(0-3,0=SEQ1D,1=OMP,2=TBB,3=G-TBB) NCpus(0-4)
  e.g.: sudo ./ValueIteration 1 3
2. For "#define ORACLE 4" implementations. ORACLE Scheduler.
sudo ./ValueIterationOracle NCpus(0-4) NGpus(0-1) RatioGPU(0.0-1.0)
  e.g.: sudo ./ValueIteration 0 3 0.5
3. For "#define DYNAMIC 5" implementations. DYNAMIC Scheduler.
sudo ./ValueIterationDynamic  NCpus(0-4) NGpus(0-1) ChunkGPU(>1)
  e.g.: sudo ./ValueIteration 0 3 2048
--------------------------------------------------------------------------------
*/
#include <cstdlib>


//#define __APPLE__
//#define LOCAL
//#define NDEBUG
//#define ENERGY_METER_ODROID
#define ENERGY_METER_INTEL
// #define HACK_TO_REMOVE_DUPLICATE_ERROR
//#define PCM_DEBUG_TOPOLOGY


//#define DEBUG
// #define DEEP_GPU_REPORT
//#define DEEP_CPU_REPORT
// #define OVERHEAD_ANALYSIS //OVERHEAD_STUDY


#define SCHEDULER
// #define DYNAMIC 5
#define ORACLE 4
//#define LOGFIT 6


#define FILE_INPUT //read T matrix arrays data from file or compute it (if not defined)
#define TEST_PLATFORM 3

//#define VALIDATE_POLICY

#include "navigation_mdp/RobotState.h"
#include "navigation_mdp/TransitionMatrix.h"
#include "navigation_mdp/Common.h"
#include "src/ValueIterationOMP.h"
#include "src/ValueIteration.h"
#include "src/ValueIterationUtils.h"
#include "src/Norm2.h"


#ifndef LOCAL
#include "src/ValueIterationOCL.h"
#include "utils/cl_helper.h"
#endif


#include "src/ValueIterationSeq1D.h"
#include "src/valueIterationOMP1D.h"
#include "src/ValueIterationOneApi.h"

#include <iostream>
#include <chrono>
#include <fstream>
#include <stdio.h>


#include "utils/timing.h"
#include <cstring>
#include <unistd.h>
//#include "energy-meter/energy_meter.h"
#include <unistd.h>
#include <stdlib.h>
#include <omp.h>
#include <string>
#include <sstream>
#include "src/ValueIterationTBB.h"
// #include "src/ValueIterationTBBClass.h"

#include <tbb/task_scheduler_init.h>
#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>

#ifdef SCHEDULER
#include "src/ValueIterationPlanifier.h"
#endif

#ifdef ENERGY_METER_INTEL
#include "cpucounters.h"
#include "utils.h"
#include "tbb/tick_count.h"
#endif

#include <CL/cl2.hpp>

using namespace tbb;
using namespace std;


// Device buffers
cl_mem d_probabilities = NULL;
cl_mem d_nextCellPos = NULL;
cl_mem d_nextStatePos = NULL;
cl_mem d_R = NULL;
cl_mem d_Q = NULL;
cl_mem d_P = NULL;
cl_mem d_V = NULL;
cl_mem d_Z = NULL;
cl_ulong probabilitySize = 0;

cl_float*   h_probability = NULL;
cl_ulong*   h_nextCellPos = NULL;
cl_ulong*   h_nextStatePos = NULL;
cl_float*   h_R      = (cl_float*)calloc(NS*NX,sizeof(cl_float));
cl_float*   h_Q      = (cl_float*)calloc(NS*NX,sizeof(cl_float));
cl_float*   h_V      = (cl_float*)calloc(NS,sizeof(cl_float));
cl_int*     h_P      = (cl_int*)calloc(NS, sizeof(cl_int));
cl_int*     h_Pseq      = (cl_int*)calloc(NS, sizeof(cl_int));
cl_float*   h_Z      = (cl_float*)calloc(NS,sizeof(cl_float));

cl_uint ns, nx, na, nr, ng, nt, nd;

//!NEEDED FOR SCHEDULER.H USE
extern cl_context context;
extern cl_command_queue command_queue;
extern cl_kernel kernel;

/** Value iteration algorithm - time measurement output files */
const static string ExecTimeEnergyLogName =         "./data/EnergyTimeMeasurements20MS100x1000E.txt";
const static string IntelExecTimeEnergyLogName =    "./data/IntelEnergyTimeMeasurements100x10E_TBB.txt";

const static string TPxTimeEnergyLogName =          "./data/evaluation/TPx-TBB-TE-K1.txt";
const static string OdroidExecTimeEnergyLogName =   "./data/OdroidEnergyTimeMeasurements100E.txt";
const static string execTimeLogName =               "./data/ExecTimeTest.txt";

string inFileDim;


//enum iBroadwell{SEQ, SEQ1D, OMP4C, OCL4C, TBB4C};
string implName[5] = {"SEQ", "SEQ1D",  "OMP1D", "OMP1D_b.L", "OCL"};


/** execute Value Iteration with a given optimization configuration */
cl_int execute(cl_int optimization, TransitionMatrix &T, cl_float **R, cl_float **Q, cl_float *V, cl_int *P, cl_float gamma, cl_float d);

float comparePolicies(const cl_int *P, const cl_int *Pxxx, bool verbose);

bool compareQValues( cl_float  Q[][NX],  cl_float Qxxx[][NX], bool verbose);
/**
 *
 * @param algorithm 0 - seq, 2 - seq1D, 3 - omp, 4 - ... TODO: tbc with all implementations
 * @param nrExecTimes - times to execute the algorithm for the mean exec. time computation
 * @param transitionMatrix
 * @param execTimeFile - output file name (contains nrExecTimes with the time that it took to execute the algorithm)
 * @param avgExecTime
 */
void measureExecutionTimeAlg(cl_int algorithm, cl_int nrExecTimes, TransitionMatrix &transitionMatrix,
                             string execTimeFile, cl_float *Q, cl_float *V, cl_int *P);
/** ODROID PLATFORM
 *
 * @param algorithm
 * @param nrExecTimes
 * @param transitionMatrix
 * @param energyMeterFileName
 * @param avgExecTime
 * @param Q
 * @param V
 * @param P
 */

void measureEnergyUseAlg(cl_int algorithm,
                         string energyMeterFileName,
                         cl_float *Q, cl_float *V, cl_int *P);
/** INTEL PLATFORM
 *
 * @param algorithm
 * @param nrExecTimes
 * @param transitionMatrix
 * @param energyMeterFileName
 * @param avgExecTime
 * @param Q
 * @param V
 * @param P
 */

void measureIntelEnergyUseAlg(string energyMeterFileName, cl_float *h_Q, cl_float *h_V, cl_int *h_P,
                              int platform, int algorithm, int numThr);

#ifdef ENERGY_METER_ODROID
/** ODROID PLATFORM
 *
 * @param nrExecTimes
 * @param transitionMatrix
 * @param energyMeterFileName
 * @param avgExecTime
 * @param Q
 * @param V
 * @param P
 */
void measureOdroidEnergyUseAlg( string energyMeterFileName,
                                cl_float *h_Q, cl_float *h_V, cl_int *h_P);
#endif


void measureAllImplemExecutionTimeAlg(int nrExecTimes, TransitionMatrix &transitionMatrix,
                                      string execTimeFileName,
                                      float *Q, float *V, int *P, float *R);

void dumpDataToFiles(cl_int *P, cl_float *V, cl_float* Q);

/** Read discretization ., probability, nextCellPos, nextStatePos and R arrays*/
void ReadInput(char *filename);

/** Write discretization params., probability, nextCellPos, nextStatePos and R arrays*/
void WriteInput(char *filename);

/** Freee allocated mem*/
void releaseMemory();



int main(cl_int argc, char **argv) {

    //find_devices();
    
    cout << "Hallo mikomi :) !" << endl;
    //setbuf(stdout, NULL); //stop buffering, for right order in printf/fprintf, etc
    //int inputId[4] = {40445, 61012, 84565, 143771};
    cout << "NS: " << NS << endl;
    cout << "NX: " << NX << endl;

#ifndef FILE_INPUT
    //vars
    /** Path to robot log file */
    string pathLogFile = "./data/LOG_05A_02S_FRANKIE.txt"; // LINUX
    cout << pathLogFile << endl;
    // Transition function specifying P(s'|s,a), NSxNAxNS;
    TransitionMatrix transitionMatrix(pathLogFile, true);
    cout << "probabilitySize: " << probabilitySize << endl;
    // Rewards matrix
    setRewardMatrix(transitionMatrix, h_R, -30000, 10000000, 0, false);

    probabilitySize = (cl_ulong)transitionMatrix.getElementsNumber();

    h_probability   = transitionMatrix.getProbabilities();
    h_nextCellPos   = transitionMatrix.getNextCellPositions();
    h_nextStatePos  = transitionMatrix.getNextStateIdPositions();

    stringstream ss;
    stringstream fnss;
    ss  << "PS" << probabilitySize
        << "_" << "S" << NS
        << "_" << "T" << NT
        << "_" << "R" << NR
        << "_" << "G" << NG
        << "_" << "D" << ND
        << "_" << "A" << NA;

    inFileDim = ss.str();
    cout << inFileDim << endl;

    fnss << "./data/in/" << inFileDim << ".txt";
    string fns = fnss.str();
    char* inputFileName  = (char*)fns.c_str();

    WriteInput(inputFileName);

#endif

#ifdef FILE_INPUT

    printf("Algorithm input id : %d\n", INPUT_ID);

    probabilitySize = pNZ[INPUT_ID];

    cout << "probabilitySize: " << probabilitySize << endl;

    stringstream ss;
    stringstream fnss;
    ss  << "PS" << probabilitySize
        << "_" << "S" << NS
        << "_" << "T" << NT
        << "_" << "R" << NR
        << "_" << "G" << NG
        << "_" << "D" << ND
        << "_" << "A" << NA;

    inFileDim = ss.str();
    //cout << inFileDim;

    fnss << "./data/in/" << inFileDim << ".txt";
    string fns = fnss.str();
    char* inputFileName  = (char*)fns.c_str();

    ReadInput(inputFileName);
#endif

#ifndef SCHEDULER  //corbera

    if (argc == 3) { // test all but scheduler based

        int algorithm, numThr;
        algorithm = atoi(argv[1]); // algorithm id (0,1,2,3)
        numThr = atoi(argv[2]); // CPU cores number
        if (algorithm < 0 || algorithm > 4) {
          cout << "Invalid ALG_ID. Valid options: 0/1/2/3/4, 0=SEQ1D,1=OMP,2=TBB,3=G-TBB,4=G-OneAPI \n";
          releaseMemory();
          return -1;
        }

        #ifdef ENERGY_METER_INTEL
            printf("ENERGY_METER_INTEL\n");
            task_scheduler_init init(task_scheduler_init::automatic);
            measureIntelEnergyUseAlg(TPxTimeEnergyLogName, h_Q, h_V, h_P, (int)TEST_PLATFORM, algorithm, numThr);
        #endif

        #ifdef ENERGY_METER_ODROID
            printf("ENERGY_METER_ODROID\n");
            measureOdroidEnergyUseAlg(OdroidExecTimeEnergyLogName, h_Q, h_V, h_P);
        #endif

        releaseMemory();
        return 0;
    }
#endif //corbera
//else, scheduler based only
#ifdef SCHEDULER
    /** Planifier */
    ValueIterationPlanifier vip;
    Params p;

    /*Checking command line parameters*/
    #if defined(ORACLE) || defined(DYNAMIC)
    if (argc < 4) {
        cerr << "Args: numcpus numgpus [ratio | gpuCHUNK]" << endl;
        exit(-1);
    }
    #elif defined(LOGFIT)
    if (argc < 3) {
        cerr << "Args: numcpus numgpus" << endl;
        exit(-1);
    }
    #endif
    p.numcpus = atoi(argv[1]);
    p.numgpus = atoi(argv[2]);
    #ifdef ORACLE
    p.ratioG = atof(argv[3]);
    cout << "Oracle Scheduler" << endl;
    #endif
    #ifdef DYNAMIC
    p.gpuChunk = atoi(argv[3]);
    cout << "Dynamic Scheduler" << endl;
    #endif
    #ifdef LOGFIT
    cout << "LogFit Scheduler" << endl;
    #endif
    sprintf(p.benchName, "./data/evaluation/TP2_K1_chunk_");
    sprintf(p.kernelName, "valueIterationKernel");
    //sprintf(p.kernelName, "policyEvaluation");
    cerr << "\nStarting with:  NCPUs: " << p.numcpus << " NGPUS: " << p.numgpus << endl;

    /*Initializing scheduler*/
    #ifdef ORACLE
    Oracle * hs = Oracle::getInstance(&p);
    #endif
    #ifdef DYNAMIC
    Dynamic * hs = Dynamic::getInstance(&p);
    #endif
    #ifdef LOGFIT
    LogFit * hs = LogFit::getInstance(&p);
    #endif

    createCommandQueue();
    CreateAndCompileProgram("valueIterationKernel");

    cout << "\nKernel compiled!";

    vip.AllocateMemoryObjects();

    cout << "\nGPU memory allocated!";

    // policy improvement
    auto f = [&](const blocked_range<size_t>& r)
         {
             for (cl_ulong s = r.begin(); s != r.end(); ++s) {
                 size_t idx = s * NX;
                 for (int a = 0; a < NX; a++) {
                     if (h_Q[idx] - h_Z[s] > EPSILON) {
                         h_Z[s] = h_Q[idx];
                         h_P[s] = a;
                         //printf("h_Z[%d] = %f \n", s, h_Z[s]);
                     }
                     idx++;
                 }
             }
         };


    bool notReady = true;
    cl_ulong nrIter = 0;
    float norm = 0;
   // Norm2 n2sum(h_Z,h_V); 
    //task_scheduler_init init(tbb::task_scheduler_init::automatic);  //corbera: initialized in scheduler

    // cout << "Probability: ";
    // print1DimArray(h_probability, 10);

    float runtime_k1  = 0.0;
    float energy_k1 = 0.0;
    //Energy Counters
    PCM * pcm_k1;
    SystemCounterState sstate1_k1, sstate2_k1;
    //timing
    tick_count start, end;
    //Initializing PCM library
    pcm_k1 = PCM::getInstance();
    pcm_k1->resetPMU();
    if (pcm_k1->program() != PCM::Success){
        cerr << "Error in PCM library initialization" << endl;
        exit(-1);
    }
    hs->startTimeAndEnergy();
    while (notReady) {
        tick_count bt, et;
        bt = tick_count::now();
        sstate1_k1 = pcm_k1->getSystemCounterState();
        // code to be measured - K1
        hs->heterogeneous_parallel_for(0, NS*NX, &vip); //begin = 0, end = NS * NX, ..
        et = tick_count::now();
        runtime_k1 += (et-bt).seconds();
        sstate2_k1 = pcm_k1->getSystemCounterState();
        energy_k1 += getConsumedJoules(sstate1_k1, sstate2_k1);
        //cout << "runtime & total_energy K1:" << runtime_k1 << "\t" << energy_k1 << endl;
       
        //select the policy with the best value (max_a Q(s,a)) for every state
        parallel_for( blocked_range<size_t>(0,NS), f);
        //Norm2
        //n2sum.my_sum = 0; //corbera: to initialize n2sum.my_sum every iteration
        //parallel_reduce( blocked_range<size_t>(0,NS), n2sum);

        //serial reduction for testing
        /*
        float auxsum = 0.0;
        for( size_t i=0; i!=NS; ++i )  {
            auxsum += pow((h_Z[i]-h_V[i]),2); 
        }
        cout << endl << "parallel my_sum: " << n2sum.my_sum << " serial my_sum: " << auxsum << endl;
        */
        //norm = sqrt(n2sum.my_sum);
        norm = parallel_reduce(blocked_range<size_t>(0,NS), 0.0f,
                               [&](blocked_range<size_t> r, float total_norm)
                               {
                                   for (size_t i=r.begin(); i<r.end(); ++i)
                                   {
                                       total_norm += pow((h_Z[i]-h_V[i]),2);
                                   }

                                   return total_norm;
                               }, std::plus<float>() );
        norm = sqrt(norm);
        notReady = norm > g && nrIter < MAX_ITER;
        //print1DimArray(h_Z, NS, 20);
        //cout << endl << "n2sum.my_sum: " << n2sum.my_sum << " norm: " << norm << " notReady: " << notReady << endl;
        //exit(-1);
        //swap old V with new V (Z)
        // cl_float *tmp = h_Z;
        // h_Z = h_V;
        // h_V = tmp;
        memcpy(h_V, h_Z, NS*sizeof(cl_float));

        nrIter++;
        // cout << "ITER: " << nrIter << ". Norm: " << norm << endl;
        //printf("\nIter: %d, Norm: %f ", nrIter, norm);
    } // end of time step
    hs->endTimeAndEnergy();
    hs->saveResultsForBenchDen(INPUT_ID, nrIter, (int)TEST_PLATFORM, runtime_k1, energy_k1);
    //hs->saveResultsForBench();
    //init.terminate(); corbera

    cout << "runtime & total_energy K1 (heter_for):" << runtime_k1 << "\t" << energy_k1 << endl;


    #ifdef VALIDATE_POLICY
    // clear the last policy and values
    for (cl_int s = 0; s < NS; ++s) {
        //h_P[s] = 0;
        h_V[s] = ::std::numeric_limits<cl_float>::min();
        h_Z[s] = ::std::numeric_limits<cl_float>::min();
    }
    int nrIterSeq = valueIterationSeq1D(h_probability, h_nextStatePos, h_nextCellPos, h_R, h_Q, h_V, h_Pseq, gamma_, d);
    float similarity = comparePolicies(h_Pseq, h_P, false);
    cout << "Heter #iter: " << nrIter << ". Seq #iter: " << nrIterSeq << ". Norm: " << norm <<  ". P similarity: " << similarity << endl ;
    #endif

#endif //SCHEDULER



/*
    //Test TBB runtime...
    task_scheduler_init init(task_scheduler_init::automatic);
    //task_scheduler_init init(4);
    timestamp_type time1, time2;
    auto begin = std::chrono::high_resolution_clock::now();
    int nrIt =  ValueIterationTBB1D(h_probability, h_nextStatePos, h_nextCellPos, h_R, h_Q, h_V, h_P, gamma_);
    get_timestamp(&time2);
    double elapsed = timestamp_diff_in_seconds(time1,time2);
    cout << "\nThe implem converged in " << nrIt << " iterations. ExecTime = " << elapsed << "\n";
*/
    //Test OpenCL
   /* omp_set_num_threads(8);
    prepareOclEnvironment();
    timestamp_type time1, time2;
    auto begin = std::chrono::high_resolution_clock::now();
    get_timestamp(&time1);
    int nrIt = ValueIterationOCL() ;
    get_timestamp(&time2);
    double elapsed = timestamp_diff_in_seconds(time1,time2);
    cout << "\nThe implem converged in " << nrIt << " iterations. ExecTime = " << elapsed << "\n";
    */

    /*Test OpenMP
    omp_set_num_threads(4);
    valueIterationOMP1D(h_probability, h_nextStatePos, h_nextCellPos, h_R, h_Q, h_V, h_P, gamma_, d);
    */

    /** Evaluate the execution time for sequential vs OpenMP on Value iteration algorithm*/


    releaseMemory();
    return 0;
}

/** Freee allocated mem*/
void releaseMemory() {
    free(h_R);
    free(h_Q);
    free(h_V);
    free(h_P);
    free(h_Pseq);
    free(h_Z);

    //released in T_matrix...
    free(h_probability);
    free(h_nextCellPos);
    free(h_nextStatePos);
}


#ifdef ENERGY_METER_ODROID

#endif

#ifdef ENERGY_METER_INTEL
void measureIntelEnergyUseAlg(string energyMeterFileName, cl_float *h_Q, cl_float *h_V, cl_int *h_P, int platform, int algorithm, int numThr) {

    //for(cl_int k = 0; k < NR_TEST_ITER; k++) {
    char *fName = (char*)energyMeterFileName.c_str();
    FILE* energyMeterFile = fopen(fName, "a");
    cl_int nrIter = 0;
    int totalIters = 0;
    float runtime_k1 = 0.0;
    float energy_k1 = 0.0;

    if ( !energyMeterFile )
    {
        cout << "ERROR: Failed opening " << energyMeterFile << " file!" << endl;
        exit(1);
    }

    //for(int algorithm = 0; algorithm < 4; algorithm++) {
        sleep(1);
        //Energy Counters
        PCM * pcm;
        vector<CoreCounterState> cstates1, cstates2;
        vector<SocketCounterState> sktstate1, sktstate2;
        SystemCounterState sstate1, sstate2;
        //timing
        tick_count start, end;
        float runtime = 0.0;


        switch (algorithm) {
            // case 0:
            //     //nrIter = valueIteration(transitionMatrix, h_R, h_Q, h_V, h_P, gamma_, d);
            //     break;
            case 0: // seq
            {//Initializing PCM library
                pcm = PCM::getInstance();
                pcm->resetPMU();
                if (pcm->program() != PCM::Success){
                    cerr << "Error in PCM library initialization" << endl;
                    exit(-1);
                }

                /*Sets the start mark of energy and time*/
                pcm->getAllCounterStates(sstate1, sktstate1, cstates1);
                start = tick_count::now();

                nrIter = valueIterationSeq1D(h_probability, h_nextStatePos, h_nextCellPos, h_R, h_Q, h_V, h_P, gamma_, d);
                /*Sets the end mark of energy and time*/
                end = tick_count::now();
                pcm->getAllCounterStates(sstate2, sktstate2, cstates2);
                runtime = (end-start).seconds();
                break;
            }
            case 1: { // omp
                omp_set_num_threads(numThr);

                //Initializing PCM library
                pcm = PCM::getInstance();
                pcm->resetPMU();
                if (pcm->program() != PCM::Success){
                    cerr << "Error in PCM library initialization" << endl;
                    exit(-1);
                }

                /*Sets the start mark of energy and time*/
                pcm->getAllCounterStates(sstate1, sktstate1, cstates1);
                start = tick_count::now();

                // code to me measured
                nrIter = valueIterationOMP1D(h_probability, h_nextStatePos, h_nextCellPos, h_R, h_Q, h_V/*, h_Z*/, h_P, gamma_, d);


                /*Sets the end mark of energy and time*/
                end = tick_count::now();
                pcm->getAllCounterStates(sstate2, sktstate2, cstates2);
                runtime = (end-start).seconds();
                break;
            }
            case 2: //tbb - 4 threads
                //Initializing PCM library
            {
                tbb::task_scheduler_init init(numThr);
                //auto f = ValueIterationTBBClass(h_probability, h_nextStatePos, h_nextCellPos, h_R, h_Q, h_V, h_Z, h_P, gamma_);


                pcm = PCM::getInstance();
                pcm->resetPMU();
                if (pcm->program() != PCM::Success){
                    cerr << "Error in PCM library initialization" << endl;
                    exit(-1);
                }


                /*Sets the start mark of energy and time*/
                pcm->getAllCounterStates(sstate1, sktstate1, cstates1);
                start = tick_count::now();

                //code to me measured
                nrIter = ValueIterationTBB1D(h_probability, h_nextStatePos, h_nextCellPos, h_R, h_Q, h_V, /*h_Z,*/ h_P, gamma_, runtime_k1, energy_k1);

                /*Sets the end mark of energy and time*/

                end = tick_count::now();
                pcm->getAllCounterStates(sstate2, sktstate2, cstates2);
                runtime = (end-start).seconds();
                init.terminate();
                break;
            }
            case 3:
            {
                tbb::task_scheduler_init init(numThr);
                //omp_set_num_threads(4);
                prepareOclEnvironment();
                // Allocate device memory
                allocate();
                //Initializing PCM library
                pcm = PCM::getInstance();
                pcm->resetPMU();
                if (pcm->program() != PCM::Success){
                    cerr << "Error in PCM library initialization" << endl;
                    exit(-1);
                }

                /*Sets the start mark of energy and time*/
                pcm->getAllCounterStates(sstate1, sktstate1, cstates1);
                start = tick_count::now();

                // code to me measured
                nrIter = ValueIterationOCL();

                /*Sets the end mark of energy and time*/
                end = tick_count::now();
                pcm->getAllCounterStates(sstate2, sktstate2, cstates2);
                runtime = (end-start).seconds();
                //realeaseOclResources();
                // Release ocl buffers
                deallocateMemory();
                // Release device resources
                //shutdown();
                init.terminate();
            }
                break;
            default:
                break;
        }

        double eG, eC, eM, eT;
        eG = getPP1ConsumedJoules(sstate1, sstate2);
        eC = getPP0ConsumedJoules(sstate1, sstate2);
        eT = getConsumedJoules(sstate1, sstate2);
        eM = eT - eG - eC;
        //print implementation id and size
        fprintf(energyMeterFile,"%d\t%d\t%d\t", algorithm, INPUT_ID, platform);
        //print energy and time
        fprintf(energyMeterFile,"%f\t%f\t%f\t%f\t%f\t"
                ,eG
                ,eC
                ,eM
                ,eT
                ,runtime
                );

        fprintf(energyMeterFile,"%d\t%f\t%f\t\n"
                ,nrIter                
                ,runtime_k1
                ,energy_k1
                );
        cout << "\nAlg. " << algorithm << ": "  << nrIter << " iter\t"
             //<< getPP1ConsumedJoules(sstate1, sstate2) << "\t"
             //<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t"
             //<< getPP0ConsumedJoules(sstate1, sstate2) << "\t"
             <<  getConsumedJoules(sstate1, sstate2) << " Joule \t"
             <<  runtime << " s\t"
             <<  runtime_k1 << " s\t"
             <<  energy_k1 << " J\t"
             << endl;
        // clear the last policy and values
        for (cl_int s = 0; s < NS; ++s) {
            h_P[s] = 0;
            h_V[s] = std::numeric_limits<cl_float>::min();
            h_Z[s] = std::numeric_limits<cl_float>::min();
        }

    //}

    fclose(energyMeterFile);

    //}
}

#endif




bool compareQValues( cl_float  Q[][NX],  cl_float Qxxx[][NX], bool verbose) { //todo:1d array Q, Qxxx
    bool sameQ = true;
    for (cl_ulong s = 0; s < NS ; ++s) {
        for (cl_int a = 0; a < NA; ++a) {
            if(Q[s][a] != Qxxx[s][a]) {
                sameQ = false;
                if (verbose)
                    cout << "Qseq[" << s << "," << a << "]=" << Q[s][a] << "; " << "Qxxx[" << s << "," << a << "]=" << Qxxx[s][a]  << "\n";
                break;
            }
        }
    }
    return sameQ;
}

//returns the percentage of similar policies P and Pxxx
float comparePolicies(const cl_int *P, const cl_int *Pxxx, bool verbose) {
    float sameP = NS;
    for (cl_int p = 0; p < NS ; ++p) {
        if(P[p] != Pxxx[p]) {
            sameP--;
            if (verbose)
                cout << "Pseq[" << p << "]=" << P[p] << "; " << "Pxxx[" << p << "]=" << Pxxx[p] << "\n";
            //break;
        }
    }
    return sameP/NS;
}

void ReadInput(char *filename) {
    register FILE *f;
    register unsigned int i;

    f = fopen(filename, "r+t");
    if (f == NULL) {
        fprintf(stderr, "file not found: %s\n", filename);
        exit(-1);
    }

    fscanf(f, "%lu", &probabilitySize);
    fscanf(f, "%d", &ns);
    fscanf(f, "%d", &nx);
    fscanf(f, "%d", &nt);
    fscanf(f, "%d", &nr);
    fscanf(f, "%d", &ng);
    fscanf(f, "%d", &nd);
    fscanf(f, "%d", &na);


    if (h_probability == NULL) {
        h_probability = (cl_float *) malloc(sizeof(cl_float) * probabilitySize);
    }

    if (h_nextCellPos == NULL) {
        h_nextCellPos = (cl_ulong *) malloc(sizeof(cl_ulong) * (NS * NX + 1));
    }

    if (h_nextStatePos == NULL) {
        h_nextStatePos = (cl_ulong *) malloc(sizeof(cl_ulong) * probabilitySize);
    }

    if (h_R == NULL) {
        h_R = (cl_float *) malloc(sizeof(cl_float) * NS * NX);
    }

    //probability
    for (i = 0; i < probabilitySize; i++) {
        fscanf(f, "%fE", &h_probability[i]);
    }
    //nextCellPos
    for (i = 0; i < NS * NX + 1; i++) {
        fscanf(f, "%lu", &h_nextCellPos[i]);
    }
    //nextStatePos
    for (i = 0; i < probabilitySize; i++) {
        fscanf(f, "%lu", &h_nextStatePos[i]);
    }
    //R
    for (i = 0; i < NS * NX; i++) {
        fscanf(f, "%f", &h_R[i]);
    }
    fclose(f);
}

void WriteInput(char *filename) {
    register FILE *f;
    register unsigned int i;

    f = fopen(filename, "w+");
    if (f == NULL) {
        fprintf(stderr, "file not created: %s\n", filename);
        exit(-1);
    }

    fprintf(f, "%lu\n", probabilitySize);
    fprintf(f, "%lu\n", NS);
    fprintf(f, "%d\n", NX);
    fprintf(f, "%d\n", NT);
    fprintf(f, "%d\n", NR);
    fprintf(f, "%d\n", NG);
    fprintf(f, "%d\n", ND);
    fprintf(f, "%d\n", NA);

    //probability
    for (i = 0; i < probabilitySize; i++) {
        fprintf(f, "%f\t", h_probability[i]);
    }
    fprintf(f, "\n");
    //nextCellPos
    for (i = 0; i < NS * NX + 1; i++) {
        fprintf(f, "%lu\t", h_nextCellPos[i]);
    }
    fprintf(f, "\n");
    //nextStatePos
    for (i = 0; i < probabilitySize; i++) {
        fprintf(f, "%lu\t", h_nextStatePos[i]);
    }
    fprintf(f, "\n");
    //R
    for (i = 0; i < NS * NX; i++) {
        fprintf(f, "%f\t", h_R[i]);
    }
    fclose(f);
}
