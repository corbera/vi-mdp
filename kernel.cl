__kernel void valueIterationKernel(
                  __global const    float* probability,
                  __global float*   V,
                  __global float*   Q,
                  __global const    unsigned long*  nextCellPos,
                  __global const    unsigned long*  nextStatePos,
                  __global const    float* R,
                  unsigned long     NS,
                  unsigned int      NX,
                  unsigned long     begin,
                  unsigned long     end
) {
    float w = 0.0;
    float gamma = 0.1;
    unsigned long /*s,*/ s2, myCellPos;
    // s = get_global_id(0) + begin;
    // myCellPos = s * NX;
    // if ( s >= end ) return;
    // //! Policy evaluation /
    // //#pragma unroll
    // for ( int a = 0; a < NX; a++ ) {
    //     //myCellPos += a;
    //     //if ( myCellPos < end * NX ) { // evaluate the actions for the current state
    //         // for all non zero probability nextState for <s,a> tuple
    //         w = 0.0;
    //         for (s2 = nextCellPos[myCellPos]; s2 < nextCellPos[myCellPos + 1]; s2++) {
    //             w += probability[s2] * V[nextStatePos[s2]];
    //         }
    //         Q[myCellPos] = R[myCellPos] + gamma * w ;
    //     //}
    //     myCellPos++;
    // }

    myCellPos = get_global_id(0) + begin;
    /** Policy evaluation */
    if ( myCellPos < end) { // evaluate the actions for the current state
        /* for all non zero probability nextState for <s,a> tuple (i.e. all elements from one cell of T) */
         for (s2 = nextCellPos[myCellPos]; s2 < nextCellPos[myCellPos + 1]; s2++) {
             w += probability[s2] * V[nextStatePos[s2]];
         }
         Q[myCellPos] =  R[myCellPos] + gamma * w ; //w * gamma + R[myCellPos];
    }
    return;
}
