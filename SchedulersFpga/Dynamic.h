//============================================================================
// Author		: Jose Carlos Romero Moreno
// Version		: 1.0
// Date			: 09 / 04 / 2019
// Copyright	: Department. Computer's Architecture (c)
// Description	: Dynamic scheduler implementation
//============================================================================

#define GPU_LAST

#include <cstdlib>
#include <iostream>
#include <fstream>
#include "Scheduler.h"
#include "tbb/pipeline.h"
#include "tbb/tick_count.h"
#include <math.h>
#include <atomic>

using namespace std;
using namespace tbb;

/*****************************************************************************
 * Defines
 * **************************************************************************/
#define CPU 0
#define GPU 1
#define GPU_OFF -100 //Arbitrary value
#define SEP "\t"


// size_t CHUNKGPUALIGN=64;
// size_t CHUNKCPUALIGN=1024;
size_t CHUNKGPUALIGN=1024;
size_t CHUNKCPUALIGN=64;

//#define ALIGN(A,B) max((B),(int)(round((float)(A)/(float)(B))*(float)(B)))

// std::max and std::round are not constexpr in C++11
// size_t ALIGN(size_t A, size_t B) {
//   return std::max(B,
//       static_cast<size_t>(std::round(A/static_cast<float>(B)) * B));
// }
//
// size_t ALIGNFLOOR(size_t A, size_t B) {
//   return static_cast<size_t>(std::floor(A/static_cast<float>(B)) * B);
// }
//
// size_t ALIGNGPU(size_t A) {
//   return ALIGN(A, CHUNKGPUALIGN);
// }
//
// size_t ALIGNFLOORGPU(size_t A) {
//   return ALIGNFLOOR(A, CHUNKGPUALIGN);
// }
//
// size_t ALIGNCPU(size_t A) {
//   return ALIGN(A, CHUNKCPUALIGN);
// }


/*****************************************************************************
 * types
 * **************************************************************************/
// typedef struct{
// 	int numcpus;
// 	int numgpus;
// 	int gpuChunk;
// 	char benchName[100];
// 	char kernelName[100];
// }Params;

/*****************************************************************************
 * Global variables
 * **************************************************************************/
std::atomic<int> gpuStatus;
int chunkCPU;
int chunkGPU;
float fG;
float gpuThroughput, cpuThroughput;
// To calculate scheduling partition overhead
tick_count end_tc;
int minChunkCPU = 10;



/*****************************************************************************
 * Heterogeneous Scheduler
 * **************************************************************************/
/*Bundle class: This class is used to store the information that items need while walking throught pipeline's stages.*/
class Bundle {
public:
	int begin;
	int end;
	int type; //GPU = 0, CPU=1

	Bundle() {};
};

/*My serial filter class represents the partitioner of the engine. This class selects a device and a rubrange of iterations*/
class MySerialFilter: public filter {
private:
	int begin;
	int end;
	int nCPUs;
public:
	/*Class constructor, it only needs the first and last iteration indexes.*/
	MySerialFilter(int b, int e, int ncpus) : filter(true) {
		begin = b;
		end = e;
		nCPUs = ncpus;
	}

	/*Mandatory operator method, TBB rules*/
	void * operator()(void *) {
		Bundle *bundle = new Bundle();

		/*If there are remaining iterations*/
		if (begin < end) {
			//Checking which resources are available
			if ( --gpuStatus >= 0 ){
				//GPU WORK
				int auxEnd = begin + chunkGPU;
#ifndef GPU_LAST
				if(nCPUs < 1){  // comentar para dar el ultimo trozo a GPU
					auxEnd = (auxEnd > end) ? end : auxEnd;
				}
#else
        			auxEnd = (auxEnd > end) ? end : auxEnd;
#endif
				if( (auxEnd <= end) ){
					//GPU CHUNK
					bundle->begin = begin;
					bundle->end = auxEnd;
					begin = auxEnd;
					bundle->type = GPU;
				}else{
					//CPU CHUNK
					gpuStatus = GPU_OFF;
					if(fG > 0){
						chunkCPU = max(min(chunkCPU, (end-begin)/(nCPUs+1)), minChunkCPU);
						  //chunkCPU = ALIGNCPU(chunkCPU);

					}
                                        /*
					bundle->begin = begin;
					bundle->end = begin + chunkCPU;
					begin = begin + chunkCPU;
					bundle->type = CPU;
                                        */
                                        int auxEnd = begin + chunkCPU;
                                        auxEnd = (auxEnd > end) ? end : auxEnd;
                                        bundle->begin = begin;
                                        bundle->end = auxEnd;
                                        begin = auxEnd;
                                        bundle->type = CPU;

				}
				return bundle;
			}else{
				//CPU WORK
				gpuStatus++;

				/*Calculating next chunkCPU*/
				if(fG > 0){
					chunkCPU = chunkGPU / fG;
					chunkCPU = max(min( chunkCPU, (end-begin)/(nCPUs)), minChunkCPU);
					  //chunkCPU = ALIGNCPU(chunkCPU);//HECHO POR JC
				}

				/*Taking a iteration chunk for CPU*/
				int auxEnd = begin + chunkCPU;
				auxEnd = (auxEnd > end) ? end : auxEnd;
				bundle->begin = begin;
				bundle->end = auxEnd;
				begin = auxEnd;
				bundle->type = CPU;
				return bundle;
			}
		}
		return NULL;
	} // end operator
};

/*MyParallelFilter class is the executor component of the engine, it executes the subrange onto the device selected by SerialFilter*/
template <class B>
class MyParallelFilter: public filter {
private:
	B *body;

public:
	/*Class' constructor*/
	//template <class B>
	MyParallelFilter(B *b) : filter(false) {
		body = b;
	}

	/*Operator function*/
	void * operator()(void * item) {
		//variables
		Bundle *bundle = (Bundle*) item;

		if(bundle->type == GPU){
			// GPU WORK
#ifdef DEBUG
			//cerr << "launchGPU(): begin: " << bundle->begin << " end: " << bundle->end << endl;
#endif
			tick_count start_tc = tick_count::now();
			body->sendObjectToGPU(bundle->begin, bundle->end, NULL);
			body->OperatorGPU(bundle->begin, bundle->end, NULL);
			body->getBackObjectFromGPU(bundle->begin, bundle->end, NULL);
			clFinish(command_queue);
			end_tc = tick_count::now();
			float time =(end_tc - start_tc).seconds()*1000;
			gpuThroughput = (bundle->end - bundle->begin) / time;

#ifdef DEBUG
			cerr << "launchGPU(): chunk: " << bundle->end-bundle->begin << " (" << bundle->begin << " - " << bundle->end <<") TH: " << gpuThroughput << " fG: " << fG << endl;
#endif

			/*If CPU has already computed some chunk, then we update fG (factor GPU)*/
			if (cpuThroughput > 0){
				fG = gpuThroughput/cpuThroughput;
			}

			/*To release GPU token*/
			gpuStatus++;
		}else{
			// CPU WORK
			tick_count start = tick_count::now();
			body->OperatorCPU(bundle->begin, bundle->end);
			tick_count end = tick_count::now();
			float time =(end-start).seconds()*1000;
			cpuThroughput = (bundle->end - bundle->begin) / time;
#ifdef DEBUG
			cerr << "launchCPU(): chunk: " << bundle->end-bundle->begin << " (" << bundle->begin << " - " << bundle->end <<") TH: " << cpuThroughput << " fG: " << fG << endl;
#endif

			/*If GPU has already computed some chunk, then we update fG (factor GPU)*/
			if (gpuThroughput > 0){
				fG = gpuThroughput/cpuThroughput;
			}
		}
		/*Deleting bundle to avoid memory leaking*/
		delete bundle;
		return NULL;
	}
};
//end class

/*Oracle Class: This scheduler version let us to split the workload in two subranges, one for GPU and one for CPUs*/
class Dynamic : public Scheduler<Dynamic, Params> {
	Params * pars;
public:
	/*This constructor just call his parent's contructor*/
	Dynamic(Params *p) : Scheduler<Dynamic, Params>(p){
		//Params * p = (Params*) params;
		pars = p;

		chunkCPU = minChunkCPU;
    //chunkCPU = ALIGNCPU(chunkCPU);//HECHO POR JC
		chunkGPU = 0;
		fG = 0.0;
		chunkGPU = p->gpuChunk;
		//chunkGPU = ALIGNGPU(chunkGPU);//HECHO POR JC
		//printf("%d\n",chunkGPU );
		gpuThroughput=0.0;
		cpuThroughput=0.0;
	}


	/*The main function to be implemented*/
	template<class T>
	void heterogeneous_parallel_for(int begin, int end, T* body){
#ifdef DEBUG
		cerr << "Heterogeneous Parallel For Dynamic " << nCPUs << " , " << nGPUs << ", " << chunkGPU << endl;
#endif
		/*Preparing pipeline*/
		pipeline pipe;
		MySerialFilter serial_filter(begin, end, nCPUs);
		MyParallelFilter<T> parallel_filter(body);
		pipe.add_filter(serial_filter);
		pipe.add_filter(parallel_filter);

		//chunkGPU = min(chunkGPU,(int)ALIGNFLOORGPU(end-begin));
		//chunkCPU = minChunkCPU;
		//fG = 0.0;
		gpuStatus = nGPUs;
		body->firsttime = true;

		/*Run the pipeline*/
		pipe.run(nCPUs + nGPUs);
		pipe.clear();
	}

	/*this function print info to a Log file*/
	void saveResultsForBench(){

		char * execution_name = (char *)malloc(sizeof(char)*100);
#ifndef GPU_LAST
		sprintf(execution_name, "_Dynamic_%d_%d.txt", nCPUs, nGPUs);
#else
		sprintf(execution_name, "_Dynamic_gpu_%d_%d.txt", nCPUs, nGPUs);
#endif
		strcat(pars->benchName, execution_name);

		/*Checking if the file already exists*/
		bool fileExists = isFile(pars->benchName);
		ofstream file(pars->benchName, ios::out | ios::app);
		if(!fileExists){
			printHeaderToFile(file);
		}
		file << nCPUs << "\t" << nGPUs << "\t"  <<  runtime << "\t"
                << "\t" << chunkGPU << endl;
		file.close();
#ifdef DEBUG
		cerr << nCPUs << "\t" << nGPUs << "\t"  << runtime << "\t"
                << endl;
#endif
	}

	void printHeaderToFile(ofstream &file){
		file << "N. CPUs" << SEP << "N. GPUs" << SEP << "Time (ms)" << SEP
                << "chunkGPU" << endl;
	}
};
