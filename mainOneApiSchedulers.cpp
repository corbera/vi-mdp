/*******************************************************************************
--------------------------------------------------------------------------------
Before Compile:
0. source /opt/intel/inteloneapi/setvars.sh 
1.Go to Common.h and set the MDP_problem_size(0..12) in #define INPUT_ID MDP_problem_size
  We used in the paper the MDP_problem_size 2,4,6,8,10
2.Go to main.cpp and comment one of the following lines. Leave uncommented the
one corresponding to the scheduler that you want to use:
  #define DYNAMIC 5
  #define ORACLE 4
3. IF you change the platform, check output file name & set the proper TEST_PLATFORM ID
--------------------------------------------------------------------------------
Compile: go to the root fo the project (where main.cpp is) and run "cmake ."
--------------------------------------------------------------------------------
Execution:
1. For 0=SEQ1D,1=OMP,2=TBB,3=G-TBB implementations. No Scheduler.
sudo ./ValueIteration ImplemId(0-3,0=SEQ1D,1=OMP,2=TBB,3=G-TBB) NCpus(0-4)
  e.g.: sudo ./ValueIteration 1 3
2. For "#define ORACLE 4" implementations. ORACLE Scheduler.
sudo ./ValueIterationOracle NCpus(0-4) NGpus(0-1) RatioGPU(0.0-1.0)
  e.g.: sudo ./ValueIteration 0 3 0.5
3. For "#define DYNAMIC 5" implementations. DYNAMIC Scheduler.
sudo ./ValueIterationDynamic  NCpus(0-4) NGpus(0-1) ChunkGPU(>1)
  e.g.: sudo ./ValueIteration 0 3 2048
--------------------------------------------------------------------------------
*/

//#define LOCAL
//#define NDEBUG
// #define HACK_TO_REMOVE_DUPLICATE_ERROR
//#define PCM_DEBUG_TOPOLOGY


//#define DEBUG
//#define DEEP_GPU_REPORT
//#define DEEP_CPU_REPORT
//#define OVERHEAD_ANALYSIS //OVERHEAD_STUDY


//#define DYNAMIC_ONE_API 7
//#define ORACLE_ONE_API 9
//#define LOGFIT_ONE_API 8


#define FILE_INPUT //read T matrix arrays data from file or compute it (if not defined)
// #define TEST_PLATFORM 3

//#define VALIDATE_POLICY

// #include "src/RobotState.h"
// #include "src/TransitionMatrix.h"
#include "navigation_mdp/Common.h"
//#include "src/ValueIterationOMP.h"
//#include "src/ValueIteration.h"
//#include "src/ValueIterationUtils.h"
#include "src/Norm2.h"



//#include "src/ValueIterationSeq1D.h"
//#include "src/valueIterationOMP1D.h"
#include "src/ValueIterationOneApiUSM.h"

#include <iostream>
#include <chrono>
#include <fstream>
#include <stdio.h>


#include "utils/timing.h"
#include <cstring>
#include <unistd.h>
//#include "energy-meter/energy_meter.h"
#include <unistd.h>
#include <stdlib.h>
// #include <omp.h>
#include <string>
#include <sstream>
#include "src/ValueIterationTBB.h"
// #include "src/ValueIterationTBBClass.h"

#include <tbb/task_scheduler_init.h>
#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>


#include "src/ValueIterationPlanifierOneApi.h"



#include "src/vi.h"


#include <cstdlib>

#include "CL/sycl.hpp"


using namespace cl::sycl;

using namespace tbb;
using namespace std;


float comparePolicies(const cl_int *P, const cl_int *Pxxx, uint ns, bool verbose);



#define MEASURE_K1

#ifdef MEASURE_K1
#include "cpucounters.h"
#include "utils.h"
#include "tbb/tick_count.h"
#endif

class property_list propList = cl::sycl::property_list{ cl::sycl::property::queue::enable_profiling() };

cl::sycl::queue gpu_queue(gpu_selector{} /*, propList*/), cpu_queue(cpu_selector{});
cl::sycl::context ctx = gpu_queue.get_context();
cl::sycl::event e1, e2, e3, e4, e5;

class vi vi;
cl_float*   d_probability = NULL;
cl_ulong*   d_nextCellPos = NULL;
cl_ulong*   d_nextStatePos = NULL;
cl_float*   d_R      = NULL;

cl_ulong  NS, NR, NG, ND, NA, NX, NT;
int inputId;

int main(cl_int argc, char **argv) {


//    float offload_ratio;

#if defined(ORACLE_ONE_API) || defined(DYNAMIC_ONE_API)
    if (argc < 5) {
        cerr << "Args: inputId numcpus numgpus [ratio | gpuCHUNK]" << std::endl;
        exit(-1);
    }

#elif defined(LOGFIT_ONE_API)
    if (argc < 4) {
        cerr << "Args: inputId numcpus numgpus" << std::endl;
        exit(-1);
    }
#endif

    inputId = atof(argv[1]);

    vi.initialize(inputId);


    ulong ns = vi.ns;
    int nx = vi.nx;

    cout << "\n\n~~~~~~~~~~~~ NS: " << ns << "~~~~~~~~~~~~~~`\n" ;
    //    ulong nextCellPos = vi.nextCellPos;

    /** Planifier */
    ValueIterationPlanifierOneApi vip;//(&vi);

    vip.AllocateMemoryObjects();

    Params p;

    p.numcpus = atoi(argv[2]);
    p.numgpus = atoi(argv[3]);
#ifdef ORACLE_ONE_API
    p.ratioG = atof(argv[4]);
    cout << "Oracle Scheduler" <<std:: endl;
#endif
#ifdef DYNAMIC_ONE_API
    p.gpuChunk = atoi(argv[4]);
    cout << "Dynamic Scheduler" << std::endl;
#endif
#ifdef LOGFIT_ONE_API
    cout << "LogFit Scheduler" << std::endl;
#endif

if ((int)TEST_PLATFORM == 2)  sprintf(p.benchName, "./data/evaluationO3/Broadwell1_K1_chunk_");
if (((int)TEST_PLATFORM == 3)) sprintf(p.benchName, "./data/evaluationO3/Envy_K1_chunk_");
if (((int)TEST_PLATFORM == 4)) sprintf(p.benchName, "./data/evaluationO3/Cafe_K1_chunk_");
//    sprintf(p.kernelName, "valueIterationKernel");

    cout << "\nStarting with:  NCPUs: " << p.numcpus << " NGPUS: " << p.numgpus << std::endl;

#ifdef ORACLE_ONE_API
    Oracle * hs = Oracle::getInstance(&p);
#endif
#ifdef DYNAMIC_ONE_API
    Dynamic * hs = Dynamic::getInstance(&p);
#endif
#ifdef LOGFIT_ONE_API
    LogFit * hs = LogFit::getInstance(&p);
#endif

    //Test OneAPI
    bool notReady = true;
    cl_ulong nrIter = 0;
    float norm = 0;

    auto policy_improve = [&](const blocked_range<size_t>& r)
    {
        for (ulong s = r.begin(); s != r.end(); ++s) {
            for (int a = 0; a < nx; a++) {
                size_t idx = s * nx + a;
                if (vi.Q[idx] - vi.Z[s] > EPSILON) {
                    vi.Z[s] = vi.Q[idx];
                    vi.P[s] = a;
//                    printf("Z[%d] = %f \n", s, vi.Z[s]);
                }
            }
        }
    };

    // Norm2 n2sum(h_Z,h_V);
    //task_scheduler_init init(tbb::task_scheduler_init::automatic);  //corbera: initialized in scheduler

    // cout << "Probability: ";
    // print1DimArray(h_probability, 10);
//
    float runtime_k1  = 0.0;
    float energy_k1 = 0.0;
//    Energy Counters
    PCM * pcm_k1;
    SystemCounterState sstate1_k1, sstate2_k1;
    //timing
    tick_count start, end;
    //Initializing PCM library
    pcm_k1 = PCM::getInstance();
    pcm_k1->resetPMU();
    if (pcm_k1->program() != PCM::Success){
        cerr << "Error in PCM library initialization" << std::endl;
        exit(-1);
    }

    try {
        // Create a command queue using the device selector above, and request profiling
        hs->startTimeAndEnergy();
        while(notReady) {

            //code to be measured - K1
            tick_count bt, et;
            bt = tick_count::now();
            sstate1_k1 = pcm_k1->getSystemCounterState();
            
            /** Policy evaluation on device*/
            hs->heterogeneous_parallel_for(0, ns * nx, &vip); //begin = 0, end = NS * NX, ..


            et = tick_count::now();
            runtime_k1 += (et-bt).seconds();
            sstate2_k1 = pcm_k1->getSystemCounterState();
            energy_k1 += getConsumedJoules(sstate1_k1, sstate2_k1);

//            std::cout << " done waiting for kernerl" << "\n";
            
            /** Policy improvement */


            parallel_for( blocked_range<size_t>(0,ns /*, grainSize*/), policy_improve);
            //select the policy with the best value (max_a Q(s,a)) for every state

//            cout << "------------------------\nV: ";
//            print1DimArray(vi.V, vi.ns, 10);

            //Norm2 - lambda
            norm = parallel_reduce(blocked_range<size_t>(0,ns), 0.0f,
                                [&](blocked_range<size_t> r, double total_norm)
                                {
                                    for (size_t i=r.begin(); i<r.end(); ++i)
                                    {
                                        total_norm += pow((vi.Z[i]-vi.V[i]),2);
                                    }

                                    return total_norm;
                                }, std::plus<double>() );

            norm = sqrt(norm);

//            std::cout <<"\nnorm: " << norm << std::endl;


            notReady = (norm > g) && nrIter < MAX_ITER;
            norm = 0.0;
            //swap old V with new V (Z)
//             for (int i=0; i<NS; i++)
//                 vi.V[i] = vi.Z[i];
            e5 = gpu_queue.memcpy(vi.V, vi.Z,sizeof(float)*ns);
            e5.wait();
    
            nrIter++; // #iters to convergence

//            std::cout << "\nValtItOCL #iter: " << nrIter << "\n";
        

        } // while
        hs->endTimeAndEnergy();
        hs->saveResultsForBenchDen(inputId, nrIter, (int)TEST_PLATFORM, runtime_k1, energy_k1);
    }
    catch (cl::sycl::exception e) {
        std::cout << "SYCL exception caught: " << e.what() << std::endl;
        return 1;
    }


    cout << "\n\nThe OneAPI implem converged in " << nrIter << " iterations.  ";
    cout << "\n\nExecTime K1: " << runtime_k1 << " Energy K1 " << energy_k1 << "\n ";
   // print1DimArray(V, ns, 20);
   
    char* outputFileName  = "OneApiUSM.txt";

    // ReadInput(inputFileName);
    FILE *f_out;

    f_out = fopen(outputFileName, "a");
    if (f_out == NULL) {
        fprintf(stderr, "file not created: %s\n", outputFileName);
        exit(-1);
    }
//    fprintf(f_out, "IN: %d \t GPU_Ratio: %f\t Het_kernel_time: %f \t Het_kernel_energy: %f \t Total_exec_time: %f \t Nr_iter: %d\n",
//     fprintf(f_out, "%d\t%f\t%f\t%f\t%f\t%d\n",
//                    inputId,
//                    offload_ratio,
//                    runtime_k1,
//                    energy_k1,
//                    elapsed,
//                    nrIter
//                    );

    /** Evaluate the execution time for sequential vs OpenMP on Value iteration algorithm*/


    return 0;
}



//returns the percentage of similar policies P and Pxxx
float comparePolicies(const cl_int *P, const cl_int *Pxxx, uint ns, bool verbose) {
    float sameP = ns;
    for (cl_int p = 0; p < ns ; ++p) {
        if(P[p] != Pxxx[p]) {
            sameP--;
            if (verbose)
                cout << "Pseq[" << p << "]=" << P[p] << "; " << "Pxxx[" << p << "]=" << Pxxx[p] << "\n";
            //break;
        }
    }
    return sameP/ns;
}