# VI-MDP

Implementations and optimizations of Value iteration algorithm (VI). We use as a benckmark a robot navigation problem modelled as a discrete MDP.
 We have generate 12 discrete MDP (numbered from 0 to 11) from simulated navigation in a room like environment and use them as benchmarks to test different VI implementations.
 
 The data for the discrete MDP models is stored in ``data/in/xxx.txt``, the simulation data is stored in ``data/LOG_05A_02S_FRANKIE.txt``.
 
 
 The MDP state components are the following:
 
- NT: number of theta discrete values
- NR = 3: number of rangefinders
- NG: number of discrete values for a rangefinder reading
- ND:  number of discrete values for the distance from the robot to the target
- NA: number of discrete values for the angle between robot orientation and target

The robot can take 6 different actions (NX):

- stay still
- turn around
- move forward
- move backward
- move to the left side 
- move to the right side

We include in a number of parallel implementations for VI that find an optimal navigation policy:

- **Cpu-only**: TBB and OpenMP; 
- **Heterogeneous**:  G-TBB, HO-OCL, HD-OCL, HL-OCL (OpenCL and TBB), 
                        BUFF, USM, HO-ONE, HD-ONE, HL-ONE (OneAPI and TBB) .
--------------------------------------------------------------------------------
# Before Compile:

1.  ``source /opt/intel/inteloneapi/setvars.sh `` # to set the oneAPI environment variables
2.  ``modprobe msr`` # as root (to use the pcm library --> cpu-gpu performance monitor, not always needed) 
 
--------------------------------------------------------------------------------
# Compile: 

1.Check the `CMakeLists.tx` file and make sure that you uses the correct paths for your environment. 
A number of options are preset: broadwel1, kaveri, odroid, mac_denisa... For instance, if you want 
to use broadwell1, make sure you set the `BROADWELL1_PLATFORM` option to `ON` and the rest to `OFF`. 
You can configure your own platform using the predefined options as an example.


...

``option (BROADWELL1_PLATFORM 
``        "Use broadwel1 platform" ON)

``option (ENVY_PLATFORM
``        "Use envy (kaby lake) platform" OFF)

``option (ODROID_PLATFORM
``        "Use odroid platform" OFF)


...

2. Choose whether you want to use oneAPI or OpenCL for the GPU execution (in CMakeLists.txt)

To set ``dpcpp`` option when compiling oneApi applications, leave these lines uncommented and comment for pure OpenCL. 
Do the opposite if you want to use OpenCL (Right now it is conflicting with oneAPI implementation).

```
option (DPCPP  "Use DPC++" ON)
if(DEFINED DPCPP)
    set(CMAKE_CXX_COMPILER "dpcpp")
    # set when compiling oneApi applications, comment for pure OpenCL
    set(SCHEDULERS_INC ${SCHEDULERS_INC} SchedulersOneApi)
endif(DEFINED DPCPP)

#option (OCL  "Use OpenCL" ON)
#if( OCL)
#    # set when compiling OpenCL (non oneAPI)
#    set(SCHEDULERS_INC ${SCHEDULERS_INC} SchedulersIntel)
#endif( OCL)
```


3.Go to the root of the project (where mainXXX.cpp is located) and run:

`$ cmake .`

`$ make` 

--------------------------------------------------------------------------------
# Executing VI implementations:
0. Requires sudo execution (for performance evaluation with pcm).

1.For 0=SEQ1D,1=OMP,2=TBB,3=G-TBB implementations. No Scheduler (comment `#define SCHEDULER`).

`sudo ./vi IdMDP(0-11) ImplemId(0-3,0=SEQ1D,1=OMP,2=TBB,3=G-TBB) NCpus(0-4)`

  e.g.: `sudo ./vi 7 1 3` # MDP 7, OMP implementation, using 3 CPU cores
  
2.For implementations using ORACLE Scheduler.

`sudo ./vi IdMDP(0-11) NCpus(0-4) NGpus(0-1) RatioGPU(0.0-1.0)`

  e.g.: `sudo ./vi 9 4 1 0.5`
  
3.For implementations using DYNAMIC Scheduler.

`sudo ./vi IdMDP(0-11) NCpus(0-4) NGpus(0-1) ChunkGPU(>1)`

  e.g.: `sudo ./vi 0 2 1 2048`
  
4.For implementations using LOGFIT Scheduler.

`sudo ./vi IdMDP(0-11) NCpus(0-4) NGpus(0-1)`

  e.g.: `sudo ./vi 4 1`
  
  **Note**: for execution, substitute ``vi`` with the name of the executable (e.g., USM, HO-ONE, HD-ONE, HL-ONE,...)
  
--------------------------------------------------------------------------------
# Verification:

E.g.: result for heterogeneous implementation with ORACLE scheduler:

....


```
dencon@mur:/home/dencon/work/vi-mdp$ sudo ./HO-ONE 7 8 1 0.5
Algorithm input id : 7
probabilitySize: 313701


~~~~~~~~~~~~ NS: 1296000~~~~~~~~~~~~~~
Intanciated ValueIterationPlanifierOneApi 
Oracle Scheduler

Starting with:  NCPUs: 8 NGPUS: 1
IBRS and IBPB supported  : yes
STIBP supported          : yes
Spec arch caps supported : no
Number of physical cores: 4
Number of logical cores: 8
Number of online logical cores: 8
Threads (logical cores) per physical core: 2
Num sockets: 1
Physical cores per socket: 4
Core PMU (perfmon) version: 4
Number of core PMU generic (programmable) counters: 3
Width of generic (programmable) counters: 48 bits
Number of core PMU fixed counters: 3
Width of fixed counters: 48 bits
Nominal core frequency: 1800000000 Hz
IBRS enabled in the kernel   : no
STIBP enabled in the kernel  : no
Package thermal spec power: 15 Watt; Package minimum power: 0 Watt; Package maximum power: 0 Watt; 
 Zeroed PMU registers
 Zeroed PMU registers

ORACLE   IN_SIZE: 7      PLATFORM_ID: 3  ratioGPU: 0.5   eT(Joule): 9.59766      runtime(s): 0.709086   nrIter: 16      0.537409


The OneAPI implem converged in 16 iterations.  

ExecTime K1: 0.537409 Energy K1 7.51135

```


1.Check if  Heter #iter is similar to Seq #iter.

`Heter #iter: 15. Seq #iter: 16. Norm: 1.62705e-07. P similarity: 0.999642`

  In our example Heter #iter = 15 and Seq #iter = 16. This is considered OK. they can differ slightly.
  If Heter #iter = 1 or 2 for instance and Seq #iter = 16, the result is considered wrong.
  The Sequential and optimised should take a similar number of iterations to converge!
  
2.Check the value of P similarity. The closer to 1 the better. If > 0.999, then the result is considered OK.

--------------------------------------------------------------------------------
The OpenCL - based code was tested on Ubuntu 14 (Odroid platform - ARM), Ubuntu 16, Ubuntu 18  & MacOS High Sierra (Intel processors)
The oneAPI - based code was tested only on Ubuntu 18 (Intel i5-8250U), it has conflicts with the OpenMP implementation.
