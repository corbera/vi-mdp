//
// Created by dconstantinescu on 06/02/2017.
//

#ifndef VALUEITERATION_ROBOTSTATE_H
#define VALUEITERATION_ROBOTSTATE_H
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif
#include <cmath>
#include <tuple>
#include "Common.h"

using namespace std;

class RobotState {

private:
    cl_int t;
    cl_int r[NR];
    cl_int d;
    cl_int a;

public:
    RobotState();
    virtual ~RobotState();

    /** Convert and scale continuous state values to discrete
     *
     * @param tc - Real value of theta (robot orientation in space)
     * @param rc - Real value of rangefinder readings
     * @param dc - Real value of the distance from the robot to the target
     * @param ac - Real value of the robot orientation relative to the target
     * % Formula for scaling from [min,max] to [a,b]
     *        (b-a)(x - min)
     * f(x) = --------------  + a
     *         max - min
     */
    void setDiscreteFromContinuous(cl_float tc, cl_float *rc, cl_float dc, cl_float ac);

    /**
    * Compute a unique id for the current (sub)state given the rangefinder index.
    * @param rangefinderIndex
    * @return (sub)state id
    */
    cl_ulong stateId(/*cl_int rangefinderIndex*/);

    /**
     * Returns a unique id given the indexes of a 5D matrix
     * @param t
     * @param r
     * @param g
     * @param d
     * @param a
     * @return id
     */
    static cl_ulong stateIdFrom5DMatIndexes(cl_int t, cl_int r, cl_int g, cl_int d, cl_int a);

    /**
     * Returns a unique id given the indexes of a 5D matrix
     * @param t
     * @param r
     * @param g
     * @param d
     * @param a
     * @return id
     */
    static cl_ulong stateIdFrom7DMatIndexes(cl_int t, cl_int* r, cl_int d, cl_int a);

    /**
     * Obtain state values for t,r,g,d and a from the state id.
     * @param id
     * @param sub Contains the computed values for t,r,g,d and a
     */
    static /*tuple<cl_int,cl_int,cl_int,cl_int,cl_int>*/ void getRobotStateTupleFromId(cl_ulong id, cl_int* sub);

    /**
     * Basic getters and setters
     */
    void setT(cl_int t);

    void setD(cl_int d);

    void setA(cl_int a);

    void setR(cl_int *r);

    void setRobotState(RobotState robotState);

    cl_int *getR()  {
        return r;
    }

    cl_int getT()  {
        return t;
    }

    cl_int getD()  {
        return d;
    }

    cl_int getA()  {
        return a;
    }

    /**
     * Display RobotState
     */
    void prcl_intRobotState();
};


#endif //VALUEITERATION_ROBOTSTATE_H
