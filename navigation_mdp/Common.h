//
// Created by dconstantinescu on 06/02/2017.
//
/*Problems:
1. Scheduler for GPU alone converges in 1 iteration.
   Q or V may not be transferred correctly!!!
   - Q is transferred corrrectly
2. Scheduler begin/end gets valua MIN_INT randomly....
   solution: in Dynamic.h: fG > 0.1 instead of fG > 0
*/

//#if defined(__APPLE__)
//#include <opencl.h>
//#else
//#include <CL/cl.h>
//#endif

#ifndef VALUEITERATION_COMMON_H
#define VALUEITERATION_COMMON_H

#define INPUT_ID 10 // Set to [0|1|2|3] - to indicate the input size

// MDP State Components
//const static int NT = tD[INPUT_ID];    // number of theta discrete values
//const static int NR = 3;    // number of rangefinders
//const static int NG = gD[INPUT_ID];    // number of discrete values for a rangefinder reading
//const static int ND = dD[INPUT_ID];    // number of discrete values for the distance from the robot to the target
//const static int NA = aD[INPUT_ID];    // number of discrete values for the angle between robot orientation and target
//const static int NX = 6;    // number of actions


//state components discretization
//----------------------{0, 1, 2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13};
const static int tD[] = {4, 5, 9,  12,  9, 13, 19, 24, 26, 19, 24, 30, 40, 60};
const static int gD[] = {3, 3, 3,   3,  5,  5,  5,  5,  6,  6, 10, 10, 10, 10};
const static int dD[] = {5, 8, 8,  13, 13, 14, 14, 18, 18, 68, 20, 24, 26, 23};
const static int aD[] = {4, 5, 9,  12,  9, 13, 19, 24, 26, 19, 24, 30, 40, 60};
// t-matrix non zero values count
const static int pNZ[] = {68828, 109202, 156952, 205710, 244101, 266078, 288166, 313701, 350477, 390242, 383657, 398257, 413635, 430963};
// MDP nr of states
const static int nStates[] = {2160, 5400, 17496, 50544, 131625, 295750, 631750, 1296000, 2628288, 5302368, 11520000, 21600000, 41600000, 82800000};


const static float   g =  0.000001; // 0.000001;
const static int sr = 20; // sample period (msec) for energy and time measurement


const static int MAX_ITER = 15;
const static int STATE_DIM = 6; // state space dimensions

const static float MAX_RANGEFINDER_DISTANCE  = 1.0;  // Max. distance detected by the laser beam (meters). Must be set in V-REP Hokuyo laser params.
const static float MAX_TARGET_DISTANCE = 6.7;  // Max distance between the robot and the target (meters)

//const static cl_ulong  NS = NT * NG * NG * NG * ND * NA; // number of states


const static float GAMMA = 0.1;
const static float EPSILON = 1e-16; // cl_float error

/** Value iteration init - global variables */
const static float   gamma_ = 0.1; 	/* Discount factor, (0,1) */
const static float   d = 0.001;		/* Convergence margin, d > 0 */



#endif //VALUEITERATION_COMMON_H
