//
// Created by dconstantinescu on 07/02/2017.
//

#ifndef VALUEITERATION_TRANSITIONMATRIX_H
#define VALUEITERATION_TRANSITIONMATRIX_H

#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif

typedef unsigned long ulong;

#include <unordered_map>
#include "Common.h"
#include <vector>

using namespace std;

typedef unordered_map<cl_ulong, cl_ulong> TransCountMap;
typedef unordered_map<cl_ulong, cl_float> TransProbMap;

class TransitionMatrix {
public:
    //m1
    //TransCountMap transitionsCount[NS][NX]; //!!! IMPROVEMENT: transitionsCount could be allocated dinamically and released once transProbMap is computed
    //TransProbMap  transProbMap[NS][NX];
    // 1D array representation of transProbMap for OPenCL implementation

    //m2
//    std::vector<std::vector<TransCountMap>> transitionsCount = std::vector<std::vector<TransCountMap>>(NS, std::vector<TransCountMap>(NX));
//    std::vector<std::vector<TransProbMap>> transProbMap = std::vector<std::vector<TransProbMap>>(NS, std::vector<TransProbMap>(NX));

    //m3
    TransCountMap **transitionsCount;
    TransProbMap  **transProbMap;

    cl_float   *probabilities;
    cl_ulong   *nextStateIdPositions;

    cl_ulong   *getNextStateIdPositions() const;

    cl_ulong   *nextCellPositions;

    cl_ulong   elementsNumber;
    cl_ulong   maximumCellSize;

    TransitionMatrix(string logFilePath, bool verbose);

    void processLogForTransitionsCounts(string logFileName, bool verbose);
    void computeTransitionsProbabilityMatrix(bool verbose);
    void displayTransitionsCount();

    double getProbabilitySAS(cl_ulong state1, cl_int action, cl_ulong state2);
    //double getNextStateCount(cl_ulong state1, cl_ulong action);

    void linearizeTransitionProbabilityMatrix();

    cl_float *getProbabilities() const;

    cl_ulong *getNextCellPositions() const;



    cl_ulong getElementsNumber() const;

    cl_ulong getMaximumCellSize() const;

    cl_ulong *getProbStates() const;

    void freeTransitionsCount();

    void freeTransProbMap();

    ~TransitionMatrix();

};


#endif //VALUEITERATION_TRANSITIONMATRIX_H
