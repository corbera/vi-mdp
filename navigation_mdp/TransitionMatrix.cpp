//
// Created by dconstantinescu on 07/02/2017.
//
#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif
#include <fstream>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <stdlib.h>

#include "TransitionMatrix.h"
#include "RobotState.h"

using namespace std;

cl_ulong *TransitionMatrix::getProbStates() const {
    return nextStateIdPositions;
}

TransitionMatrix::TransitionMatrix(string logFilePath, bool verbose)  {

    //m3
    transitionsCount = new TransCountMap*[NS];
    transProbMap = new TransProbMap*[NS];
    for(ulong i = 0; i < NS; ++i)
        transitionsCount[i] = new TransCountMap[NX];
    for(ulong i = 0; i < NS; ++i)
        transProbMap[i] = new TransProbMap[NX];

    processLogForTransitionsCounts(logFilePath, verbose);
    computeTransitionsProbabilityMatrix(verbose);
    linearizeTransitionProbabilityMatrix();
}

void TransitionMatrix::processLogForTransitionsCounts(string logFileName, bool verbose) {

    ifstream file(logFileName);
    string   line;

    RobotState state1, state2;

    cl_ulong key;

    cl_float state1_t, state1_a, state1_d, state2_t, state2_a, state2_d;
    cl_int action1, action2;
    cl_float state1_r[NR] = {0};
    cl_float state2_r[NR] = {0};

    /** Obtain state - action - state transition counts from the log file ( used to compute the transition matrix) */
    if ( !file )
    {
        cout << "ERROR: Failed opening the log file " << endl;
        exit(1);
    }

    cl_int count_lines = 1;

    if (verbose) {
        cout << "\n\nprocessLogForTransitionsCounts: \n";
        cout << "\nLog line: " << count_lines << endl;
    }

    // read first state from file
    try {
        getline(file, line);
        // line contents: t r r r d a x
        stringstream   linestream(line);
        string         data;

        getline(linestream, data, '\t');  // read up-to the first tab (discard tab).

        // read state 1
        state1_t = atoi(data.c_str());
        for (cl_int i = 0; i < NR; i++)
            linestream >> state1_r[i];
        linestream >> state1_d;
        linestream >> state1_a;

        // read transition action
        linestream >> action1;
        action1--; // in simulation 1 - NX while

        // convert the real values of the sensors' readings to discrete values
        state1.setDiscreteFromContinuous(state1_t, state1_r, state1_d, state1_a);

        if (verbose) {
            cout << " State: ";
            state1.prcl_intRobotState();
            cout << " Action: " << action1 << endl;
        }


    } catch (exception& e) {
        cout << "A standard exception was caught while reading and processing the first log line, with message: '"
             << e.what() << "'\n";
    }

    // read and process the rest of the log file
    try {
        while(getline(file, line))
        {
            // line contents: t r r r d a x
            stringstream   linestream(line);
            string         data;

            getline(linestream, data, '\t');  // read up-to the first tab (discard tab).

            // read state 2
            state2_t = atoi(data.c_str());
            for (cl_int i = 0; i < NR; i++)
                linestream >> state2_r[i];
            linestream >> state2_d;
            linestream >> state2_a;

            // read transition action
            linestream >> action2;
            action2--;

            // convert the real values of the sensors' readings to discrete values
            state2.setDiscreteFromContinuous(state2_t, state2_r, state2_d, state2_a);

            if (verbose) {
                cout << "\nLog line: " << count_lines << endl;
                cout << " State: ";
                state2.prcl_intRobotState();
                cout << " Action: " << action2 << endl;
            }

            // increment transitions' counts corresponding to state1 - action1 -> state2
//            for (cl_int rf = 0; rf < NR; rf++)
//            {
                key = state2.stateId(/*rf*/);
                // Update transitions matrix. Increment the times the state state2 was visited.
                if(transitionsCount[state1.stateId(/*rf*/)][action1].count(key) == 0) // they key dos not exist; inset pair
                {
                    transitionsCount[state1.stateId(/*rf*/)][action1].insert(TransCountMap::value_type(key,1));
                }
                else { // they key exists, so increment the counter
                    auto tmp = transitionsCount[state1.stateId(/*rf*/)][action1].find(key);
                    tmp->second++;
//                }
            }
            count_lines++;

            // state1 becomes state2, the same for action
            state1.setRobotState(state2);
            action1 = action2;
        }
    } catch (exception& e) {
        cout << "A standard exception was caught while reading and processing the log file on line "
             << count_lines
             <<", with message: '"
             << e.what() << "'\n";
    }

    file.close();

}

void TransitionMatrix::computeTransitionsProbabilityMatrix(bool verbose) {

    cl_ulong key;
    double probability;

    if (verbose)
        cout << "\n\ncomputeTransitionsProbabilityMatrix: \n\n";
    for (cl_int s = 0; s < NS; ++s) {
        for (cl_int a = 0; a < NX; ++a) {
            cl_ulong sumTransitionsSA = 0;
            //compute the sum of all transitions from state s with action a
            for(auto pair : transitionsCount[s][a]) {
                sumTransitionsSA += pair.second;
            }
            // compute the probability to get to state s2(key) from state s with action a
            for(auto pair : transitionsCount[s][a]) {
                key = pair.first;
                probability = (double) pair.second / sumTransitionsSA; //transitionsCount[s][a].size();
                transProbMap[s][a].insert(TransProbMap::value_type(key,probability));
                if (verbose)
                    cout << "P(" << s << "," << a << "," << key << ") = " << probability << endl;
            }

        }
    }
    freeTransitionsCount();
}

double TransitionMatrix::getProbabilitySAS(cl_ulong state1, cl_int action, cl_ulong state2) {
     //TODO: check state1, action and state2 are not out of range!!
    if (transProbMap[state1][action].count(state2) == 0)
        return 0;
    return transProbMap[state1][action].at(state2);
}

void TransitionMatrix::displayTransitionsCount() {
    for (cl_int s = 0; s < NS; ++s) {
        for (cl_int a = 0; a < NX; ++a) {
            for(auto pair : transitionsCount[s][a]) {
                cout << "Count(" << s << "," << a << "," << pair.first << ") = " << pair.second << endl;
            }
        }
    }
}

//! could do this directly and release the memory for the unordered_map earlyer??? --- NOPE :P
void TransitionMatrix::linearizeTransitionProbabilityMatrix()
{
 //   if(linearized) return; // already linearized, no need to do it again.

    elementsNumber = 0;
    maximumCellSize = 0;

    cl_ulong cellSize;

    //1. Obtain the number of elements in transProbMap and the maximum number of elements in a cell
    for (cl_int s = 0; s < NS; ++s) {
        for (cl_int a = 0; a < NX; ++a) {
            cellSize = 0;
            for(auto pair : transProbMap[s][a]) { // count the elements from the map (cell)
                cellSize++;
            }
           // cout << "\tCellSize(" << s << "," << a << ")=" << cellSize;
            elementsNumber += cellSize;
            if (cellSize > maximumCellSize) maximumCellSize = cellSize;
        }
        //cout << endl;
    }

    cout << "\nElements number: " << elementsNumber << endl;

    probabilities = (cl_float*)malloc(sizeof(cl_float)*elementsNumber);
    nextStateIdPositions = (cl_ulong*)malloc(sizeof(cl_ulong)*elementsNumber);
    nextCellPositions = (cl_ulong*)malloc(sizeof(cl_ulong)*(NX*NS+1)); // add an extra element for kernel acccess with the last pos of probability
    if (probabilities == NULL || nextStateIdPositions == NULL || nextCellPositions == NULL) exit (1);
    cl_ulong probIndex = 0;
    cl_ulong cellIndex = 0;

    //2. Write the probability (pair.second) elements from transProbMap to probabilities 1D array  and annotate
    // the beginning of the probabilities from every cell (unordered_map) in nextCellPosition array.
    for (cl_int s = 0; s < NS; ++s) {
        for (cl_int a = 0; a < NX; ++a) {
            nextCellPositions[cellIndex] = probIndex; // mark the index of the beginning of every cell (map)
            cellIndex++;
            for(auto pair : transProbMap[s][a]) {
                probabilities[probIndex] = pair.second; // make a copy the probabilities from the unordered_map
                nextStateIdPositions[probIndex] = pair.first;
                probIndex++;
            }
        }
    }
    // add in the last position th etotal nr of probability elements
    nextCellPositions[NX*NS] = elementsNumber;
}

cl_float *TransitionMatrix::getProbabilities() const {
/*    cl_float* prob   = (cl_float*)calloc(elementsNumber,sizeof(cl_float));
    for (int i = 0; i < elementsNumber; i++)
        prob[i] = probabilities[i];
    return prob;
*/
    return probabilities;
}

cl_ulong *TransitionMatrix::getNextCellPositions() const {
/*    cl_ulong* nextCellPos   = (cl_ulong*)calloc(NS*NX+1,sizeof(cl_ulong));
    for (int i = 0; i < NS*NX+1; i++)
        nextCellPos[i] = nextCellPositions[i];
    return nextCellPos;
*/
    return nextCellPositions;
}

cl_ulong TransitionMatrix::getElementsNumber() const {
    return elementsNumber;
}

cl_ulong TransitionMatrix::getMaximumCellSize() const {
    return maximumCellSize;
}

cl_ulong *TransitionMatrix::getNextStateIdPositions() const {
/*    cl_ulong* nextStatePos  = (cl_ulong*)calloc(elementsNumber,sizeof(cl_ulong));
    for (int i = 0; i < elementsNumber; i++)
        nextStatePos[i] = nextStateIdPositions[i];
    return nextStatePos;
*/
    return nextStateIdPositions;
}

void TransitionMatrix::freeTransitionsCount() {
    for(ulong i = 0; i < NS; ++i)
        delete[] transitionsCount[i];
    delete[] transitionsCount;
}

void TransitionMatrix::freeTransProbMap() {
    for(ulong i = 0; i < NS; ++i)
        delete[] transProbMap[i];
    delete[] transProbMap;
}

TransitionMatrix::~TransitionMatrix() {
    //freeTransitionsCount();
    freeTransProbMap();
    free(probabilities);
    free(nextCellPositions);
    free(nextStateIdPositions);
}
