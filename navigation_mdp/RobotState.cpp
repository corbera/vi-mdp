//
// Created by dconstantinescu on 06/02/2017.
//

#if defined(__APPLE__)
#include <opencl.h>
#else
#include <CL/cl.h>
#endif

#include <iostream>

#include "RobotState.h"
#include "Common.h"
#include <math.h>

using namespace std;

RobotState::RobotState():t(0), d(0), a(0) {

};

RobotState::~RobotState() {

};


// Formula for scaling from [min,max] to [a,b]
//        (b-a)(x - min)
// f(x) = --------------  + a
//           max - min
// [0..NK-1]
void RobotState:: setDiscreteFromContinuous(cl_float tc, cl_float *rc, cl_float dc, cl_float ac)
{
    cl_float PI = 3.14159;
    t = (cl_int)lround((NT - 1) * tc / (2 * PI ));
    d = (cl_int)lround((ND - 1) * dc / MAX_TARGET_DISTANCE);
    a = (cl_int)lround((NA - 1) * ac / (2 * PI));

    for (cl_int ri = 0; ri < NR ; ++ri) {
        r[ri] = (cl_int)lround((NG - 1) * rc[ri] / MAX_RANGEFINDER_DISTANCE);
    }
}


cl_ulong RobotState::stateId(/*cl_int rangefinderIndex*/) {
/*
    if (rangefinderIndex < 0 || rangefinderIndex >= NR )
        throw runtime_error("Error computing the stateId. Value of \"rangefinderIndex\" < 0 || >= NR: " + rangefinderIndex );
*/

/*    cl_ulong id = (cl_ulong) (t * NR * NG * ND * NA
                                        + rangefinderIndex * NG * ND * NA
                                        + r[rangefinderIndex] * ND * NA
                                        + d * NA
                                        + a);*/
    cl_ulong id = (cl_ulong) (            t * NG * NG * NG * ND * NA
                                               + r[2] * NG * NG * ND * NA
                                               + r[1] * NG * ND * NA
                                               + r[0] * ND * NA
                                               + d    * NA
                                               + a);
    return id;
}


void RobotState::setT(cl_int theta) {
    if (theta < 0 || theta >= NT )
        throw runtime_error("Error in RobotState::setT(). Value of \"theta\" is out of range." );
    t = theta;
}

void RobotState::setD(cl_int distance) {
    if (distance < 0 || distance >= ND )
        throw runtime_error("Error in RobotState::setD(). Value of \"distance\" is out of range." );
    d = distance;
}

void RobotState::setA(cl_int angle) {
    if (angle < 0 || angle >= NA )
        throw runtime_error("Error in RobotState::setA(). Value of \"angle\" is out of range.");
    a = angle;
}

void RobotState::setR(cl_int *rangefinder) {
    for (cl_int i = 0; i < NR; ++i) {
        if (rangefinder[i] < 0 || rangefinder[i] >= NG) {
            cout << "\n" << rangefinder[i] << " ::\t" << "\n";
            throw runtime_error(" Error in RobotState::setR(). One of more values of \"*rangefinder\" are out of range.");
        }// not necessary, just copies a state that was generated correctly
        r[i] = rangefinder[i];
    }
}

void RobotState::prcl_intRobotState() {
    cout << "t: " << t << "\t"
         << "d: " << d << "\t"
         << "a: " << a << "\t"
         << "r: ";
    for (cl_int i = 0; i < NR; ++i) {
        cout << r[i] << " ";
    }
    cout << endl;
}

void RobotState::setRobotState(RobotState robotState) {
    t = robotState.getT();
    d = robotState.getD();
    a = robotState.getA();
    this->setR(robotState.getR());
}

/*tuple<cl_int, cl_int, cl_int, cl_int, cl_int>*/ void RobotState::getRobotStateTupleFromId(cl_ulong id, cl_int* sub) {
    cl_int tt, /*rr,gg*/ gg1, gg2, gg3, dd, aa, tmp;

/*
    aa = id % NA;
    sub[4] = aa;

    tmp = (id - aa) / NA;
    dd = tmp % ND;
    sub[3] = dd;

    tmp = (tmp - dd) / ND;
    gg = tmp % NG;
    sub[2] = gg;

    tmp = (tmp - gg) / NG;
    rr = tmp % NR;
    sub[1] = rr;

    tt = (tmp - rr) / NR;
    sub[0] = tt;
*/
    aa = id % NA;
    sub[5] = aa;

    tmp = (id - aa) / NA;
    dd = tmp % ND;
    sub[4] = dd;

    tmp = (tmp - dd) / ND;
    gg1 = tmp % NG;
    sub[3] = gg1;

    tmp = (tmp - gg1) / NG;
    gg2 = tmp % NG;
    sub[2] = gg2;

    tmp = (tmp - gg2) / NG;
    gg3 = tmp % NG;
    sub[1] = gg3;

    tt = (tmp - gg3) / NG;
    sub[0] = tt;


    //return tuple<cl_int, cl_int, cl_int, cl_int, cl_int>(tt,rr,gg,dd,aa); //make_tuple (tt,rr,gg,dd,aa);
}

cl_ulong RobotState::stateIdFrom5DMatIndexes(cl_int t, cl_int r, cl_int g, cl_int d, cl_int a) {
    cl_ulong id = (cl_ulong) (  t * NR * NG * ND * NA
                                        + r * NG * ND * NA
                                        + g * ND * NA
                                        + d * NA
                                        + a);
    return id;
}

cl_ulong RobotState::stateIdFrom7DMatIndexes(cl_int t, cl_int *r, cl_int d, cl_int a) {
    cl_ulong id = (cl_ulong) (       t * NG * NG * NG * ND * NA
                                          + r[2] * NG * NG * ND * NA
                                          + r[1] * NG * ND * NA
                                          + r[0] * ND * NA
                                          + d * NA
                                          + a);
    return id;
}
