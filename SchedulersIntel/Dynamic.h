//============================================================================
// Name			: Dynamic.h
// Author		: Antonio Vilches
// Version		: 1.0
// Date			: 02 / 01 / 2014
// Copyright	: Department. Computer's Architecture (c)
// Description	: Dynamic scheduler implementation
//============================================================================

//#define DEEP_GPU_REPORT

#include <cstdlib>
#include <iostream>
#include <fstream>
#include "Scheduler.h"
#include "tbb/pipeline.h"
#include "tbb/tick_count.h"
#include <math.h>

#ifdef Win32
#include "PCM_Win/windriver.h"
#else
#include "cpucounters.h"
#endif

#ifdef PJTRACER
#include "pfortrace.h"
#endif

using namespace std;
using namespace tbb;

/*****************************************************************************
 * Defines
 * **************************************************************************/
#define CPU 0
#define GPU 1
#define GPU_OFF -100 //Arbitrary value
#define SEP "\t"

/*****************************************************************************
 * types
 * **************************************************************************/
/*
typedef struct{
	int numcpus;
	int numgpus;
	int gpuChunk;
	char benchName[100];
	char kernelName[100];
}Params;
*/
/*****************************************************************************
 * Global variables
 * **************************************************************************/
tbb::atomic<int> gpuStatus;
int chunkCPU;
int chunkGPU;
float fG;
float gpuThroughput, cpuThroughput;
// To calculate scheduling partition overhead
tick_count end_tc;

#ifdef DEEP_CPU_REPORT
ofstream deep_cpu_report;
#endif

#ifdef DEEP_GPU_REPORT
ofstream deep_gpu_report;
#endif

#ifdef OVERHEAD_ANALYSIS
// overhead accumulators
double overhead_sp = 0.0;
double overhead_h2d = 0.0;
double overhead_kl = 0.0;
double kernel_execution = 0.0;
double overhead_d2h = 0.0;
double overhead_td = 0.0;
#endif

long totalIterationsGPU = 0;
long totalIterations = 0;


/*****************************************************************************
 * Heterogeneous Scheduler
 * **************************************************************************/
/*Bundle class: This class is used to store the information that items need while walking throught pipeline's stages.*/
class Bundle {
public:
	int begin;
	int end;
	int type; //GPU = 0, CPU=1

	Bundle() {};
};

/*My serial filter class represents the partitioner of the engine. This class selects a device and a rubrange of iterations*/
class MySerialFilter: public filter {
private:
	int begin;
	int end;
	int nCPUs;
public:
	/*Class constructor, it only needs the first and last iteration indexes.*/
	MySerialFilter(int b, int e, int ncpus) : filter(true) {
#ifdef DEBUG
    printf("\nDEBUG MySerialFilter: begin = %d\n",b);
    printf("\nDEBUG MySerialFilter: end = %d\n",e);
#endif
		begin = b;
		end = e;
		nCPUs = ncpus;
	}

	/*Mandatory operator method, TBB rules*/
	void * operator()(void *) {
		Bundle *bundle = new Bundle();

		/*If there are remaining iterations*/
		if (begin < end /*&& begin > -1 && end > -1*/) {
#ifdef DEBUG
            /*if (begin<0)*/ printf("\nDEBUG operator: begin = %d\n",begin);
            /*if (end<0)*/ printf("\nDEBUG operator: end = %d\n",end);
#endif
			//Checking which resources are available
			if ( --gpuStatus >= 0 ){
				//GPU WORK
				int auxEnd = begin + chunkGPU;
				if(nCPUs < 1){
					auxEnd = (auxEnd > end) ? end : auxEnd;
				}

				if( (auxEnd <= end) ){
					//GPU CHUNK
					bundle->begin = begin;
					bundle->end = auxEnd;
					begin = auxEnd;
					bundle->type = GPU;
				}else{
					//CPU CHUNK
					gpuStatus = GPU_OFF;
					if(fG > 0){
						chunkCPU = min( chunkCPU, max((end-begin)/(nCPUs+1), 1));
					}
					bundle->begin = begin;
					bundle->end = begin + chunkCPU;
					begin = begin + chunkCPU;
					bundle->type = CPU;
				}
#ifdef DEBUG
                if (bundle->begin<0) printf("\nDEBUG operator: if ( --gpuStatus >= 0 ) begin = %d\n",bundle->begin);
                if (bundle->end<0) printf("\nDEBUG operator: if ( --gpuStatus >= 0 ) end = %d\n",bundle->begin);
#endif

				return bundle;
				/*! HERE IS THE PROBLEM */
			}else{
				//CPU WORK
				gpuStatus++;

				/*Calculating next chunkCPU*/
				if(fG > 0.01 /*0*/){ /*! HERE IS THE PROBLEM */
#ifdef DEBUG
				printf("\nDEBUG operator: else  chunkCPU init = %d \n", chunkCPU);
#endif
					chunkCPU = chunkGPU / fG; /*fG 0 0.000..1 => a chunk size that is not accheptable*/
#ifdef DEBUG
				printf("\nDEBUG operator: else  chunkCPU/fG = %d, fg = %f \n", chunkCPU, fG);
#endif
					chunkCPU = min( chunkCPU, max((end-begin)/(nCPUs), 1));
				}

				/*Taking a iteration chunk for CPU*/
				int auxEnd = begin + chunkCPU;
#ifdef DEBUG
				printf("\nDEBUG operator: else auxEnd = %d, chunkCPU = %d \n",auxEnd, chunkCPU);
#endif
				auxEnd = (auxEnd > end) ? end : auxEnd;
				bundle->begin = begin;
				bundle->end = auxEnd;
				begin = auxEnd;
				bundle->type = CPU;
#ifdef DEBUG
                if (bundle->begin<0) printf("\nDEBUG operator: else begin = %d\n",bundle->begin);
                if (bundle->end<0) printf("\nDEBUG operator: else end = %d\n",bundle->begin);
#endif
				return bundle;
			}
		}
		return NULL;
	} // end operator
};

/*MyParallelFilter class is the executor component of the engine, it executes the subrange onto the device selected by SerialFilter*/
template <class B>
class MyParallelFilter: public filter {
private:
	B *body;

public:
	/*Class' constructor*/
	//template <class B>
	MyParallelFilter(B *b) : filter(false) {
		body = b;
	}

	/*Operator function*/
	void * operator()(void * item) {
		//variables
		Bundle *bundle = (Bundle*) item;
		totalIterations += (bundle->end - bundle->begin);

		if(bundle->type == GPU){
			totalIterationsGPU = totalIterationsGPU + (bundle->end - bundle->begin);
			// GPU WORK
#ifdef DEBUG
			cerr << "\nDEBUG::launchGPU(): begin: " << bundle->begin << " end: " << bundle->end << endl;
#endif
			tick_count start_tc = tick_count::now();
#ifdef OVERHEAD_ANALYSIS
			//Calculating partition scheduling overhead
			overhead_sp += (start_tc - end_tc).seconds()*1000;

			//Adding a marker in the command queue
			cl_event event_before_h2d;
			int error = clEnqueueMarkerWithWaitList(command_queue,	0, NULL, &event_before_h2d);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl;
				exit(0);
			}
#endif
			body->sendObjectToGPU(bundle->begin, bundle->end, NULL);

#ifdef OVERHEAD_ANALYSIS
			//Adding a marker in the command queue
			cl_event event_before_kernel;
			error = clEnqueueMarkerWithWaitList(command_queue,	0, NULL, &event_before_kernel);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl;
				exit(0);
			}
#endif
			cl_event event_kernel;
			body->OperatorGPU(bundle->begin, bundle->end, &event_kernel);

#ifdef OVERHEAD_ANALYSIS
			//Adding a marker in the command queue
			cl_event event_after_kernel;
			error = clEnqueueMarkerWithWaitList(command_queue,	0, NULL, &event_after_kernel);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl;
				exit(0);
			}
#endif
			body->getBackObjectFromGPU(bundle->begin, bundle->end, NULL);

#ifdef OVERHEAD_ANALYSIS
			//Adding a marker in the command queue
			cl_event event_after_d2h;
			error = clEnqueueMarkerWithWaitList(command_queue,	0, NULL, &event_after_d2h);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl;
				exit(0);
			}
#endif
			clFinish(command_queue);

#ifdef OVERHEAD_ANALYSIS
			//Adding a marker in the command queue to computer thread dispatch
			cl_event event_after_finish;
			error = clEnqueueMarkerWithWaitList(command_queue,	0, NULL, &event_after_finish);
			if (error != CL_SUCCESS) {
				cerr <<  "Failed equeuing start event" << endl;
				exit(0);
			}
#endif
			end_tc = tick_count::now();

#ifdef OVERHEAD_ANALYSIS

			//Calculating host to device transfer overhead
			cl_ulong tg1, tg2, tg3, tg4, tg5, tc3;
			clGetEventProfilingInfo(event_before_h2d, CL_PROFILING_COMMAND_COMPLETE, sizeof(cl_ulong), &tg1, NULL);
			clGetEventProfilingInfo(event_before_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg2, NULL);
			overhead_d2h += (tg2-tg1)/1000000.0; // ms

			//Calculating kernel launch overheads
			clGetEventProfilingInfo(event_before_kernel, CL_PROFILING_COMMAND_COMPLETE, sizeof(cl_ulong), &tg2, NULL);
			clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg3, NULL);
			overhead_kl += (tg3-tg2)/1000000.0; // ms

			//Calculating kernel execution
			clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg3, NULL);
			clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_COMPLETE, sizeof(cl_ulong), &tg4, NULL);
			kernel_execution += (tg4-tg3)/1000000.0; // ms

			//Calculating device to host transfer overhead
			clGetEventProfilingInfo(event_kernel, CL_PROFILING_COMMAND_COMPLETE, sizeof(cl_ulong), &tg4, NULL);
			clGetEventProfilingInfo(event_after_d2h, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tg5, NULL);
			overhead_d2h += (tg5-tg4)/1000000.0; // ms

			//Calculating device overhead thread dispatch
			clGetEventProfilingInfo(event_after_d2h, CL_PROFILING_COMMAND_COMPLETE, sizeof(cl_ulong), &tg5, NULL);
			clGetEventProfilingInfo(event_after_finish, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tc3, NULL);
			overhead_td += (tc3-tg5)/1000000.0; // ms

			//Releasing event objects
			clReleaseEvent(event_before_h2d);
			clReleaseEvent(event_before_kernel);
			clReleaseEvent(event_kernel);
			clReleaseEvent(event_after_kernel);
			clReleaseEvent(event_after_d2h);
			clReleaseEvent(event_after_finish);
#endif
			float time =(end_tc - start_tc).seconds()*1000;
			gpuThroughput = (bundle->end - bundle->begin) / time;

#ifdef DEEP_GPU_REPORT
			deep_gpu_report << bundle->end-bundle->begin << "\t" << gpuThroughput << endl;
#endif
#ifdef DEBUG
			cerr << "\nDEBUG::launchGPU(): chunk: " << bundle->end-bundle->begin << " TH: " << gpuThroughput << endl;
#endif

			/*If CPU has already computed some chunk, then we update fG (factor GPU)*/
			if (cpuThroughput > 0){
				fG = gpuThroughput/cpuThroughput;
			}

			/*To release GPU token*/
			gpuStatus++;
		}else{
			// CPU WORK
#ifdef DEBUG
			cerr << "\nDEBUG::launchCPU(): begin: " << bundle->begin << " end: " << bundle->end << endl;
#endif
			tick_count start = tick_count::now();
			body->OperatorCPU(bundle->begin, bundle->end);
			tick_count end = tick_count::now();
			float time =(end-start).seconds()/1000;
			cpuThroughput = (bundle->end - bundle->begin) / time;

			/*If GPU has already computed some chunk, then we update fG (factor GPU)*/
			if (gpuThroughput > 0){
				fG = gpuThroughput/cpuThroughput;
			}
		}
		/*Deleting bundle to avoid memory leaking*/
		delete bundle;
		return NULL;
	}
};
//end class

/*Oracle Class: This scheduler version let us to split the workload in two subranges, one for GPU and one for CPUs*/
class Dynamic : public Scheduler<Dynamic> {
	Params * pars;
public:
	/*This constructor just call his parent's contructor*/
	Dynamic(void *params) : Scheduler(params){
		Params * p = (Params*) params;
		pars = p;

		chunkCPU = 0;
		chunkGPU = 0;
		fG = 0.0;
		chunkGPU = p->gpuChunk;
		gpuThroughput=0.0;
		cpuThroughput=0.0;

		//Initializing library PJTRACER
		initializePJTRACER();
	}

	/*Initializes PJTRACER library*/
	void initializePJTRACER(){
#ifdef PJTRACER
		char traceFname[1024];
		sprintf(traceFname, "DYNAMIC_C_%d_G_%d.trace", nCPUs, nGPUs);
		tracer = new PFORTRACER(traceFname);
		tracer->beginThreadTrace();
#endif
	}

	/*The main function to be implemented*/
	template<class T>
	void heterogeneous_parallel_for(int begin, int end, T* body){
#ifdef DEBUG
		cerr << "Heterogeneous Parallel For Dynamic " << nCPUs << " , " << nGPUs << ", " << chunkGPU << endl;
#endif
		/*Preparing pipeline*/
		pipeline pipe;
		MySerialFilter serial_filter(begin, end, nCPUs);
		MyParallelFilter<T> parallel_filter(body);
		pipe.add_filter(serial_filter);
		pipe.add_filter(parallel_filter);
		chunkCPU = chunkGPU * 0.2;
		fG = 0.0;
		gpuStatus = nGPUs;
//		body->firsttime = true; //todo: comment here and init in VIPlanifier

#ifdef DEEP_CPU_REPORT
		char version[50];
		sprintf(version, "_Dynamic_deep_CPU_report_grain_%d.txt", chunkCPU);
		strcat(pars->benchName, version);
		deep_cpu_report.open(pars->benchName, ios::out | ios::app);
#endif
#ifdef DEEP_GPU_REPORT
		char version[100];
		sprintf(version, "%s_Dynamic_deep_GPU_report_grain_%d.txt",pars->benchName, pars->gpuChunk);
		deep_gpu_report.open(version, ios::out | ios::app);
#endif

		/*Seeting a mark to recognize a timestep*/
#ifdef PJTRACER
		tracer->newEvent();
#endif

#ifdef OVERHEAD_ANALYSIS
		end_tc = tick_count::now();
#endif
		/*Run the pipeline*/
		pipe.run(nCPUs + nGPUs);
		pipe.clear();
#ifdef DEEP_CPU_REPORT
		deep_cpu_report.close();
#endif
#ifdef DEEP_GPU_REPORT
		deep_gpu_report.close();
#endif
	}

	/*this function print info to a Log file*/
	void saveResultsForBench(){

		char * execution_name = (char *)malloc(sizeof(char)*100);
		sprintf(execution_name, "_Dynamic_%d_%d.txt", nCPUs, nGPUs);
		strcat(pars->benchName, execution_name);

		/*Checking if the file already exists*/
		bool fileExists = isFile(pars->benchName);
		ofstream file(pars->benchName, ios::out | ios::app);
		if(!fileExists){
			printHeaderToFile(file);
		}
		file << nCPUs << "\t" << nGPUs << "\t"  <<  runtime << "\t"
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2)
                << "\t" << chunkGPU << endl;
		file.close();
#ifdef DEBUG
		cerr << nCPUs << "\t" << nGPUs << "\t"  << runtime << "\t"
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
#endif
	}

//#define PRINTCPUSGPUS
	/*this function print info to a Log file*/
	void saveResultsForBenchDen(int inputId, int nrIter, int platformId, float runtime_k1, float energy_k1){

		char * execution_name = (char *)malloc(sizeof(char)*100);
		sprintf(execution_name, "_GD-TBB_P.txt");
		strcat(pars->benchName, execution_name);

		/*Checking if the file already exists*/
		bool fileExists = isFile(pars->benchName);
		ofstream file(pars->benchName, ios::out | ios::app);
		if(!fileExists){
			printHeaderToFileDen(file);
		}
		double eG, eC, eM, eT;
        eG = getPP1ConsumedJoules(sstate1, sstate2);
        eC = getPP0ConsumedJoules(sstate1, sstate2);
        eT = getConsumedJoules(sstate1, sstate2);
        eM = eT - eG - eC;
        //print implementation id and size

		//file << (int)DYNAMIC << "\t" << inputId << "\t" << platformId << "\t"
		file << (int)DYNAMIC << "\t" << inputId << "\t"
		#ifdef PRINTCPUSGPUS
      << pars->numcpus << "\t"
      << pars->numgpus << "\t"
		#endif
			 << platformId << "\t"
			 << chunkGPU << "\t"
			 << eG << "\t"
			 << eC << "\t"
			 << eM << "\t"
			 << eT << "\t"
			 << runtime << "\t"
			 << nrIter << "\t"
       << runtime_k1 << "\t"
			 << energy_k1  
			 << endl;
		file.close();

	  //cout << "\nDYNAMIC" << "\t" << inputId << "\t" << platformId << "\t"
		cout << "\nDYNAMIC" << "\t IN_SIZE: " << inputId << "\t PLATFORM_ID: " << platformId << "\t ratioGPU: "
			 << chunkGPU << "\t eT(Joule): "
			 << eT << "\t runtime(s): "
			 << runtime << "\tnrIter: "
			 << nrIter << "\tgpuRation: "
			 << (float)totalIterationsGPU/totalIterations << "\truntime_k1: " 
       		 << runtime_k1  << endl;
#ifdef DEBUG
		cerr << nCPUs << "\t" << nGPUs << "\t"  << runtime << "\t"
		<< getPP0ConsumedJoules(sstate1, sstate2) << "\t" << getPP1ConsumedJoules(sstate1, sstate2) << "\t"
		<< getConsumedJoules(sstate1, sstate2) - getPP0ConsumedJoules(sstate1, sstate2) - getPP1ConsumedJoules(sstate1, sstate2) << "\t" <<  getConsumedJoules(sstate1, sstate2) << "\t"
		<< getL2CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL2CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getL3CacheHits(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheMisses(sktstate1[0], sktstate2[0]) << "\t" << getL3CacheHitRatio(sktstate1[0], sktstate2[0]) <<"\t"
		<< getCyclesLostDueL3CacheMisses(sstate1, sstate2) << endl;
#endif
#ifdef OVERHEAD_ANALYSIS
				cout <<         "\n KernelEx(ms): " << kernel_execution
                        << "\t ThDispatch(ms): " << overhead_td
                        << "\t SchedPartitioning(ms): " << overhead_sp
                        << "\t H2D(ms): " << overhead_h2d
                        << "\t KernelLauch(ms): " << overhead_kl
                        << "\t D2H(ms): " << overhead_d2h
                        << "\t Total overhead(s): " << (kernel_execution+overhead_td+overhead_sp+overhead_h2d+overhead_kl+overhead_d2h)/1000
												<< endl;

#endif
    }

	void printHeaderToFile(ofstream &file){
		file << "N. CPUs" << SEP << "N. GPUs" << SEP << "Time (ms)" << SEP
		<< "CPU Energy(J)" << SEP << "GPU Enegy(J)" << SEP << "Uncore Energy(J)" << SEP << "Total Energy (J)" << SEP
		<< "L2 Cache Hits" << SEP << "L2 Cache Misses" << SEP << "L2 Cache Hit Ratio" << SEP
		<< "L3 Cache Hits" << SEP << "L3 Cache Misses" << SEP << "L3 Cache Hit Ratio" << SEP << "Cycles lost Due to L3 Misses" << SEP
                << "chunkGPU" << endl;
	}
	void printHeaderToFileDen(ofstream &file){
        file << "ALG_ID" << SEP << "INPUT_ID" << SEP << "PLATFORM_ID" << SEP
             << "GPU_RATIO" << SEP << "E_GPU(J)" << SEP << "E_CPU(J)" << SEP << "E_MEM(J)" << SEP << "E_TOT(J)" << SEP
             << "TIME(s)" << SEP
						 << "NR_ITER" << SEP
						<< "HET_FOR_TIME(s)" << SEP
						<< "HET_FOR_ENERGY(Joule)"
						<< endl;

	}
};
