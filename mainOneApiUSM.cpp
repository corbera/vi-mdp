/*******************************************************************************
--------------------------------------------------------------------------------
Before Compile:
0. source /opt/intel/inteloneapi/setvars.sh 
1.Go to Common.h and set the MDP_problem_size(0..12) in #define INPUT_ID MDP_problem_size
  We used in the paper the MDP_problem_size 2,4,6,8,10
2.Go to main.cpp and comment one of the following lines. Leave uncommented the
one corresponding to the scheduler that you want to use:
  #define DYNAMIC 5
  #define ORACLE 4
3. IF you change the platform, check output file name & set the proper TEST_PLATFORM ID
--------------------------------------------------------------------------------
Compile: go to the root fo the project (where main.cpp is) and run "cmake ."
--------------------------------------------------------------------------------
Execution:
1. For 0=SEQ1D,1=OMP,2=TBB,3=G-TBB implementations. No Scheduler.
sudo ./ValueIteration ImplemId(0-3,0=SEQ1D,1=OMP,2=TBB,3=G-TBB) NCpus(0-4)
  e.g.: sudo ./ValueIteration 1 3
2. For "#define ORACLE 4" implementations. ORACLE Scheduler.
sudo ./ValueIterationOracle NCpus(0-4) NGpus(0-1) RatioGPU(0.0-1.0)
  e.g.: sudo ./ValueIteration 0 3 0.5
3. For "#define DYNAMIC 5" implementations. DYNAMIC Scheduler.
sudo ./ValueIterationDynamic  NCpus(0-4) NGpus(0-1) ChunkGPU(>1)
  e.g.: sudo ./ValueIteration 0 3 2048
--------------------------------------------------------------------------------
*/


//#define __APPLE__
//#define LOCAL
//#define NDEBUG
//#define ENERGY_METER_ODROID
#define ENERGY_METER_INTEL
// #define HACK_TO_REMOVE_DUPLICATE_ERROR
//#define PCM_DEBUG_TOPOLOGY


//#define DEBUG
//#define DEEP_GPU_REPORT
//#define DEEP_CPU_REPORT
//#define OVERHEAD_ANALYSIS //OVERHEAD_STUDY


//#define SCHEDULER
// #define DYNAMIC 5
//#define ORACLE 4
//#define LOGFIT 6


#define FILE_INPUT //read T matrix arrays data from file or compute it (if not defined)
// #define TEST_PLATFORM 3

//#define VALIDATE_POLICY

//#include "src/RobotState.h"
//#include "src/TransitionMatrix.h"
#include "navigation_mdp/Common.h"
//#include "src/ValueIterationOMP.h"
//#include "src/ValueIteration.h"
//#include "src/ValueIterationUtils.h"
#include "src/Norm2.h"



// #include "src/ValueIterationSeq1D.h"
// #include "src/valueIterationOMP1D.h"
#include "src/ValueIterationOneApiUSM.h"

#include <iostream>
#include <chrono>
#include <fstream>
#include <stdio.h>


#include "utils/timing.h"
#include <cstring>
#include <unistd.h>
//#include "energy-meter/energy_meter.h"
#include <unistd.h>
#include <stdlib.h>
// #include <omp.h>
#include <string>
#include <sstream>
// #include "src/ValueIterationTBB.h"
// #include "src/ValueIterationTBBClass.h"

#include <tbb/task_scheduler_init.h>
#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>

#ifdef SCHEDULER
#include "src/ValueIterationPlanifier.h"
#endif





#include <cstdlib>

#include "CL/sycl.hpp"
#include <iostream>


using namespace tbb;
using namespace std;
using namespace cl::sycl;





float comparePolicies(const cl_int *P, const cl_int *Pxxx, uint ns, bool verbose);




#define MEASURE_K1

#ifdef MEASURE_K1
#include "cpucounters.h"
#include "utils.h"
#include "tbb/tick_count.h"
#endif

class PolicyEvaluationFunctor {
    private:
        ulong offset; 
        cl_ulong* nextCellPos;
        cl_float* probability;
        cl_float* V;
        cl_ulong* nextStatePos;
        cl_float* R;
        cl_float* Q;
    
    public:
    PolicyEvaluationFunctor(ulong offset_,
                            cl_ulong* nextCellPos_,
                            cl_float* probability_,
                            cl_float* V_,
                            cl_ulong* nextStatePos_,
                            cl_float* R_,
                            cl_float* Q_
    ):  offset(offset_), nextCellPos(nextCellPos_), 
        probability(probability_), V(V_),
        nextStatePos(nextStatePos_), R(R_), Q(Q_) {}

    void operator()(cl::sycl::id<1> index) {
        
        float w = 0.0;
        float gamma = 0.1;
        ulong /*s,*/ s2, myCellPos;

        myCellPos = index.get(0) + offset;
        // evaluate the value of the actions for the current state
        // for all non zero probability nextState for <s,a> tuple (i.e. all elements from one cell of T) 
        for (s2 = nextCellPos[myCellPos]; s2 < nextCellPos[myCellPos + 1]; s2++) {
            w += probability[s2] * V[nextStatePos[s2]];
        }
        Q[myCellPos] =  R[myCellPos] + gamma * w ; //w * gamma + R[myCellPos];
    
    }
};

cl_ulong  NS, NR, NG, ND, NA, NX, NT;


int main(cl_int argc, char **argv) {

    int inputId = 6;
    float offload_ratio = 0.5;

    if (argc == 3) {
        inputId = atof(argv[1]);
        offload_ratio = atof(argv[2]);
        
    }

    uint nt = tD[inputId];    // number of theta discrete values
    uint nr = 3;    // number of rangefinders
    uint ng = gD[inputId];    // number of discrete values for a rangefinder reading
    uint nd = dD[inputId];    // number of discrete values for the distance from the robot to the target
    uint na = aD[inputId];    // number of discrete values for the angle between robot orientation and target
    uint nx = 6;    // number of actions
 

    ulong  ns = nt * ng * ng * ng * nd * na; 

    
    queue q_cpu(cpu_selector{});
    queue q(gpu_selector{});
    auto ctx = q.get_context();


    // See what device was actually selected for this queue.
    std::cout << "\n\n q running on " << q.get_device().get_info<cl::sycl::info::device::name>() << "\n";
    std::cout << "\n q_cpu running on " << q_cpu.get_device().get_info<cl::sycl::info::device::name>() << "\n\n\n";

    // Using these events to time command group execution
    cl::sycl::event e1, e2, e3;

    cl_ulong probabilitySize = 0;

    cl_float*   probability = NULL;
    cl_ulong*   nextCellPos = NULL;
    cl_ulong*   nextStatePos = NULL;
    cl_float*   R      = NULL;
    cl_float*   Q      = NULL;
    cl_float*   V      = NULL;
    cl_int*     P      = NULL;
    cl_float*   Z      = NULL;

 

    cout << "Hallo mikomi :) !" << std::endl;
    //setbuf(stdout, NULL); //stop buffering, for right order in printf/fprintf, etc
    //int inputId[4] = {40445, 61012, 84565, 143771};

    // R      = (cl_float*)malloc_shared(NS*NX * sizeof(cl_float), q.get_device(), ctx);
    // Q      = (cl_float*)malloc_shared(NS*NX * sizeof(cl_float), q.get_device(), ctx);
    // V      = (cl_float*)malloc_shared(NS * sizeof(cl_float), q.get_device(), ctx);
    // P      = (cl_int*)malloc_shared(NS * sizeof(cl_int), q.get_device(), ctx);
    // Z      = (cl_float*)malloc_shared(NS * sizeof(cl_float), q.get_device(), ctx);

    R  = (cl_float*)malloc_host(ns * nx * sizeof(cl_float),  ctx);
    Q  = (cl_float*)malloc_shared(ns * nx * sizeof(cl_float), q.get_device(), ctx);
    V  = (cl_float*)malloc_host(ns * sizeof(cl_float),  ctx);
    P  = (cl_int*)malloc_host(ns * sizeof(cl_int),  ctx);
    Z  = (cl_float*)malloc_host(ns * sizeof(cl_float), ctx);


    for (ulong i = 0; i < ns; i++) {
        V[i] = 0;
        P[i] = 0;
        Z[i] = 0;
    }

    printf("Algorithm input id : %d\n", inputId);

    probabilitySize = pNZ[inputId];

    cout << "probabilitySize: " << probabilitySize << std::endl;

    stringstream ss;
    stringstream fnss;
    ss  << "PS" << probabilitySize
        << "_" << "S" << ns
        << "_" << "T" << nt
        << "_" << "R" << nr
        << "_" << "G" << ng
        << "_" << "D" << nd
        << "_" << "A" << na;

    string inFileDim = ss.str();
    //cout << inFileDim;

    fnss << "./data/in/" << inFileDim << ".txt";
    string fns = fnss.str();
    char* inputFileName  = (char*)fns.c_str();

    // ReadInput(inputFileName);
    FILE *f;

    f = fopen(inputFileName, "r+t");
    if (f == NULL) {
        fprintf(stderr, "file not found: %s\n", inputFileName);
        exit(-1);
    }

    fscanf(f, "%lu", &probabilitySize);
    fscanf(f, "%d", &ns);
    fscanf(f, "%d", &nx);
    fscanf(f, "%d", &nt);
    fscanf(f, "%d", &nr);
    fscanf(f, "%d", &ng);
    fscanf(f, "%d", &nd);
    fscanf(f, "%d", &na);


    // probability = (cl_float *) malloc_shared(sizeof(cl_float) * probabilitySize, q.get_device(), ctx);
    // nextCellPos = (cl_ulong *) malloc_shared(sizeof(cl_ulong) * (NS * NX + 1), q.get_device(), ctx);
    // nextStatePos = (cl_ulong *) malloc_shared(sizeof(cl_ulong) * probabilitySize, q.get_device(), ctx);
    // R = (cl_float *) malloc_shared(sizeof(cl_float) * NS * NX, q.get_device(), ctx);

    probability  = (cl_float *) malloc_host(sizeof(cl_float) * probabilitySize, ctx);
    nextCellPos  = (cl_ulong *) malloc_host(sizeof(cl_ulong) * (ns * nx + 1), ctx);
    nextStatePos = (cl_ulong *) malloc_host(sizeof(cl_ulong) * probabilitySize, ctx);
//    R = (cl_float *) malloc_host(sizeof(cl_float) * NS * NX,  ctx);


    unsigned int i;
    //probability
    for (i = 0; i < probabilitySize; i++) {
        fscanf(f, "%fE", &probability[i]);
    }
    //nextCellPos
    for (i = 0; i < ns * nx + 1; i++) {
        fscanf(f, "%lu", &nextCellPos[i]);
    }
    //nextStatePos
    for (i = 0; i < probabilitySize; i++) {
        fscanf(f, "%lu", &nextStatePos[i]);
    }
    //R
    for (i = 0; i < ns * nx; i++) {
        fscanf(f, "%f", &R[i]);
    }



    //Test OneAPI

    double runtime_k1, energy_k1;
    timestamp_type time1, time2;

    auto begin = std::chrono::high_resolution_clock::now();
    get_timestamp(&time1);


    bool notReady = true;
    double norm = 0.0;
    int nrIter = 0;

    uint nrStates = NS;
    uint nrActions = NA;
    float eps = EPSILON;

    auto policy_improve = [&](const blocked_range<size_t>& r)
    {
        for (cl_ulong s = r.begin(); s != r.end(); ++s) {
            for (int a = 0; a < nx; a++) {
                size_t idx = s * nx + a;
                if (Q[idx] - Z[s] > EPSILON) {
                    Z[s] = Q[idx];
                    P[s] = a;
                    //printf("h_Z[%d] = %f \n", s, h_Z[s]);
                }
            }
        }
    };





    try {
        // Create a command queue using the device selector above, and request profiling

        #ifdef MEASURE_K1
        float rt = 0.0;
        tick_count bt, et;
        //Energy Counters
        PCM * pcm;
        SystemCounterState sstate1, sstate2;
        //timing
        tick_count start, end;
        //Initializing PCM library
        pcm = PCM::getInstance();
        pcm->resetPMU();
        if (pcm->program() != PCM::Success){
            std:cout << "\nError in PCM library initialization" << std::endl;
            exit(-1);
        }
        #endif //MEASURE_K1

        while(notReady) {

            #ifdef MEASURE_K1
                
                bt = tick_count::now();
                sstate1 = pcm->getSystemCounterState();
            #endif MEASURE_K1
            //code to be measured - K1
            
            /** Policy evaluation on device*/
            // Submit  command group to gpu device. The kernel is represented as a functor
            
        // {
            unsigned long offsetGpu = 0;
            unsigned long elementsGpu = ceil( ns* nx * offload_ratio);
            e1 = q.submit([&](cl::sycl::handler& cgh) {
    
                // // execute the kernel
                PolicyEvaluationFunctor kernel( offsetGpu,
                                                nextCellPos,
                                                probability,
                                                V,
                                                nextStatePos,
                                                R,
                                                Q );
                //** functor kernel */
                cgh.parallel_for(cl::sycl::range<1>(elementsGpu), kernel);

            });
        // }
         

          //code to be measured - K1

          // print1DimArray(Q, NS, 20);
            //std::cout << " waiting for kernerl" << "\n";
// {
            unsigned long offsetCpu = elementsGpu;
            unsigned long elementsCpu = ns * nx - elementsGpu;
            e2 = q_cpu.submit([&](cl::sycl::handler& cgh) {
                // execute the kernel
                PolicyEvaluationFunctor kernel( offsetCpu,
                                                nextCellPos,
                                                probability,
                                                V,
                                                nextStatePos,
                                                R,
                                                Q );
                //** functor kernel */
                cgh.parallel_for(cl::sycl::range<1>(elementsCpu), kernel);
            });
// }         
            q.wait();
            q_cpu.wait();

            #ifdef MEASURE_K1
            et = tick_count::now();
            sstate2 = pcm->getSystemCounterState();
            runtime_k1 += (et-bt).seconds();
            energy_k1 += getConsumedJoules(sstate1, sstate2);
            //cout << "runtime & total_energy K1:" << runtime_k1 << "\t" << energy_k1 << endl;
            #endif MEASURE_K1

            //std::cout << " done waiting for kernerl" << "\n";
            
            /** Policy improvement */

            parallel_for( blocked_range<size_t>(0,ns /*, grainSize*/), policy_improve);
            //select the policy with the best value (max_a Q(s,a)) for every state
        
// {
            
//             e3 = q_cpu.submit([&](cl::sycl::handler& cgh) {

//                 // execute the kernel
//                 cgh.parallel_for<class kernel2>(range<1>(nrStates), [&](id<1> index) {
//                     cl_ulong  s = index.get(0);
//                     for (int a = 0; a < nrActions; a++) {
//                         size_t idx = s * nrActions + a;
//                         if (Q[idx] - Z[s] > eps) {
//                             Z[s] = Q[idx];
//                             P[s] = a;                            
//                         }
//                     }

//                 });
//             }); 
// // }
//             q_cpu.wait();

            //print1DimArray(Z, NS, 20);

            //std::cout << " done with policy improvement" << "\n";


            // //Norm2 - functor class
            // Norm2 n2sum(Z,V); 
            // norm = sqrt(n2sum.my_sum);
            // parallel_reduce( blocked_range<size_t>(0,NS,1), n2sum);

            //Norm2 - lambda
            norm = parallel_reduce(blocked_range<size_t>(0,ns), 0.0f,
                                [&](blocked_range<size_t> r, double total_norm)
                                {
                                    for (size_t i=r.begin(); i<r.end(); ++i)
                                    {
                                        total_norm += pow((Z[i]-V[i]),2);
                                    }

                                    return total_norm;
                                }, std::plus<double>() );

            norm = sqrt(norm);
            // std::cout <<"\nnorm: " << norm << std::endl;


            notReady = (norm > g) && nrIter < MAX_ITER;
            norm = 0.0;
            //swap old V with new V (Z)
            // for (int i=0; i<NS; i++)
            //     V[i] = Z[i]; 
            memcpy(V, Z, ns*sizeof(cl_float));
    
            nrIter++; // #iters to convergence

            //std::cout << "\nValtItOCL #iter: " << nrIter << "\n";
        

        } // while
    }
    catch (cl::sycl::exception e) {
        std::cout << "SYCL exception caught: " << e.what() << std::endl;
        return 1;
    }


    get_timestamp(&time2);
    double elapsed = timestamp_diff_in_seconds(time1,time2);

    cout << "\n\nThe OneAPI implem converged in " << nrIter << " iterations. Total ExecTime = " << elapsed << "\nPolicy values sample: ";
    cout << "\n\nExecTime K1: " << runtime_k1 << " Energy K1 " << energy_k1 << "\n ";
   // print1DimArray(V, ns, 20);
   
    char outputFileName[30];//  = "OneApiUSM.txt";

    if (((int)TEST_PLATFORM == 2))  sprintf(outputFileName, "./data/evaluationO3/Broadwell1_OneApiUSM");
    if (((int)TEST_PLATFORM == 3)) sprintf(outputFileName, "./data/evaluationO3/Envy_OneApiUSM");
    if (((int)TEST_PLATFORM == 4)) sprintf(outputFileName, "./data/evaluationO3/Cafe_OneApiUSM");

    // ReadInput(inputFileName);
    FILE *f_out;

    f_out = fopen(outputFileName, "a");
    if (f_out == NULL) {
        fprintf(stderr, "file not created: %s\n", outputFileName);
        exit(-1);
    }
//    fprintf(f_out, "IN: %d \t GPU_Ratio: %f\t Het_kernel_time: %f \t Het_kernel_energy: %f \t Total_exec_time: %f \t Nr_iter: %d\n",
     fprintf(f_out, "%d\t%f\t%f\t%f\t%f\t%d\n",
                    inputId,
                    offload_ratio,
                    runtime_k1,
                    energy_k1,
                    elapsed,
                    nrIter
                    );




/*
    //Test TBB runtime...
    task_scheduler_init init(task_scheduler_init::automatic);
    //task_scheduler_init init(4);
    timestamp_type time1, time2;
    auto begin = std::chrono::high_resolution_clock::now();
    int nrIt =  ValueIterationTBB1D(h_probability, h_nextStatePos, h_nextCellPos, h_R, h_Q, h_V, h_P, gamma_);
    get_timestamp(&time2);
    double elapsed = timestamp_diff_in_seconds(time1,time2);
    cout << "\nThe implem converged in " << nrIt << " iterations. ExecTime = " << elapsed << "\n";
*/
    //Test OpenCL
   /* omp_set_num_threads(8);
    prepareOclEnvironment();
    timestamp_type time1, time2;
    auto begin = std::chrono::high_resolution_clock::now();
    get_timestamp(&time1);
    int nrIt = ValueIterationOCL() ;
    get_timestamp(&time2);
    double elapsed = timestamp_diff_in_seconds(time1,time2);
    cout << "\nThe implem converged in " << nrIt << " iterations. ExecTime = " << elapsed << "\n";
    */

    /*Test OpenMP
    omp_set_num_threads(4);
    valueIterationOMP1D(h_probability, h_nextStatePos, h_nextCellPos, h_R, h_Q, h_V, h_P, gamma_, d);
    */

    /** Evaluate the execution time for sequential vs OpenMP on Value iteration algorithm*/


    free(R, ctx);
    free(Q, ctx);
    free(V, ctx);
    free(P, ctx);
    // free(Pseq);
    free(Z, ctx);

    //released in T_matrix...
    free(probability, ctx);
    free(nextCellPos, ctx);
    free(nextStatePos, ctx);

    return 0;
}




bool compareQValues( cl_float  Q[][NX],  cl_float Qxxx[][NX], bool verbose) { //todo:1d array Q, Qxxx
    bool sameQ = true;
    for (cl_ulong s = 0; s < NS ; ++s) {
        for (cl_int a = 0; a < NA; ++a) {
            if(Q[s][a] != Qxxx[s][a]) {
                sameQ = false;
                if (verbose)
                    cout << "Qseq[" << s << "," << a << "]=" << Q[s][a] << "; " << "Qxxx[" << s << "," << a << "]=" << Qxxx[s][a]  << "\n";
                break;
            }
        }
    }
    return sameQ;
}

//returns the percentage of similar policies P and Pxxx
float comparePolicies(const cl_int *P, const cl_int *Pxxx, uint ns, bool verbose) {
    float sameP = ns;
    for (cl_int p = 0; p < ns ; ++p) {
        if(P[p] != Pxxx[p]) {
            sameP--;
            if (verbose)
                cout << "Pseq[" << p << "]=" << P[p] << "; " << "Pxxx[" << p << "]=" << Pxxx[p] << "\n";
            //break;
        }
    }
    return sameP/ns;
}