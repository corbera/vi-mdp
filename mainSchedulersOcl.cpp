/*******************************************************************************
--------------------------------------------------------------------------------
Before Compile:
1.Go to Common.h and set the MDP_problem_size(0..12) in #define INPUT_ID MDP_problem_size
  We used in the paper the MDP_problem_size 2,4,6,8,10
2.Go to main.cpp and comment one of the following lines. Leave uncommented the
one corresponding to the scheduler that you want to use:
  #define DYNAMIC 5
  #define ORACLE 4
3. IF you change the platform, check output file name & set the proper TEST_PLATFORM ID
--------------------------------------------------------------------------------
Compile: go to the root fo the project (where main.cpp is) and run "cmake ."
--------------------------------------------------------------------------------
Execution:
1. For 0=SEQ1D, 1=OMP, 2=TBB, 3=G-TBB implementations. No Scheduler.
sudo ./ValueIteration inputId(1-11) ImplemId(0-3,0=SEQ1D,1=OMP,2=TBB,3=G-TBB) NCpus(0-4)
  e.g.: sudo ./ValueIteration 11 2 4

2. For "#define ORACLE 4" implementations. ORACLE Scheduler.
sudo ./ValueIterationOracle inputId(1-11) NCpus(0-4) NGpus(0-1) RatioGPU(0.0-1.0)
  e.g.: sudo ./ValueIteration 10 4 1 0.5

3. For "#define DYNAMIC 5" implementations. DYNAMIC Scheduler.
sudo ./ValueIterationDynamic inputId(1-11) NCpus(0-4) NGpus(0-1) ChunkGPU(>1)
  e.g.: sudo ./ValueIteration 10 3 1 2048
--------------------------------------------------------------------------------
*/
#include <cstdlib>


//#define __APPLE__
//#define LOCAL
//#define NDEBUG
//#define ENERGY_METER_ODROID
#define ENERGY_METER_INTEL
// #define HACK_TO_REMOVE_DUPLICATE_ERROR
//#define PCM_DEBUG_TOPOLOGY


//#define DEBUG
#define DEEP_GPU_REPORT
//#define DEEP_CPU_REPORT
//#define OVERHEAD_ANALYSIS //OVERHEAD_STUDY


#define SCHEDULER
// #define DYNAMIC 5
//#define ORACLE 4
//#define LOGFIT 6


//#define FILE_INPUT //read T matrix arrays data from file or compute it (if not defined)
#define TEST_PLATFORM 3

//#define VALIDATE_POLICY

//#include "src/RobotState.h"
//#include "src/TransitionMatrix.h"
#include "navigation_mdp/Common.h"
//#include "src/ValueIterationOMP1D.h"
//#include "src/ValueIteration.h"
//#include "src/ValueIterationUtils.h"
#include "src/Norm2.h"




#include "src/ValueIterationSeq1D.h"
#include "src/valueIterationOMP1D.h"
#include "src/ValueIterationOneApi.h"

#include <iostream>
#include <chrono>
#include <fstream>
#include <stdio.h>


#include "utils/timing.h"
#include <cstring>
#include <unistd.h>
//#include "energy-meter/energy_meter.h"
#include <unistd.h>
#include <stdlib.h>
#include <omp.h>
#include <string>
#include <sstream>
#include "src/ValueIterationTBB.h"
// #include "src/ValueIterationTBBClass.h"

#include <tbb/task_scheduler_init.h>
#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>


#include "src/ValueIterationPlanifier.h"


#ifdef ENERGY_METER_INTEL
#include "cpucounters.h"
#include "utils.h"
#include "tbb/tick_count.h"
#endif

#include <CL/cl2.hpp>

using namespace tbb;
using namespace std;


// Device buffers
cl_mem d_probabilities = NULL;
cl_mem d_nextCellPos = NULL;
cl_mem d_nextStatePos = NULL;
cl_mem d_R = NULL;
cl_mem d_Q = NULL;
cl_mem d_P = NULL;
cl_mem d_V = NULL;
cl_mem d_Z = NULL;
cl_ulong probabilitySize = 0;

cl_ulong  NS, NR, NG, ND, NA, NX, NT;

cl_float*   h_probability = NULL;
cl_ulong*   h_nextCellPos = NULL;
cl_ulong*   h_nextStatePos = NULL;
cl_float*   h_R      = NULL;
cl_float*   h_Q     = NULL;
cl_float*   h_V     = NULL;
cl_int*     h_P     = NULL;
cl_int*     h_Pseq   = NULL;
cl_float*   h_Z      = NULL;

int inputId;

//!NEEDED FOR SCHEDULER.H USE
extern cl_context context;
extern cl_command_queue command_queue;
extern cl_kernel kernel;

/** Value iteration algorithm - time measurement output files */
const static string ExecTimeEnergyLogName =         "./data/EnergyTimeMeasurements20MS100x1000E.txt";
const static string IntelExecTimeEnergyLogName =    "./data/IntelEnergyTimeMeasurements100x10E_TBB.txt";

const static string TPxTimeEnergyLogName =          "./data/evaluation/Envy-TBB-TE-K1.txt";
const static string OdroidExecTimeEnergyLogName =   "./data/OdroidEnergyTimeMeasurements100E.txt";
const static string execTimeLogName =               "./data/ExecTimeTest.txt";

string inFileDim;


//enum iBroadwell{SEQ, SEQ1D, OMP4C, OCL4C, TBB4C};
string implName[5] = {"SEQ", "SEQ1D",  "OMP1D", "OMP1D_b.L", "OCL"};



float comparePolicies(const cl_int *P, const cl_int *Pxxx, bool verbose);

//bool compareQValues( cl_float  Q[][NX],  cl_float Qxxx[][NX], bool verbose);
/**
 *
 * @param algorithm 0 - seq, 2 - seq1D, 3 - omp, 4 - ... TODO: tbc with all implementations
 * @param nrExecTimes - times to execute the algorithm for the mean exec. time computation
 * @param transitionMatrix
 * @param execTimeFile - output file name (contains nrExecTimes with the time that it took to execute the algorithm)
 * @param avgExecTime
 */
void measureExecutionTimeAlg(cl_int algorithm, cl_int nrExecTimes, TransitionMatrix &transitionMatrix,
                             string execTimeFile, cl_float *Q, cl_float *V, cl_int *P);
/** ODROID PLATFORM
 *
 * @param algorithm
 * @param nrExecTimes
 * @param transitionMatrix
 * @param energyMeterFileName
 * @param avgExecTime
 * @param Q
 * @param V
 * @param P
 */

void measureEnergyUseAlg(cl_int algorithm,
                         string energyMeterFileName,
                         cl_float *Q, cl_float *V, cl_int *P);
/** INTEL PLATFORM
 *
 * @param algorithm
 * @param nrExecTimes
 * @param transitionMatrix
 * @param energyMeterFileName
 * @param avgExecTime
 * @param Q
 * @param V
 * @param P
 */

void measureIntelEnergyUseAlg(string energyMeterFileName, cl_float *h_Q, cl_float *h_V, cl_int *h_P,
                              int platform, int algorithm, int numThr);

#ifdef ENERGY_METER_ODROID
/** ODROID PLATFORM
 *
 * @param nrExecTimes
 * @param transitionMatrix
 * @param energyMeterFileName
 * @param avgExecTime
 * @param Q
 * @param V
 * @param P
 */
void measureOdroidEnergyUseAlg( string energyMeterFileName,
                                cl_float *h_Q, cl_float *h_V, cl_int *h_P);
#endif


void measureAllImplemExecutionTimeAlg(int nrExecTimes, TransitionMatrix &transitionMatrix,
                                      string execTimeFileName,
                                      float *Q, float *V, int *P, float *R);

void dumpDataToFiles(cl_int *P, cl_float *V, cl_float* Q);

/** Read discretization ., probability, nextCellPos, nextStatePos and R arrays*/
void ReadInput(int inputId);

/** Write discretization params., probability, nextCellPos, nextStatePos and R arrays*/
void WriteInput(char *filename);

/** Freee allocated mem*/
void releaseMemory();



int main(cl_int argc, char **argv) {

    if (argc < 1) {
        cout << "Indicate the input id as the first argument! \n";
        return -1;
    }

    inputId = atof(argv[1]);

    ReadInput(inputId);

    cout << "NS: " << NS << endl;
    cout << "NX: " << NX << endl;

#ifdef SCHEDULER
    /** Planifier */
    ValueIterationPlanifier vip;
    Params p;

    /*Checking command line parameters*/
    #if defined(ORACLE) || defined(DYNAMIC)
    if (argc < 5) {
        cerr << "Args: numcpus numgpus [ratio | gpuCHUNK]" << endl;
        exit(-1);
    }
    #elif defined(LOGFIT)
    if (argc < 4) {
        cerr << "Args: numcpus numgpus" << endl;
        exit(-1);
    }
    #endif
    p.numcpus = atoi(argv[2]);
    p.numgpus = atoi(argv[3]);
    #ifdef ORACLE
    p.ratioG = atof(argv[4]);
    cout << "Oracle Scheduler" << endl;
    #endif
    #ifdef DYNAMIC
    p.gpuChunk = atoi(argv[4]);
    cout << "Dynamic Scheduler" << endl;
    #endif
    #ifdef LOGFIT
    cout << "LogFit Scheduler" << endl;
    #endif
    sprintf(p.benchName, "./data/evaluationO3/Envy_K1_chunk_");
    sprintf(p.kernelName, "valueIterationKernel");
    //sprintf(p.kernelName, "policyEvaluation");
    cerr << "\nStarting with:  NCPUs: " << p.numcpus << " NGPUS: " << p.numgpus << endl;

    /*Initializing scheduler*/
    #ifdef ORACLE
    Oracle * hs = Oracle::getInstance(&p);
    #endif
    #ifdef DYNAMIC
    Dynamic * hs = Dynamic::getInstance(&p);
    #endif
    #ifdef LOGFIT
    LogFit * hs = LogFit::getInstance(&p);
    #endif

    createCommandQueue();
    CreateAndCompileProgram("valueIterationKernel");

    cout << "\nKernel compiled!";

    vip.AllocateMemoryObjects();

    cout << "\nGPU memory allocated!";

    // policy improvement
    auto f = [&](const blocked_range<size_t>& r)
         {
             for (cl_ulong s = r.begin(); s != r.end(); ++s) {
                 size_t idx = s * NX;
                 for (int a = 0; a < NX; a++) {
                     if (h_Q[idx] - h_Z[s] > EPSILON) {
                         h_Z[s] = h_Q[idx];
                         h_P[s] = a;
                         //printf("h_Z[%d] = %f \n", s, h_Z[s]);
                     }
                     idx++;
                 }
             }
         };


    bool notReady = true;
    cl_ulong nrIter = 0;
    float norm = 0;
   // Norm2 n2sum(h_Z,h_V); 
    //task_scheduler_init init(tbb::task_scheduler_init::automatic);  //corbera: initialized in scheduler

    // cout << "Probability: ";
    // print1DimArray(h_probability, 10);

    float runtime_k1  = 0.0;
    float energy_k1 = 0.0;
    //Energy Counters
    PCM * pcm_k1;
    SystemCounterState sstate1_k1, sstate2_k1;
    //timing
    tick_count start, end;
    //Initializing PCM library
    pcm_k1 = PCM::getInstance();
    pcm_k1->resetPMU();
    if (pcm_k1->program() != PCM::Success){
        cerr << "Error in PCM library initialization" << endl;
        exit(-1);
    }
    hs->startTimeAndEnergy();
    while (notReady) {
        tick_count bt, et;
        bt = tick_count::now();
        sstate1_k1 = pcm_k1->getSystemCounterState();
        // code to be measured - K1
        hs->heterogeneous_parallel_for(0, NS*NX, &vip); //begin = 0, end = NS * NX, ..
        et = tick_count::now();
        runtime_k1 += (et-bt).seconds();
        sstate2_k1 = pcm_k1->getSystemCounterState();
        energy_k1 += getConsumedJoules(sstate1_k1, sstate2_k1);
        //cout << "runtime & total_energy K1:" << runtime_k1 << "\t" << energy_k1 << endl;
       
        //select the policy with the best value (max_a Q(s,a)) for every state
        parallel_for( blocked_range<size_t>(0,NS), f);
        //Norm2
        //n2sum.my_sum = 0; //corbera: to initialize n2sum.my_sum every iteration
        //parallel_reduce( blocked_range<size_t>(0,NS), n2sum);

        //serial reduction for testing
        /*
        float auxsum = 0.0;
        for( size_t i=0; i!=NS; ++i )  {
            auxsum += pow((h_Z[i]-h_V[i]),2); 
        }
        cout << endl << "parallel my_sum: " << n2sum.my_sum << " serial my_sum: " << auxsum << endl;
        */
        //norm = sqrt(n2sum.my_sum);
        norm = parallel_reduce(blocked_range<size_t>(0,NS), 0.0f,
                               [&](blocked_range<size_t> r, float total_norm)
                               {
                                   for (size_t i=r.begin(); i<r.end(); ++i)
                                   {
                                       total_norm += pow((h_Z[i]-h_V[i]),2);
                                   }

                                   return total_norm;
                               }, std::plus<float>() );
        norm = sqrt(norm);
        notReady = norm > g && nrIter < MAX_ITER;
        //print1DimArray(h_Z, NS, 20);
        //cout << endl << "n2sum.my_sum: " << n2sum.my_sum << " norm: " << norm << " notReady: " << notReady << endl;
        //exit(-1);
        //swap old V with new V (Z)
        // cl_float *tmp = h_Z;
        // h_Z = h_V;
        // h_V = tmp;
        memcpy(h_V, h_Z, NS*sizeof(cl_float));

        nrIter++;
        // cout << "ITER: " << nrIter << ". Norm: " << norm << endl;
        //printf("\nIter: %d, Norm: %f ", nrIter, norm);
    } // end of time step
    hs->endTimeAndEnergy();
    hs->saveResultsForBenchDen(inputId, nrIter, (int)TEST_PLATFORM, runtime_k1, energy_k1);
    //hs->saveResultsForBench();
    //init.terminate(); corbera

    cout << "runtime & total_energy K1 (heter_for):" << runtime_k1 << "\t" << energy_k1 << endl;


    #ifdef VALIDATE_POLICY
    // clear the last policy and values
    for (cl_int s = 0; s < NS; ++s) {
        //h_P[s] = 0;
        h_V[s] = ::std::numeric_limits<cl_float>::min();
        h_Z[s] = ::std::numeric_limits<cl_float>::min();
    }
    int nrIterSeq = valueIterationSeq1D(h_probability, h_nextStatePos, h_nextCellPos, h_R, h_Q, h_V, h_Pseq, gamma_, d);
    float similarity = comparePolicies(h_Pseq, h_P, false);
    cout << "Heter #iter: " << nrIter << ". Seq #iter: " << nrIterSeq << ". Norm: " << norm <<  ". P similarity: " << similarity << endl ;
    #endif

#endif //SCHEDULER


    releaseMemory();
    return 0;
}

/** Freee allocated mem*/
void releaseMemory() {
    free(h_R);
    free(h_Q);
    free(h_V);
    free(h_P);
    free(h_Pseq);
    free(h_Z);

    //released in T_matrix...
    free(h_probability);
    free(h_nextCellPos);
    free(h_nextStatePos);
}


#ifdef ENERGY_METER_ODROID

#endif





//returns the percentage of similar policies P and Pxxx
float comparePolicies(const cl_int *P, const cl_int *Pxxx, bool verbose) {
    float sameP = NS;
    for (cl_int p = 0; p < NS ; ++p) {
        if(P[p] != Pxxx[p]) {
            sameP--;
            if (verbose)
                cout << "Pseq[" << p << "]=" << P[p] << "; " << "Pxxx[" << p << "]=" << Pxxx[p] << "\n";
            //break;
        }
    }
    return sameP/NS;
}



void ReadInput(int inputId) {

    uint nt = tD[inputId];    // number of theta discrete values
    uint nr = 3;    // number of rangefinders
    uint ng = gD[inputId];    // number of discrete values for a rangefinder reading
    uint nd = dD[inputId];    // number of discrete values for the distance from the robot to the target
    uint na = aD[inputId];    // number of discrete values for the angle between robot orientation and target
    uint nx = 6;

    uint ns = nt * ng * ng * ng * nd * na;


    // Using these events to time command group execution

    probabilitySize = 0;

    //setbuf(stdout, NULL); //stop buffering, for right order in printf/fprintf, etc
//int inputId[4] = {40445, 61012, 84565, 143771};


    printf("Algorithm input id : %d\n", inputId);

    probabilitySize = pNZ[inputId];

    cout << "probabilitySize: " << probabilitySize << std::endl;

    stringstream ss;
    stringstream fnss;
    ss  << "PS" << probabilitySize
        << "_" << "S" << ns
        << "_" << "T" << nt
        << "_" << "R" << nr
        << "_" << "G" << ng
        << "_" << "D" << nd
        << "_" << "A" << na;

    string inFileDim = ss.str();
    //cout << inFileDim;

    fnss << "./data/in/" << inFileDim << ".txt";
    string fns = fnss.str();
    char* inputFileName  = (char*)fns.c_str();

    // ReadInput(inputFileName);
    FILE *f;

    f = fopen(inputFileName, "r+t");
    if (f == NULL) {
        fprintf(stderr, "file not found: %s\n", inputFileName);
        exit(-1);
    }

//
//    fscanf(f, "%lu", &probabilitySize);
//    fscanf(f, "%d", &ns);
//    fscanf(f, "%d", &nx);
//    fscanf(f, "%d", &nt);
//    fscanf(f, "%d", &nr);
//    fscanf(f, "%d", &ng);
//    fscanf(f, "%d", &nd);
//    fscanf(f, "%d", &na);

    fscanf(f, "%lu", &probabilitySize);
    fscanf(f, "%d", &NS);
    fscanf(f, "%d", &NX);
    fscanf(f, "%d", &NT);
    fscanf(f, "%d", &NR);
    fscanf(f, "%d", &NG);
    fscanf(f, "%d", &ND);
    fscanf(f, "%d", &NA);


    if (h_probability == NULL) {
        h_probability = (cl_float *) malloc(sizeof(cl_float) * probabilitySize);
    }

    if (h_nextCellPos == NULL) {
        h_nextCellPos = (cl_ulong *) malloc(sizeof(cl_ulong) * (NS * NX + 1));
    }

    if (h_nextStatePos == NULL) {
        h_nextStatePos = (cl_ulong *) malloc(sizeof(cl_ulong) * probabilitySize);
    }

    if (h_R == NULL) {
        h_R = (cl_float*)calloc(NS*NX,sizeof(cl_float));
    }

    h_Q      = (cl_float*)calloc(NS*NX,sizeof(cl_float));
    h_V      = (cl_float*)calloc(NS,sizeof(cl_float));
    h_P      = (cl_int*)calloc(NS, sizeof(cl_int));
    h_Pseq      = (cl_int*)calloc(NS, sizeof(cl_int));
    h_Z      = (cl_float*)calloc(NS,sizeof(cl_float));

    unsigned int i;
    //probability
    for (i = 0; i < probabilitySize; i++) {
        fscanf(f, "%fE", &h_probability[i]);
    }
    //nextCellPos
    for (i = 0; i < NS * NX + 1; i++) {
        fscanf(f, "%lu", &h_nextCellPos[i]);
    }
    //nextStatePos
    for (i = 0; i < probabilitySize; i++) {
        fscanf(f, "%lu", &h_nextStatePos[i]);
    }
    //R
    for (i = 0; i < NS * NX; i++) {
        fscanf(f, "%f", &h_R[i]);
    }
    fclose(f);
}
