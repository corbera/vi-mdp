#!/usr/bin/python

import os
import time

niter = 1
# inputs = [2, 3, 7, 8, 9, 10]
inputs = [8, 9, 10, 11]
inputs2 = [2, 4]
ratios = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
#ratios = [1]
chunkSize = [2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608]#[8, 16, 32, 64, 128, 256, 512, 1024, 2048]#128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536]


# for input in inputs:
# 	for rt in ratios:
# 		for it in range(niter):
# 			os.system('./oneApiBUF %d %s'%(input,rt))
# 			time.sleep(2)
# #
# 
#  #SEQ
# for input in inputs:
# 	for it in range(niter):
# 		os.system('./viSeqOmpTbbOcl %d 0 4' %input)
# 		time.sleep(2)
#
# #TBB
# for input in inputs:
# 	for it in range(niter):
# 		os.system('./viSeqOmpTbbOcl %d 2 4' %input)
# 		time.sleep(2)

#OCL
# for input in inputs:
# 	for it in range(niter):
# 		os.system('./viSeqOmpTbbOcl %d 3 4' %input)
# 		time.sleep(2)




# for input in inputs:
# 	for rt in ratios:
# 		for it in range(niter):
# 			os.system('./usmVi %d %s'%(input,rt))
# 			time.sleep(2)

# #ORACLE OneAPI
# for input in inputs2:
# 	for rt in ratios:
# 		for it in range(niter):
# 			os.system('./oneApiOracleVi %d 4 1 %s'%(input,rt))
# 			time.sleep(2)
# #
# #DYNAMIC OneAPI
# for input in inputs2:
# 	for rt in chunkSize:
# 		for it in range(niter):
# 			os.system('./oneApiDynamicVi %d 4 1 %s'%(input,rt))
# 			time.sleep(2)

#LOGFIT	OneAPI
for input in inputs:
	for it in range(niter):
		os.system('./oneApiLogFitVi %d 4 1' %input)
		time.sleep(2)

# #ORACLE
# for input in inputs:
# 	for rt in ratios:
# 		for it in range(niter):
# 			os.system('./oclOracleVi %d 4 1 %s'%(input,rt))
# 			time.sleep(2)

# #DYNAMIC
# for input in inputs:
# 	for rt in chunkSize:
# 		for it in range(niter):
# 			os.system('./oclDynamicVi %d 4 1 %s'%(input,rt))
# 			time.sleep(2)

#LOGFIT
for input in inputs:
	for it in range(niter):
		os.system('./oclLogFitVi %d 4 1' %input)
		time.sleep(2)



#
# # exp = [];
# # exp.append(['TP2CP_K1_2', '2'])
# # exp.append(['TP2CP_K1_4', '4'])
# # exp.append(['TP2CP_K1_6', '4'])
# # exp.append(['TP2CP_K1_8', '4'])
# # exp.append(['TP2CP_K1_10', '4'])
# # exp.append(['TP2CP_K1_3', '2'])
# # exp.append(['TP2CP_K1_5', '4'])
# # exp.append(['TP2CP_K1_7', '4'])
# # exp.append(['TP2CP_K1_9', '4'])
# # exp.append(['TP2CP_K1_11', '4'])
#
# # alg = [2] #[0, 1, 2, 3]
# # numcores = [4]
#
# # wdir = os.getcwd()
#
# # #NO SCHEDULER
#
# # for x in range(len(exp)):
# #     for a in alg:
# #         for it in range(niter):
# #             for nc in numcores:
# #                 os.system('./%s %d %d'%(exp[x][0],a,nc))
# #             # time.sleep(x)
#
#
#
# # exp = [];
# # exp.append(['TP2OP_K1_2', '2'])
# # exp.append(['TP2OP_K1_4', '4'])
# # exp.append(['TP2OP_K1_6', '4'])
# # exp.append(['TP2OP_K1_8', '4'])
# # exp.append(['TP2OP_K1_10', '4'])
# # exp.append(['TP2OP_K1_3', '2'])
# # exp.append(['TP2OP_K1_5', '4'])
# # exp.append(['TP2OP_K1_7', '4'])
# # exp.append(['TP2OP_K1_9', '4'])
# # exp.append(['TP2OP_K1_11', '4'])
#
# # ## ORACLE
# # scheduler = 'Oracle'
# # numcores = [4]
# # numGPUS = [1]
# # ratios = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
# # wdir = os.getcwd()
#
# # for it in range(niter):
# #     for x in range(len(exp)):
# #         for ng in numGPUS:
# #             for nc in numcores:
# #                 if nc != 0 or ng != 0:
# #                 	for rt in ratios:
# #                 		if not (rt == 0 and nc == 0) and not (rt > 0 and ng == 0):
# # 		                    os.chdir(wdir)
# # 		                    #os.chdir(exp[x][0])
# # 		                    # print 'Iter ',it,': ',os.getcwd()
# # 		                    #print './%s %s %d %d %s'%(exp[x][1],exp[x][2],nc,ng,rt)
# # 		                    #os.system('rm *.txt')
# # 		                    os.system('./%s %d %d %s'%(exp[x][0],nc,ng,rt))
# # 		                    time.sleep(x)
# # # #
# # # #DYNAMIC
# scheduler = 'Dynamic'
# exp = [];
# exp.append(['TP2DP_K1_2', '2'])
# exp.append(['TP2DP_K1_4', '4'])
# exp.append(['TP2DP_K1_6', '4'])
# exp.append(['TP2DP_K1_8', '4'])
# exp.append(['TP2DP_K1_10', '4'])
# exp.append(['TP2DP_K1_3', '2'])
# exp.append(['TP2DP_K1_5', '4'])
# exp.append(['TP2DP_K1_7', '4'])
# exp.append(['TP2DP_K1_9', '4'])
# exp.append(['TP2DP_K1_11', '4'])
#
# numcores = [4]
# numGPUS = [1]
# chunkSize = [2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304]#[8, 16, 32, 64, 128, 256, 512, 1024, 2048]#128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536]
# wdir = os.getcwd()
#
# for it in range(niter):
#     for x in range(len(exp)):
#         for ng in numGPUS:
#             for nc in numcores:
#                 if nc != 0 or ng != 0:
#                 	for cs in chunkSize:
#                 		if not (cs == 0 and nc == 0) and not (cs == 0 and ng == 1):
# 		                    os.chdir(wdir)
# 		                    #os.chdir(exp[x][0])
# 		                    print 'Iter ',it,': ',os.getcwd()
# 		                    #print './%s %s %d %d %s'%(exp[x][1],exp[x][2],nc,ng,cs)
# 		                    os.system('./%s %d %d %s'%(exp[x][0],nc,ng,cs))
# 		                    time.sleep(x)
#
# ## LOGFIT
#
# # exp = [];
# # exp.append(['TP2LP_K1_2', '2'])
# # exp.append(['TP2LP_K1_4', '4'])
# # exp.append(['TP2LP_K1_6', '4'])
# # exp.append(['TP2LP_K1_8', '4'])
# # exp.append(['TP2LP_K1_10', '4'])
# # exp.append(['TP2LP_K1_3', '2'])
# # exp.append(['TP2LP_K1_5', '4'])
# # exp.append(['TP2LP_K1_7', '4'])
# # exp.append(['TP2LP_K1_9', '4'])
# # exp.append(['TP2LP_K1_11', '4'])
#
# # scheduler = 'LogFit'
# # numcores = [4]
# # numGPUS = [1]
# # wdir = os.getcwd()
#
# # for it in range(niter):
# #     for x in range(len(exp)):
# #         for ng in numGPUS:
# #             for nc in numcores:
# #                 if nc != 0 or ng != 0:
# #                     os.chdir(wdir)
# #                     #os.chdir(exp[x][0])
# #                     print 'Iter ',it,': ',os.getcwd()
# #                     #print './%s %s %d %d %s'%(exp[x][1],exp[x][2],nc,ng,rt)
# #                     #os.system('rm *.txt')
# #                     os.system('./%s %d %d'%(exp[x][0],nc,ng))
#                     time.sleep(x)
